#include <userint.h>
#include "SOLENOID.h"

#include "global_var.h" 

int CVICALLBACK AcquireSolenoidCharCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK QuitSolenoidCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// disconnetti ramo con resistenza di shunt 
			//SetEnableEvCurrent(NO);
			
			// nascondi pannello per caratterizzazione solenoidi
			DiscardPanel (solenoidpanel);
			solenoidpanel=-1000;
			
			break;
	}
	return 0;
}
