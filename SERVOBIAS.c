#include <userint.h>
#include "SERVOBIAS.h"

#include "global_var.h"    


int CVICALLBACK QuitBiasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// disconnetti ramo con resistenza di shunt
			//SetEnableNullBias(NO);
			
			// scarica pannello per bias servovalvole a bordo servocomando
			DiscardPanel (biaspanel);
			biaspanel=-1000;
			
			break;
	}
	return 0;
}
