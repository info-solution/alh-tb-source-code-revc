/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2008. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  DATALOGPNL                       1
#define  DATALOGPNL_TEXTBOX               2
#define  DATALOGPNL_CLEARBTN              3       /* callback function: ClearLogCB */
#define  DATALOGPNL_SAVEBTN               4       /* callback function: SaveLogCB */

#define  ENGPANEL                         2
#define  ENGPANEL_LOGOFFBTN               2       /* callback function: LogOffCB */
#define  ENGPANEL_VIEWERBTN               3       /* callback function: ReportViewerCB */
#define  ENGPANEL_DECORATION              4
#define  ENGPANEL_BENCHCALLBTN            5       /* callback function: BenchCalibrationCB */
#define  ENGPANEL_MANUALBTN               6       /* callback function: ManualTestCB */
#define  ENGPANEL_UUTCALIBRATIONBTN_2     7       /* callback function: UutElectricalCalibrationCB */
#define  ENGPANEL_UUTMECHCALBTN           8       /* callback function: UutMechanicalCalibrationCB */
#define  ENGPANEL_STRESSBTN               9       /* callback function: EssScreeningCB */
#define  ENGPANEL_DECORATION_2            10
#define  ENGPANEL_DECORATION_3            11
#define  ENGPANEL_STATBTN                 12      /* callback function: StatisticsCB */
#define  ENGPANEL_PATBTN                  13      /* callback function: ProductionAcceptanceTestCB */
#define  ENGPANEL_ATPBTN                  14      /* callback function: AcceptanceTestProcedureCB */
#define  ENGPANEL_SELFTESTBTN             15      /* callback function: BenchSelfTestCB */
#define  ENGPANEL_TEXTMSG                 16
#define  ENGPANEL_TEXTMSG_2               17
#define  ENGPANEL_TEXTMSG_3               18


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK AcceptanceTestProcedureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BenchCalibrationCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BenchSelfTestCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearLogCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK EssScreeningCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LogOffCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ManualTestCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ProductionAcceptanceTestCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ReportViewerCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveLogCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StatisticsCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK UutElectricalCalibrationCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK UutMechanicalCalibrationCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
