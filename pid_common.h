
/**************************************/
/* Header file for PID functionality. */
/**************************************/
;
/* Function prototypes */
int CreatePid (int pid_type, double dt, double outputMin, double outputMax);
int GetNextPidOutput (int pid_type, double processVariable, double *output);
void ClosePid (int pid_type);
int SetPidOutputRate (int pid_type, double outputRate);
int SetPidSetpoint (int pid_type, double setpoint);
int SetPidParameters (int pid_type, double kC, double tI, double tD);
