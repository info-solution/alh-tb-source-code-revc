#include <userint.h>
#include <windows.h>
#include <ansi_c.h>
#include "MECHCALIBRATION.h"
#include "RAMCONNECTION.h"

#include "global_var.h"    

/* Routine di verifica errori HW scheda DAQmx */
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else                


int CVICALLBACK InputEngageCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int ctrl_type=0;		// tipo di controllo utilizzato: martinetto o csas
	
	switch (event)
	{
		case EVENT_COMMIT:
			// leggi tipo di controllo desiderato
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_CTRL_VAL, &ctrl_type);  
			
			// disabilita controlli
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWEROFFBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_DIMMED, NO);   
			
			// abilita movimento
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, YES);
			
			// azzera ampiezza e frequenza dell'oscillazione
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_CTRL_VAL, 0.1);
			
			
			/***********************************************************************/
			/* PROCEDURA DI ENGAGE DEL MARTINETTO DI CONTROLLO DELLA LEVA DI INPUT */
			
			// carica pannello con immagini di procedura manuale di connessione della leva di input
			if (inputconnectionpanel<0)
			{
				if ((inputconnectionpanel = LoadPanel (0, "RAMCONNECTION.uir", INRAMCONPN)) < 0)
				return -1;
			}

			// visualizza pannello con procedura manuale
			DisplayPanel (inputconnectionpanel);
	
			SetPanelAttribute (mechcalibrationpanel, ATTR_DIMMED, NO);
								
			/***********************************************************************/
			/* PROCEDURA DI ENGAGE DEI SISTEMI AUTOPILOTI                          */        
			
			if (ctrl_type==CSAS)
			{
				// alimenta solenoid valve di entrambe gli autopiloti     
				//CsasPowerUp(BOTH_SYS);
					
				// impostazione oscillazione
				SetCsasPosition(STATIC, 0.0, 0.0, 0.1);
		
				Sleep(100);
		
				// attiva controllore PID degli autopiloti
				SetEnableCsas1(YES);
				SetEnableCsas2(YES);
			}
			/***********************************************************************/  
			
			// Azzera conteggio dei cicli
			SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CYCLENMR, ATTR_CTRL_VAL, 0);
			number_of_cycle=0;
			
			break;
	}
	return 0;
}

int CVICALLBACK InputUnEngageCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int ctrl_type=0;		// tipo di controllo utilizzato: martinetto o csas 
	
	switch (event)
	{
		case EVENT_COMMIT:
			
			// leggi tipo di controllo desiderato
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_CTRL_VAL, &ctrl_type);  
			
			// abilita controlli
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTENGAGEBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWEROFFBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_DIMMED, YES);  
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_DIMMED, YES);
			
			// disabilita controlli
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_DIMMED, NO);
			
			/***********************************************************************/
			/* PROCEDURA DI DISENGAGE DEL MARTINETTO DI INPUT                      */        
			
			// impostazione oscillazione
			SetInputPosition(STATIC, 0.0, 0.0, 0.1);
	
			Sleep(100);
	
			// attiva controllore PID del martinetto di controllo della leva manuale
			SetEnableInput(NO);
		
			// togli alimentazione idraulica al martinetto di controllo della leva manuale
			SetSolenoidValve(EV07,CLOSE);

			/***********************************************************************/
			/* PROCEDURA DI DISENGAGE DEI CSAS               				       */        
			
			// diseccita solenoid valve degli autopiloti		
			if (ctrl_type==CSAS)
			{
				CsasShutDown();
				
				// attiva controllore PID degli autopiloti
				SetEnableCsas1(NO);
				SetEnableCsas2(NO);
			}
			/***********************************************************************/       			
			
			break;
	}
	return 0;
}


int CVICALLBACK PowerOnPressureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	int system_choise=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// scelta dei sistemi da alimentare
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_SYSTEMCHOISERNG, ATTR_CTRL_VAL, &system_choise);
			
			// alimentazione sistema desiderato
			ErrChk( UutPowerUp(system_choise) );
																	  
			// abilita regolazione di pressione di mandata e azzera i valori
			ErrChk( SetUutPressure(VARIABLE, 0.0, LW) );
			
			// azzera i precedenti valori anche su interfaccia grafica
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_CTRL_VAL, 0);
			
			// abilita movimentazione leva di input
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTENGAGEBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWEROFFBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_DIMMED, YES);
			
			// disabilita controlli
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWERONBTN, ATTR_DIMMED, NO);			
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWEROFFBTN, ATTR_DIMMED, NO); 
			
			// posiziona a off i controlli di alimentazione CSAS e derivazione del ritorno UUT per misure di leakage
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_CTRL_VAL, 4);				// off
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, 0);		// off
	
			// abilita possibilita' di alimentazione CSAS
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_DIMMED, YES);
			
			// abilita opzioni di lettura leakage
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_DIMMED, YES);
	
			break;
	}

Error:
	return 0;
}

int CVICALLBACK PowerOffPressureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// spegnimento alimentazione stadi autopiloti
			CsasShutDown();
			
			// disabilitazione circuito misura leakage
			SetLeakageCondition(0);
			
			// spegnimento alimentazione UUT
			ErrChk( UutShutDown() );

			// disabilita regolazione di pressione di mandata e azzera i valori
			ErrChk( SetUutPressure(POWER_OFF, 0.0, LW) );
			
			// disabilita attivazione movimentazione leva di input
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_DIMMED, NO);
	
			// disabilita controllo pressione
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_DIMMED, NO);
			
			// azzera i precedenti valori anche su interfaccia grafica
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_CTRL_VAL, 0);
			
			// abilita controlli
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWERONBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWEROFFBTN, ATTR_DIMMED, YES); 
			
			// disabilita controlli
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWEROFFBTN, ATTR_DIMMED, NO);
			
			// disabilita possibilita' di alimentazione CSAS
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_DIMMED, NO);
			
			// disabilita opzioni di lettura leakage
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_DIMMED, NO);
			
			// posiziona a off i controlli di alimentazione CSAS e derivazione del ritorno UUT per misure di leakage
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_CTRL_VAL, 4);				// off
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, 0);		// off
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_CTRL_VAL, 0.0);
			
			break;
	}
	
Error:
	return 0;
}

int CVICALLBACK ArrestMechCalibrationCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			EmergencyStop();
			
			break;
	}
	return 0;
}

int CVICALLBACK QuitMechCalibrationPanelCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			HidePanel (mechcalibrationpanel);
			DisplayPanel (engpanel);
			
			break;
	}
	return 0;
}


/* ------------------------------------------------------------------------------------ */
/* Accensione banco idraulico															*/
/* ------------------------------------------------------------------------------------ */

int CVICALLBACK BenchPowerOnCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
		
	switch (event)
	{
		case EVENT_COMMIT:

			// alimentazione banco
			ErrChk( RatbPowerUp() );
			
			// disabilita
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWERONBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_QUITBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_SYSTEMCHOISERNG, ATTR_DIMMED, NO); 
			
			// abilita
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWEROFFBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWERONBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_SYSTEMCHOISERNG, ATTR_DIMMED, YES);      
			
			break;
	}
	
Error:
	return 0;
}


/* ------------------------------------------------------------------------------------ */
/* Spegnimento banco idraulico															*/
/* ------------------------------------------------------------------------------------ */

int CVICALLBACK BenchPowerOffCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	double coefficienti[3]={-8500,0,0};
	
	switch (event)
	{
		case EVENT_COMMIT:
				
			// invio parametri di controllo verso regolatore 
			SetCsasPid(coefficienti);
			
			// spegnimento banco
			ErrChk( RatbShutDown() );
			
			// disabilita
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWEROFFBTN, ATTR_DIMMED, NO);    
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWERONBTN, ATTR_DIMMED, NO);    
			
			// abilita
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWERONBTN, ATTR_DIMMED, YES);    
			SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_QUITBTN, ATTR_DIMMED, YES);          
			
			break;
	}
	
Error:
	return 0;
}

int CVICALLBACK ChangeReturnPressureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double delivery_pressure=0.0;
	int return_pressure=LW;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// azzera i precedenti valori anche su interfaccia grafica
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_CTRL_VAL, &delivery_pressure);
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_CTRL_VAL, &return_pressure);

			// abilita regolazione di pressione di mandata e azzera i valori
			SetUutPressure(VARIABLE, delivery_pressure, return_pressure);

			// abilita opzioni di lettura leakage
			if (return_pressure==LW)
			{
				SetLeakageCondition(0);
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, 0);		// off
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_DIMMED, YES);
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_DIMMED, YES);
			}
			else
			{
				SetLeakageCondition(0);
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, 0);		// off
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_DIMMED, NO);
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_DIMMED, NO);	
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeDeliveryPressureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double delivery_pressure=0.0;
	int return_pressure=LW;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// azzera i precedenti valori anche su interfaccia grafica
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_CTRL_VAL, &delivery_pressure);
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_CTRL_VAL, &return_pressure);

			// abilita regolazione di pressione di mandata e azzera i valori
			SetUutPressure(VARIABLE, delivery_pressure, return_pressure);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeInputAmplitudeCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double ampiezza=0.0;
	double frequenza=0.0;
	int ctrl_type=0;		// tipo di controllo utilizzato: martinetto o csas      
	
	switch (event)
	{
		case EVENT_COMMIT:

			// leggi tipo di controllo desiderato
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_CTRL_VAL, &ctrl_type);  
			
			// leggo ampiezza e frequenza desiderati
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_CTRL_VAL, &ampiezza);
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_CTRL_VAL, &frequenza);
			
			if (ctrl_type==CSAS)
			{
				// impostazione oscillazione
				SetCsasPosition(SINE, 0.0, ampiezza, frequenza);
			}
			else
			{
				// impostazione oscillazione
				SetInputPosition(SINE, 0.0, ampiezza, frequenza);
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeInputFrequencyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double ampiezza=0.0;
	double frequenza=0.1;
	int ctrl_type=0;		// tipo di controllo utilizzato: martinetto o csas   
		
	switch (event)
	{
		case EVENT_COMMIT:

			// leggi tipo di controllo desiderato
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_CTRL_VAL, &ctrl_type);  
			
			// leggo ampiezza e frequenza desiderati
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_CTRL_VAL, &ampiezza);
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_CTRL_VAL, &frequenza);
			
			if (ctrl_type==CSAS)
			{
				// impostazione oscillazione
				SetCsasPosition(SINE, 0.0, ampiezza, frequenza);
			}
			else
			{
				// impostazione oscillazione
				SetInputPosition(SINE, 0.0, ampiezza, frequenza);
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeMechCalSystemCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	int system_choise=0;
	int stato_sys1, stato_sys2;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// scelta dei sistemi da alimentare
			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_SYSTEMCHOISERNG, ATTR_CTRL_VAL, &system_choise);

			// alimentazione sistema desiderato
			ErrChk( GetStatus (EV02_STATUS,&stato_sys1) );
			ErrChk( GetStatus (EV03_STATUS,&stato_sys2) ); 

			if ((stato_sys1==HI)||(stato_sys2==HI))
				ErrChk( UutPowerUp(system_choise) );	
			
			break;
	}
	
Error:
	return 0;
}


int CVICALLBACK ChangeMechCalCsasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int csas_option=4;
	double coefficienti[3]={-1000,0,0};		// Kc,Ti,Td  
	
	switch (event)
	{
		case EVENT_COMMIT:

			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_CTRL_VAL, &csas_option); 
			
			if (csas_option!=4)
			{
				// alimenta solenoid valve degli autopiloti
				//CsasPowerUp(csas_option);
				
				// invio parametri di controllo verso regolatore 
				// SetCsasPid(coefficienti);
				
				// Sleep(200);
				
				SetCsasPosition(STATIC, 0.0, 0.0, 0.1);
		
				Sleep(100);
		
				// attiva controllore PID degli autopiloti
				if (csas_option == 0) {
					SetEnableCsas1(YES);
					SetEnableCsas2(YES);
				}
				if (csas_option == 1) {
					SetEnableCsas1(YES);
					SetEnableCsas2(NO);
				}
				if (csas_option == 2) {
					SetEnableCsas1(NO);
					SetEnableCsas2(YES);
				}
			}
			else
			{		
				// diseccita solenoid valve degli autopiloti
				CsasShutDown();
				
				Sleep(100);
		
				// attiva controllore PID degli autopiloti
				SetEnableCsas1(NO);
				SetEnableCsas2(NO);
				
				// coefficienti[0]=-8500;
				// coefficienti[1]=0;
				// coefficienti[2]=0;
				
				// invio parametri di controllo verso regolatore 
				// SetCsasPid(coefficienti);
			}
			
			break;
	}
	return 0;
}


int CVICALLBACK ChangeMechCalSystemLeakCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int system_leakage=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, &system_leakage);      
			
			if (system_leakage==0)
				SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_CTRL_VAL, 0.0);
			
			SetLeakageCondition(system_leakage);
			
			break;
	}
	return 0;
}

int CVICALLBACK MechCycTimerCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_TIMER_TICK:

			cycling_time+=1.0;
			SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_TIMERNMR, ATTR_CTRL_VAL, cycling_time);
		
			break;
	}
	return 0;
}

int clock_status=0;		// 0 disattivato
						// 1 attivato

int CVICALLBACK ChangeMechClockStatusCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			
			if (clock_status==0)
			{
				// abilita il timer
				SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_TIMER,ATTR_ENABLED, 1);
				
				clock_status=1;
			}
			else if (clock_status==1)
			{
				// disabilita timer
				SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_TIMER,ATTR_ENABLED, 0);	
			
				clock_status=1;
			}
			
			break;
			
		case EVENT_LEFT_DOUBLE_CLICK:
			
			clock_status=0;
			cycling_time=0.0;
			
			SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_TIMER,ATTR_ENABLED, 0);			// disabilita timer  	
			SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_TIMERNMR, ATTR_CTRL_VAL, 0.0);    // azzera conteggio
			
			break;	
	}
	return 0;
}
