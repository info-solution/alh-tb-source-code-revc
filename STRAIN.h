/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  STRAINPNL                        1
#define  STRAINPNL_SAVEBTN                2       /* callback function: SaveStripChartStrainCB */
#define  STRAINPNL_ZEROBTN                3       /* callback function: AcquireCellsZeroCB */
#define  STRAINPNL_INPUTEFFORTGRPH        4       /* callback function: ChangeScaleInputStrainCB */
#define  STRAINPNL_STOPBTN                5       /* callback function: StopStripChartStrainCB */
#define  STRAINPNL_CLEARBTN               6       /* callback function: ClearStripChartStrainCB */
#define  STRAINPNL_QUITBTN                7       /* callback function: QuitStrainCB */
#define  STRAINPNL_SPEEDSTR               8
#define  STRAINPNL_POSITIONSTR            9       /* callback function: ChangeScaleYstrainCB */
#define  STRAINPNL_DECORATION_2           10
#define  STRAINPNL_OUTPUTSTRENGTHSTR      11      /* callback function: ChangeScaleOutputStrainCB */
#define  STRAINPNL_TEXTMSG_2              12
#define  STRAINPNL_STRENGTHAVENMR         13
#define  STRAINPNL_EFFORTAVENMR           14
#define  STRAINPNL_SPEEDAVENMR            15
#define  STRAINPNL_POSITIONAVENMR         16


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK AcquireCellsZeroCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleInputStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleOutputStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleYstrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearStripChartStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveStripChartStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopStripChartStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
