
// ************************************************************
//
// 		
// 						DESCRIZIONE HARDWARE
//				      Rotor Actuator Test Bench
// 				    
// 					Schede, Trasduttori e Cablaggi
//
//
// ************************************************************








// ************************************************************
//
// PROTOTIPI FUNZIONI
//
// ************************************************************



// ------------------------------------------------------------
// Main Functions
// ------------------------------------------------------------

int RunPid(void);
int InitNetworkConnection(void);
int RunDigitalControl(void);


// ------------------------------------------------------------
// Routine Livello di controllo
// ------------------------------------------------------------

void InitVar(void);										// routine di inizializzazione variabili globali
int InitTask (void);									// crea i task DAQmx
int InitDIO(void);										// definizione porte di digital I/O
int InitCNT(void);										// definizione counter per lettura di frequenze
int InitAIN(void);										// definizione canali di analog input
int InitLvdt(void);
int InitBridge(void);
int InitAOUT(void);										// definizione canali di analog output
int InitSync (void);									// routine di sincronizzazione dei sample clock dei canali analogici di input/output
int ExecCalibration (void);								// routine di esecuzione procedura di auto-calibrazione
int ExecNulling (void);									// routine di auto-zero polarizzazione ponti

int SetDO(int channel, int bit_value);					// routine di scrittura del bit sulla porta0 di output
int SetDO6341(int channel, int bit_value);				// routine di scrittura del bit sulla porta0 di output PXIe 6341
int GetDI(int channel, int *bit_value);					// routine di lettura del bit sulla porta1 di input
int GetFrequency(int channel, double *frequency_value);	// routine di lettura di frequenza mediante counter0

//int GetRelay(int channel, int *relay_status);			// routine di lettura dello stato di un rele'
//int SetRelay(int channel, int relay_status);			// routine di eccitazione del rele' indicato

int CloseTask(void);									// chiude i task DAQmx aperti
int StopHwProgram(void);								// arresta esecuzione del programma RT secondo procedura standard

int HydraulicPowerOn(void);								// procedura di alimentazione idraulica del banco
int HydraulicShutDown(void);							// arresta alimentazione circuito idraulico
int EmergencyStop(void);								// arresto di emergenza

// ------------------------------------------------------------
// Routine Livello di presentazione
// ------------------------------------------------------------

void SendLeakage(double flow);					// comunicazione misure di portata
void SendStatus(unsigned int stato);					// comunicazione stati digitali (proximity switch, fine corsa...)
void ReleaseConnections(void);							// rilascia tutte le connessioni di rete


// ************************************************************
//
// COSTANTI SIMBOLICHE
//
// ************************************************************


// ------------------------------------------------------------
// Costanti ad uso generico
// ------------------------------------------------------------

#define NU				-1		// Not Used  
#define PASS			0		// Routine eseguita correttamente o test passato
#define FAIL			-1		// Routine in errore o test fallito

#define HI				1		// Stato logico 1
#define LW				0		// Stato logico 0

#define OPEN			1		// EV aperta - solenoide eccitato
#define CLOSE			0		// EV chiusa - solenoide diseccitato

#define OFF				0
#define ON				1


// ------------------------------------------------------------
// Descrizione idraulica Banco di Test Rotor Actuator
// ------------------------------------------------------------

#define EV01	5	// elettrovalvola 2 vie Mandata banco idraulico
#define EV02	6	// elettrovalvola 2 vie SYS1 servocomando 
#define EV03	7	// elettrovalvola 2 vie SYS2 servocomando 
#define EV04A	8	// elettrovalvola 4 vie Leakage su SYS1
#define EV04B	9	// elettrovalvola 4 vie Leakage su SYS2
#define EV05	10	// elettrovalvola 2 vie Leakage su ritorno del servocomando
#define EV06	11	// elettrovalvola 2 vie Contropressione 155 bar servocomando
#define EV07	12	// elettrovalvola 2 vie Input
#define EV08	13	// elettrovalvola 2 vie Load
#define EV09A	14	// elettrovalvola 2 vie Sgancio Load
#define EV09B	15	// elettrovalvola 2 vie Aggancio Load

#define NUM_EV	11	// numero di solenoidi del banco
#define NUM_EV_6341		2	// numero di EV su scheda PXIe 6341

#define G1		32	// filtro + valvole di reverse flow + by-pass + indicatore di intasamento differenziale
#define FC1		33
#define FC2		34
#define FC3		35
#define FC4		36
#define FC5		37
#define FC6		38

#define V01		11	// valvola di non ritorno
#define V02		12	// valvola di sicurezza
#define V03		13	// valvola di controllo proporzionale pressione
#define V04		14	// valvola di ritegno
#define V05		15	// valvola di sicurezza
#define V06		16	// valvola di sicurezza
#define V07		17	// valvola di riduzione pressione
#define V08		18	// valvola di riduzione pressione
#define V09		19	// valvola di riduzione pressione
#define V10		20	// valvola di non ritorno

#define V11		21	// valvola di non ritorno

#define M1		23	// esclusore manuale
#define M2		24	// saracinesca manuale

#define T1		26	// accumulatore
#define T2		27	// accumulatore precaricato a gas
#define RF		28	// strozzatore
#define A1		29	// martinetto idraulico
#define A2		30	// martinetto idraulico
#define A3		31	// martinetto idraulico
#define A4		32	// load engage pneumatico

#define P01		 0	// manometro
#define P02		 3	// sensore di pressione
#define P03		 4	// sensore di pressione
#define P04		 5	// sensore di pressione
#define P05		 6	// sensore di pressione
#define P06		 7	// sensore di pressione
#define P07		 8	// sensore di pressione
#define P08		11	// sensore di pressione
#define P09		12	// sensore di pressione
#define P10		13	// sensore di pressione
#define P11		14	// sensore di pressione

#define K1		15	// sensore di temperatura

#define L1		17	// sensore di livello / posizione
#define L2		18	// sensore di livello / posizione

#define N1		 3	// cella di carico N1 Input
#define N2		 7	// cella di carico N2 Load

#define F1		19	// sensore di portata
#define F2		0	// sensore di portata

//#define R1_SHUNT		0	// current sensing SV_SYS1
//#define R2_SHUNT		1	// current sensing SV_SYS2
//#define R9_SHUNT		2	// current sensing EV_SYS1
//#define R10_SHUNT		3	// current sensing EV_SYS2

#define SW0		 0	// Controllo servovalvola SV1 input
#define SW1		 1	// Controllo servovalvola CSAS1
#define SW2		 2	// Controllo servovalvola CSAS2
#define SW3		 3	// Controllo servovalvola SV2 carico
#define SW4		 4	// Controllo on/off alimentatore programmabile EV
#define SW5		 5	// Controllo setpoint fisso/variabile alimentatore programmabile EV
#define SW6		 6	// Controllo on/off elettrovalvola V3
#define SW7		 7	// Controllo setpoint fisso/variabile elettrovalvola V3
#define SW8		 8	// Switch per current sensing  elettrovalvola CSAS1
#define SW9		 9	// Switch per current sensing  elettrovalvola CSAS2
#define SW10	10	// Switch per current sensing  servovalvola CSAS1
#define SW11	11	// Switch per current sensing  servovalvola CSAS2
#define SW12	12	// Allarme visivo / acustico
#define SW13	13	// Allarme visivo / acustico

#define SV1		 1	// servovalvola Input
#define SV2		 0	// servovalvola Load
#define V3		 3	// riferimento pressione

#define CV_SYS1		0	// corrente SV SYS1
#define CV_SYS2		1	// corrente SV SYS2
#define SYS1_S		2   // solenoide SYS1
#define SYS2_S		3   // solenoide SYS2
#define PSU3		4   // PSU3 28V

// ------------------------------------------------------------
// Descrizione Servocomando
// ------------------------------------------------------------

#define SV_SYS1		0	// servovalvola CSAS sys1
#define SV_SYS2		1	// servovalvola CSAS sys2
#define EV_SYS1		0	// elettrovalvola CSAS sys1
#define EV_SYS2		1	// elettrovalvola CSAS sys2
#define LVDT_SYS1	1	// LVDT CSAS sys1
#define LVDT_SYS2	2	// LVDT CSAS sys2


// ------------------------------------------------------------
// Ordine canali in Multiplexing verso l'ADC
// ------------------------------------------------------------

#define P02_MUX			0
#define P03_MUX			1
#define P04_MUX			2
#define P05_MUX			3
#define P06_MUX			4
#define P07_MUX			5
#define P08_MUX			6
#define P09_MUX			7
#define P10_MUX			8
#define P11_MUX			9
#define K1_MUX			10
#define PSU3_MUX		11  
#define L1_MUX			12
#define L2_MUX			13
#define CV_SYS1_SHUNT	14	//per corrente di SV SYS1
#define CV_SYS2_SHUNT	15  //per corrente di SV SYS2 
#define SYS1_S_SHUNT	16  //per corrente solenoide SYS1
#define SYS2_S_SHUNT	17  //per corrente solenoide SYS2
#define F1_MUX			18
#define LVDT1_MUX		19
#define LVDT2_MUX		20
#define N1_MUX			21
#define N2_MUX			22

#define INPUT_MUX		23
#define CSAS_MUX		24
#define LOAD_MUX		25

// ------------------------------------------------------------
// Ordine canali in Multiplexing dal DAC
// ------------------------------------------------------------

//#define SV1_MUX			0
//#define SV_SYS1_MUX		1
//#define SV_SYS2_MUX		2
//#define SV2_MUX			3



// ------------------------------------------------------------
// Stato di attuatori e sensori "digitali" (EV, proximity e FC)
// ------------------------------------------------------------

#define EV01_STATUS		0
#define EV02_STATUS		1
#define EV03_STATUS		2
#define EV04A_STATUS	3
#define EV04B_STATUS	4
#define EV05_STATUS		5
#define EV06_STATUS		6
#define EV07_STATUS		7
#define EV08_STATUS		8
#define EV09A_STATUS	9
#define EV09B_STATUS	10

#define FC1_STATUS		11
#define FC2_STATUS		12
#define FC3_STATUS		13

#define G1_STATUS		14

#define FC4_STATUS		15
#define FC5_STATUS		16
#define FC6_STATUS		17

// ------------------------------------------------------------
// Descrizione schede del cestello National Instruments
// ------------------------------------------------------------

//#define NI_PXI_1050		1	// Chassis cestello National Instruments per bus PXI / SCXI
//#define NI_PXI_8106		2	// Personal Computer integrato al cestello NI
//#define NI_PXI_6229		3	// Scheda serie di I/O digitale e analogico
//#define NI_PXI_2566		4	// Scheda rel� elettro-meccanici
//#define NI_SCXI_1540	5	// Scheda di condizionamento segnali LVDT
//#define NI_SCXI_1520	6	// Scheda di condizionamento segnali Celle di Carico

#define NI_PXI_1082		1	// Chassis cestello National Instruments per bus PXI / SCXI
#define NI_PXI_8840		2	// Personal Computer integrato al cestello NI
#define NI_PXI_6363		3	// Scheda serie di I/O digitale e analogico
#define NI_PXI_4340		4	// Scheda di condizionamento segnali LVDT
#define NI_PXI_4339		5	// Scheda di condizionamento segnali Celle di Carico 
#define NI_PXI_6341		6	// Scheda serie di I/O digitale e analogico  

// ------------------------------------------------------------
// NI PXIe 6363 Slot 2 Config
// ------------------------------------------------------------
#define NCH_6363_AIN	32	// Numero di canali di Analog Input NI PXIe 6363 DIFFERENTIAL
#define NCH_6363_AOUT	4	// Numero di canali di Analog Output NI PXIe 6363
#define NCH_6363_DIO	48	// Numero di canali di Digital I/O NI PXIe 6363
#define NCH_6363_CTR	2	// Numero di canali Counter NI PXIe 6363 

// ------------------------------------------------------------
// NI PXIe 4340 Slot 3 Config
// ------------------------------------------------------------
//#define NCH_1540		8	// Numero di canali di condizionamento LVDT
#define NCH_4340		4	// Numero di canali di condizionamento LVDT

// ------------------------------------------------------------
// NI PXIe 4339 Slot 4 Config
// ------------------------------------------------------------
//#define NCH_1520		8	// Numero di canali di condizionamento Celle di Carico
#define NCH_4339		8	// Numero di canali di condizionamento Celle di Carico

// ------------------------------------------------------------
// NI PXIe 6341 Slot 5 Config
// ------------------------------------------------------------
#define NCH_6341_AIN	16	// Numero di canali di Analog Input NI PXIe 6341 DIFFERENTIAL
#define NCH_6341_AOUT	2	// Numero di canali di Analog Output NI PXIe 6341
#define NCH_6341_DIO	24	// Numero di canali di Digital I/O NI PXIe 6341
#define NCH_6341_CTR	1	// Numero di canali Counter NI PXIe 6341
//#define NCH_6229_AIN	32	// Numero di canali di Analog Input 
//#define NCH_6229_AOUT	4	// Numero di canali di Analog Output  
//#define NCH_6229_DIO	48	// Numero di canali di Digital I/O  
//#define NCH_6229_CTR	2	// Numero di canali Counter


#define NCH_2566		16	// Numero di rel� a bordo scheda   DA TOGLIERE

#define NCH_SCC68		4	// Numero di canali SCC-AI06 di analog input a bordo delle morsettiere SCC-68

#define NUM_CH_AIN		23	// Numero di canali di conversione A/D utilizzati (AIN + AI06 + LVDT + StrainGauge)
#define NUM_MEAS_IN		26	// 23 canali reali + 3 setpoint generati matematicamente (input, csas, load)
#define NUM_CH_AOUT		3   // Numero di canali di conversione D/A utilizzati
#define NUM_CH_AOUT_6341		2   // Numero di canali di conversione D/A utilizzati su PXIe 6341
#define NUM_SWITCH		13	// Numero di switch utilizzati


// ------------------------------------------------------------
// Descrizione controlli
// ------------------------------------------------------------

#define CSAS1_PID		0
#define CSAS2_PID		1
#define INPUT_PID		2
#define LOAD_PID		3

#define PRESSURE_CTRL	4
#define VOLTAGE_CTRL	5

#define STATIC			1		// controllo di posizione in un punto fisso
#define SINE			2		// controllo di posizione con set point secondo forma d'onda sinusoidale
#define SQUARE			3		// controllo di posizione con set point secondo forma d'onda quadrata
#define TRIANGULAR		4		// controllo di posizione con set point secondo forma d'onda triangolare

#define COMPRESSION		0		// carico oleodinamico in compressione sul main piston
#define EXTENSION		1		// carico oleodinamico in estensione sul main piston

#define FIXED			2		// indica se un valore deve essere prefissato (es. pressioni operative)
#define VARIABLE		1		// indica se lo stesso valore puo' essere variato dall'utente
#define POWER_OFF		0		// indica se la grandezza fisica di regolazione e' ad un valore di "riposo"


// ------------------------------------------------------------
// Descrizione interfacce verso i trasduttori
// ------------------------------------------------------------

#define PRESSURE_AIMIN		-10.0		// tensioni in uscita da trasduttori di pressione P02-P12
#define PRESSURE_AIMAX		10.0

#define FLOW_AIMIN			0.0		// tensioni in uscita da trasduttori di portata 
#define FLOW_AIMAX			10.0

#define TEMP_AIMIN			0.0		// tensioni in uscita da trasduttori di temperatura 
#define TEMP_AIMAX			10.0

#define EV_AIMIN			0.0		// tensioni di alimentazione delle solenoid valve dall'alimentatore pilotato SIEMENS
#define EV_AIMAX			10.0

#define MAGNETIC_AIMIN		0.0		// tensioni in uscita dai sensori di posizione magnetostrittivi
#define MAGNETIC_AIMAX		10.0

#define STRAIN_IN_AIMIN		-200	// sforzo di attuazione sulla leva di input [N]	
#define STRAIN_IN_AIMAX		+200

#define STRAIN_OUT_AIMIN	-35e3	// carico sul pistone principale del servocomando [N]
#define STRAIN_OUT_AIMAX	+35e3

#define LVDT_SENSITIVITY	100.0	// sensibilita' dell'LVDT a bordo servocomando espressa in mV/mm/V
#define LVDT_AIMIN			-20e-3	// corsa dell'LVDT espressa in metri
#define LVDT_AIMAX			+20e-3
#define LVDT_FREQUENCY		5000.0  // frequenza di eccitazione in Hz
#define LVDT_VOLTAGE		3.0		// tensione di eccitazione in V

#define SERVOVALVE_AOMIN	-10.0	// tensione di comando servo-valvole
#define SERVOVALVE_AOMAX	10.0

#define LOAD_AOMIN			-2.0	// limiti di tensione per la servovalvola di controllo del pistone oleodinamico di carico
#define LOAD_AOMAX			+2.0

#define UUT_VOLTAGE_AOMIN	0.0		// limiti tensione di pilotaggio alimentatore SIEMENS
#define UUT_VOLTAGE_AOMAX	2.5

#define UUT_PRESSURE_AOMIN	0.0		// limiti tensione di regolazione pressione di mandata al servocomando
#define UUT_PRESSURE_AOMAX	10.0

#define PID_RATE			250.0	// rate di regolazione del controllore PID [Hz]

#define CSAS_LW_LIMIT		-0.05	// limite inferiore set-point di posizione per gli autopiloti CSAS [mm]
#define CSAS_UP_LIMIT		+0.05	// limite superiore set-point di posizione per gli autopiloti CSAS [mm]

#define INPUT_LW_LIMIT		-10.0		// limite inferiore set-point di posizione per il martinetto sulla leva di input [mm]
#define INPUT_UP_LIMIT		10.0	// limite superiore set-point di posizione per il martinetto sulla leva di input [mm]

#define LOAD_LW_LIMIT		0.0		// limite inferiore set-point di forza per il martinetto di carico sul main piston [N]
#define LOAD_UP_LIMIT		35000.0	// limite superiore set-point di forza per il martinetto di carico sul main piston [N]

#define PRESSURE_LW_LIMIT	0.0		// limite inferiore di regolazione della pressione di mandata al servocomando [bar]
#define PRESSURE_UP_LIMIT	310.0	// limite superiore di regolazione della pressione di mandata al servocomando [bar]

#define VOLTAGE_LW_LIMIT	0.0		// limite inferiore di regolazione della tensione di eccitazione dei solenoidi CSAS [V]
#define VOLTAGE_UP_LIMIT	10.0	// limite superiore di regolazione della tensione di eccitazione dei solenoidi CSAS [V]

#define AMPLITUDE_LW_LIMIT	0.0		// limite inferiore di ampiezza nelle prove di oscillazione [mm]
#define AMPLITUDE_UP_LIMIT	30.0	// limite superiore di ampiezza nelle prove di oscillazione [mm]

#define FREQUENCY_LW_LIMIT	0.0		// limite inferiore di frequenza nelle prove di oscillazione [Hz]
#define FREQUENCY_UP_LIMIT	10.0	// limite superiore di frequenza nelle prove di oscillazione [Hz]

#define SHUNT_LW_LIMIT	-0.1		// limite inferiore di tensione ai capi della resistenza di shunt per il sensing di corrente
#define SHUNT_UP_LIMIT	0.1			// limite superiore di tensione ai capi della resistenza di shunt per il sensing di corrente

#define SAFETY_EFFORT_LIMIT	0.02	// limite di sicurezza per lo sforzo sulla leva di input 20mV@10Vex ~ 200N

// ************************************************************
//
// ABSTRACT DATA TYPE
//
// ************************************************************


/* Struttura descrittiva di un controllore automatico PID */
struct Channel {
	int Ch_number;					// ID progressivo del canale della scheda
	int ID_transducer;				// costante simbolica ID del trasduttore
	char address[30];				// indirizzo fisico del canale
};


/* Struttura descrittiva di un controllore automatico PID */
struct PID_ctrl {
	double proportional_gain;		// guadagno proporzionale = 100 / proportional band
	double integral_time;			// tempo di integrazione del controllore [min]
	double derivative_time;			// tempo di derivazione del controllore [min]
};



// ************************************************************
//
// COSTANTI GLOBALI
//
// ************************************************************


// ------------------------------------------------------------
// Descrizioni cablaggi schede National Instruments
// ------------------------------------------------------------

// ------------------------------------------------------------
// Analog Inputs
// ------------------------------------------------------------
//static const struct Channel Ch_NI6229_AIN[NCH_6229_AIN]
static const struct Channel Ch_NI6363_AIN[NCH_6363_AIN] = {
	// 			n�ch				sensors				physical address 
	{			0	,				NU			,		""					},		
	{			1	,				NU			,		""					},
	{			2	,				NU			,		""					},
	{			3	,				P02			,		"PXI1Slot2/ai3"		},		// Pressione
	{			4	,				P03			,		"PXI1Slot2/ai4"		},
	{			5	,				P04			,		"PXI1Slot2/ai8"		},		// capillare 1
	{			6	,				P05			,		"PXI1Slot2/ai7"		},		// capillare 2
	{			7	,				P06			,		"PXI1Slot2/ai6"		},		// capillare 3
	{			8	,				P07			,		"PXI1Slot2/ai5"		},		// capillare 4
	{			9	,				NU			,		""					},		 
	{			10	,				NU			,		""					},
	{			11	,				P08			,		"PXI1Slot2/ai11"	},		// Pressione
	{			12	,				P09			,		"PXI1Slot2/ai12"	},
	{			13	,				P10			,		"PXI1Slot2/ai13"	},		
	{			14	,				P11			,		"PXI1Slot2/ai14"	},		
	{			15	,				K1			,		"PXI1Slot2/ai15"	},		// Temperatura
	{			16	,				NU			,		""					},		
	{			17	,				L1			,		"PXI1Slot2/ai17"	},		// Magnetostrittivo sul martinetto di comando input lever DIFFERENTIAL
	{			18	,				L2			,		"PXI1Slot2/ai18"	},		// Magnetostrittivo sul main piston del servocomando DIFFERENTIAL
	{			19	,				F1			,		"PXI1Slot2/ai19"	},		// Turbina misura di portata sul condotto di mandata del banco
	{			20	,				NU			,		""					},		
	{			21	,				NU			,		""					},
	{			22	,				NU			,		""					},
	{			23	,				NU			,		""					},		
	{			24	,				NU			,		""					},
	{			25	,				NU			,		""					},		
	{			26	,				NU			,		""					},
	{			27	,				NU			,		""					},		
	{			28	,				NU			,		""					},
	{			29	,				NU			,		""					},
	{			30	,				NU			,		""					},
	{			31	,				NU			,		""					},
};

static const struct Channel Ch_NI6341_AIN[NCH_6341_AIN] = {							// scheda dedicata ai canali del servocomando   
	// 			n�ch				sensors				physical address 
	{			0	,				CV_SYS1		,		"PXI1Slot5/ai0"		},		//corrente CV_SYS1 DIFFERENTIAL	
	{			1	,				CV_SYS2		,		"PXI1Slot5/ai1"		},		//corrente CV_SYS2 DIFFERENTIAL
	{			2	,				SYS1_S		,		"PXI1Slot5/ai2"		},		//corrente SYS1_S DIFFERENTIAL
	{			3	,				SYS2_S		,		"PXI1Slot5/ai3"		},		//corrente SYS2_S DIFFERENTIAL    
	{			4	,				PSU3		,		"PXI1Slot5/ai4"		},		//PSU3 28V
	{			5	,				NU			,		""					},
	{			6	,				NU			,		""					},
	{			7	,				NU			,		""					},
	{			8	,				NU			,		""					},
	{			9	,				NU			,		""					},		
	{			10	,				NU			,		""					},
	{			11	,				NU			,		""					},
	{			12	,				NU			,		""					},
	{			13	,				NU			,		""					},		
	{			14	,				NU			,		""					},		
	{			15	,				NU			,		""					},
};

/*
static const struct Channel Ch_SCC68_AIN[NCH_SCC68] = {
	// 			n�ch				sensors				physical address 
	{			0	,				R1_SHUNT	,		"SCC1Mod3/ai0"		},		// Current sensing servovalvola UUT Sistema1
	{			1	,				R2_SHUNT	,		"SCC1Mod3/ai1"		},		// Current sensing servovalvola UUT Sistema2
	{			2	,				R9_SHUNT	,		"SCC1Mod2/ai0"		},		// Current sensing elettrovalvola UUT Sistema1
	{			3	,				R10_SHUNT	,		"SCC1Mod2/ai1"		},		// Current sensing elettrovalvola UUT Sistema1
};*/


// ------------------------------------------------------------
// Analog Outputs
// ------------------------------------------------------------
static const struct Channel Ch_NI6363_AOUT[NCH_6363_AOUT] = {
	// 			n�ch				actuator			physical address
	{			0	,				SV2			,		"PXI1Slot2/ao0"		},		// Input lever
	{			1	,				SV1			,		"PXI1Slot2/ao1"		},		// Load / PSU
	{			2	,				NU			,		""					},		
	{			3	,				V3			,		"PXI1Slot2/ao3"		},		// V3 Reference di pressione 
};

static const struct Channel Ch_NI6341_AOUT[NCH_6341_AOUT] = {
	// 			n�ch				actuator			physical address
	{			0	,				SV_SYS1		,		"PXI1Slot5/ao0"		},		// Servovalvola CSAS1 
	{			1	,				SV_SYS2		,		"PXI1Slot5/ao1"		},		// Servovalvola CSAS2
};

// ------------------------------------------------------------
// Digital I/O
// ------------------------------------------------------------
//static const struct Channel Ch_NI6229_DIO[NCH_6229_DIO]
static const struct Channel Ch_NI6363_DIO[NCH_6363_DIO] = {
	// 			n�ch				actuator			physical address
	{			0	,				NU		,			""					},		// Riservato SCXI
	{			1	,				NU		,			""					},		// Riservato SCC-AI06
	{			2	,				NU		,			""					},	
	{			3	,				NU		,			""					},			 
	{			4	,				NU		,			""					},	    
	{			5	,				EV01	,			"PXI1Slot2/port0"	},	    // Elettrovalvole RATB
	{			6	,				EV02	,			"PXI1Slot2/port0"	},	
	{			7	,				EV03	,			"PXI1Slot2/port0"	},	
	{			8	,				EV04A	,			"PXI1Slot2/port0"	},	
	{			9	,				EV04B	,			"PXI1Slot2/port0"	},	
	{			10	,				EV05	,			"PXI1Slot2/port0"	},	
	{			11	,				EV06	,			"PXI1Slot2/port0"	},		
	{			12	,				EV07	,			"PXI1Slot2/port0"	},		   
	{			13	,				EV08	,			"PXI1Slot2/port0"	},	
	{			14	,				EV09A	,			"PXI1Slot2/port0"	},	
	{			15	,				EV09B	,			"PXI1Slot2/port0"	},	
	{			16	,				NU		,			""					},
	{			17	,				NU		,			""					},		
	{			18	,				NU		,			""					},		
	{			19	,				NU		,			""					},	
	{			20	,				NU		,			""					},	
	{			21	,				NU		,			""					},	
	{			22	,				NU		,			""					},	
	{			23	,				NU		,			""					},	
	{			24	,				NU		,			""					},		
	{			25	,				NU		,			""					},
	{			26	,				NU		,			""					},	
	{			27	,				NU		,			""					},	
	{			28	,				NU		,			""					},	
	{			29	,				NU		,			""					},	
	{			30	,				NU		,			""					},	
	{			31	,				NU		,			""					},	
	{			32	,				G1		,			"PXI1Slot2/port1/line0"	},		// Avviso di intasamento filtro
	{			33	,				FC1		,			"PXI1Slot2/port1/line1"	},		// Proximity switch
	{			34	,				FC2		,			"PXI1Slot2/port1/line2"	},	
	{			35	,				FC3		,			"PXI1Slot2/port1/line3"	},	
	{			36	,				FC4		,			"PXI1Slot2/port1/line4"	},	
	{			37	,				FC5		,			"PXI1Slot2/port1/line5"	},	
	{			38	,				FC6		,			"PXI1Slot2/port1/line6"	},	
	{			39	,				NU		,			""					},		
	{			40	,				NU		,			""					},		
	{			41	,				NU		,			""					},	
	{			42	,				NU		,			""					},	
	{			43	,				NU		,			""					},
	{			44	,				NU		,			""					},	
	{			45	,				NU		,			""					},	
	{			46	,				NU		,			""					},	
	{			47	,				NU		,			""					},	
};

static const struct Channel Ch_NI6341_DIO[NCH_6341_DIO] = {
	// 			n�ch				actuator			physical address
	{			0	,				EV_SYS1	,			"PXI1Slot5/port0"	},		// EV_SYS1 elettrovalvola SYS1
	{			1	,				EV_SYS2	,			"PXI1Slot5/port0"	},		// EV_SYS2 elettrovalvola SYS2	
	{			2	,				NU		,			""					},	
	{			3	,				NU		,			""					},			 
	{			4	,				NU		,			""					},	    
	{			5	,				NU		,			""					},	   
	{			6	,				NU		,			""					},	
	{			7	,				NU		,			""					},	
	{			8	,				NU		,			""					},	
	{			9	,				NU		,			""					},	
	{			10	,				NU		,			""					},	
	{			11	,				NU		,			""					},		
	{			12	,				NU		,			""					},		   
	{			13	,				NU		,			""					},
	{			14	,				NU		,			""					},
	{			15	,				NU		,			""					},
	{			16	,				NU		,			""					},	
	{			17	,				NU		,			""					},		
	{			18	,				NU		,			""					},		
	{			19	,				NU		,			""					},	
	{			20	,				NU		,			""					},	
	{			21	,				NU		,			""					},	
	{			22	,				NU		,			""					},	
	{			23	,				NU		,			""					},
};	
	

// ------------------------------------------------------------
// Counters
// ------------------------------------------------------------
//static const struct Channel Ch_NI6229_CTR[NCH_6229_CTR] 
static const struct Channel Ch_NI6363_CTR[NCH_6363_CTR] = {
	// 			n�ch				sensors				physical address 
	{			0	,				F2			,		"PXI1Slot2/ctr0"	},		// Turbina di precisione per misura del leakage
	{			1	,				NU			,		""					},		
};


/*static const struct Channel Ch_NI2566[NCH_2566] = {
	//			n�ch				sensors
	{			0	,				SW0			,		"/Dev1/k0"			},		// Switch per setpoint servo
	{			1	,				SW1			,		"/Dev1/k1"			},	
	{			2	,				SW2			,		"/Dev1/k2"			},		
	{			3	,				SW3			,		"/Dev1/k3"			},
	{			4	,				SW4			,		"/Dev1/k4"			},		// Switch per setpoint alimentatore EV
	{			5	,				SW5			,		"/Dev1/k5"			},
	{			6	,				SW6			,		"/Dev1/k6"			},		// Switch per setpoint valvola proporzionale
	{			7	,				SW7			,		"/Dev1/k7"			},
	{			8	,				SW8			,		"/Dev1/k8"			},		// Switch per misura correnti
	{			9	,				SW9			,		"/Dev1/k9"			},	
	{			10	,				SW10		,		"/Dev1/k10"			},
	{			11	,				SW11		,		"/Dev1/k11"			},
	{			12	,				SW12		,		"/Dev1/k12"			},		// Allarme ottico / acustico
	{			13	,				SW13		,		"/Dev1/k13"			},		// Not Used
	{			14	,				NU			,		""					},
	{			15	,				NU			,		""					},
};*/


// ------------------------------------------------------------
// LVDT
// ------------------------------------------------------------
static const struct Channel Ch_NI4340[NCH_4340] = {
	// 			n�ch				sensors				physical address
	{			0	,				NU			,		""					},		// Not Used
	{			1	,				LVDT_SYS1	,		"PXI1Slot3/ai1"		},		// LVDT a bordo del servocomando SYS1
	{			2	,				LVDT_SYS2	,		"PXI1Slot3/ai2"		},		// LVDT a bordo del servocomando SYS2
	{			3	,				NU			,		""					},		// Not Used
};


// ------------------------------------------------------------
// Load Cells
// ------------------------------------------------------------
static const struct Channel Ch_NI4339[NCH_4339] = {
	// 			n�ch				sensors				physical address
	{			0	,				NU			,		""					},		// Not Used
	{			1	,				NU			,		""					},	
	{			2	,				NU			,		""					},		
	{			3	,				N1			,		"PXI1Slot4/ai3"		},		// Celle di carico N1 Input
	{			4	,				NU			,		""					},		// Not Used 
	{			5	,				NU			,		""					},		// Not Used
	{			6	,				NU			,		""					},		// Not Used 
	{			7	,				N2			,		"PXI1Slot4/ai7"		},		// Celle di carico N2 Load
};




// ************************************************************
//
// VARIABILI GLOBALI
//
// ************************************************************


/*------------------------------------------------------------------*/
/* Handler di pool e thread											*/
/*------------------------------------------------------------------*/
int gPoolHandleRT;				// ID del pool di thread  

int gAnalogThreadID; 			// ID thread function di gestione dei segnali analogici
int gCommunicationThreadID;	 	// ID thread function di comunicazione tra Host e RTtarget

int gTsqDataIn;					// handler TSQ - Multithreading
int gTsqDataOut;				// handler TSQ - Multithreading
int SendMeasureQueueCB;			// handler callback della coda

/*------------------------------------------------------------------*/
/* Variabili di controllo											*/
/*------------------------------------------------------------------*/

int QuitProgram;		// Uscita SW dal programma: 0-esecuzione 1-Esci
int Emergency;			// Arresto di emergenza: 0-Ok 1-Emergenza

int enCsas1Pid;			// Variabili di abilitazione dei loop di controllo
int enCsas2Pid;			// 0-controllo inattivo 1-controllo PID attivo 
int enInputPid;
int enLoadPid;

int enUutVoltageReg;	// Abilita variazione della tensione di alimentazione delle solenoid valve su servocomando
int enUutPressureReg;	// Abilita variazione della pressione di alimentazione del servocomando rispetto ai 206bar operativi

int enNullBias;			// Abilita il percorso di lettura della corrente di Null Bias delle servovalvole a bordo dei CSAS
int enEvCurrent;		// Abilita il percorso di lettura della corrente di eccitazione delle bobine delle elettrovalvole a bordo dei CSAS


/*------------------------------------------------------------------*/
/* Variabili con significati fisici									*/
/*------------------------------------------------------------------*/

int solenoid_valve[NUM_EV];		// Array immagine dello stato delle solenoid valve su banco
int valve_6341[NUM_EV_6341];	// Array immagine dello stato delle solenoid valve su banco PXIe 6341

struct PID_ctrl PID_parameters[4];
								//	ID			GuadagnoProporzionale		TempoDiIntegrazione		TempoDiDerivazione
								//	CSAS1				x							x						x
								//	CSAS2				x							x						x
								//	INPUT				x							x						x
								//	LOAD				x							x						x

// Set-point dei regolatori PID
double CsasSetPoint;			// [mm]
double InputSetPoint;			// [mm]
double LoadSetPoint;			// [N]
double UutVoltageSetpoint;		// [V]
double UutPressureSetpoint;		// [bar]

// Forme d'onda set-point
int WaveType;					// option: STATIC, SINE, SQUARE, TRIANGULAR
double WaveAmplitude;			// [mm]
double WaveFrequency;			// [Hz]

// Direzione del carico
int LoadDirection;				// option: COMPRESSION, EXTENSION



unsigned int id_thread;
