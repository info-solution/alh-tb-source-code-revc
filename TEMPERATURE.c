#include <userint.h>
#include "TEMPERATURE.h"

#include "global_var.h"     


int CVICALLBACK QuitTemperaturePanelCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello misure di pressione
			HidePanel (temperaturepanel);
			
			break;
	}
	return 0;
}
