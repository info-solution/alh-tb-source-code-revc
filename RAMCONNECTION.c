#include <userint.h>
#include <windows.h>   
#include "RAMCONNECTION.h"

#include "global_var.h"    

/* Routine di verifica errori HW scheda DAQmx */
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else             


/* variabili di visibilita' ristretta al file */



/*--------------------------------------------*/




int CVICALLBACK QuitInputManualConnectionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int Visible;

	switch (event)
	{
		case EVENT_COMMIT:

			// scarica pannello con procedura manuale
			DiscardPanel (inputconnectionpanel);
			inputconnectionpanel = -1000;
			
			/* Verifico da quale punto e' stato richiamato il pannello di connessione e lo riattivo */
			
			// Pannello di calibrazione meccanica
			if (mechcalibrationpanel>0)
			{
				GetPanelAttribute (mechcalibrationpanel, ATTR_VISIBLE, &Visible);
				if (Visible==1)
				{
					// riattiva il pannello calibrazione meccanica
					SetPanelAttribute (mechcalibrationpanel, ATTR_DIMMED, YES);
				}
				
				// impostazione oscillazione
				SetInputPosition(STATIC, 0.0, 0.0, 0.1);
			
				Sleep(100);
			
				// attiva controllore PID del martinetto di controllo della leva manuale
				SetEnableInput(YES);
				
				// alimenta il martinetto di controllo della leva manuale
				SetSolenoidValve(EV07,OPEN);
				
				// azzeramento timer
				number_of_cycle=0;
			}
		
			// Pannello di test manuale
			if (manualpanel>0)
			{
				GetPanelAttribute (manualpanel, ATTR_VISIBLE, &Visible);
				if (Visible==1)
				{
					// riattiva il pannello calibrazione meccanica
					SetPanelAttribute (manualpanel, ATTR_DIMMED, YES);
				}
				
				// impostazione oscillazione
				SetInputPosition(STATIC, 0.0, 0.0, 0.1);
			
				Sleep(100);
			
				// attiva controllore PID del martinetto di controllo della leva manuale
				SetEnableInput(YES);
				
				// alimenta il martinetto di controllo della leva manuale
				SetSolenoidValve(EV07,OPEN);
			}
			 
			break;
	}
	return 0;
}

int CVICALLBACK QuitOutputManualConnectionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			DiscardPanel (outputconnectionpanel);
			outputconnectionpanel = -1000;
			
			break;
	}
	return 0;
}

int CVICALLBACK QuitLoadManualConnectionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	int Visible;
	int carter_open=YES;
	
	int load_engage_pic_index=0; 
	
	switch (event)
	{
		case EVENT_COMMIT:

			// controllo l'immagine visualizzata attualmente
			GetCtrlAttribute(inertialconnectionpanel, INERCONPNL_LOADPICRNG, ATTR_CTRL_VAL, &load_engage_pic_index);
			
			// se la sequenza di fotografie non e' arrivata al termine allora avanzo di uno scatto
			// altrimenti esco dalla procedura di guida operatore e torno al pannello di testing
			
			if (load_engage_pic_index<7)
			{
				// avanzo di uno scatto
				load_engage_pic_index++;
				SetCtrlAttribute(inertialconnectionpanel, INERCONPNL_LOADPICRNG, ATTR_CTRL_VAL, load_engage_pic_index);
			}
			else
			{
				
				// verifica che il carter sia chiuso  
				GetStatus(FC6_STATUS,&carter_open);   
				if (carter_open==YES)
				{
					MessagePopup ("Attention!", "Close the carter!");    
					
					// disattiva possibilita' di creare nuove istanze dell'evento Done
					SetCtrlAttribute (inertialconnectionpanel, INERCONPNL_DONEBTN, ATTR_DIMMED, NO); 	
				}
				
				while(carter_open==YES)
				{
					GetStatus(FC6_STATUS,&carter_open);   
					ProcessDrawEvents();
					ProcessSystemEvents();
					Sleep(1000);	
				}
			
				// riazzero la sequenza ed esco dal pannello
				load_engage_pic_index=0;
				SetCtrlAttribute(inertialconnectionpanel, INERCONPNL_LOADPICRNG, ATTR_CTRL_VAL, load_engage_pic_index);
				
				// scarica pannello
				DiscardPanel (inertialconnectionpanel);
				inertialconnectionpanel = -1000;
			
				/* Verifico da quale punto e' stato richiamato il pannello di connessione e lo riattivo */
			
				// Pannello di test manuale
				if (manualpanel>0)
				{
					GetPanelAttribute (manualpanel, ATTR_VISIBLE, &Visible);
					if (Visible==1)
					{
						// riattiva il pannello calibrazione meccanica
						SetPanelAttribute (manualpanel, ATTR_DIMMED, YES);
					}
				}
			}
			
			break;
	}
	
Error:
	return error;
}


int CVICALLBACK ExitInertialConnectionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target ed Host
			EmergencyStop();
			
			break;
	}

	return 0;
}
	
	
int CVICALLBACK ExitInputConnectionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target ed Host
			EmergencyStop();
			
			break;
	}

	return 0;
}


int CVICALLBACK ExitPistonConnectionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target ed Host
			EmergencyStop();
			
			break;
	}

	return 0;
}
	
	
