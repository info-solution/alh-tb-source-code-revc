/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2010. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  MECHCALPNL                       1
#define  MECHCALPNL_PRESSURESLD           2       /* callback function: ChangeDeliveryPressureCB */
#define  MECHCALPNL_FREQUENCYSLD          3       /* callback function: ChangeInputFrequencyCB */
#define  MECHCALPNL_AMPLITUDESLD          4       /* callback function: ChangeInputAmplitudeCB */
#define  MECHCALPNL_BENCHPOWEROFFBTN      5       /* callback function: BenchPowerOffCB */
#define  MECHCALPNL_PICTURE_2             6
#define  MECHCALPNL_BENCHPOWERONBTN       7       /* callback function: BenchPowerOnCB */
#define  MECHCALPNL_PICTURE               8
#define  MECHCALPNL_INPUTUNENGAGEBTN      9       /* callback function: InputUnEngageCB */
#define  MECHCALPNL_POWEROFFBTN           10      /* callback function: PowerOffPressureCB */
#define  MECHCALPNL_POWERONBTN            11      /* callback function: PowerOnPressureCB */
#define  MECHCALPNL_QUITBTN               12      /* callback function: QuitMechCalibrationPanelCB */
#define  MECHCALPNL_STOPBTN               13      /* callback function: ArrestMechCalibrationCB */
#define  MECHCALPNL_INPUTENGAGEBTN        14      /* callback function: InputEngageCB */
#define  MECHCALPNL_DECORATION            15
#define  MECHCALPNL_TEXTMSG_4             16
#define  MECHCALPNL_TEXTMSG_3             17
#define  MECHCALPNL_TEXTMSG               18
#define  MECHCALPNL_DECORATION_5          19
#define  MECHCALPNL_DECORATION_4          20
#define  MECHCALPNL_DECORATION_2          21
#define  MECHCALPNL_CAP4PRESSURENMR       22
#define  MECHCALPNL_CAP3PRESSURENMR       23
#define  MECHCALPNL_CAP2PRESSURENMR       24
#define  MECHCALPNL_RATBFLOWNMR           25
#define  MECHCALPNL_LEAKAGENMR            26
#define  MECHCALPNL_CAP1PRESSURENMR       27
#define  MECHCALPNL_UUTRETURNNMR          28
#define  MECHCALPNL_RATBRETURNNMR         29
#define  MECHCALPNL_RATBPRESSURENMR       30
#define  MECHCALPNL_UUTPRESSURENMR        31
#define  MECHCALPNL_TEXTMSG_2             32
#define  MECHCALPNL_DECORATION_3          33
#define  MECHCALPNL_RETURNRNG             34      /* callback function: ChangeReturnPressureCB */
#define  MECHCALPNL_SYSTEMCHOISERNG       35      /* callback function: ChangeMechCalSystemCB */
#define  MECHCALPNL_LEAKAGESYSTEMRNG      36      /* callback function: ChangeMechCalSystemLeakCB */
#define  MECHCALPNL_CSASRNG               37      /* callback function: ChangeMechCalCsasCB */
#define  MECHCALPNL_CTRLSLD               38
#define  MECHCALPNL_TIMERNMR              39      /* callback function: ChangeMechClockStatusCB */
#define  MECHCALPNL_CYCLENMR              40
#define  MECHCALPNL_TIMER                 41      /* callback function: MechCycTimerCB */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ArrestMechCalibrationCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BenchPowerOffCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BenchPowerOnCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeDeliveryPressureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeInputAmplitudeCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeInputFrequencyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeMechCalCsasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeMechCalSystemCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeMechCalSystemLeakCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeMechClockStatusCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeReturnPressureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK InputEngageCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK InputUnEngageCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK MechCycTimerCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PowerOffPressureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PowerOnPressureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitMechCalibrationPanelCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
