
#include <windows.h>
#include <formatio.h>
#include <analysis.h>
#include <rtutil.h>
#include <userint.h>
#include <cvinetv.h>
#include <utility.h>  
#include <ansi_c.h>    
#include "global_var.h" 
#include "error_table.h"

#include "MANUALTEST.h"
#include "PRESSURE.h"
#include "MECHCALIBRATION.h"
#include "ELECALIBRATION.h"
#include "LVDT.h"
#include "PLOTX.h"
#include "PLOTY.h"
#include "STRAIN.h"       
#include "PLOTXY.h"
#include "SOLENOID.h"
#include "TEMPERATURE.h" 
#include "SERVOBIAS.h"

/* Routine di verifica errori HW scheda DAQmx */
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else                

//#define IP_target "194.185.75.51"

/* ------------------------------------------------------------------------------------ */
/* Variabili con visibilita' locale														*/
/* ------------------------------------------------------------------------------------ */

double InitFilterStateX[50];
double InitFilterStateY[50];
double previousPosition=0.0; 


/* ------------------------------------------------------------------------------------ */
/* Connessioni Network Variables														*/
/* ------------------------------------------------------------------------------------ */

// WRITER

static CNVWriter	wHandQuitProgram;			// comando di chiusura programma di testing
static CNVWriter	wHandEmergency;				// comando di arresto di emergenza

static CNVWriter	wHandEnCsas1;				// comando di abilitazione regolazione del CSAS sul Sistema 1
static CNVWriter	wHandEnCsas2;				// comando di abilitazione regolazione del CSAS sul Sistema 2
static CNVWriter	wHandEnInput;				// comando di abilitazione regolazione del martinetto sulla leva di Input
static CNVWriter	wHandEnLoad;				// comando di abilitazione regolazione del carico sul main piston
//static CNVWriter	wHandEnUutVoltage;			// comando di abilitazione regolazione della tensione solenoid valve UUT
//static CNVWriter	wHandEnUutPressure;			// comando di abilitazione regolazione della pressione di mandata verso UUT
//												// 0-controllo inattivo 1-controllo attivo

//static CNVWriter	wHandEnNullBias;			// comando di abilitazione misura bias servovalvole a bordo attuatore
//static CNVWriter	wHandEnEVcurrent;			// comando di abilitazione misura assorbimento solenoid valve

static CNVWriter	wHandEV01;					// comandi di controllo elettrovalvole banco
static CNVWriter	wHandEV02;
static CNVWriter	wHandEV03;
static CNVWriter	wHandEV04A;
static CNVWriter	wHandEV04B;
static CNVWriter	wHandEV05;
static CNVWriter	wHandEV06;
static CNVWriter	wHandEV07;
static CNVWriter	wHandEV08;
static CNVWriter	wHandEV09A;
static CNVWriter	wHandEV09B;

//static CNVWriter	wHandSolenoid1;				// comandi di controllo elettrovalvole servocomando
//static CNVWriter	wHandSolenoid2;

static CNVWriter	wHandCsasSetpoint;			// setpoint di posizionamento degli attuatori CSAS a bordo servocomando
static CNVWriter	wHandInputSetpoint;			// setpoint di posizionamento del martinetto sulla leva di input
static CNVWriter	wHandLoadSetpoint;			// setpoint di carico del main piston

static CNVWriter	wHandUutPressureSetpoint;	// setpoint di pressione verso il servocomando
static CNVWriter	wHandUutVoltageSetpoint;	// setpoint di tensione verso le solenoid valve del servocomando

static CNVWriter	wHandWaveType;				// tipologia di forma d'onda di controllo
static CNVWriter	wHandWaveAmplitude;			// ampiezza di oscillazione dell'onda di controllo
static CNVWriter	wHandWaveFrequency;			// frequenza di oscillazione dell'onda di controllo

static CNVWriter	wHandLoadDirection;			// direzione di applicazione del carico rispetto al main piston (compressione o estensione)

static CNVWriter	wHandCsasPid;				// array 3x1 contenente i coefficienti di regolazione PID Kc, Ti, Td dei CSAS
static CNVWriter	wHandInputPid;				// array 3x1 contenente i coefficienti di regolazione PID Kc, Ti, Td del martinetto di input
static CNVWriter	wHandLoadPid;				// array 3x1 contenente i coefficienti di regolazione PID Kc, Ti, Td del carico oleodinamico

// SUBSCRIBER

static CNVSubscriber	sHandError;				// codice di errore inviato all'Host
static CNVSubscriber	sHandStatus;			// maschera binaria inviata all'Host per indicare lo stato di 
												// solenoid valve e switch di banco e servocomando
static CNVSubscriber	sHandMeasure;			// acquisizione analogiche raggruppate per grandezze fisiche
static CNVSubscriber	sHandLeakage;

						
/* ------------------------------------------------------------------------------------ */
/* Prototipi Callback delle connessioni Subscriber										*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ErrorCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK MeasureCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK LeakageCB (void * handle, CNVData data, void * callbackData);


/* ------------------------------------------------------------------------------------ */
/* Prototipi Funzioni a visibilita' locale												*/
/* ------------------------------------------------------------------------------------ */

void PressureManager(void);
void DisplacementManager(void);
void StrainManager(void);
void CharacteristicManager(void);
void SolenoidManager(void);
void TemperatureManager(void);
void BiasManager(void);
void FlowManager(void);
void InitPidParameter(void);
void CVICALLBACK StopLoadRam (void *callbackData);  
void mobile_mean(double m[], double signal[],int order,int SIGNAL_SMPL, int print);
void mobile_mean2(double m[], double signal[],int order,int size, int print);


/* ------------------------------------------------------------------------------------ */
/* Routine di inizializzazione delle connessioni di rete tra RTtarget ed Host sia di	*/
/* tipo subscriber che writer															*/
/* ------------------------------------------------------------------------------------ */

int InitNetworkConnection_PC(void)
{
	int error=0;
	char connection_name[80]="";
	
	
	//error = CNVCreateSubscriber (name, DataCallback, StatusCallback, 0,
	//			10000, 0, &subscriber);
	//		SetWaitCursor (0);
	//		if (error < 0)
	//			MessagePopup ("Error", CNVGetErrorDescription(error));
	
	/*------------------------------------------*/
	/* Apertura connessioni di tipo Subscriber	*/
	/*------------------------------------------*/
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_error", IP_target);
	CNVCreateSubscriber(connection_name , ErrorCB, 0, 0, 2000, 0, &sHandError);

	sprintf(connection_name, "\\\\%s\\system\\CNV_status", IP_target);
	CNVCreateSubscriber(connection_name , StatusCB, 0, 0, 2000, 0, &sHandStatus);
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_Measure", IP_target);
	CNVCreateSubscriber(connection_name , MeasureCB, 0, 0, 2000, 0, &sHandMeasure);

	sprintf(connection_name, "\\\\%s\\system\\CNV_Flow", IP_target);
	CNVCreateSubscriber(connection_name , LeakageCB, 0, 0, 2000, 0, &sHandLeakage);

	/*------------------------------------------*/
	/* Apertura connessioni di tipo Writer	*/
	/*------------------------------------------*/
	
	// Comandi di arresto
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_QuitProgram", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandQuitProgram);
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_Emergency", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEmergency);
	
	// Comandi di abilitazione

	sprintf(connection_name, "\\\\%s\\system\\CNV_EnCsas1", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnCsas1);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EnCsas2", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnCsas2);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EnInput", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnInput);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EnLoad", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnLoad);

	//sprintf(connection_name, "\\\\%s\\system\\CNV_EnUutVoltage", IP_target);
	//CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnUutVoltage);

	//sprintf(connection_name, "\\\\%s\\system\\CNV_EnUutPressure", IP_target);
	//CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnUutPressure);

	//sprintf(connection_name, "\\\\%s\\system\\CNV_EnNullBias", IP_target);
	//CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnNullBias);

	//sprintf(connection_name, "\\\\%s\\system\\CNV_EnEVcurrent", IP_target);
	//CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEnEVcurrent);

	// Controllo elettro-valvole banco
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_EV01", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV01);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV02", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV02);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV03", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV03);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV04A", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV04A);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV04B", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV04B);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV05", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV05);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV06", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV06);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV07", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV07);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV08", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV08);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV09A", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV09A);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV09B", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandEV09B);

	// Controllo elettro-valvole servocomando
	
/*	sprintf(connection_name, "\\\\%s\\system\\CNV_EV_SYS1", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandSolenoid1);

	sprintf(connection_name, "\\\\%s\\system\\CNV_EV_SYS2", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandSolenoid2);*/	
	
	// Impostazione dei set-point
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_CsasSetPoint", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandCsasSetpoint);

	sprintf(connection_name, "\\\\%s\\system\\CNV_InputSetPoint", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandInputSetpoint);

	sprintf(connection_name, "\\\\%s\\system\\CNV_LoadSetPoint", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandLoadSetpoint);

	sprintf(connection_name, "\\\\%s\\system\\CNV_UutPressureSetPoint", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandUutPressureSetpoint);

	sprintf(connection_name, "\\\\%s\\system\\CNV_UutVoltageSetPoint", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandUutVoltageSetpoint);

	// Waveform	
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_WaveType", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandWaveType);

	sprintf(connection_name, "\\\\%s\\system\\CNV_WaveAmplitude", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandWaveAmplitude);

	sprintf(connection_name, "\\\\%s\\system\\CNV_WaveFrequency", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandWaveFrequency);

	// Load
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_LoadDirection", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandLoadDirection);

	// Controllori PID
	
	sprintf(connection_name, "\\\\%s\\system\\CNV_InputPid", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandInputPid);

	sprintf(connection_name, "\\\\%s\\system\\CNV_CsasPid", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandCsasPid);
														 
	sprintf(connection_name, "\\\\%s\\system\\CNV_LoadPid", IP_target);
	CNVCreateWriter (connection_name, NULL, 0, 2000, 0, &wHandLoadPid);

	Delay(2.0);
	
	// Inizializzazione controllori PID
	InitPidParameter();
		
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Inizializzazione parametri PID														*/
/* ------------------------------------------------------------------------------------ */

void InitPidParameter(void)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td #FM          

	/* Inizializzazione coefficienti di regolazione PID */

	// martinetto input
	PID_parameters[PILOT].proportional_gain = -30.0;
	PID_parameters[PILOT].integral_time = 0.0;	
	PID_parameters[PILOT].derivative_time = 0.0;	
	
	coefficienti[0] = PID_parameters[PILOT].proportional_gain;
	coefficienti[1] = PID_parameters[PILOT].integral_time;
	coefficienti[2] = PID_parameters[PILOT].derivative_time;
	
	SetInputPid(coefficienti);
	Sleep(200);
	
	// autopiloti
	PID_parameters[CSAS].proportional_gain = -10000.0;
	PID_parameters[CSAS].integral_time = 0.0;	
	PID_parameters[CSAS].derivative_time = 0.0;		
	
	coefficienti[0] = PID_parameters[CSAS].proportional_gain;
	coefficienti[1] = PID_parameters[CSAS].integral_time;
	coefficienti[2] = PID_parameters[CSAS].derivative_time;

	SetCsasPid(coefficienti);
	Sleep(200);
		
	// martinetto di carico
	PID_parameters[LOAD].proportional_gain = -4500.0;
	PID_parameters[LOAD].integral_time = 0.00383;	
	PID_parameters[LOAD].derivative_time = 0.000957;	
	
	coefficienti[0] = PID_parameters[LOAD].proportional_gain;
	coefficienti[1] = PID_parameters[LOAD].integral_time;
	coefficienti[2] = PID_parameters[LOAD].derivative_time;
	
	SetLoadPid(coefficienti);
	Sleep(200);
	
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target dello stop del programma									*/
/* ------------------------------------------------------------------------------------ */

int SetStopProgram(int stop_flag)
{
	CNVData CNVStopFlag;	
	int error=0;
	
	if ((stop_flag!=YES)&&(stop_flag!=NO))
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}
	
	CNVCreateScalarDataValue (&CNVStopFlag, CNVInt32, stop_flag);
	CNVWrite (wHandQuitProgram, CNVStopFlag, 2000);
	CNVDisposeData(CNVStopFlag);	

Error:	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target dell'avvio arresto di emergenza							*/
/* ------------------------------------------------------------------------------------ */

int SetEmergency(int emergency_flag)
{
	CNVData CNVEmergencyFlag;	
	int error=0;
	
	if ((emergency_flag!=YES)&&(emergency_flag!=NO))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}
		
	CNVCreateScalarDataValue (&CNVEmergencyFlag, CNVInt32, emergency_flag);
	CNVWrite (wHandEmergency, CNVEmergencyFlag, 2000);
	CNVDisposeData(CNVEmergencyFlag);	

Error:	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione controllo CSAS1								*/
/* ------------------------------------------------------------------------------------ */

int SetEnableCsas1(int enable_flag)
{
	CNVData CNVEnableFlag;	
	int error=0;
	
	if ((enable_flag!=YES)&&(enable_flag!=NO))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}
	
	if (enable_flag==YES)
		enable_flag=HI;
	else
		enable_flag=LW;
		
	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
	CNVWrite (wHandEnCsas1, CNVEnableFlag, 2000);
	CNVDisposeData(CNVEnableFlag);	
	
	#ifdef DEBUG 
	PrintLog("Enable CSAS1");
	#endif
	
Error:	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione controllo CSAS2								*/
/* ------------------------------------------------------------------------------------ */

int SetEnableCsas2(int enable_flag)
{
	CNVData CNVEnableFlag;
	int error=0;
	
	if ((enable_flag!=YES)&&(enable_flag!=NO))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}
	
	if (enable_flag==YES)
		enable_flag=HI;
	else
		enable_flag=LW;
		
	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
	CNVWrite (wHandEnCsas2, CNVEnableFlag, 2000);
	CNVDisposeData(CNVEnableFlag);	

	#ifdef DEBUG 
	PrintLog("Enable CSAS2");
	#endif
	
Error:	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione controllo martinetto su Input Lever			*/
/* ------------------------------------------------------------------------------------ */

int SetEnableInput(int enable_flag)
{
	CNVData CNVEnableFlag;	
	int error=0;
	
	if ((enable_flag!=YES)&&(enable_flag!=NO))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}
	
	if (enable_flag==YES)
		enable_flag=HI;
	else
		enable_flag=LW;
	
	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
	CNVWrite (wHandEnInput, CNVEnableFlag, 2000);
	CNVDisposeData(CNVEnableFlag);	
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione carico oleodinamico							*/
/* ------------------------------------------------------------------------------------ */

int SetEnableLoad(int enable_flag)
{
	CNVData CNVEnableFlag;	
	int error=0;
	
	if ((enable_flag!=YES)&&(enable_flag!=NO))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}
	
	if (enable_flag==YES)
		enable_flag=HI;
	else
		enable_flag=LW;
		
	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
	CNVWrite (wHandEnLoad, CNVEnableFlag, 2000);
	CNVDisposeData(CNVEnableFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione regolazione proporzionale della tensione		*/
/* di eccitazione delle bobine delle solenoid valve a bordo dei sistemi CSAS.			*/
/* ------------------------------------------------------------------------------------ */
//*******NOT USED INTO THE CODE******* 
//int SetEnableUutVoltage(int enable_flag)
//{
//	CNVData CNVEnableFlag;	
//	int error=0;
//	
//	if ((enable_flag!=YES)&&(enable_flag!=NO))
//	{
//		error=ERROR_PARAMETER_VAL; 
//		goto Error;
//	}
//	
//	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
//	CNVWrite (wHandEnUutVoltage, CNVEnableFlag, 2000);
//	CNVDisposeData(CNVEnableFlag);	
//	
//Error:
//	return error;	
//}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione regolazione proporzionale della pressione	*/
/* di mandata ai due sistemi del servocomando sotto test.								*/
/* ------------------------------------------------------------------------------------ */

//int SetEnableUutPressure(int enable_flag)
//{
//	CNVData CNVEnableFlag;	
//	int error=0;
//	char message[200];
//	
//	if ((enable_flag!=FIXED)&&(enable_flag!=VARIABLE)&&(enable_flag!=POWER_OFF))
//	{
//		error=ERROR_PARAMETER_VAL; 
//		goto Error;
//	}
//	
//	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
//	CNVWrite (wHandEnUutPressure, CNVEnableFlag, 2000);
//	CNVDisposeData(CNVEnableFlag);	
//	
//	#ifdef DEBUG
//	sprintf(message,"opzione controllo -> %d",enable_flag);
//	PrintLog(message);
//	#endif
//	
//Error:
//	return error;	
//}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione misura della corrente di Null Bias			*/
/* assorbita dalle servovalvole a bordo dei due sistemi CSAS del servocomando.			*/
/* ------------------------------------------------------------------------------------ */

//int SetEnableNullBias(int enable_flag)
//{
//	CNVData CNVEnableFlag;
//	int error=0;
//	
//	if ((enable_flag!=YES)&&(enable_flag!=NO))
//	{
//		error=ERROR_PARAMETER_VAL; 
//		goto Error;
//	}
//	
//	if (enable_flag==YES)
//		enable_flag=HI;
//	else
//		enable_flag=LW;
//		
//	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
//	CNVWrite (wHandEnNullBias, CNVEnableFlag, 2000);
//	CNVDisposeData(CNVEnableFlag);	
//	
//Error:
//	return error;	
//}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione al RT Target abilitazione misura della corrente di eccitazione			*/
/* assorbita dalle solenoid valve a bordo dei due sistemi CSAS del servocomando.		*/
/* ------------------------------------------------------------------------------------ */

//int SetEnableEvCurrent(int enable_flag)
//{
//	CNVData CNVEnableFlag;	
//	int error=0;
//	
//	if ((enable_flag!=YES)&&(enable_flag!=NO))
//	{
//		error=ERROR_PARAMETER_VAL; 
//		goto Error;
//	}
//		
//	if (enable_flag==YES)
//		enable_flag=HI;
//	else
//		enable_flag=LW;
//	
//	CNVCreateScalarDataValue (&CNVEnableFlag, CNVInt32, enable_flag);
//	CNVWrite (wHandEnEVcurrent, CNVEnableFlag, 2000);
//	CNVDisposeData(CNVEnableFlag);	

//Error:
//	return error;	
//}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV01 sulla mandata del banco							*/
/* ------------------------------------------------------------------------------------ */

int SetEV01(int status_flag)
{
	CNVData CNVStatusFlag;	
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV01, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV02 sulla mandata del Sistema1 del servocomando 		*/
/* sotto test																			*/
/* ------------------------------------------------------------------------------------ */

int SetEV02(int status_flag)
{
	CNVData CNVStatusFlag;	
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV02, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV03 sulla mandata del Sistema2 del servocomando 		*/
/* sotto test																			*/
/* ------------------------------------------------------------------------------------ */

int SetEV03(int status_flag)
{
	CNVData CNVStatusFlag;
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV03, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV04A sul ritorno del servocomando per la misura del 	*/
/* leakage sul Sistema1 dell'unita' sotto test.											*/
/* ------------------------------------------------------------------------------------ */

int SetEV04A(int status_flag)
{
	CNVData CNVStatusFlag;	
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV04A, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV04B sul ritorno del servocomando per la misura del 	*/
/* leakage sul Sistema2 dell'unita' sotto test.											*/
/* ------------------------------------------------------------------------------------ */

int SetEV04B(int status_flag)
{
	CNVData CNVStatusFlag;
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV04B, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV05 per le misure di leakage sul ritorno del 		*/
/* servocomando sotto test.																*/
/* ------------------------------------------------------------------------------------ */

int SetEV05(int status_flag)
{
	CNVData CNVStatusFlag;
	int error=0;
	
	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV05, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV06 per il test di contropressione a 155bar sul 		*/
/* ritorno del servocomando.															*/
/* ------------------------------------------------------------------------------------ */

int SetEV06(int status_flag)
{
	CNVData CNVStatusFlag;	
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV06, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV07 per l'alimentazione della mandata del martinetto	*/
/* di controllo della leva di input.													*/
/* ------------------------------------------------------------------------------------ */

int SetEV07(int status_flag)
{
	CNVData CNVStatusFlag;
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV07, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV08 per l'alimentazione della mandata del martinetto	*/
/* di carico sul main piston del servocomando.											*/
/* ------------------------------------------------------------------------------------ */

int SetEV08(int status_flag)
{
	CNVData CNVStatusFlag;
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV08, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV09A per l'engage del martinetto	di carico 			*/
/* oleodinamico sul main piston del servocomando.										*/
/* ------------------------------------------------------------------------------------ */

int SetEV09A(int status_flag)
{
	CNVData CNVStatusFlag;
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV09A, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato elettrovalvola EV09B per lo sgancio del martinetto di carico 		*/
/* oleodinamico sul main piston del servocomando.										*/
/* ------------------------------------------------------------------------------------ */

int SetEV09B(int status_flag)
{
	CNVData CNVStatusFlag;	
	int error=0;

	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}

	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
	CNVWrite (wHandEV09B, CNVStatusFlag, 2000);
	CNVDisposeData(CNVStatusFlag);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato solenoid valve a bordo dello stadio autopilota CSAS1 				*/
/* ------------------------------------------------------------------------------------ */

//int SetSolenoid1(int status_flag)
//{
//	CNVData CNVStatusFlag;	
//	int error=0;

//	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
//	{
//		error=ERROR_PARAMETER_VAL; 
//		goto Error;
//	}

//	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
//	CNVWrite (wHandSolenoid1, CNVStatusFlag, 2000);
//	CNVDisposeData(CNVStatusFlag);	

//Error:
//	return error;	
//}


/* ------------------------------------------------------------------------------------ */
/* Controllo stato solenoid valve a bordo dello stadio autopilota CSAS2 				*/
/* ------------------------------------------------------------------------------------ */

//int SetSolenoid2(int status_flag)
//{
//	CNVData CNVStatusFlag;	
//	int error=0;

//	if ((status_flag!=OPEN)&&(status_flag!=CLOSE))
//	{
//		error=ERROR_PARAMETER_VAL; 
//		goto Error;
//	}

//	CNVCreateScalarDataValue (&CNVStatusFlag, CNVInt32, status_flag);
//	CNVWrite (wHandSolenoid2, CNVStatusFlag, 2000);
//	CNVDisposeData(CNVStatusFlag);	

//Error:
//	return error;	
//}


/* ------------------------------------------------------------------------------------ */
/* Impostazione del set-point dei controllori PID relativi ai sistemi autopilota CSAS.	*/
/* ------------------------------------------------------------------------------------ */

int SetCsasSetpoint(double setpoint_value)
{
	CNVData CNVSetpointVal;	
	int error=0;
	char message[200];

	// scalatura setpoint di posizione
	setpoint_value = setpoint_value / Characteristic[CSAS_MUX].conv_factor + Characteristic[CSAS_MUX].offset;

	CNVCreateScalarDataValue (&CNVSetpointVal, CNVDouble, setpoint_value);
	CNVWrite (wHandCsasSetpoint, CNVSetpointVal, 2000);
	CNVDisposeData(CNVSetpointVal);	

	#ifdef DEBUG 
	sprintf(message,"setpoint[m] = %f", setpoint_value);
	PrintLog(message);
	#endif
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione del set-point del controllore PID relativo al martinetto di pilotaggio	*/
/* leva di input manuale.																*/
/* ------------------------------------------------------------------------------------ */

int SetInputSetpoint(double setpoint_value)
{
	CNVData CNVSetpointVal;	
	int error=0;
	char message[200];     
		
	// scalatura setpoint di posizione
	setpoint_value = ( setpoint_value / Characteristic[L1_MUX].conv_factor ) + Characteristic[L1_MUX].offset;
	
	CNVCreateScalarDataValue (&CNVSetpointVal, CNVDouble, setpoint_value);
	CNVWrite (wHandInputSetpoint, CNVSetpointVal, 2000);
	CNVDisposeData(CNVSetpointVal);	
	
	#ifdef DEBUG 
	sprintf(message,"setpoint[m] = %f", setpoint_value);
	PrintLog(message);
	#endif
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione del set-point del controllore PID relativo al martinetto di carico del	*/
/* main piston.																			*/
/* ------------------------------------------------------------------------------------ */

int SetLoadSetpoint(double setpoint_value)
{
	CNVData CNVSetpointVal;	
	int error=0;
	char message[200];
	
	// scalatura setpoint di carico
	setpoint_value = ( setpoint_value / Characteristic[N2_MUX].conv_factor ) + Characteristic[N2_MUX].offset;
	
	#ifdef DEBUG 
	sprintf(message,"Bench Status = %f", setpoint_value);
	PrintLog(message);
	#endif
	
	CNVCreateScalarDataValue (&CNVSetpointVal, CNVDouble, setpoint_value);
	CNVWrite (wHandLoadSetpoint, CNVSetpointVal, 2000);
	CNVDisposeData(CNVSetpointVal);	
	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione del set-point di regolazione della pressione di mandata ai sistemi 		*/
/* 1 e 2 del servocomando sotto test.													*/
/* ------------------------------------------------------------------------------------ */

int SetUutPressureSetpoint(double setpoint_value)
{
	CNVData CNVSetpointVal;	
	int error=0;

	char message[100];
	
	// scalatura setpoint di pressione 
	setpoint_value = ( setpoint_value + V3.offset )/ V3.conv_factor;	
	
	#ifdef DEBUG 
	sprintf(message,"Pressure [V] = %f", setpoint_value);
	PrintLog(message);
	#endif
	
	CNVCreateScalarDataValue (&CNVSetpointVal, CNVDouble, setpoint_value);
	CNVWrite (wHandUutPressureSetpoint, CNVSetpointVal, 2000);
	CNVDisposeData(CNVSetpointVal);	
	
	return error;	
}
	
	
/* ------------------------------------------------------------------------------------ */
/* Impostazione del set-point di regolazione della tensione di eccitazione delle bobine	*/
/* delle solenoid valve a bordo dei sistemi CSAS del servocomando.						*/
/* ------------------------------------------------------------------------------------ */
//*******NOT USED INTO THE CODE******* 
//int SetUutVoltageSetpoint(double setpoint_value)
//{
//	CNVData CNVSetpointVal;
//	int error=0;
//	struct sensor PSU;
//	
//	/* dati di interfacciamento con l'alimentatore pilotato SITOP di costruzione Siemens */
//	PSU.offset = 0.0;				// si suppone che a 0V in input nel riferimento corrispondano 0V in uscita dall'alimentatore
//	PSU.conv_factor = 52.0/2.5;		// V/V
//	
//	// scalatura setpoint di tensione 
//	setpoint_value = setpoint_value / PSU.conv_factor + PSU.offset;
//	
//	CNVCreateScalarDataValue (&CNVSetpointVal, CNVDouble, setpoint_value);
//	CNVWrite (wHandUutVoltageSetpoint, CNVSetpointVal, 2000);
//	CNVDisposeData(CNVSetpointVal);	
//	
//	return error;	
//}	
	

/* ------------------------------------------------------------------------------------ */
/* Impostazione opzione di forma d'onda per il comando dei set-point di posizione degli	*/
/* attuatori: sinusoidale, quadra, triangolare.											*/
/* ------------------------------------------------------------------------------------ */

int SetWaveType(int option_value)
{
	CNVData CNVOptionVal;
	int error=0;
	char message[200];    
	
	if ((option_value!=STATIC)&&(option_value!=SINE)&&(option_value!=SQUARE)&&(option_value!=TRIANGULAR))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}	
	
	CNVCreateScalarDataValue (&CNVOptionVal, CNVInt32, option_value);
	CNVWrite (wHandWaveType, CNVOptionVal, 2000);
	CNVDisposeData(CNVOptionVal);	

	#ifdef DEBUG 
	sprintf(message,"Wave Type = %d", option_value);
	PrintLog(message);
	#endif
	
Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione valore di ampiezza massima della forma d'onda di controllo del 			*/
/* set-point durante le prove in oscillazione.											*/
/* ------------------------------------------------------------------------------------ */

int SetInputWaveAmplitude(double amplitude_value)
{
	CNVData CNVAmplitudeVal;
	int error=0;
	char message[200];    
	
	// scalatura setpoint di posizione
	
	amplitude_value = amplitude_value / Characteristic[L1_MUX].conv_factor;

	CNVCreateScalarDataValue (&CNVAmplitudeVal, CNVDouble, amplitude_value);
	CNVWrite (wHandWaveAmplitude, CNVAmplitudeVal, 2000);
	CNVDisposeData(CNVAmplitudeVal);	
	
	#ifdef DEBUG 
	sprintf(message,"Wave Amplitude = %f", amplitude_value);
	PrintLog(message);
	#endif
	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione valore di ampiezza massima della forma d'onda di controllo del 			*/
/* set-point durante le prove in oscillazione.											*/
/* ------------------------------------------------------------------------------------ */

int SetCsasWaveAmplitude(double amplitude_value)
{
	CNVData CNVAmplitudeVal;
	int error=0;
	char message[200];    
	
	// scalatura setpoint di posizione
	amplitude_value = amplitude_value / Characteristic[CSAS_MUX].conv_factor;
	
	CNVCreateScalarDataValue (&CNVAmplitudeVal, CNVDouble, amplitude_value);
	CNVWrite (wHandWaveAmplitude, CNVAmplitudeVal, 2000);
	CNVDisposeData(CNVAmplitudeVal);	
	
	#ifdef DEBUG 
	sprintf(message,"Wave Amplitude = %f", amplitude_value);
	PrintLog(message);
	#endif
	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione del valore di frequenza massima della forma d'onda di controllo del 	*/
/* set-point durante le prove in oscillazione.											*/
/* ------------------------------------------------------------------------------------ */

int SetWaveFrequency(double frequency_value)
{
	CNVData CNVFrequencyVal;
	int error=0;
	char message[200]; 
	
	CNVCreateScalarDataValue (&CNVFrequencyVal, CNVDouble, frequency_value);
	CNVWrite (wHandWaveFrequency, CNVFrequencyVal, 2000);
	CNVDisposeData(CNVFrequencyVal);	
	
	#ifdef DEBUG 
	sprintf(message,"Wave Frequency = %f", frequency_value);
	PrintLog(message);
	#endif
	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione opzione sul verso di applicazione del carico oleodinamico sul main		*/
/* piston, in compressione o in estensione.												*/
/* ------------------------------------------------------------------------------------ */

int SetLoadDirectionType(int option_value)
{
	CNVData CNVOptionVal;
	int error=0;

	if ((option_value!=COMPRESSION)&&(option_value!=EXTENSION))
	{
		error=ERROR_PARAMETER_VAL; 
		goto Error;
	}	

	CNVCreateScalarDataValue (&CNVOptionVal, CNVInt32, option_value);
	CNVWrite (wHandLoadDirection, CNVOptionVal, 2000);
	CNVDisposeData(CNVOptionVal);	

Error:
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione dei coefficienti di regolazione PID del controllore di comando dei due	*/
/* stadi autopilota CSAS.																*/
/* ------------------------------------------------------------------------------------ */

int SetCsasPid(double* pid_data)
{
	CNVData CNVPidVal;
	int error=0;
	int dimensione_array=3;
	char message[200];
	
	CNVCreateArrayDataValue (&CNVPidVal, CNVDouble, pid_data, 1, &dimensione_array);
	CNVWrite (wHandCsasPid, CNVPidVal, 2000);
	CNVDisposeData(CNVPidVal);	

	#ifdef DEBUG 
	sprintf(message,"CSAS PID data = %f %f %f", pid_data[0],pid_data[1],pid_data[2]);
	PrintLog(message);
	#endif
	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Impostazione dei coefficienti di regolazione PID del controllore di comando del		*/
/* martinetto sulla leva di input.														*/
/* ------------------------------------------------------------------------------------ */

int SetInputPid(double* pid_data)
{
	CNVData CNVPidVal;
	int error=0;
	int dimensione_array=3;
	
	CNVCreateArrayDataValue (&CNVPidVal, CNVDouble, pid_data, 1, &dimensione_array);
	CNVWrite (wHandInputPid, CNVPidVal, 2000);
	CNVDisposeData(CNVPidVal);	
	
	return error;	
}

	
/* ------------------------------------------------------------------------------------ */
/* Impostazione dei coefficienti di regolazione PID del controllore di comando del 		*/
/* martinetto di carico oleodinamico.													*/
/* ------------------------------------------------------------------------------------ */

int SetLoadPid(double* pid_data)
{
	CNVData CNVPidVal;
	int error=0;
	int dimensione_array=3;
	
	CNVCreateArrayDataValue (&CNVPidVal, CNVDouble, pid_data, 1, &dimensione_array);
	CNVWrite (wHandLoadPid, CNVPidVal, 2000);
	CNVDisposeData(CNVPidVal);	
	
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Inizializza i fattori di scala dei sensori									 		*/
/* ------------------------------------------------------------------------------------ */

int InitSensorsAcquisition(void)
{
	int error=0;
	
	Characteristic[P02_MUX].offset = 0.0;					// [V]
	Characteristic[P02_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P03_MUX].offset = 0.0;					// [V]
	Characteristic[P03_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P04_MUX].offset = 0.0;					// [V]
	Characteristic[P04_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P05_MUX].offset = 0.0;					// [V]
	Characteristic[P05_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P06_MUX].offset = 0.0;					// [V]
	Characteristic[P06_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P07_MUX].offset = 0.0;					// [V]
	Characteristic[P07_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P08_MUX].offset = 0.0;					// [V]
	Characteristic[P08_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P09_MUX].offset = 0.0;					// [V]
	Characteristic[P09_MUX].conv_factor = 40.0;				// [bar/V]
	
	Characteristic[P10_MUX].offset = 0.0;					// [V]
	Characteristic[P10_MUX].conv_factor = 40.0;				// [bar/V]
						
	Characteristic[P11_MUX].offset = 0.0;					// [V]
	Characteristic[P11_MUX].conv_factor = 1.0;				// [bar/V]
	
	Characteristic[K1_MUX].offset = 0.0;					// [V]
	Characteristic[K1_MUX].conv_factor = 100.0;				// [degC/V]
	
	Characteristic[PSU3_MUX].offset = 0.0;					// [V]
	Characteristic[PSU3_MUX].conv_factor = (2.49+6.49)/2.49;// [V/V]

	Characteristic[L1_MUX].offset = 5.0;					// [V]
	Characteristic[L1_MUX].conv_factor = 10.0;				// [mm/V]

	Characteristic[L2_MUX].offset = 5.0;					// [V]
	Characteristic[L2_MUX].conv_factor = 8.0;				// [mm/V]

	Characteristic[CV_SYS1_SHUNT].offset = 0.0;				// [V]
	Characteristic[CV_SYS1_SHUNT].conv_factor = 0.05;		// [A/V]

	Characteristic[CV_SYS2_SHUNT].offset = 0.0;				// [V]
	Characteristic[CV_SYS2_SHUNT].conv_factor = 0.05;		// [A/V]

	Characteristic[SYS1_S_SHUNT].offset = 0.0;				// [V]
	Characteristic[SYS1_S_SHUNT].conv_factor = 10.0;		// [A/V]

	Characteristic[SYS2_S_SHUNT].offset = 0.0;				// [V]
	Characteristic[SYS2_S_SHUNT].conv_factor = 10.0;		// [A/V]

	Characteristic[F1_MUX].offset = 0.0;					// [V]
	Characteristic[F1_MUX].conv_factor = 12.0;				// [lit/min/V * 1000 cc/lit]
	
	Characteristic[LVDT1_MUX].offset = 0.0;					// [V]
	Characteristic[LVDT1_MUX].conv_factor = 1000.0;			// [mm] con 3Vrms di eccitazione
	
	Characteristic[LVDT2_MUX].offset = 0.0;					// [V]
	Characteristic[LVDT2_MUX].conv_factor = 1000.0;			// [mm] con 3Vrms di eccitazione
	
	Characteristic[N1_MUX].offset = 0.0; 					// [V]
	Characteristic[N1_MUX].conv_factor = 10000.0;			// [N/V] 200N FS con 10Vdc di eccitazione
	
	Characteristic[N2_MUX].offset = 0.0;					// [V]
	Characteristic[N2_MUX].conv_factor = 2500000.0;			// [N/V] 50kN FS con 10Vdc di eccitazione
	
	Characteristic[INPUT_MUX].offset = 5.0;					// [V]
	Characteristic[INPUT_MUX].conv_factor = 10.0;			// [mm/V]

	Characteristic[CSAS_MUX].offset = 0.0;					// [V]
	Characteristic[CSAS_MUX].conv_factor = 1000.0;			// [mm]

	Characteristic[LOAD_MUX].offset = 0.0;					// [V]
	Characteristic[LOAD_MUX].conv_factor = 2500000.0;		// [N/V]
	
	Characteristic[F2_MUX].offset = 0.0;					// [V]
	Characteristic[F2_MUX].conv_factor = 0.025;				// [cc/pulse]
	
	V3.offset = 3.0;										// [bar]
	V3.conv_factor = 32.0;									// [bar/V]

	return error;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione della Network Variable contenente i dati provenienti dalla 	*/
/* scheda di acquisizione ADC															*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK MeasureCB (void * handle, CNVData data, void * callbackData)
{
	double buffer[13000];
	int buffer_dim;
	int i=0,j=0;
	double ave_value=0.0;
	int Visible=0;
	
	/************************************************************/
	/* Importazione della Network Variable						*/
	/************************************************************/
	
	// leggo numero di elementi dell'array network variable monodimensionale
	CNVGetArrayDataDimensions (data, 1, &buffer_dim);
	
	// leggo dati presenti nella network variable
	CNVGetArrayDataValue (data, CNVDouble, buffer, buffer_dim);
	
	// ricavo numero di samples per singolo canale in scan list di acquisizone
	number_of_samples = (int)(buffer_dim/NUM_CH_MUX);
	
	if (number_of_samples<=0)
		goto Error;
	
	// converto la variabile network in array bidimensionale scalando i dati secondo offset e
	// coefficiente di conversione caratteristici del canale ADC
	
	for (i=0;i<number_of_samples;i++)
		for (j=0 ; j<NUM_CH_MUX ; j++)
		{
			Measures[j][i]= (buffer[i*NUM_CH_MUX+j] - Characteristic[j].offset)*Characteristic[j].conv_factor ;	
		}
	
	// calcolo valori medi dei dati acquisiti nel buffer
	for (j=0 ; j<NUM_CH_MUX ; j++)
		Mean (Measures[j], number_of_samples, &MeasuresAve[j]);
			
	
	/************************************************************/
	/* Gestione misure											*/
	/************************************************************/
	
	// Misure di pressione
	PressureManager();

	// Misure di posizione
	DisplacementManager();
	
	// Misure di sforzo
	StrainManager();
	
	// Misure I/V solenoidi UUT
	SolenoidManager();
	
	// Misura null bias servovalvole
	BiasManager();
	
	// Misura della temperatura dell'olio nella condotta di ritorno dal UUT
	TemperatureManager();     
	
	// Misura della portata di ingresso al banco
	FlowManager();
	
	// Analisi caratteristiche del ciclo di controllo
	CharacteristicManager();
	
Error:
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Elaborazione messaggi di errore provenienti dal RT Target					 		*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ErrorCB (void * handle, CNVData data, void * callbackData)
{
	int errore=0;	
	char messaggio[200];
	
	CNVGetScalarDataValue (data, CNVInt32, &errore);
	
	if (errore!=OK)
	{
		sprintf(messaggio, "ERROR ON RT TARGET ---> %d", errore);
		PrintLog(messaggio);
	}

	return;
}


/* ------------------------------------------------------------------------------------ */
/* Elaborazione messaggi di stato provenienti dal RT Target						 		*/
/* ------------------------------------------------------------------------------------ */

int filter_clogged=LW;
int safety_status=LW;

void CVICALLBACK StatusCB (void * handle, CNVData data, void * callbackData)
{
	int error=0;
	char message[200];
	
	CNVGetScalarDataValue (data, CNVUInt32, &bench_status);

	/****************************************************/
	/* Gestione allarmi	e segnalazioni					*/
	/****************************************************/
	
	// filtro intasato
	if (((bench_status>>G1_STATUS)&0x1)==HI)
	{
		if (filter_clogged==LW)
		{
			MessagePopup ("Hydraulic Test Bench", "FILTER IS CLOGGED !!!");
			filter_clogged=HI;
		}
	}
		// cover di protezione aperta
	/*if (((bench_status>>FC6_STATUS)&0x1)==LW)
	{
		if (safety_status==LW)
		{
			MessagePopup ("Hydraulic Test Bench", "SAFETY COVER HAS BEEN REMOVED, PLEASE, REPLACE THE COVER !!!");
			safety_status=HI;
		}
	}
	
	/****************************************************/
	/* Gestione stato switch							*/
	/****************************************************/
	
	/* Riconoscimento proximity switch su flangia Tail */
	
	if (((bench_status>>FC1_STATUS)&0x1)==HI)
		actuatorType = MRA;
	else
		actuatorType = TRA;
	
	
	#ifdef DEBUG
	sprintf(message,"Bench Status = 0x%08X", bench_status);
	PrintLog(message);
	#endif
	
	return;
}

 
/* ------------------------------------------------------------------------------------ */
/* Ricezione misure di leakage sul ritorno del servocomando sotto test dalla 	 		*/
/* turbina F2																			*/
/* ------------------------------------------------------------------------------------ */

double previous_leakage[3];

void CVICALLBACK LeakageCB (void * handle, CNVData data, void * callbackData)
{
	double leakage=0;
	double frequenza=0;
	int Visible=0;
	int LeakActive=0;

	char messaggio[200];
	
	// dati caratteristici della turbina F2
	CharacteristicGear.offset = 0.0;			// [Hz]
	CharacteristicGear.conv_factor = 1.5;		// [cc/min = 0.025 tick/sec * cc/min * 60 sec/min]

	// importo il valore della network variable nella variabile globale leakage
	CNVGetScalarDataValue (data, CNVDouble, &frequenza);

	// scalo il valore di frequenza in dato di portata espresso in cc/min
	leakage = (frequenza - CharacteristicGear.offset)*CharacteristicGear.conv_factor;
	
	// verifico se il pannello di calibrazione meccanica con la lettura del leakage e' visibile
	GetPanelAttribute (mechcalibrationpanel, ATTR_VISIBLE, &Visible);  
	GetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, &LeakActive);
	
	if ((mechcalibrationpanel>0)&&(Visible==1)&&(LeakActive!=0))
	{
		// plot valore puntuale di leakage
		if ((leakage<5000.0)&&(leakage>0.0))
		{
			// media mobile
			leakage = (leakage + previous_leakage[0] + previous_leakage[1] + previous_leakage[2])/4;
			
			//shift array
			previous_leakage[3] = previous_leakage[2];
			previous_leakage[2] = previous_leakage[1]; 
			previous_leakage[1] = previous_leakage[0]; 
			
			previous_leakage[0] = leakage;
			
			SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_CTRL_VAL, leakage);
		}
	}
	
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misure di pressione												*/
/* ------------------------------------------------------------------------------------ */

void PressureManager(void)
{
	int Visible=0;

	/************************************************************/
	/* Visualizzazione misure su pannello di testing manuale	*/
	/************************************************************/

	/* Verifica apertura pannello */
	GetPanelAttribute (manualpanel, ATTR_VISIBLE, &Visible);
	
	if ((manualpanel>0)&&(Visible==1))
	{
		/* Misura pressione di mandata del servocomando */
		
		GetCtrlAttribute(manualpanel, MANPANEL_UUTPRESSURESTR, ATTR_DIMMED, &Visible);
		if (Visible==FALSE)
		{
			// plot strip chart
			PlotStripChart (manualpanel, MANPANEL_UUTPRESSURESTR, Measures[P03_MUX], number_of_samples , 0, 0, VAL_DOUBLE);
			// plot valore medio
			SetCtrlAttribute(manualpanel, MANPANEL_UUTPRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P03_MUX]);
		}
		
		/* Misura pressione di mandata del banco */
		
		GetCtrlAttribute(manualpanel, MANPANEL_BENCHPRESSURESTR, ATTR_DIMMED, &Visible);
		if (Visible==FALSE)
		{
			// plot strip chart
			PlotStripChart (manualpanel, MANPANEL_BENCHPRESSURESTR, Measures[P02_MUX], number_of_samples , 0, 0, VAL_DOUBLE);
			// plot valore medio
			SetCtrlAttribute(manualpanel, MANPANEL_BENCHPRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P02_MUX]);
		}
	}

	/************************************************************/
	/* Visualizzazione misure su pannello pressioni				*/
	/************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (pressurepanel, ATTR_VISIBLE, &Visible);
	
	if ((pressurepanel>0)&&(Visible==1))
	{
		/* Misura pressione di mandata del banco */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P02NMR, ATTR_CTRL_VAL, MeasuresAve[P02_MUX]);

		/* Misura pressione di ritorno del banco */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P11NMR, ATTR_CTRL_VAL, MeasuresAve[P11_MUX]);

		/* Misura pressione di mandata del servocomando */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P03NMR, ATTR_CTRL_VAL, MeasuresAve[P03_MUX]);

		/* Misura pressione di ritorno del servocomando */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P08NMR, ATTR_CTRL_VAL, MeasuresAve[P08_MUX]);

		/* Misura pressione dei capillari connessi alle camere dei cilindri del servocomando */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P04NMR, ATTR_CTRL_VAL, MeasuresAve[P04_MUX]);

		SetCtrlAttribute(pressurepanel, PRESSPNL_P05NMR, ATTR_CTRL_VAL, MeasuresAve[P05_MUX]);

		SetCtrlAttribute(pressurepanel, PRESSPNL_P06NMR, ATTR_CTRL_VAL, MeasuresAve[P06_MUX]);

		SetCtrlAttribute(pressurepanel, PRESSPNL_P07NMR, ATTR_CTRL_VAL, MeasuresAve[P07_MUX]);

		/* Misura pressione di mandata al martinetto di controllo leva di input */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P09NMR, ATTR_CTRL_VAL, MeasuresAve[P09_MUX]);

		/* Misura pressione di mandata al martinetto di carico oleodinamico */
		SetCtrlAttribute(pressurepanel, PRESSPNL_P10NMR, ATTR_CTRL_VAL, MeasuresAve[P10_MUX]);
	}
	
	/****************************************************************/
	/* Visualizzazione misure su pannello calibrazione meccanica	*/
	/****************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (mechcalibrationpanel, ATTR_VISIBLE, &Visible);
	
	if ((mechcalibrationpanel>0)&&(Visible==1))
	{
		/* Misura pressione di mandata del servocomando */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_RATBPRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P02_MUX]);
		
		/* Misura pressione di ritorno dal servocomando */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_RATBRETURNNMR, ATTR_CTRL_VAL, MeasuresAve[P11_MUX]);

		/* Misura pressione di mandata del servocomando */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_UUTPRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P03_MUX]);
		
		/* Misura pressione di ritorno dal servocomando */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_UUTRETURNNMR, ATTR_CTRL_VAL, MeasuresAve[P08_MUX]);
		
		/* Misura pressione del capillare SYS1 esterno */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CAP1PRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P06_MUX]);
		
		/* Misura pressione del capillare SYS1 interno */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CAP2PRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P07_MUX]);

		/* Misura pressione del capillare SYS2 interno */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CAP3PRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P05_MUX]);

		/* Misura pressione del capillare SYS1 esterno */
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CAP4PRESSURENMR, ATTR_CTRL_VAL, MeasuresAve[P04_MUX]);
	}
	 
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misure di posizione lineare										*/
/* ------------------------------------------------------------------------------------ */

double init_condition_x[2];
double init_condition_y[2];

double final_condition_x[2];
double final_condition_y[2];

double initMovingAveStateX[4]={0.0,0.0,0.0,0.0};  
double initMovingAveStateY[4]={0.0,0.0,0.0,0.0};  

double speed_buffer[500]; 

double lastCsasSp=0;
double lastInputSp=0; 

void DisplacementManager(void)
{
	int Visible=0;  
	int i=0;
	int option=0;
	
	double buffer2D[500][4];
	double bufferLvdt1[500][2]; 
	double bufferLvdt2[500][2];
	double bufferInput[500][2]; 
	
	double SpeedMeasure[500];
	double speed_ave=0;
	
	double maxVal;
	double minVal;
	double newMaxVal;
	double newMinVal;
	int newMaxIdx;
	int newMinIdx;
	
	double StrainFileData[500][4];    

	double deltaPosition;
	
	char nomefile[400];					// nome file per salvataggio dei dati numerici
	
	/***************************
	FIR window Hamming ordine 30
	fs = 250 Hz
	fc = 20 Hz
	****************************/

	double InitYdotFilterState[50];
	int lenFIR = 31;		// lunghezza del filtro
	double coefFIR[31]={
		+1.6089917736446394e-003, 
		+1.3967476066001920e-003, 
		+7.2678890880823030e-004, 
		-1.1034499572868965e-003, 
		-4.5837560009782769e-003, 
		-9.3522646843094658e-003, 
		-1.3774204070959136e-002, 
		-1.5029019217447530e-002, 
		-9.8103433758477531e-003, 
		+4.5200397929181695e-003, 
		+2.8713635057743148e-002, 
		+6.0834200967646537e-002, 
		+9.6257573844515276e-002, 
		+1.2858953033809514e-001, 
		+1.5128161173390686e-001, 
		+1.5944783456590156e-001, 
		+1.5128161173390686e-001, 
		+1.2858953033809514e-001, 
		+9.6257573844515276e-002, 
		+6.0834200967646537e-002, 
		+2.8713635057743148e-002, 
		+4.5200397929181695e-003, 
		-9.8103433758477531e-003, 
		-1.5029019217447530e-002, 
		-1.3774204070959136e-002, 
		-9.3522646843094658e-003, 
		-4.5837560009782769e-003, 
		-1.1034499572868965e-003, 
		+7.2678890880823030e-004, 
		+1.3967476066001920e-003,
		+1.6089917736446394e-003, 
	};	// coefficienti del filtro FIR
	
	/* Media mobile di 4 elementi */
	double coefMovingAve[4]={0.25,0.25,0.25,0.25};         
	
	/****************************************************************/
	/* Visualizzazione misure su pannello calibrazione elettrica	*/
	/****************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (elecalibrationpanel, ATTR_VISIBLE, &Visible);
	
	if ((elecalibrationpanel>0)&&(Visible==1))
	{
		/* Misura posizione LVDT del CSAS1 */
		SetCtrlAttribute(elecalibrationpanel, ELECALPNL_LVDT1OUTNMR, ATTR_CTRL_VAL, MeasuresAve[LVDT1_MUX]);
		
		/* Misura posizione LVDT del CSAS2 */
		SetCtrlAttribute(elecalibrationpanel, ELECALPNL_LVDT2OUTNMR, ATTR_CTRL_VAL, MeasuresAve[LVDT2_MUX]);
	}
	
	/****************************************************************/
	/* Visualizzazione misure su pannello LVDT						*/
	/****************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (lvdtpanel, ATTR_VISIBLE, &Visible);
	
	if ((lvdtpanel>0)&&(Visible==1)&&(PlotCsasEnable==YES))
	{
		for (i=0;i<number_of_samples;i++)
		{
			bufferLvdt1[i][0] = -Measures[CSAS_MUX][i];
			bufferLvdt1[i][1] = Measures[LVDT1_MUX][i];
			
			bufferLvdt2[i][0] = Measures[CSAS_MUX][i];
			bufferLvdt2[i][1] = Measures[LVDT2_MUX][i];
			
			buffer2D[i][0] = -Measures[CSAS_MUX][i];
			buffer2D[i][1] = Measures[LVDT1_MUX][i];
			buffer2D[i][2] = Measures[CSAS_MUX][i];
			buffer2D[i][3] = Measures[LVDT2_MUX][i];
		}
		
		PlotStripChart (lvdtpanel, LVDTOUTPNL_POSITION1STR, &bufferLvdt1[0][0], 2*number_of_samples, 0, 0, VAL_DOUBLE);
		PlotStripChart (lvdtpanel, LVDTOUTPNL_POSITION2STR, &bufferLvdt2[0][0], 2*number_of_samples, 0, 0, VAL_DOUBLE);
		
		// indicatore numerico LVDT1
		SetCtrlAttribute(lvdtpanel, LVDTOUTPNL_LVDT1AVENMR, ATTR_CTRL_VAL, MeasuresAve[LVDT1_MUX]);    
		// indicatore numerico LVDT2
		SetCtrlAttribute(lvdtpanel, LVDTOUTPNL_LVDT2AVENMR, ATTR_CTRL_VAL, MeasuresAve[LVDT2_MUX]);    
		
		/* Salvataggio su file della forma d'onda */
		if ((int)save_plotcsas==1)
		{
			sprintf(nomefile,"%s\\displacement_csas.dat",save_plotcsas_dir);   
			
			ArrayToFile (nomefile, buffer2D, VAL_DOUBLE, 4*number_of_samples, 2, VAL_DATA_MULTIPLEXED, VAL_GROUPS_AS_COLUMNS,
						 VAL_CONST_WIDTH, 30, VAL_ASCII, VAL_APPEND);
		}
	}

	/********************************************************************/
	/* Visualizzazione misure su pannello Plot X per visualizzazione	*/
	/* caratteristiche martinetto di ingresso							*/
	/********************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (plotxpanel, ATTR_VISIBLE, &Visible);
	
	/************************************************************/
	
	if ((plotxpanel>0)&&(Visible==1)&&(PlotxEnable==YES))
	{
		// plot strip chart posizione      
		for (i=0;i<number_of_samples;i++)
		{
			bufferInput[i][0] = Measures[INPUT_MUX][i];
			bufferInput[i][1] = Measures[L1_MUX][i];
		}
	
		PlotStripChart (plotxpanel, PLOTXPNL_POSITIONSTR, &bufferInput[0][0], 2*number_of_samples, 0, 0, VAL_DOUBLE);
		
		// indicatore numerico
		SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONAVENMR, ATTR_CTRL_VAL, MeasuresAve[L1_MUX]);
		
		// indicatore di massimo&minimo posizione
		MaxMin1D (Measures[L1_MUX], number_of_samples, &newMaxVal, &newMaxIdx, &newMinVal, &newMinIdx);

		// Gestione visualizzazione massimo&minimo posizione
		if (newMaxVal>previous_max_x)
		{
			SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONMAXNMR, ATTR_CTRL_VAL, newMaxVal);
			previous_max_x=newMaxVal;
		}
		
		if (newMinVal<previous_min_x)
		{
			SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONMINNMR, ATTR_CTRL_VAL, newMinVal);
			previous_min_x=newMinVal;
		}
		
		// previsioni condizioni finali
		deltaPosition = Measures[L1_MUX][number_of_samples-1] - Measures[L1_MUX][number_of_samples-2];
		final_condition_x[0]=Measures[L1_MUX][number_of_samples-1] + deltaPosition;
		final_condition_x[1]=Measures[L1_MUX][number_of_samples-1] + 2*deltaPosition;
			
		// calcolo velocita' come derivata della posizione
		// Difference (Measures[L1_MUX], number_of_samples, DT, Measures[L1_MUX][1], Measures[L1_MUX][number_of_samples-1], SpeedMeasure);
		DifferenceEx (Measures[L1_MUX], number_of_samples, DT, init_condition_x, 2, final_condition_x, 2, 1, SpeedMeasure);
		
		// salva condizioni iniziali prossimo buffer
		init_condition_x[0]=Measures[L1_MUX][number_of_samples-2];
		init_condition_x[1]=Measures[L1_MUX][number_of_samples-1];
		
		// calcolo velocita' media sul singolo buffer di acquisizione: v=spazio/tempo
		Mean (SpeedMeasure, number_of_samples, &speed_ave);
		
		// plot strip chart velocita'
		PlotStripChart (plotxpanel, PLOTXPNL_SPEEDSTR, SpeedMeasure, number_of_samples, 0, 0, VAL_DOUBLE);
		SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDAVENMR, ATTR_CTRL_VAL, speed_ave);
		
		// indicatore di massimo&minimo posizione
		MaxMin1D (SpeedMeasure, number_of_samples, &newMaxVal, &newMaxIdx, &newMinVal, &newMinIdx);

		// Gestione visualizzazione massimo&minimo posizione
		GetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDMAXNMR, ATTR_CTRL_VAL, &maxVal);  
		GetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDMINNMR, ATTR_CTRL_VAL, &minVal);  
		
		if (newMaxVal>maxVal)
			SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDMAXNMR, ATTR_CTRL_VAL, newMaxVal);
		
		if (newMinVal<minVal)
			SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDMINNMR, ATTR_CTRL_VAL, newMinVal);
		
		/* Salvataggio su file della forma d'onda */
		if (save_plotx==1)
		{
			sprintf(nomefile,"%s\\displacement_input.dat",save_plotx_dir);  
		
			ArrayToFile (nomefile, Measures[L1_MUX], VAL_DOUBLE, number_of_samples, 1, VAL_GROUPS_TOGETHER, VAL_GROUPS_AS_COLUMNS,
					 VAL_SEP_BY_TAB, 10, VAL_ASCII, VAL_APPEND);
		}
	}	
		

	/********************************************************************/
	/* Visualizzazione misure su pannello Plot Y per visualizzazione	*/
	/* caratteristiche output main piston								*/
	/********************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (plotypanel, ATTR_VISIBLE, &Visible);
	
	if ((plotypanel>0)&&(Visible==1)&&(PlotyEnable==YES))
	{
		// plot strip chart posizione
		PlotStripChart (plotypanel, PLOTYPNL_POSITIONSTR, Measures[L2_MUX], number_of_samples, 0, 0, VAL_DOUBLE);
		SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONAVENMR, ATTR_CTRL_VAL, MeasuresAve[L2_MUX]);
		
		// indicatore di massimo&minimo posizione
		MaxMin1D (Measures[L2_MUX], number_of_samples, &newMaxVal, &newMaxIdx, &newMinVal, &newMinIdx);

		// Gestione visualizzazione massimo&minimo posizione
		GetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONMAXNMR, ATTR_CTRL_VAL, &maxVal);  
		GetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONMINNMR, ATTR_CTRL_VAL, &minVal);  
		
		if (newMaxVal>previous_max_y)
		{
			SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONMAXNMR, ATTR_CTRL_VAL, newMaxVal);
			previous_max_y=newMaxVal;
		}
		
		if (newMinVal<previous_min_y)
		{
			SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONMINNMR, ATTR_CTRL_VAL, newMinVal);
			previous_min_y=newMinVal;
		}
		
		// previsioni condizioni finali
		deltaPosition = Measures[L2_MUX][number_of_samples-1] - Measures[L2_MUX][number_of_samples-2];
		final_condition_y[0]=Measures[L2_MUX][number_of_samples-1] + deltaPosition;
		final_condition_y[1]=Measures[L2_MUX][number_of_samples-1] + 2*deltaPosition;
			
		// calcolo velocita' come derivata della posizione
		// Difference (Measures[L1_MUX], number_of_samples, DT, Measures[L1_MUX][1], Measures[L1_MUX][number_of_samples-1], SpeedMeasure);
		DifferenceEx (Measures[L2_MUX], number_of_samples, DT, init_condition_y, 2, final_condition_y, 2, 1, SpeedMeasure);
		
		// salva condizioni iniziali prossimo buffer
		init_condition_y[0]=Measures[L2_MUX][number_of_samples-2];
		init_condition_y[1]=Measures[L2_MUX][number_of_samples-1];
		
		// velocita' media
		//Mean (SpeedMeasure, number_of_samples, &speed_ave);
		
		// plot grafico velocita'
		Shift (speed_buffer, 500, -number_of_samples, speed_buffer);
		
		for (i=0 ; i<number_of_samples ; i++)
			speed_buffer[500-number_of_samples+i] = SpeedMeasure[i];
		
		DeleteGraphPlot (plotypanel, PLOTYPNL_SPEEDGRPH, -1, VAL_IMMEDIATE_DRAW);
		
		PlotY (plotypanel, PLOTYPNL_SPEEDGRPH, speed_buffer, 500, VAL_DOUBLE, VAL_FAT_LINE, VAL_NO_POINT, VAL_SOLID, 1,
			   VAL_BLUE);
	
		// indicatore di massimo&minimo velocita'
		MaxMin1D (SpeedMeasure, number_of_samples, &newMaxVal, &newMaxIdx, &newMinVal, &newMinIdx);

		// Gestione visualizzazione massimo&minimo velocita'
		GetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDMAXNMR, ATTR_CTRL_VAL, &maxVal);  
		GetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDMINNMR, ATTR_CTRL_VAL, &minVal);  
		
		if (newMaxVal>maxVal)
			SetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDMAXNMR, ATTR_CTRL_VAL, newMaxVal);
		
		if (newMinVal<minVal)
			SetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDMINNMR, ATTR_CTRL_VAL, newMinVal);
		
		/* Salvataggio su file della forma d'onda */
		if (save_ploty==1)
		{
			sprintf(nomefile,"%s\\displacement_output.dat",save_ploty_dir);  
		
			ArrayToFile (nomefile, Measures[L2_MUX], VAL_DOUBLE, number_of_samples, 1, VAL_GROUPS_TOGETHER, VAL_GROUPS_AS_COLUMNS,
					 VAL_SEP_BY_TAB, 10, VAL_ASCII, VAL_APPEND);
		}
	}	
	
	
	/********************************************************************/
	/* Visualizzazione misure su pannello Strain per visualizzazione	*/
	/* caratteristiche output main piston								*/
	/********************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (strainpanel, ATTR_VISIBLE, &Visible);
	
	if ((strainpanel>0)&&(Visible==1)&&(PlotEffortEnable==YES))
	{
		// plot strip chart posizione
		PlotStripChart (strainpanel, STRAINPNL_POSITIONSTR, Measures[L2_MUX], number_of_samples, 0, 0, VAL_DOUBLE);
		SetCtrlAttribute(strainpanel, STRAINPNL_POSITIONAVENMR, ATTR_CTRL_VAL, MeasuresAve[L2_MUX]);
		
		// calcolo velocita' come derivata della posizione
		Difference (Measures[L2_MUX], number_of_samples, DT, Measures[L2_MUX][1], Measures[L2_MUX][number_of_samples-1], SpeedMeasure);
		//FIRFiltering (SpeedMeasure, number_of_samples, coefFIR, InitYdotFilterState, lenFIR, SpeedMeasure);    
		//Subset1D (SpeedMeasure, number_of_samples, (number_of_samples-31), 31, InitYdotFilterState); 
		
		// velocita' media
		speed_ave = (MeasuresAve[L2_MUX] - previousPosition)/(DT*number_of_samples);
		previousPosition = MeasuresAve[L2_MUX];
		
		// plot strip chart velocita'
		PlotStripChart (strainpanel, STRAINPNL_SPEEDSTR, SpeedMeasure, number_of_samples, 0, 0, VAL_DOUBLE);
		SetCtrlAttribute(strainpanel, STRAINPNL_SPEEDAVENMR, ATTR_CTRL_VAL, speed_ave);
		
		/* Salvataggio su file della forma d'onda */
		if (save_plotstrain==1)
		{
			for (i=0;i<number_of_samples;i++)
			{
				StrainFileData[i][0] = Measures[L2_MUX][i];
				StrainFileData[i][1] = SpeedMeasure[i];
				StrainFileData[i][2] = Measures[N1_MUX][i];
				StrainFileData[i][3] = Measures[N2_MUX][i];
			}
			
			sprintf(nomefile,"%s\\strain_data.dat",save_plotstrain_dir);  
		
			ArrayToFile (nomefile, StrainFileData, VAL_DOUBLE, 4*number_of_samples, 4, VAL_DATA_MULTIPLEXED, VAL_GROUPS_AS_COLUMNS,
						 VAL_CONST_WIDTH, 30, VAL_ASCII, VAL_APPEND);
		}
	}	

	
	/************************************************************************/
	/* Visualizzazione del numero cicli su pannello calibrazione meccanica	*/
	/************************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (mechcalibrationpanel, ATTR_VISIBLE, &Visible);
	
	if ((mechcalibrationpanel>0)&&(Visible==1))
	{
		GetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_CTRL_VAL, &option); 
		
		/* Conta numero parziale di cicli */
		if (option==PILOT)
		{
			for (i=0;i<number_of_samples;i++)
			{
				if ((i==0)&&(Measures[INPUT_MUX][0]>=0)&&(lastInputSp<0))
				{
					number_of_cycle++;
					SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CYCLENMR, ATTR_CTRL_VAL, number_of_cycle);
				}
				else if ((Measures[INPUT_MUX][i]>=0)&&(Measures[INPUT_MUX][i-1]<0))
				{
					number_of_cycle++;
					SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CYCLENMR, ATTR_CTRL_VAL, number_of_cycle);
				}		
			}
		}
		else if (option==CSAS)
		{
			for (i=0;i<number_of_samples;i++)
			{
				if ((i==0)&&(Measures[CSAS_MUX][0]>=0)&&(lastCsasSp<0))
				{
					number_of_cycle++;
					SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CYCLENMR, ATTR_CTRL_VAL, number_of_cycle);
				}
				if ((Measures[CSAS_MUX][i]>=0)&&(Measures[CSAS_MUX][i-1]<0))
				{
					number_of_cycle++;
					SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_CYCLENMR, ATTR_CTRL_VAL, number_of_cycle);
				}
			}
		}
	
		lastCsasSp=Measures[CSAS_MUX][number_of_samples-1];
		lastInputSp=Measures[INPUT_MUX][number_of_samples-1]; 
	}
	
	return;	
}



/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misure relative ad un singolo ciclo armonico						*/
/* ------------------------------------------------------------------------------------ */

static int sample_index=0;		

void CharacteristicManager(void)
{
	int Visible=0; 
	double signal_offset=0.0;
	double signal_frequency=0.0;
	int waveform_type=0;
	int ctrl_type;
	int csas_option=0;
	
	double xdot[5000];
	double ydot[5000];
	
	double time[5000];
	double errore[5000];     
	int decFactor = 2;
	
	int i=0;
	int samples_per_period=0;
	
	double InitXFilterState[50];
	double InitYFilterState[50];
	
	double media_x=0, media_y=0;
	
	/* Filtraggio digitale FIR */
	int lenFIR = 31;		// lunghezza del filtro
	double coefFIR[31]={
		+1.6089917736446394e-003, 
		+1.3967476066001920e-003, 
		+7.2678890880823030e-004, 
		-1.1034499572868965e-003, 
		-4.5837560009782769e-003, 
		-9.3522646843094658e-003, 
		-1.3774204070959136e-002, 
		-1.5029019217447530e-002, 
		-9.8103433758477531e-003, 
		+4.5200397929181695e-003, 
		+2.8713635057743148e-002, 
		+6.0834200967646537e-002, 
		+9.6257573844515276e-002, 
		+1.2858953033809514e-001, 
		+1.5128161173390686e-001, 
		+1.5944783456590156e-001, 
		+1.5128161173390686e-001, 
		+1.2858953033809514e-001, 
		+9.6257573844515276e-002,
		+6.0834200967646537e-002, 
		+2.8713635057743148e-002, 
		+4.5200397929181695e-003, 
		-9.8103433758477531e-003, 
		-1.5029019217447530e-002, 
		-1.3774204070959136e-002, 
		-9.3522646843094658e-003, 
		-4.5837560009782769e-003, 
		-1.1034499572868965e-003, 
		+7.2678890880823030e-004, 
		+1.3967476066001920e-003, 
		+1.6089917736446394e-003, 
	};	// coefficienti del filtro FIR
	
	/* Verifica apertura pannello */
	GetPanelAttribute (plotxypanel, ATTR_VISIBLE, &Visible);
	
	if ((plotxypanel>0)&&(Visible==1)&&(PlotxyEnable==YES))
	{
		// rilevo il centro di oscillazione del controllo di posizione
		GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &signal_offset);
		// rilevo la frequenza di oscillazione
		GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &signal_frequency);
		// rilevo la frequenza di oscillazione
		GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &waveform_type);
		
		// Leggo tipo di input da analizzare
		GetCtrlAttribute (plotxypanel, PLOTXYPNL_CTRLCHOISE, ATTR_CTRL_VAL, &ctrl_type);
		
		// leggo quali sono gli CSAS attivati
		GetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_CTRL_VAL, &csas_option); 
		
		
		if ((waveform_type==1)||(signal_frequency<=0))
			goto Error;
		
		// numero di campioni acquisiti per descrivere un periodo di oscillazione
		// il valore viene arrotondato per eccesso dalla funzione "ceil"
		samples_per_period = (int)ceil((PID_RATE*(1.0/signal_frequency)));
		
		if (ctrl_type==PILOT)
		{
			for (i=0; (i+sample_index<samples_per_period) && (i<number_of_samples) ; i++)
			{
				x_signal_wave[i+sample_index] = -1*Measures[L1_MUX][i];
				y_signal_wave[i+sample_index] = Measures[L2_MUX][i];
				
				time[i+sample_index] = (i+sample_index)*DT;
			}
		}
		else
		{
			for (i=0; (i+sample_index<samples_per_period) && (i<number_of_samples) ; i++)
			{
				// gestione del rapporto di leva tra CSAS e output sui diversi servocomandi
				if (actuatorPN==PN_3101)
				{
					// gestione Tail
					x_signal_wave[i+sample_index] = -2*Measures[CSAS_MUX][i]; 		
				}
				else
				{
					if (csas_option!=BOTH_SYS) 
					{
						// gestione del connettore invertito sul Collective
						if (actuatorPN==PN_3104)
							x_signal_wave[i+sample_index] = Measures[CSAS_MUX][i]/2; 	
						else	
							x_signal_wave[i+sample_index] = -1*Measures[CSAS_MUX][i]/2;
					}
					else
					{
						// gestione del connettore invertito sul Collective 
						if (actuatorPN==PN_3104)
							x_signal_wave[i+sample_index] = Measures[CSAS_MUX][i]; 	
						else	
							x_signal_wave[i+sample_index] = -1*Measures[CSAS_MUX][i];	
					}
				}
				
				y_signal_wave[i+sample_index] = Measures[L2_MUX][i];
				
				time[i+sample_index] = (i+sample_index)*DT;
			}	
		}

		// incremento indice di riempimento dell'array contenete la forma d'onda
		sample_index += i;
		
		SetCtrlAttribute (plotxypanel, PLOTXYPNL_LINEARITYNMR, ATTR_CTRL_VAL, (double)samples_per_period);  
		SetCtrlAttribute (plotxypanel, PLOTXYPNL_HYSTERESISNMR, ATTR_CTRL_VAL, (double)sample_index);

		if (sample_index>=samples_per_period)
		{
			Subset1D (x_signal_wave, samples_per_period, (samples_per_period-31), 31, InitXFilterState);
			Subset1D (y_signal_wave, samples_per_period, (samples_per_period-31), 31, InitYFilterState);    
			
			/****************************************************************************************/
			/* Grafico di caratterizzazione isteresi/linearita' 									*/
			/****************************************************************************************/
			
			// filtraggio digitale: media mobile
			if (ctrl_type==PILOT) 
				FIRFiltering (x_signal_wave, samples_per_period, coefFIR, InitXFilterState, lenFIR, x_signal_hyst);
				
			else
				Copy1D (x_signal_wave, samples_per_period, x_signal_hyst);
			
			FIRFiltering (y_signal_wave, samples_per_period, coefFIR, InitYFilterState, lenFIR, y_signal_hyst);
			
			int order = 30; 

			double z_signal_hyst[10000];
			double w_signal_hyst[10000];
			
			if (((ctrl_type==PILOT)||(ctrl_type==CSAS))&&(signal_frequency==0.1)){
	            //se sono in signal freq 0.1 and PILOT || CSAS allora mm su x_signal_wave e y_signal_wave
	            mobile_mean(z_signal_hyst,x_signal_hyst,order,samples_per_period, 0);
	            mobile_mean(w_signal_hyst,y_signal_hyst,order,samples_per_period, 0);
            }
			else
			{
				// inserimento media mobile (ad 1/10 della finestra) su frequanze != 0.1
				mobile_mean(z_signal_hyst,x_signal_hyst,order/10,samples_per_period, 0);
	            mobile_mean(w_signal_hyst,y_signal_hyst,order/10,samples_per_period, 0);
			}

			// fix per forzare la chiusura del grafico
			z_signal_hyst[samples_per_period-1] = z_signal_hyst[0];
			w_signal_hyst[samples_per_period-1] = w_signal_hyst[0];

			Spline (time, z_signal_hyst, samples_per_period, 0.0, 0.0, xdot);
			Spline (time, w_signal_hyst, samples_per_period, 0.0, 0.0, ydot);
			
			SpInterp (time, z_signal_hyst, xdot, samples_per_period, 0.0, z_signal_hyst);
			SpInterp (time, w_signal_hyst, ydot, samples_per_period, 0.0, w_signal_hyst);

			displacement_char = PlotXY (plotxypanel, PLOTXYPNL_POSITIONGRP, z_signal_hyst, w_signal_hyst, samples_per_period, VAL_DOUBLE,
										VAL_DOUBLE, VAL_FAT_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_RED);
			
			/****************************************************************************************/
			/* Grafico di caratterizzazione risposta in frequenza 									*/
			/****************************************************************************************/
			
			// interpolazione
			Spline (time, x_signal_wave, samples_per_period, 0.0, 0.0, xdot);
			Spline (time, y_signal_wave, samples_per_period, 0.0, 0.0, ydot); 
			
			SpInterp (time, x_signal_wave, xdot, samples_per_period, 0.0, x_signal_wave);
			SpInterp (time, y_signal_wave, ydot, samples_per_period, 0.0, y_signal_wave);
			
			// Valor medio dei segnali acquisiti
			Mean (x_signal_wave, samples_per_period, &media_x);
			Mean (y_signal_wave, samples_per_period, &media_y);
			
			// Centratura forme d'onda attorno al valor medio
			for (i=0;i<samples_per_period;i++)
			{
				x_signal_wave[i] -= media_x;	
				y_signal_wave[i] -= media_y;
			}
			
			x_signal_char = PlotXY (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, time, x_signal_wave, samples_per_period, VAL_DOUBLE,
										VAL_DOUBLE, VAL_THIN_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_BLUE);
			
			y_signal_char = PlotXY (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, time, y_signal_wave, samples_per_period, VAL_DOUBLE,
										VAL_DOUBLE, VAL_THIN_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_RED);


			// inizializzo il posizionamento dei puntatori
			SetGraphCursorIndex (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, 1, x_signal_char, 0);
			SetGraphCursorIndex (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, 2, y_signal_char, 10);
			
			// azzeramento contatore sample
			sample_index=0;

			PlotxyEnable=NO;
			//free(z_signal_hyst);
			//free(w_signal_hyst);
		}
	}

Error:
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misure di sforzo													*/
/* ------------------------------------------------------------------------------------ */
double effort_buffer[500]; 

void StrainManager(void)
{
	int Visible=0,i=0;  

	/****************************************************************/
	/* Visualizzazione misure su pannello delle forze				*/
	/****************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (strainpanel, ATTR_VISIBLE, &Visible);
	
	if ((strainpanel>0)&&(Visible==1)&&(PlotEffortEnable==YES))
	{
		// Plot misura di sforzo martinetto di input
		Shift (effort_buffer, 500, -number_of_samples, effort_buffer);
		
		for (i=0 ; i<number_of_samples ; i++)
			effort_buffer[500-number_of_samples+i] = Measures[N1_MUX][i];
		
		DeleteGraphPlot (strainpanel, STRAINPNL_INPUTEFFORTGRPH, -1, VAL_IMMEDIATE_DRAW);
		
		PlotY (strainpanel, STRAINPNL_INPUTEFFORTGRPH, effort_buffer, 500, VAL_DOUBLE, VAL_FAT_LINE, VAL_NO_POINT, VAL_SOLID, 1,
			   VAL_BLUE);
		
		// Plot misura di sforzo martinetto di output
		PlotStripChart (strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR, Measures[N2_MUX], number_of_samples, 0, 0, VAL_DOUBLE);
		SetCtrlAttribute(strainpanel, STRAINPNL_STRENGTHAVENMR, ATTR_CTRL_VAL, MeasuresAve[N2_MUX]);
	}
	
	/****************************************************************/
	/* Controllo battuta martinetto di carico su pistone servo		*/
	/****************************************************************/	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (manualpanel, ATTR_VISIBLE, &Visible);
	
	if ((manualpanel>0)&&(Visible==1))			
	{
		// Se martinetto di carico e' alimentato ma l'engage no
		if (( SolenoidValve[EV09B_STATUS]==CLOSE )&&(SolenoidValve[EV08_STATUS]==OPEN))
		{
			// se il carico in compressione eccede i 3000N
			if (MeasuresAve[N2_MUX]>=3000.0)
			{
				// post deferred per l'esecuzione del loop back
				PostDeferredCall (StopLoadRam, 0);

				StopLoadCB (manualpanel, MANPANEL_STOPLOADBTN ,EVENT_COMMIT, 0, 0, 0);
			}
		}
	}
	
Error:
	return;
}

/* ------------------------------------------------------------------------------------ */
/* Routine di stop del martinetto di carico in controllo ad anello aperto				*/
/* ATTENZIONE: da utilizzare in PostDeferred altrimenti non viene eseguito il loop back */
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK StopLoadRam (void *callbackData)
{
	StopLoadCB (manualpanel, MANPANEL_STOPLOADBTN ,EVENT_COMMIT, 0, 0, 0);   
	
	return;	
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misure di tensione e corrente alimentazione solenoidi UUT		*/
/* ------------------------------------------------------------------------------------ */

void SolenoidManager(void)
{
	int Visible=0;  
	
	double sys1_current_ave=0.0;    
	double sys1_resistence=0.0;

	double sys2_current_ave=0.0;    
	double sys2_resistence=0.0;

	// Calcolo valori medi corrente assorbita
	sys1_current_ave = MeasuresAve[SYS1_S_SHUNT];	// corrente solenoide SYS1  
	sys2_current_ave = MeasuresAve[SYS2_S_SHUNT];	// corrente solenoide SYS2  
		
	// calcolo resistenze degli avvolgimenti dei solenoidi
	sys1_resistence = MeasuresAve[PSU3_MUX] / sys1_current_ave;
	sys2_resistence = MeasuresAve[PSU3_MUX] / sys2_current_ave;
	
	if(sys1_current_ave<0.05)
		sys1_resistence = 0.0;
	
	if(sys2_current_ave<0.05)   
		sys2_resistence = 0.0;	
	
	
	/* Verifica apertura pannello */
	GetPanelAttribute (solenoidpanel, ATTR_VISIBLE, &Visible);

	if ((solenoidpanel>0)&&(Visible==1))
	{
		// Plot misure di tensione, corrente e resistenza Sistema1
		SetCtrlAttribute(solenoidpanel, SOLENPNL_VSOL1NMR, ATTR_CTRL_VAL, MeasuresAve[PSU3_MUX]);
		SetCtrlAttribute(solenoidpanel, SOLENPNL_VSOL2NMR, ATTR_CTRL_VAL, MeasuresAve[PSU3_MUX]);
		
		SetCtrlAttribute(solenoidpanel, SOLENPNL_ISOL1NMR, ATTR_CTRL_VAL, sys1_current_ave);
		SetCtrlAttribute(solenoidpanel, SOLENPNL_ISOL2NMR, ATTR_CTRL_VAL, sys2_current_ave);
		
		SetCtrlAttribute(solenoidpanel, SOLENPNL_RESISTANCE1NMR, ATTR_CTRL_VAL, sys1_resistence);
		SetCtrlAttribute(solenoidpanel, SOLENPNL_RESISTANCE2NMR, ATTR_CTRL_VAL, sys2_resistence);
	}

	return;	
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misura di corrente di bias servovalvole UUT						*/
/* ------------------------------------------------------------------------------------ */

void BiasManager(void)
{
	int Visible=0;  
	
	double sys1_current_ave=0.0;    
	double sys2_current_ave=0.0;    

	// Calcolo corrente media assorbita
	sys1_current_ave = MeasuresAve[CV_SYS1_SHUNT];	// corrente servovalvola SYS1
	sys2_current_ave = MeasuresAve[CV_SYS2_SHUNT];	// corrente servovalvola SYS2

	/* Verifica apertura pannello */
	GetPanelAttribute (biaspanel, ATTR_VISIBLE, &Visible);

	if ((biaspanel>0)&&(Visible==1))
	{
		// Plot misure di tensione, corrente e resistenza Sistema1
		SetCtrlAttribute(biaspanel, BIASPNL_SERVO1AVENMR, ATTR_CTRL_VAL, 2*1000*sys1_current_ave);
		SetCtrlAttribute(biaspanel, BIASPNL_SERVO2AVENMR, ATTR_CTRL_VAL, 2*1000*sys2_current_ave);
	}

	return;	
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misura di temperatura dell'olio sul ritorno del servocomando		*/
/* ------------------------------------------------------------------------------------ */

void TemperatureManager(void)
{
	int Visible=0;  

	/* Verifica apertura pannello */
	GetPanelAttribute (temperaturepanel, ATTR_VISIBLE, &Visible);

	if ((temperaturepanel>0)&&(Visible==1))
	{
		// Plot misure di tensione, corrente e resistenza Sistema1
		SetCtrlAttribute(temperaturepanel, TEMPPNL_TEMPERATURENMR, ATTR_CTRL_VAL, MeasuresAve[K1_MUX]);
	}

	return;	
}



/* ------------------------------------------------------------------------------------ */
/* Routine di gestione misura di temperatura dell'olio sul ritorno del servocomando		*/
/* ------------------------------------------------------------------------------------ */

void FlowManager(void)
{
	int Visible=0;  

	/* Verifica apertura pannello */
	GetPanelAttribute (mechcalibrationpanel, ATTR_VISIBLE, &Visible);

	if ((mechcalibrationpanel>0)&&(Visible==1))
	{
		// Plot misure di tensione, corrente e resistenza Sistema1
		SetCtrlAttribute(mechcalibrationpanel, MECHCALPNL_RATBFLOWNMR, ATTR_CTRL_VAL, MeasuresAve[F1_MUX]);
	}

	return;	
}

// media mobile calcolata sui valori precedenti
void mobile_mean(double m[], double signal[],int order,int SIGNAL_SMPL, int print){

	int count=0, l=0, k;
	double buff=0;

	if (print) {
		printf("signal: ");
		for(int u=0; u<SIGNAL_SMPL; u++) {
			printf("%f, ", signal[u]);
		}
		printf("\n");
	}

	while (count<SIGNAL_SMPL){
		if (print)
			printf("(");
		while (l<=count && l<order){
			if (print)
				printf("%f, ", signal[count-l]);
			buff=signal[count-l]+buff;
			l++;
		}
		for (k=SIGNAL_SMPL-1; k>SIGNAL_SMPL-order+count; k--) {
			if (print)
				printf("*%f, ", signal[k]);
			buff=signal[k]+buff;
			if (l != count)
				l++;
		}
		if (print)
			printf("[l=%d] ", l);
		m[count]=buff/l;
		if (print)
			printf(") media = %f (count=%d)\n", m[count], count);
		count = count+1;
		buff=0;
		l=0;
	}
}

// media mobile calcolata sui valori sucessivi
void mobile_mean2(double m[], double signal[],int order,int size, int print){
	int n = 0, i, k, j;
	double somma = 0.0;
	if (print) {
		printf("signal: ");
		for(int u=0; u<size; u++) {
			printf("%f, ", signal[u]);
		}
		printf("\n");
	}
	for (i=0; i<size;i++)
	{
		if (print)
			printf("(");
		// a partire dalla posizione i sommo gli elementi fino ad order + i
		for(k=i;k<order+i && k<size;k++)
		{
			somma=somma+signal[k];
			if (i+order > size && n == 0) {
				// inizializzo n con il numero di elementi mancanti
				n = order - (size - k);
			}
			if (print)
				printf("%f, ", signal[k]);
		}
		// sommo gli elementi mancanti a partire dall'inizio dell'array
		for(j=0; j<n; j++)
		{
			if (print)
				printf("*%f, ", signal[j]);
			somma=somma+signal[j];
		}
		m[i] = somma/order;
		if (print)
			printf(") media = %f (k=%d)\n", m[i], i);
		somma = 0.0;
		n = 0;
	}
}
