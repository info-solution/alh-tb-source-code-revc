#include <windows.h>
#include <ansi_c.h>
#include <rtutil.h>
#include <userint.h>
#include <cvinetv.h>
#include <utility.h>  
#include "HW_Description.h"
#include "error_table.h"


/* Routine di verifica errori HW scheda DAQmx */
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else                

;

 
/* ------------------------------------------------------------------------------------ */
/* Handler delle connessioni alle variabili network										*/
/*																						*/
/* N.B. - per scelta le variabili network risiederanno unicamente su RTtarget			*/
/* ------------------------------------------------------------------------------------ */

// SUBSCRIBER
static CNVSubscriber	sHandQuitProgram;			// comando di chiusura programma di testing
static CNVSubscriber	sHandEmergency;				// comando di arresto di emergenza

static CNVSubscriber	sHandEnCsas1;				// comando di abilitazione regolazione del CSAS sul Sistema 1
static CNVSubscriber	sHandEnCsas2;				// comando di abilitazione regolazione del CSAS sul Sistema 2
static CNVSubscriber	sHandEnInput;				// comando di abilitazione regolazione del martinetto sulla leva di Input
static CNVSubscriber	sHandEnLoad;				// comando di abilitazione regolazione del carico sul main piston
static CNVSubscriber	sHandEnUutVoltage;			// comando di abilitazione regolazione della tensione solenoid valve UUT
static CNVSubscriber	sHandEnUutPressure;			// comando di abilitazione regolazione della pressione di mandata verso UUT
													// 0-controllo inattivo 1-controllo attivo

static CNVSubscriber	sHandEnNullBias;			// comando di abilitazione misura bias servovalvole a bordo attuatore
static CNVSubscriber	sHandEnEVcurrent;			// comando di abilitazione misura assorbimento solenoid valve

static CNVSubscriber	sHandEV01;					// comandi di controllo elettrovalvole banco
static CNVSubscriber	sHandEV02;
static CNVSubscriber	sHandEV03;
static CNVSubscriber	sHandEV04A;
static CNVSubscriber	sHandEV04B;
static CNVSubscriber	sHandEV05;
static CNVSubscriber	sHandEV06;
static CNVSubscriber	sHandEV07;
static CNVSubscriber	sHandEV08;
static CNVSubscriber	sHandEV09A;
static CNVSubscriber	sHandEV09B;

static CNVSubscriber	sHandSolenoid1;				// comandi di controllo elettrovalvole servocomando
static CNVSubscriber	sHandSolenoid2;

static CNVSubscriber	sHandCsasSetpoint;			// setpoint di posizionamento degli attuatori CSAS a bordo servocomando
static CNVSubscriber	sHandInputSetpoint;			// setpoint di posizionamento del martinetto sulla leva di input
static CNVSubscriber	sHandLoadSetpoint;			// setpoint di carico del main piston

static CNVSubscriber	sHandUutPressureSetpoint;	// setpoint di pressione verso il servocomando
static CNVSubscriber	sHandUutVoltageSetpoint;	// setpoint di tensione verso le solenoid valve del servocomando

static CNVSubscriber	sHandWaveType;				// tipologia di forma d'onda di controllo
static CNVSubscriber	sHandWaveAmplitude;			// ampiezza di oscillazione dell'onda di controllo
static CNVSubscriber	sHandWaveFrequency;			// frequenza di oscillazione dell'onda di controllo

static CNVSubscriber	sHandLoadDirection;			// direzione di applicazione del carico rispetto al main piston (compressione o estensione)

static CNVSubscriber	sHandCsasPid;				// array 3x1 contenente i coefficienti di regolazione PID Kc, Ti, Td dei CSAS
static CNVSubscriber	sHandInputPid;				// array 3x1 contenente i coefficienti di regolazione PID Kc, Ti, Td del martinetto di input
static CNVSubscriber	sHandLoadPid;				// array 3x1 contenente i coefficienti di regolazione PID Kc, Ti, Td del carico oleodinamico

// WRITER

static CNVWriter		wHandError;					// codice di errore inviato all'Host
static CNVWriter		wHandStatus;				// maschera binaria inviata all'Host per indicare lo stato di 
													// solenoid valve e switch di banco e servocomando
static CNVWriter		wHandMeasure;				// acquisizione analogiche raggruppate per grandezze fisiche
static CNVWriter		wHandLeakage;




/* ------------------------------------------------------------------------------------ */
/* Prototipi Callback delle connessioni Subscriber										*/
/* ------------------------------------------------------------------------------------ */

// Chiusura SW del programma
void CVICALLBACK StopProgramCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK EmergencyCB (void * handle, CNVData data, void * callbackData);

// Variazione stato elettrovalvole banco
void CVICALLBACK ChangeEV01StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV02StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV03StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV04AStatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV04BStatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV05StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV06StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV07StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV08StatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV09AStatusCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeEV09BStatusCB (void * handle, CNVData data, void * callbackData);

// Variazione stato elettrovalvole servocomando
//void CVICALLBACK ChangeSolenoid1StatusCB (void * handle, CNVData data, void * callbackData);
//void CVICALLBACK ChangeSolenoid2StatusCB (void * handle, CNVData data, void * callbackData);

// Abilitazione regolazioni analogiche
void CVICALLBACK EnableCsas1CB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK EnableCsas2CB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK EnableInputCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK EnableLoadCB (void * handle, CNVData data, void * callbackData);

//void CVICALLBACK EnableUutVoltageCB (void * handle, CNVData data, void * callbackData);
//void CVICALLBACK EnableUutPressureCB (void * handle, CNVData data, void * callbackData);

// Abilitazione misure di corrente
//void CVICALLBACK EnableNullBiasCB (void * handle, CNVData data, void * callbackData);
//void CVICALLBACK EnableEVcurrentCB (void * handle, CNVData data, void * callbackData);

// Variazioni Setpoint
void CVICALLBACK ChangeCsasSetpointCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeInputSetpointCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeLoadSetpointCB (void * handle, CNVData data, void * callbackData);

void CVICALLBACK ChangeUutPressureSetpointCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeUutVoltageSetpointCB (void * handle, CNVData data, void * callbackData);

// Gestione forme d'onda di controllo
void CVICALLBACK ChangeWaveTypeCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeWaveAmplitudeCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeWaveFrequencyCB (void * handle, CNVData data, void * callbackData);

// Gestione del carico
void CVICALLBACK ChangeLoadDirectionCB (void * handle, CNVData data, void * callbackData);

// Gestione dei coefficienti di regolazione PID
void CVICALLBACK ChangeInputPidCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeCsasPidCB (void * handle, CNVData data, void * callbackData);
void CVICALLBACK ChangeLoadPidCB (void * handle, CNVData data, void * callbackData);

// Post deferred callback per la comunicazione in loop back del messaggio di errore
void SendError (int errore);



/* ------------------------------------------------------------------------------------ */
/* Routine di inizializzazione delle connessioni di rete tra RTtarget ed Host sia di	*/
/* tipo subscriber che writer															*/
/* ------------------------------------------------------------------------------------ */

int InitNetworkConnection(void)
{
	int error=0;
	
	/*------------------------------------------*/
	/* Apertura connessioni di tipo Subscriber	*/
	/*------------------------------------------*/
	
	// connessione per ricezione Quit Program
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_QuitProgram" , StopProgramCB, 0, 0, 2000, 0, &sHandQuitProgram) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_Emergency" , EmergencyCB, 0, 0, 2000, 0, &sHandEmergency) );	
	
	// connessioni per ricezione comandi di controllo elettrovalvole banco
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV01" , ChangeEV01StatusCB, 0, 0, 2000, 0, &sHandEV01) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV02" , ChangeEV02StatusCB, 0, 0, 2000, 0, &sHandEV02) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV03" , ChangeEV03StatusCB, 0, 0, 2000, 0, &sHandEV03) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV04A" , ChangeEV04AStatusCB, 0, 0, 2000, 0, &sHandEV04A) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV04B" , ChangeEV04BStatusCB, 0, 0, 2000, 0, &sHandEV04B) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV05" , ChangeEV05StatusCB, 0, 0, 2000, 0, &sHandEV05) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV06" , ChangeEV06StatusCB, 0, 0, 2000, 0, &sHandEV06) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV07" , ChangeEV07StatusCB, 0, 0, 2000, 0, &sHandEV07) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV08" , ChangeEV08StatusCB, 0, 0, 2000, 0, &sHandEV08) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV09A" , ChangeEV09AStatusCB, 0, 0, 2000, 0, &sHandEV09A) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV09B" , ChangeEV09BStatusCB, 0, 0, 2000, 0, &sHandEV09B) );

	// connessioni per ricezione comandi di controllo elettrovalvole banco
	//ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV_SYS1" , ChangeSolenoid1StatusCB, 0, 0, 2000, 0, &sHandSolenoid1) );
	//ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EV_SYS2" , ChangeSolenoid2StatusCB, 0, 0, 2000, 0, &sHandSolenoid2) );
	
	// connessioni per ricezione comandi di abilitazione alle regolazioni analogiche
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnCsas1" , EnableCsas1CB, 0, 0, 2000, 0, &sHandEnCsas1) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnCsas2" , EnableCsas2CB, 0, 0, 2000, 0, &sHandEnCsas2) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnInput" , EnableInputCB, 0, 0, 2000, 0, &sHandEnInput) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnLoad" , EnableLoadCB, 0, 0, 2000, 0, &sHandEnLoad) );

	//ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnUutVoltage" , EnableUutVoltageCB, 0, 0, 2000, 0, &sHandEnUutVoltage) );
	//ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnUutPressure" , EnableUutPressureCB, 0, 0, 2000, 0, &sHandEnUutPressure) );

	// connessioni per ricezione comadni di abilitazione misure di corrente
	//ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnNullBias" , EnableNullBiasCB, 0, 0, 2000, 0, &sHandEnNullBias) );
	//ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_EnEVcurrent" , EnableEVcurrentCB, 0, 0, 2000, 0, &sHandEnEVcurrent) );	
	
	// connessioni per ricezione set-point di controllori PID e altre regolazioni
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_CsasSetPoint" , ChangeCsasSetpointCB, 0, 0, 2000, 0, &sHandCsasSetpoint) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_InputSetPoint" , ChangeInputSetpointCB, 0, 0, 2000, 0, &sHandInputSetpoint) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_LoadSetPoint" , ChangeLoadSetpointCB, 0, 0, 2000, 0, &sHandLoadSetpoint) );
	
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_UutPressureSetPoint" , ChangeUutPressureSetpointCB, 0, 0, 2000, 0, &sHandUutPressureSetpoint) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_UutVoltageSetPoint" , ChangeUutVoltageSetpointCB, 0, 0, 2000, 0, &sHandUutVoltageSetpoint) );
	
	// gestione delle forme d'onda di controllo dei setpoint
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_WaveType" , ChangeWaveTypeCB, 0, 0, 2000, 0, &sHandWaveType) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_WaveAmplitude" , ChangeWaveAmplitudeCB, 0, 0, 2000, 0, &sHandWaveAmplitude) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_WaveFrequency" , ChangeWaveFrequencyCB, 0, 0, 2000, 0, &sHandWaveFrequency) );
	
	// gestione del carico
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_LoadDirection" , ChangeLoadDirectionCB, 0, 0, 2000, 0, &sHandLoadDirection) );

	// gestione dei parametri di regolazione PID
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_InputPid" , ChangeInputPidCB, 0, 0, 2000, 0, &sHandInputPid) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_CsasPid" , ChangeCsasPidCB, 0, 0, 2000, 0, &sHandCsasPid) );
	ErrChk ( CNVCreateSubscriber("\\\\localhost\\system\\CNV_LoadPid" , ChangeLoadPidCB, 0, 0, 2000, 0, &sHandLoadPid) );
	
	/*------------------------------------------*/
	/* Apertura connessioni di tipo Writer		*/
	/*------------------------------------------*/

	// connessione per invio messaggio di errore
	ErrChk ( CNVCreateWriter ("\\\\localhost\\system\\CNV_error", NULL, 0, CNVWaitForever , 0, &wHandError) );
	
	// connessione per invio stato di banco e servocomando
	ErrChk ( CNVCreateWriter ("\\\\localhost\\system\\CNV_status", NULL, 0, CNVWaitForever , 0, &wHandStatus) );
	
	// connessione subscriber di aggiornamento dello stato del sistema
	ErrChk ( CNVCreateWriter ("\\\\localhost\\system\\CNV_Measure", NULL, 0, CNVWaitForever , 0, &wHandMeasure) );
	ErrChk ( CNVCreateWriter ("\\\\localhost\\system\\CNV_Flow", NULL, 0, CNVWaitForever , 0, &wHandLeakage) );
	
Error:
	return error;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di lettura variabile di terminazione del programma di testing				*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK StopProgramCB (void * handle, CNVData data, void * callbackData)
{
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	CNVGetScalarDataValue (data, CNVInt32, &QuitProgram);
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di esecuzione dell'arresto di emergenza	potenza idraulica					*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK EmergencyCB (void * handle, CNVData data, void * callbackData)
{
	CNVGetScalarDataValue (data, CNVInt32, &Emergency);
	
	/* PROCEDURA DI ARRESTO DI EMERGENZA */
	if (Emergency==1)
	{
		/* Arresto controlli e movimentazioni */
		enCsas1Pid=0;
		enCsas2Pid=0;
		enInputPid=0;
		enLoadPid=0;
		Delay(0.5);
		
		/* Sequenza di emergenza chiusura elettrovalvole */
		EmergencyStop();
	}
	
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV01 sulla mandata del banco				*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV01StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;

	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV01,LW);
	else if (stato==1)
		error=SetDO(EV01,HI);
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:	
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);  
	
	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV02 sulla mandata del Sistema1 del		*/
/* servocomando sotto test																*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV02StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV02,LW);
	else if (stato==1)
		error=SetDO(EV02,HI);
	else
		error=ERROR_DATA_INTEGRITY;

Error:	
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV03 sulla mandata del Sistema2 del		*/
/* servocomando sotto test																*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV03StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV03,LW);
	else if (stato==1)
		error=SetDO(EV03,HI);
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV04A sul ritorno del servocomando per	*/
/* la misura del leakage sul Sistema1 dell'unita' sotto test.							*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV04AStatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV04A,LW);
	else if (stato==1)
	{
		if (solenoid_valve[EV04B-EV01]==0)
			error=SetDO(EV04A,HI);	
		else
			error=ERROR_DANGEROUS_SET;
	}
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}




/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV04B sul ritorno del servocomando per	*/
/* la misura del leakage sul Sistema2 dell'unita' sotto test.							*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV04BStatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV04B,LW);
	else if (stato==1)
	{
		if (solenoid_valve[EV04A-EV01]==0)
			error=SetDO(EV04B,HI);	
		else
			error=ERROR_DANGEROUS_SET;
	}
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV05 per la misura di leakage sul 		*/
/* ritorno del servocomando sotto test													*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV05StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV05,LW);
	else if (stato==1)
		error=SetDO(EV05,HI);
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV06 per il test di contropressione 		*/
/* a 155bar sul ritorno del servocomando.												*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV06StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV06,LW);
	else if (stato==1)
		error=SetDO(EV06,HI);
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV07 per l'alimentazione della mandata	*/
/* del martinetto di controllo della leva di input.										*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV07StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV07,LW);
	else if (stato==1)
		error=SetDO(EV07,HI);
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV08 per l'alimentazione della mandata	*/
/* del martinetto di carico sul main piston del servocomando.							*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV08StatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV08,LW);
	else if (stato==1)
		error=SetDO(EV08,HI);
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV09A per l'engage del martinetto		*/
/* di carico oleodinamico sul main piston del servocomando.								*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV09AStatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV09A,LW);
	else if (stato==1)
	{
		// permetto il collegamento del carico solo se il cilindro e' alimentato
		if (solenoid_valve[EV09B-EV01]==0)
			error=SetDO(EV09A,HI);	
		else
			error=ERROR_DANGEROUS_SET;
	}
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola EV09B per lo sgancio del martinetto		*/
/* di carico oleodinamico sul main piston del servocomando.								*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeEV09BStatusCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato elettrovalvola
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato elettrovalvola
	if (stato==0)
		error=SetDO(EV09B,LW);
	else if (stato==1)
	{
		if (solenoid_valve[EV09A-EV01]==0)
			error=SetDO(EV09B,HI);	
		else
			error=ERROR_DANGEROUS_SET;
	}
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola a bordo servocomando del sistema 		*/
/* autopilota CSAS1.																	*/
/* ------------------------------------------------------------------------------------ */

//void CVICALLBACK ChangeSolenoid1StatusCB (void * handle, CNVData data, void * callbackData)
//{
//	CNVData CNVerror;
//	int stato;
//	int error=0;
//	
//	int solenoid_status=OFF;			// stato solenoidi servocomando
//	
//	// In caso di emergenza ogni callback viene ignorata
//	if (Emergency==1) return;
//	
//	// Acquisisci nuovo stato elettrovalvola
//	CNVGetScalarDataValue (data, CNVInt32, &stato);  
//	
//	// Varia stato elettrovalvola
//	if (stato==CLOSE)
//	{
//		ErrChk( GetRelay(SW7,&solenoid_status) );
//		
//		// se anche il solenoide del Sistema2 e' disconnesso allora azzero il riferimento dell'alimentatore SIEMENS
//		// azzerando quindi anche la tensione erogata
//		
//		if (solenoid_status==OFF)
//			ErrChk( SetRelay(SW11,OFF) );	// diseccita elettrovalvola CSAS
//		
//		Sleep(100);
//		ErrChk( SetRelay(SW5,OFF) );	// diseccita elettrovalvola CSAS
//	}
//	else if (stato==OPEN)
//	{
//		ErrChk( SetRelay(SW11,ON) );	// diseccita elettrovalvola CSAS
//		Sleep(100);
//		ErrChk( SetRelay(SW5,ON) );		// eccita elettrovalvola CSAS
//	}
//	else
//		error=ERROR_DATA_INTEGRITY;
//	
//Error:     
//	// In caso di errore invia segnalazione al PC-Host
//	SendError (error);       

//	return;
//}

	
/* ------------------------------------------------------------------------------------ */
/* Callback di variazione stato elettrovalvola a bordo servocomando del sistema 		*/
/* autopilota CSAS2.																	*/
/* ------------------------------------------------------------------------------------ */

//void CVICALLBACK ChangeSolenoid2StatusCB (void * handle, CNVData data, void * callbackData)
//{
//	CNVData CNVerror;
//	int stato;
//	int error=0;
//	
//	int solenoid_status=OFF;			// stato solenoidi servocomando         
//	
//	// In caso di emergenza ogni callback viene ignorata
//	if (Emergency==1) return;
//	
//	// Acquisisci nuovo stato elettrovalvola
//	CNVGetScalarDataValue (data, CNVInt32, &stato);  
//	
//	// Varia stato elettrovalvola
//	if (stato==CLOSE)
//	{
//		ErrChk( GetRelay(SW5,&solenoid_status) );
//		
//		// se anche il solenoide del Sistema2 e' disconnesso allora azzero il riferimento dell'alimentatore SIEMENS
//		// azzerando quindi anche la tensione erogata
//		
//		if (solenoid_status==OFF)
//			ErrChk( SetRelay(SW11,OFF) );	// diseccita elettrovalvola CSAS
//		
//		Sleep(100);
//		ErrChk( SetRelay(SW7,OFF) );	// diseccita elettrovalvola CSAS
//	}
//	else if (stato==OPEN)
//	{
//		ErrChk( SetRelay(SW11,ON) );	// diseccita elettrovalvola CSAS
//		Sleep(100);
//		ErrChk( SetRelay(SW7,ON) );		// eccita elettrovalvola CSAS
//	}
//	else
//		error=ERROR_DATA_INTEGRITY;
//	
//Error:     
//	// In caso di errore invia segnalazione al PC-Host
//	SendError (error);       

//	return;
//}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione controllore digitale PID per il controllo	*/
/* dell'attuatore CSAS1 del servocomando												*/
/* ------------------------------------------------------------------------------------ */
  
void CVICALLBACK EnableCsas1CB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato controllo
	if (stato==0)
	{
		enCsas1Pid=0;
		Sleep(100);
		//ErrChk( SetRelay(SW1,OFF) );	// porta massa alla servovalvola
		ErrChk( SetDO6341(EV_SYS1,CLOSE) ); //spegnimento CSAS1
	}
	else if (stato==1)
	{
		//ErrChk( SetRelay(SW1,ON) );		// porta AO alla servovalvola 
		ErrChk( SetDO6341(EV_SYS1,OPEN) ); //accensione CSAS1
		Sleep(100);
		enCsas1Pid=1;
	}
	else
		error=ERROR_DATA_INTEGRITY;
	
Error:
	// In caso di errore invia segnalazione al PC-Host
	if (error!=0)  
		printf ("%d \n CSAS1 ",error);
	SendError (error);       

	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione controllore digitale PID per il controllo	*/
/* dell'attuatore CSAS2 del servocomando												*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK EnableCsas2CB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato controllo
	if (stato==0)
	{
		enCsas2Pid=0;
		Sleep(100);
		//ErrChk( SetRelay(SW3,OFF) );	// porta massa alla servovalvola
		ErrChk( SetDO6341(EV_SYS2,CLOSE) ); //spegnimento CSAS2 
	}
	else if (stato==1)
	{
		//ErrChk( SetRelay(SW3,ON) ); 	// porta AO alla servovalvola
		ErrChk( SetDO6341(EV_SYS2,OPEN) ); //accensione CSAS2 
		Sleep(100);
		enCsas2Pid=1;
	}
	else
		error=ERROR_DATA_INTEGRITY;

Error:
	// In caso di errore invia segnalazione al PC-Host
	if (error!=0)  
		printf ("%d \n CSAS2 ",error);
	SendError (error);       

	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione controllore digitale PID per il controllo	*/
/* del martinetto di pilotaggio della leva di input manuale del servocomando.			*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK EnableInputCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato controllo
	if (stato==0)
	{
		enInputPid=0;
		//Sleep(100);
		//ErrChk( SetRelay(SW0,OFF) );	// porta massa alla servovalvola
	}
	else if (stato==1)
	{
		//ErrChk( SetRelay(SW0,ON) );		// porta AO alla servovalvola 
		//Sleep(100);
		enInputPid=1;
	}
	else
		error=ERROR_DATA_INTEGRITY;

Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;	
}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione controllore digitale PID per il controllo	*/
/* del martinetto di carico sul main piston del servocomando.							*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK EnableLoadCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int stato;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVInt32, &stato);  
	
	// Varia stato controllo
	if (stato==LW)
	{
		enLoadPid=0;
		//Sleep(100);
		//ErrChk( SetRelay(SW9,OFF) );	// porta massa alla servovalvola
	}
	else if (stato==HI)
	{
		//ErrChk( SetRelay(SW10,OFF) );   // apri altri percorsi di AO3
		//ErrChk( SetRelay(SW12,OFF) );
		//Sleep(100);          
		//ErrChk( SetRelay(SW9,ON) );		// porta AO alla servovalvola 
		//Sleep(100);
		enLoadPid=1;
	}
	else
		error=ERROR_DATA_INTEGRITY;

Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       

	return;		
}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione regolazione proporzionale della tensione	*/
/* di eccitazione delle bobine delle solenoid valve a bordo dei sistemi CSAS.			*/
/* ------------------------------------------------------------------------------------ */
//*******NOT USED INTO THE CODE*******
//void CVICALLBACK EnableUutVoltageCB (void * handle, CNVData data, void * callbackData)
//{
//	CNVData CNVerror;
//	int stato;
//	int error=0;
//	
//	// In caso di emergenza ogni callback viene ignorata
//	if (Emergency==1) return;
//	
//	// Acquisisci nuovo stato flag di controllo
//	CNVGetScalarDataValue (data, CNVInt32, &stato);  
//	
//	// Varia stato controllo
//	if (stato==0)
//	{
//		enUutVoltageReg=0;
//		Sleep(100);
//		ErrChk( SetRelay(SW10,OFF) );	// apri percorso dalla AO3 all'alimentatore programmabile
//	}
//	else if (stato==1)
//	{
//		ErrChk( SetRelay(SW10,ON) );			// porta AO3 in riferimento all'alimentatore programmabile
//		ErrChk( SetRelay(SW11,ON) );			// apre il percorso di massa all'alimentatore programmabile
//		Sleep(100); 
//		enUutVoltageReg=1;
//	}
//	else
//		error=ERROR_DATA_INTEGRITY;

//Error:
//	// In caso di errore invia segnalazione al PC-Host
//	SendError (error);       

//	return;			
//}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione regolazione proporzionale della pressione	*/
/* di mandata ai due sistemi del servocomando sotto test.								*/
/* ------------------------------------------------------------------------------------ */

//void CVICALLBACK EnableUutPressureCB (void * handle, CNVData data, void * callbackData)
//{
//	CNVData CNVerror;
//	int stato;
//	int error=0;
//	
//	// In caso di emergenza ogni callback viene ignorata
//	if (Emergency==1) return;
//	
//	// Acquisisci nuovo stato flag di controllo
//	CNVGetScalarDataValue (data, CNVInt32, &stato);  
//	
//	// Varia stato controllo
//	
//	if (stato==VARIABLE)
//	{
//		ErrChk( SetRelay(SW12,ON) );		// porta AO3 in riferimento alla valvola proporzionale di pressione
//		Sleep(100); 
//		ErrChk( SetRelay(SW13,ON) );		// apre il percorso di massa alla valvola proporzionale V3
//		Sleep(100); 
//		enUutPressureReg=1;	
//	}
//	else if (stato==POWER_OFF)     
//	{
//		enUutPressureReg=0;
//		Sleep(100); 
//		ErrChk( SetRelay(SW12,OFF) );		// apri percorso dalla AO3 alla valvola proporzionale di pressione V3
//		Sleep(100); 
//		ErrChk( SetRelay(SW13,OFF) );		// porta a massa regolazione V3
//	}
//	else if (stato==FIXED)     
//	{
//		enUutPressureReg=0;
//		Sleep(100); 
//		ErrChk( SetRelay(SW12,OFF) );	
//		Sleep(100); 
//		ErrChk( SetRelay(SW13,ON) );	
//	}
//	
//Error:
//	// In caso di errore invia segnalazione al PC-Host
//	SendError (error);       

//	return;				
//}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione misura della corrente di Null Bias		*/
/* assorbita dalle servovalvole a bordo dei due sistemi CSAS del servocomando.			*/
/* ------------------------------------------------------------------------------------ */

//void CVICALLBACK EnableNullBiasCB (void * handle, CNVData data, void * callbackData)
//{
//	CNVData CNVerror;
//	int stato;
//	int error=0;
//	
//	// In caso di emergenza ogni callback viene ignorata
//	if (Emergency==1) return;
//	
//	// Acquisisci nuovo stato flag di controllo
//	CNVGetScalarDataValue (data, CNVInt32, &stato);  
//	
//	// Varia stato controllo
//	if (stato==0)
//	{
//		enNullBias=0;
//		Sleep(100);  
//		ErrChk( SetRelay(SW2,OFF) );	// disabilita resistenza shunt servovalvola CSAS1
//		ErrChk( SetRelay(SW4,OFF) );	// disabilita resistenza shunt servovalvola CSAS2
//	}
//	else if (stato==1)
//	{
//		ErrChk( SetRelay(SW2,ON) );		// abilita resistenza shunt servovalvola CSAS1
//		ErrChk( SetRelay(SW4,ON) );		// abilita resistenza shunt servovalvola CSAS2
//		Sleep(100);
//		enNullBias=1;
//	}
//	else
//		error=ERROR_DATA_INTEGRITY;

//Error:
//	// In caso di errore invia segnalazione al PC-Host
//	SendError (error);       
//	
//	return;
//}



/* ------------------------------------------------------------------------------------ */
/* Callback di abilitazione / disabilitazione misura della corrente di eccitazione		*/
/* assorbita dalle solenoid valve a bordo dei due sistemi CSAS del servocomando.		*/
/* ------------------------------------------------------------------------------------ */

//void CVICALLBACK EnableEVcurrentCB (void * handle, CNVData data, void * callbackData)
//{
//	CNVData CNVerror;
//	int stato;
//	int error=0;
//	
//	// In caso di emergenza ogni callback viene ignorata
//	if (Emergency==1) return;
//	
//	// Acquisisci nuovo stato flag di controllo
//	CNVGetScalarDataValue (data, CNVInt32, &stato);  
//	
//	// Varia stato controllo
//	if (stato==0)
//	{
//		enEvCurrent=0;
//		Sleep(100);  
//		ErrChk( SetRelay(SW6,OFF) );	// disabilita resistenza shunt solenoid valve CSAS1
//		ErrChk( SetRelay(SW8,OFF) );	// disabilita resistenza shunt solenoid valve CSAS2
//	}
//	else if (stato==1)
//	{
//		ErrChk( SetRelay(SW6,ON) );		// abilita resistenza shunt solenoid valve CSAS1
//		ErrChk( SetRelay(SW8,ON) );		// abilita resistenza shunt solenoid valve CSAS2
//		Sleep(100);
//		enEvCurrent=1;
//	}
//	else
//		error=ERROR_DATA_INTEGRITY;

//Error:
//	// In caso di errore invia segnalazione al PC-Host
//	SendError (error);       
//	
//	return;
//}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del set-point dei controllori PID relativi ai sistemi 		*/
/* autopilota CSAS.																		*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeCsasSetpointCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	// Varia stato controllo
	//if ((value>=CSAS_LW_LIMIT)&&(value<=CSAS_UP_LIMIT))
		CsasSetPoint=value;
	//else 
	//	error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;	
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del set-point del controllore PID relativo al martinetto 		*/
/* di pilotaggio leva di input manuale.													*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeInputSetpointCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	// Varia stato controllo
	if ((value>=INPUT_LW_LIMIT)&&(value<=INPUT_UP_LIMIT))
		InputSetPoint=value;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del set-point del controllore PID relativo al martinetto 		*/
/* di carico del main piston.															*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeLoadSetpointCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value=0;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	LoadSetPoint = value;

Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del set-point di regolazione della pressione di mandata 		*/
/* ai sistemi 1 e 2 del servocomando sotto test.										*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeUutPressureSetpointCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	// Varia stato controllo
	if ((value>=PRESSURE_LW_LIMIT)&&(value<=PRESSURE_UP_LIMIT))
		UutPressureSetpoint=value;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;	
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del set-point di regolazione della tensione di eccitazione	*/
/* delle bobine delle solenoid valve a bordo dei sistemi CSAS del servocomando.			*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeUutVoltageSetpointCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	// Varia stato controllo
	if ((value>=VOLTAGE_LW_LIMIT)&&(value<=VOLTAGE_UP_LIMIT))
		UutVoltageSetpoint=value;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;		
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione nell'opzione di tipologia di forma d'onda per il comando		*/
/* dei set-point di posizione degli attuatori.											*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeWaveTypeCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int option;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVInt32, &option);
	
	// Varia stato controllo
	if ((option==STATIC)||(option==SINE)||(option==SQUARE)||(option==TRIANGULAR))
		WaveType=option;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;		
}

  

/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del valore di ampiezza massima della forma d'onda di  		*/
/* controllo del set-point durante le prove in oscillazione.							*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeWaveAmplitudeCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	// Varia stato controllo
	if ((value>=AMPLITUDE_LW_LIMIT)&&(value<=AMPLITUDE_UP_LIMIT))
		WaveAmplitude=value;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;			
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del valore di frequenza massima della forma d'onda di  		*/
/* controllo del set-point durante le prove in oscillazione.							*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeWaveFrequencyCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double value;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVDouble, &value);
	
	// Varia stato controllo
	if ((value>=FREQUENCY_LW_LIMIT)&&(value<=FREQUENCY_UP_LIMIT))
		WaveFrequency=value;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;			
}



/* ------------------------------------------------------------------------------------ */
/* Callback di variazione del verso di applicazione del carico oleodinamico sul main	*/
/* piston, in compressione o in estensione.												*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeLoadDirectionCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	int option;
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovo stato flag di controllo
	CNVGetScalarDataValue (data, CNVInt32, &option);
	
	// Varia stato controllo
	if ((option==COMPRESSION)||(option==EXTENSION))
		LoadDirection=option;
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;			
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione dei coefficienti di regolazione PID del controllore di 		*/
/* comando del martinetto sulla leva di input.											*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeInputPidCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double controller[3]={0,0,0};
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovi coefficienti
	CNVGetArrayDataValue (data, CNVDouble, controller, 3);
	
	// Varia coefficienti
	if ((controller[1]>=0)||(controller[2]>=0))
	{
		PID_parameters[INPUT_PID].proportional_gain = controller[0];
		PID_parameters[INPUT_PID].integral_time = controller[1];
		PID_parameters[INPUT_PID].derivative_time = controller[2];
	}
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;			
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione dei coefficienti di regolazione PID del controllore di 		*/
/* comando dei due stadi autopilota CSAS.												*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeCsasPidCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double controller[3]={0,0,0};
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovi coefficienti
	CNVGetArrayDataValue (data, CNVDouble, controller, 3);
	
	// Varia coefficienti
	if ((controller[1]>=0)||(controller[2]>=0))
	{
		// Si assume che, essendo costruttivamente identici, i due controllori CSAS abbiano medesimi
		// coefficienti di regolazione
		
		PID_parameters[CSAS1_PID].proportional_gain = controller[0];
		PID_parameters[CSAS1_PID].integral_time = controller[1];
		PID_parameters[CSAS1_PID].derivative_time = controller[2];
		
		PID_parameters[CSAS2_PID].proportional_gain = controller[0];
		PID_parameters[CSAS2_PID].integral_time = controller[1];
		PID_parameters[CSAS2_PID].derivative_time = controller[2];
	}
	else 
		error=ERROR_DATA_INTEGRITY;

Error:
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;			
}


/* ------------------------------------------------------------------------------------ */
/* Callback di variazione dei coefficienti di regolazione PID del controllore di 		*/
/* comando del martinetto di carico oleodinamico.										*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK ChangeLoadPidCB (void * handle, CNVData data, void * callbackData)
{
	CNVData CNVerror;
	double controller[3]={0,0,0};
	int error=0;
	
	// In caso di emergenza ogni callback viene ignorata
	if (Emergency==1) return;
	
	// Acquisisci nuovi coefficienti
	CNVGetArrayDataValue (data, CNVDouble, controller, 3);
	
	// Varia coefficienti
	if ((controller[1]>=0)||(controller[2]>=0))
	{
		PID_parameters[LOAD_PID].proportional_gain = controller[0];
		PID_parameters[LOAD_PID].integral_time = controller[1];
		PID_parameters[LOAD_PID].derivative_time = controller[2];
	}
	else 
		error=ERROR_DATA_INTEGRITY;
	
Error:     
	// In caso di errore invia segnalazione al PC-Host
	SendError (error);       
	
	return;			
}


/* ------------------------------------------------------------------------------------ */
/* Procedura di comunicazione verso l'Host degli stati digitali dovuti a proximity 		*/
/* switch, fine corsa o indicatori di intasamento del filtro idraulcio.					*/
/* ------------------------------------------------------------------------------------ */

void SendStatus(unsigned int stato)
{
	CNVData CNVstatus;

	CNVCreateScalarDataValue (&CNVstatus, CNVUInt32, stato);
	CNVWrite (wHandStatus, CNVstatus, 2000);
	CNVDisposeData(CNVstatus);	
	
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Comunicazione all'host del valore di leakage misurato sul ritorno del servocomando	*/
/* ------------------------------------------------------------------------------------ */

void SendLeakage(double flow)
{
	CNVData CNVLeakageData;	
	
	CNVCreateScalarDataValue (&CNVLeakageData, CNVDouble, flow);
	CNVWrite (wHandLeakage, CNVLeakageData, 2000);
	CNVDisposeData(CNVLeakageData);	
	
	return;	
}


/* ------------------------------------------------------------------------------------ */
/* Callback relativa alla Thread Safe Queue contenente i valori di acquisizione 		*/
/* analogica da scheda DAQ. La callback legge dalla coda ed invia al PC Host i dati		*/
/* mediante network variable															*/
/* ------------------------------------------------------------------------------------ */

void CVICALLBACK SendMeasuresToHostCB (int queueHandle, unsigned int event, int value, void *callbackData)
{
	double readBuffer[13000];					// buffer di riempimento dati
	int nItems=0, numItemRead=0;
	
	CNVData CNVMeasureData;
	
	// Lettura numero di elementi presenti nella TSQ
	CmtGetTSQAttribute (gTsqDataIn, ATTR_TSQ_ITEMS_IN_QUEUE, &nItems);
	
	// Nel caso siano presenti dati in coda si procede con l'allocazione dinamica di un vettore
	// in cui verranno inseriti i dati in coda per essere poi inviati al Host
	
	if (nItems>0)
	{
		/************************************************************/
		/* Lettura della Thread Safe Queue delle misure analogiche	*/
		/************************************************************/
		numItemRead = CmtReadTSQData (gTsqDataIn, readBuffer, nItems, 500, 0);
	
		/************************************************************/
		/* Comunicazione verso PC Host								*/
		/************************************************************/

		CNVCreateArrayDataValue (&CNVMeasureData, CNVDouble, readBuffer, 1, &numItemRead);
		
		CNVWrite (wHandMeasure, CNVMeasureData, 3000);
		CNVDisposeData (CNVMeasureData);
	}
	
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di rilascio delle connessioni a Network Variable tra Host e RT Target 		*/
/* ------------------------------------------------------------------------------------ */		

void ReleaseConnections(void)
{
	/************************************************************/
	/* Rilascio Subscriber										*/
	/************************************************************/
	
	CNVDispose (sHandQuitProgram);
	CNVDispose (sHandEmergency);
	
	CNVDispose (sHandEnCsas1);
	CNVDispose (sHandEnCsas2);
	CNVDispose (sHandEnInput);
	CNVDispose (sHandEnLoad);
	CNVDispose (sHandEnUutVoltage);
	CNVDispose (sHandEnUutPressure);
	
	CNVDispose (sHandEnNullBias);
	CNVDispose (sHandEnEVcurrent);
	
	CNVDispose (sHandEV01);
	CNVDispose (sHandEV02);
	CNVDispose (sHandEV03);
	CNVDispose (sHandEV04A);
	CNVDispose (sHandEV04B);
	CNVDispose (sHandEV05);
	CNVDispose (sHandEV06);
	CNVDispose (sHandEV07);
	CNVDispose (sHandEV08);
	CNVDispose (sHandEV09A);
	CNVDispose (sHandEV09B);
	
	CNVDispose (sHandCsasSetpoint);
	CNVDispose (sHandInputSetpoint);
	CNVDispose (sHandLoadSetpoint);
	
	CNVDispose (sHandUutPressureSetpoint);
	CNVDispose (sHandUutVoltageSetpoint);
	
	CNVDispose (sHandWaveType);
	CNVDispose (sHandWaveAmplitude);
	CNVDispose (sHandWaveFrequency);
	
	CNVDispose (sHandLoadDirection);
	
	CNVDispose (sHandCsasPid);
	CNVDispose (sHandInputPid);
	CNVDispose (sHandLoadPid);
	
	/************************************************************/
	/* Rilascio Writer											*/
	/************************************************************/	
	
	CNVDispose (wHandError);
	CNVDispose (wHandStatus);
	
	CNVDispose (wHandMeasure);
	CNVDispose (wHandLeakage);
	
	/************************************************************/
	/* Chiudi connessione di rete								*/
	/************************************************************/	
	CNVFinish();
	
	return;	
}


/* ------------------------------------------------------------------------------------ */
/* Callback per la comunicazione in loop back delle informazione di errore				*/
/* ------------------------------------------------------------------------------------ */

void SendError (int errore)    
{
	CNVData CNVerror;
	int error=0;
	
	if (errore!=0)
	{
		// In caso di variabile di rete corrotta invia segnalazione al PC-Host
		ErrChk ( CNVCreateScalarDataValue (&CNVerror, CNVInt32, errore) );
		ErrChk ( CNVWrite (wHandError, CNVerror, 2000) );
	
		Sleep(2);
		CNVDisposeData(CNVerror);
	}

Error:	
	return;
}


