#include <userint.h>
#include "ATPPANEL.h"

#include "global_var.h"

static char test_index_msg[30]="";


int CVICALLBACK StopAtpCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target ed Host
			EmergencyStop();
			
			break;
	}
	return 0;
}

int CVICALLBACK StartAtpCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// attiva pagina 0
			SetActiveTabPage (atppanel, ATPPNL_TAB, 0);  
			
			// abilita i tab
			SetCtrlAttribute (atppanel, ATPPNL_TAB, ATTR_DIMMED, YES);  
			
			break;
	}
	return 0;
}

int CVICALLBACK QuitAtpCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Scarica pannello e resetta puntatore
			DiscardPanel (atppanel);
			atppanel=-1000; 
			
			DisplayPanel (engpanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK ClearOperatorNameCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int tabHandle;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:

			SetCtrlAttribute (init_atppanel, INITTAB_OPNAMESTR, ATTR_CTRL_VAL, "");
			
			break;
	}
	return 0;
}
