/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  LVDTOUTPNL                       1
#define  LVDTOUTPNL_SAVEBTN               2       /* callback function: SaveStripChartPlotCsasCB */
#define  LVDTOUTPNL_STOPBTN               3       /* callback function: StopStripChartPlotCsasCB */
#define  LVDTOUTPNL_CLEARBTN              4       /* callback function: ClearStripChartLvdtCB */
#define  LVDTOUTPNL_QUITBTN               5       /* callback function: QuitLvdtCB */
#define  LVDTOUTPNL_POSITION2STR          6       /* callback function: ChangeScaleLvdt2CB */
#define  LVDTOUTPNL_POSITION1STR          7       /* callback function: ChangeScaleLvdt1CB */
#define  LVDTOUTPNL_LVDT2AVENMR           8
#define  LVDTOUTPNL_LVDT1AVENMR           9
#define  LVDTOUTPNL_DECORATION            10
#define  LVDTOUTPNL_TEXTMSG_4             11


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ChangeScaleLvdt1CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleLvdt2CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearStripChartLvdtCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitLvdtCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveStripChartPlotCsasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopStripChartPlotCsasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
