/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  SOLENPNL                         1
#define  SOLENPNL_QUITBTN                 2       /* callback function: QuitSolenoidCB */
#define  SOLENPNL_ISOL2NMR                3
#define  SOLENPNL_VSOL2NMR                4
#define  SOLENPNL_RESISTANCE2NMR          5
#define  SOLENPNL_TEXTMSG_5               6
#define  SOLENPNL_TEXTMSG_6               7
#define  SOLENPNL_TEXTMSG_7               8
#define  SOLENPNL_DECORATION_2            9
#define  SOLENPNL_TEXTMSG_8               10
#define  SOLENPNL_ISOL1NMR                11
#define  SOLENPNL_VSOL1NMR                12
#define  SOLENPNL_RESISTANCE1NMR          13
#define  SOLENPNL_TEXTMSG                 14
#define  SOLENPNL_TEXTMSG_4               15
#define  SOLENPNL_TEXTMSG_2               16
#define  SOLENPNL_DECORATION              17
#define  SOLENPNL_TEXTMSG_3               18


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK QuitSolenoidCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
