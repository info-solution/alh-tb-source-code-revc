#include "toolbox.h"
#include <userint.h>
#include "PLOTX.h"

#include "global_var.h"


int CVICALLBACK QuitPlotxCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// raizzera la variabile di controllo del salvataggio dati
			save_plotx=0;
			
			// nascondi pannello per Plot X
			HidePanel (plotxpanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK ClearStripChartPlotxCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// cancella tracce sulle strip chart
			ClearStripChart (plotxpanel, PLOTXPNL_POSITIONSTR);
			ClearStripChart (plotxpanel, PLOTXPNL_SPEEDSTR);
			
			// azzera storico dei massimi&minimi
			SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONMAXNMR, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONMINNMR, ATTR_CTRL_VAL, 0.0);
			
			SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDMAXNMR, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDMINNMR, ATTR_CTRL_VAL, 0.0);

			// azzera valori di massimo e minimo
			previous_max_x=MeasuresAve[L1_MUX];
			previous_min_x=MeasuresAve[L1_MUX];

			PlotxEnable=YES;
				
			break;
	}
	return 0;
}


int CVICALLBACK StopStripChartPlotxCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			PlotxEnable=NO;
			
			break;
	}
	return 0;
}



int CVICALLBACK SaveStripChartPlotxCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int done=0;
	char nomefile[400];
	
	switch (event)
	{
		case EVENT_COMMIT:

			if (save_plotx==0)
			{
				done = DirSelectPopup ("C:", "Select Directory", 1, 1, save_plotx_dir);
				
				if (done<=0)
					goto Error;
				
				sprintf(nomefile,"%s\\displacement_input.bmp",save_plotx_dir);
				
				// al primo click sull'opzione Save converto il controllo in bitmap
				SaveCtrlDisplayToFile (plotxpanel, PLOTXPNL_POSITIONSTR, 0, -1, -1, nomefile);
				save_plotx=1;
				
				// modifica la label in STOP
				SetCtrlAttribute(plotxpanel,PLOTXPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Stop");  
			}
			else
			{
				// modifica la label in START
				SetCtrlAttribute(plotxpanel,PLOTXPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Start");
				save_plotx=0;
			}

			break;
	}
	
Error:
	return 0;
}

int CVICALLBACK ChangeScaleXCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (plotxpanel, PLOTXPNL_POSITIONSTR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>40.0)
			{
				SetAxisScalingMode (plotxpanel, PLOTXPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -3.0, 3.0);	
			}
			else if ((maxScale>4.0)&&(maxScale<30.0))
			{
				SetAxisScalingMode (plotxpanel, PLOTXPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -50.0, 50.0);	
			}
			else if (maxScale<4.0)
			{
				SetAxisScalingMode (plotxpanel, PLOTXPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -15.0, 15.0);	
			}
		break; 
	}
	return 0;	
}

int CVICALLBACK ChangeScaleXdotCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (plotxpanel, PLOTXPNL_SPEEDSTR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>180.0)
			{
				SetAxisScalingMode (plotxpanel, PLOTXPNL_SPEEDSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -50.0, 50.0);	
			}
			else if ((maxScale>100.0)&&(maxScale<180.0))
			{
				SetAxisScalingMode (plotxpanel, PLOTXPNL_SPEEDSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -200.0, 200.0);	
			}
			else if (maxScale<100.0)
			{
				SetAxisScalingMode (plotxpanel, PLOTXPNL_SPEEDSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -150.0, 150.0);	
			}
		break; 
	}
	return 0;	
}
