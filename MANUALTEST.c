#include <cvirte.h>		
#include <userint.h>
#include <windows.h> 

#include "MANUALTEST.h"
#include "ENGINEERPANEL.h"
#include "PLOTX.h"
#include "PLOTY.h" 
#include "PLOTXY.h"
#include "PRESSURE.h"
#include "STRAIN.h"  
#include "SOLENOID.h" 
#include "LVDT.h"
#include "SERVOBIAS.h"
#include "TEMPERATURE.h"    
#include "PIDCOEFFICIENT.h"
#include "RAMCONNECTION.h" 
#include "STROKEPANEL.h"     

#include "global_var.h"


#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else             


/* -------------------------------------------------------- */
/* Variabili di visibilita' ridotta al file					*/
/* -------------------------------------------------------- */


int CVICALLBACK QuitManualPanelCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// UUT non alimentata
			uut_power_on = OFF;
			
			HidePanel (manualpanel);
			DisplayPanel (engpanel);

			break;
	}
	return 0;
}

int CVICALLBACK ChangePowerUutSystemCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int system_on;

	switch (event)
	{
		case EVENT_COMMIT:

			// acquisisci quale sistema idraulico e' stato attivato e disabilita l'opzione CSAS per il sistema spento
			GetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_CTRL_VAL, &system_on);
			
			
			
			//routine di power servocomando
			if (uut_power_on == ON)
			{
				// abilita riferimento fisso pressione di mandata
				SetUutPressure(FIXED, 0.0, LW);
				
				// eccita elettrovalvola
				UutPowerUp(system_on);
			}
			
			// pulisce la lista degli item di selezione CSAS
			ClearListCtrl (manualpanel, MANPANEL_CSASCHOISE);
			
			if (actuatorPN!=PN_3101)
			{
				switch(system_on)
				{
					case BOTH_SYS:
						// both systems
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Both CSAS", BOTH_SYS);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 1, "Only CSAS1", ONLY_SYS1);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 2, "Only CSAS2", ONLY_SYS2);
						break;
					case ONLY_SYS1:
						// system 1
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Both CSAS", BOTH_SYS);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 1, "Only CSAS1", ONLY_SYS1);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 2, "Only CSAS2", ONLY_SYS2);
						break;
					case ONLY_SYS2:
						// system 2
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Both CSAS", BOTH_SYS);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 1, "Only CSAS1", ONLY_SYS1);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 2, "Only CSAS2", ONLY_SYS2);
						break;
				}
			}
			else
			{
				switch(system_on)
				{
					case BOTH_SYS:
						// se accendo sistema2 posso alimentare CSAS1
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Only CSAS1", ONLY_SYS1);
						break;
					case ONLY_SYS1:
						// se il sistema2 non e' alimentato allora non posso attivare l'autopilota del servocomando Tail
						ClearListCtrl (manualpanel, MANPANEL_CSASCHOISE);
						break;
					case ONLY_SYS2:
						// se accendo sistema2 posso alimentare CSAS1
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Only CSAS1", ONLY_SYS1);
						break;
				}			
			}
			
			break;
	}
	return 0;
}


int CVICALLBACK RatbPowerOnCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{   
	switch (event)
	{
		case EVENT_COMMIT:

			//routine di power on Test Bench   
			RatbPowerUp();
			
			// Disabilita controlli
			SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWERONBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_PARTNUMBERRNG, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_QUITBTN, ATTR_DIMMED, NO);
			
			// Abilita controlli
			SetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWERONBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWEROFFBTN, ATTR_DIMMED, YES);  
			
			// abilita controlli scelta movimentazione
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, YES);
			
			// selezione autopilota
			SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_CTRL_VAL, PILOT);
			break;
	}
	return 0;
}

int CVICALLBACK RatbPowerOffCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			//routine di power off Test Bench   
			RatbShutDown();
			
			// Disabilita controlli
			SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWEROFFBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWERONBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, NO);
			
			// Abilita controlli
			SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWERONBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_PARTNUMBERRNG, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_QUITBTN, ATTR_DIMMED, YES);  
			
			// disabilita controlli scelta movimentazione
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, NO);
			
			break;
	}
	return 0;
}


int CVICALLBACK UutPowerOnCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int system_on;

	switch (event)
	{
		case EVENT_COMMIT:

			// acquisisci quale sistema idraulico e' stato attivato e disabilita l'opzione CSAS per il sistema spento
			GetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_CTRL_VAL, &system_on);
			
			// abilita riferimento fisso pressione di mandata
			SetUutPressure(FIXED, 0.0, LW);
			
			//routine di power servocomando
			UutPowerUp(system_on);
			
			// Disabilita controlli
			SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWEROFFBTN, ATTR_DIMMED, NO);     
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWERONBTN, ATTR_DIMMED, NO);
			//SetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_DIMMED, NO);
			
			// Abilita controlli    
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, YES);
			
			// pulisce la lista degli item di selezione CSAS
			ClearListCtrl (manualpanel, MANPANEL_CSASCHOISE);
			
			if (actuatorPN!=PN_3101)
			{
				switch(system_on)
				{
					case BOTH_SYS:
						// both systems
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Both CSAS", BOTH_SYS);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 1, "Only CSAS1", ONLY_SYS1);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 2, "Only CSAS2", ONLY_SYS2);
						break;
					case ONLY_SYS1:
						// system 1
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Both CSAS", BOTH_SYS);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 1, "Only CSAS1", ONLY_SYS1);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 2, "Only CSAS2", ONLY_SYS2);
						break;
					case ONLY_SYS2:
						// system 2
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Both CSAS", BOTH_SYS);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 1, "Only CSAS1", ONLY_SYS1);
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 2, "Only CSAS2", ONLY_SYS2);
						break;
				}
			}
			else
			{
				switch(system_on)
				{
					case BOTH_SYS:
						// se accendo sistema2 posso alimentare CSAS1
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Only CSAS1", ONLY_SYS1);
						break;
					case ONLY_SYS1:
						// se il sistema2 non e' alimentato allora non posso attivare l'autopilota del servocomando Tail
						ClearListCtrl (manualpanel, MANPANEL_CSASCHOISE);
						break;
					case ONLY_SYS2:
						// se accendo sistema2 posso alimentare CSAS1
						InsertListItem (manualpanel, MANPANEL_CSASCHOISE, 0, "Only CSAS1", ONLY_SYS1);
						break;
				}			
			}
			
			// UUT alimentata
			uut_power_on = ON;
			
			// @sz per la prima versione sw manuale si decide di non automatizzare la procedura di engage del carico
			
			// abilita comandi per l'applicazione del carico al servocomando 
			EnableLoadControlManualPanel(YES);
			
			// abilita controlli scelta carico
			SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, YES);
			
			SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, NO);
			
			// riporta a zero il valore del carico applicato
			SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 1.0);
			
			break;
	}
	return 0;
}

int CVICALLBACK UutPowerOffCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			// disabilita riferimento fisso pressione di mandata e azzera i valori
			SetUutPressure(POWER_OFF, 0.0, LW);
			
			// Procedura di Power Off del servocomando
			UutShutDown();
			
			// Abilita controlli    
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWERONBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWEROFFBTN, ATTR_DIMMED, YES);

			// Disabilita controlli
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, NO);
			
			// disabilita controlli scelta carico
			SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
			
			// UUT non alimentata
			uut_power_on = OFF;
			
			break;
	}
	return 0;
}




int CVICALLBACK GetStrokeCenterCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello di acquisizione della corsa
			if (strokepanel<0)
			{
				if ((strokepanel = LoadPanel (0, "STROKEPANEL.uir", STROKEPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello acquisizione corsa
			DisplayPanel (strokepanel);  
			
			// inizializza pannello di acquisizione della corsa
			InitStrokePanel();
			
			break;
	}
	return 0;
}




int CVICALLBACK SetStrokeCenterCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int wave_type;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione forma d'onda
			GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &wave_type); 
				
			if (control_engage==PILOT)
			{
				// impostazione oscillazione input manuale
				SetInputPosition(wave_type, 0.0, 0.0, 0.1);
				SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, 0.0);
			}
			else if (control_engage==CSAS)
			{
				// impostazione oscillazione CSAS
				SetCsasPosition(wave_type, 0.0, 0.0, 0.1);
				SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, 0.0); 
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeControlCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int ctrl_type = -1;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// leggo tipo di controllo desiderato
			GetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_CTRL_VAL, &ctrl_type);
			
			// abilita il comando di selezione dei CSAS
			switch (ctrl_type)
			{
				case CSAS:
					
					// abilita selezione del CSAS
					SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, YES);
					break;
					
				default:
					
					// disabilita selezione del CSAS
					SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);
					break;
			}

			break;
	}
	return 0;
}


int CVICALLBACK CtrlEngageCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	int csas_option=0;
	int num_option=0;
	
	
	switch (event)
	{
		case EVENT_COMMIT:

			// abilita comandi per controllo servocomando
			EnableMotionControlManualPanel(YES);

			// inizializza la variabile globale con il tipo di controllo
			GetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_CTRL_VAL, &control_engage);
			
			
			/***********************************************************************/
			/* PROCEDURA DI ENGAGE DEL MARTINETTO DI CONTROLLO DELLA LEVA DI INPUT */
			
			// carica pannello con immagini di procedura manuale di connessione della leva di input
			if (inputconnectionpanel<0)
			{
				if ((inputconnectionpanel = LoadPanel (0, "RAMCONNECTION.uir", INRAMCONPN)) < 0)
				return -1;
			}

			// visualizza pannello con procedura manuale
			DisplayPanel (inputconnectionpanel);
	
			SetPanelAttribute (manualpanel, ATTR_DIMMED, NO);
								
			/***********************************************************************/
			/* PROCEDURA DI ENGAGE DEI SISTEMI AUTOPILOTI                          */        
			
			if (control_engage==CSAS)
			{
				GetNumListItems (manualpanel, MANPANEL_CSASCHOISE, &num_option);
				
				if (num_option==0)
				{
					MessagePopup ("Attention!", "If SYS2 is turn off the autopilot can't be activated on Tail Servoactuator.");
				}
				else
				{
					// leggo quali CSAS attivare
					GetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_CTRL_VAL, &csas_option); 
				
					// alimenta solenoid valve degli autopiloti     
					//CsasPowerUp(csas_option);
					
					// impostazione oscillazione
					SetCsasPosition(STATIC, 0.0, 0.0, 0.1);
		
					Sleep(500);
		
					// attiva controllore PID del martinetto di controllo degli autopiloti
					switch ((int)csas_option)
					{
						case BOTH_SYS:
							SetEnableCsas1(YES);
							Sleep(200);
							SetEnableCsas2(YES);
							break;
						case ONLY_SYS1:
							SetEnableCsas1(YES);
							Sleep(200);
							SetEnableCsas2(NO);
							break;
						case ONLY_SYS2:
							SetEnableCsas1(NO);
							Sleep(200);
							SetEnableCsas2(YES);
							break;
					}
				}
			}
			/***********************************************************************/  
			
			// disabilita spegnimento
			SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, NO); 

			// engage del comando
			uut_control_engage=ON;
			
			break;
	}
	
Error:
	return error;
}


int CVICALLBACK CtrlUnEngageCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// disabilita comandi per controllo servocomando
			EnableMotionControlManualPanel(NO);
			
			// riporta a zero il valore della posizione
			SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, 0.0);

											
			/***********************************************************************/
			/* PROCEDURA DI DISENGAGE DEL MARTINETTO DI INPUT                      */        
			
			// impostazione oscillazione
			SetInputPosition(STATIC, 0.0, 0.0, 0.1);
	
			Sleep(100);
	
			// attiva controllore PID del martinetto di controllo della leva manuale
			SetEnableInput(NO);
		
			// togli alimentazione idraulica al martinetto di controllo della leva manuale
			SetSolenoidValve(EV07,CLOSE);
			
			Sleep(500);
			/***********************************************************************/
			/* PROCEDURA DI DISENGAGE DEI CSAS               				       */        
			
			// diseccita solenoid valve degli autopiloti		
			if (control_engage==CSAS)
				CsasShutDown();

			/***********************************************************************/               
	
			// reinizializza al valore -1 il tipo di controllo del servocomando
			control_engage = NOT_ENGAGE;

			// abilita la possibilita' di spegnimento
			if ( (control_engage==NOT_ENGAGE) && (load_engage==NOT_ENGAGE) )
				SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, YES);

			// Azzera valori dei controlli nel pannello
			SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, 0.1);
			SetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, STATIC);
			
			// engage del comando
			uut_control_engage=OFF;
			
			break;
	}
	
Error:
	return error;
}


int CVICALLBACK ChangeCtrlSignal (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int wave_type=0;
	double offset=0.0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// leggo tipo di segnale di controllo
			GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &wave_type);
			
			// acquisizione offset
			GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &offset);
			
			// abilita il comando di selezione dei CSAS
			switch (wave_type)
			{
				case STATIC:
					
					// disabilita controlli di ampiezza e frequenza
					SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_DIMMED, NO);		// ampiezza oscillazioni
					SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_DIMMED, NO);		// frequenza oscillazioni
					SetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, NO);		// inizio movimento
					SetCtrlAttribute (manualpanel, MANPANEL_STOPMOTIONBTN, ATTR_DIMMED, NO);		// inizio movimento		

					// azzera valori di ampiezza e frequenza
					SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, 0.0);	
					SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, 0.1);					
				
					break;
					
				default:
					
					// abilita controlli ampiezza e frequenza
					SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_DIMMED, YES);		// ampiezza oscillazioni
					SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_DIMMED, YES);		// frequenza oscillazioni
					SetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, YES);		// inizio movimento
					
					// azzera valori di ampiezza e frequenza
					SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, 0.0);	
					SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, 0.1);	
					
					break;
			}			

			if (control_engage==PILOT)
			{
				// impostazione oscillazione input manuale
				SetInputPosition(wave_type, offset, 0.0, 0.1);	
			}
			else if (control_engage==CSAS)
			{
				// impostazione oscillazione CSAS
				SetCsasPosition(wave_type, offset, 0.0, 0.1);
			}
			
			break;
	}
	return 0;
}


int CVICALLBACK ChangePositionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double offset;
	int wave_type;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione offset
			GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &offset);
			
			// acquisizione forma d'onda
			GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &wave_type); 

			// impostazione oscillazione
			if (control_engage==PILOT)
			{
				// variazione set point PID martinetto di input
				SetInputPosition(wave_type, offset, 0.0, 0.1);
			}
			else if (control_engage==CSAS)
			{
				// variazione set point PID di regolazione autopiloti CSAS
				SetCsasPosition(wave_type, offset, 0.0, 0.1);
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeAmplitudeCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double offset;
	double ampiezza;
	double frequenza;
	int wave_type;
	int Dimmed;
	
	switch (event)
	{
		case EVENT_COMMIT:

			GetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, &Dimmed);
			
			// Se il tasto di motion e' disabilitato allora regola ampiezza e frequenza di oscillazione
			if (Dimmed==NO)
			{
				// acquisizione offset
				GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &offset);
				// acquisizione forma d'onda
				GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &wave_type); 
				// acquisizione ampiezza massima di oscillazione
				GetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, &ampiezza);
				// acquisizione frequenza di oscillazione
				GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &frequenza); 
			
				if (control_engage==PILOT)
				{
					// impostazione oscillazione input manuale
					SetInputPosition(wave_type, offset, ampiezza, frequenza);  
				}
				else if (control_engage==CSAS)
				{
					// impostazione oscillazione CSAS
					SetCsasPosition(wave_type, offset, ampiezza, frequenza);  
				}
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeFrequencyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double offset;
	double ampiezza;
	double frequenza;
	int wave_type;
	int Dimmed;    
	
	switch (event)
	{
		case EVENT_COMMIT:

			GetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, &Dimmed);
			
			// Se il tasto di motion e' disabilitato allora regola ampiezza e frequenza di oscillazione
			if (Dimmed==NO)
			{
				// acquisizione offset
				GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &offset);
				// acquisizione forma d'onda
				GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &wave_type); 
				// acquisizione ampiezza massima di oscillazione
				GetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, &ampiezza);
				// acquisizione frequenza di oscillazione
				GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &frequenza); 
			
				if (control_engage==PILOT)
				{
					// impostazione oscillazione input manuale
					SetInputPosition(wave_type, offset, ampiezza, frequenza);  
				}
				else if (control_engage==CSAS)
				{
					// impostazione oscillazione CSAS
					SetCsasPosition(wave_type, offset, ampiezza, frequenza);  
				}
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK StartMotionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double offset;
	double ampiezza;
	double frequenza;
	int wave_type;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione offset
			GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &offset);
			// acquisizione forma d'onda
			GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &wave_type); 
			// acquisizione ampiezza massima di oscillazione
			GetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, &ampiezza);
			// acquisizione frequenza di oscillazione
			GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &frequenza); 
			
			if (control_engage==PILOT)
			{
				// impostazione oscillazione input manuale
				SetInputPosition(wave_type, offset, ampiezza, frequenza);  
			}
			else if (control_engage==CSAS)
			{
				// impostazione oscillazione CSAS
				SetCsasPosition(wave_type, offset, ampiezza, frequenza);  
			}

			// disabilita start e comandi di regolazione
			EnableMotionControlManualPanel(NO);
			
			// disabilita engage
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, NO);    
			SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);    
			
			// abilita stop
			SetCtrlAttribute (manualpanel, MANPANEL_STOPMOTIONBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_DIMMED, YES);
			
			break;
	}
	return 0;
}


int CVICALLBACK StopMotionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double offset;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione offset
			GetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, &offset);
			
			if (control_engage==PILOT)
			{
				// impostazione oscillazione input manuale
				SetInputPosition(STATIC, offset, 0.0, 0.1);       
			}
			else if (control_engage==CSAS)
			{
				// impostazione oscillazione CSAS
				SetCsasPosition(STATIC, offset, 0.0, 0.1);      
			}

			// abilita start e comandi di regolazione
			EnableMotionControlManualPanel(YES);
			
			// abilita unengage
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, NO);    
			SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);    
			
			// abilita stop
			SetCtrlAttribute (manualpanel, MANPANEL_STOPMOTIONBTN, ATTR_DIMMED, NO);
			
			break;
	}
	return 0;
}


int CVICALLBACK LoadEngageCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	
	switch (event)
	{
		case EVENT_COMMIT:
			// inizializza la variabile globale con il tipo di carico
			GetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_CTRL_VAL, &load_engage);

			switch (load_engage)
			{
				case LOAD:
					
					// Finestra con avvertimento engage 
					MessagePopup ("Attention!", "Oleodinamic load will be applied!");
					
					/************************************************/
					/* Connessione carico oleodinamico				*/
					/************************************************/
					
					// Procedura di engage carico oleodinamico
					SetPanelAttribute (manualpanel, ATTR_DIMMED, NO);
					Sleep(200);
					
					// @sz ogni procedura automatizzata di engage viene rimandata a successive versioni
					//ErrChk( LoadEngageProcedure() );
					
					// aggancia carico oleodinamico
					SetSolenoidValve(EV09A,CLOSE);	// valvola sgancio spenta
					Sleep(2000);
					SetSolenoidValve(EV09B,OPEN);	// valvola aggancio accesa
					
					SetPanelAttribute (manualpanel, ATTR_DIMMED, YES);
					
					/************************************************/
			
					// riporta a zero il valore del carico applicato
					SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 0.0);
					SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, YES); 
					SetCtrlAttribute (manualpanel, MANPANEL_APPLYLOADBTN, ATTR_DIMMED, YES);
					
					// abilita/disabilita comandi
					SetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_DIMMED, YES);
					SetCtrlAttribute (manualpanel, MANPANEL_STOPLOADBTN, ATTR_DIMMED, YES);
					
					SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, YES);
					SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, NO); 
					
					// disabilita la possibilita' di spegnimento
					SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, NO); 
					
					break;
					
				case INERTIAL:
			
					// Pannello con procedura/avvertimento del posizionamento del carico inerziale
					if (inertialconnectionpanel<0)
					{
						if ((inertialconnectionpanel = LoadPanel (0, "RAMCONNECTION.uir", INERCONPNL)) < 0)
						return -1;
					}
	
					// visualizza pannello con procedura manuale
					DisplayPanel (inertialconnectionpanel);
			
					SetPanelAttribute (manualpanel, ATTR_DIMMED, NO);
					
					// disabilita la possibilita' di spegnimento
					SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, NO); 
				
					// disabilita controlli
					SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, NO);

					// abilita controlli
					SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, YES);
					
					break;
				
				default:
					
					goto Error;
					
					break;
			}
			
			// engage del carico
			uut_load_engage=ON;
			
			// disabilita possibilita' di variare tipologia di carico
			SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, NO);     
				
			break;
	}
	
Error:
	return error;
}


int CVICALLBACK LoadUnEngageCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			switch (load_engage)
			{
				case LOAD:
					
					// Finestra con avvertimento engage 
					MessagePopup ("Attention!", "Oleodinamic load will be disconnected!");

					/************************************************/
					/* Disconnessione carico oleodinamico			*/
					/************************************************/

					// Procedura di engage carico oleodinamico
					SetPanelAttribute (manualpanel, ATTR_DIMMED, NO);
					Sleep(200);
					
					// @sz ogni procedura automatizzata di engage viene rimandata a successive versioni
					// ErrChk( LoadDisengageProcedure() );					
					
					// sgancia carico oleodinamico
					SetSolenoidValve(EV09B,CLOSE);	// valvola aggancio spenta
					Sleep(300);
					SetSolenoidValve(EV09A,OPEN);	// valvola sgancio accesa
					Sleep(3000);
					//SetSolenoidValve(EV09A,CLOSE);	// valvola sgancio spenta
					
					SetPanelAttribute (manualpanel, ATTR_DIMMED, YES);        
					
					/************************************************/
					
					// abilita comandi per l'applicazione del carico al servocomando 
					EnableLoadControlManualPanel(YES);
			
					SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, NO);   
					
					// abilita controlli scelta carico
					SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, YES);
					SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
					
					SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, YES); 
					SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, NO);
			
					// riporta a zero il valore del carico applicato
					SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 1.0);
			
					break;
				
				case INERTIAL:
					
					// Pannello con procedura/avvertimento del posizionamento del carico inerziale
					if (inertialconnectionpanel<0)
					{
						if ((inertialconnectionpanel = LoadPanel (0, "RAMCONNECTION.uir", INERCONPNL)) < 0)
						return -1;
					}
	
					// visualizza pannello con procedura manuale
					DisplayPanel (inertialconnectionpanel);
			
					SetPanelAttribute (manualpanel, ATTR_DIMMED, NO);
					SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, YES); 
					
					break;
					
				default:
					
					goto Error;
					
					break;	
			}		
			
			// abilita engage carico e tipologia
			SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, YES); 			
			SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);  
			
			// re-inizializza variabile globale
			load_engage = NOT_ENGAGE;
			// engage del carico
			uut_load_engage=OFF;
			    
			// abilita la possibilita' di spegnimento
			if ( (control_engage==NOT_ENGAGE) && (load_engage==NOT_ENGAGE) )
				SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, YES);
			
			break;
	}

Error:
	return error;
}


int CVICALLBACK ChangeLoadCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	double carico=0.0;
	int direzione=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione valore del carico
			GetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, &carico);
			
			// acquisizione verso di applicazione della forza
			GetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_CTRL_VAL, &direzione); 
			
			ErrChk( SetHydraulicLoad(direzione,carico) );
			
			break;
	}
	
Error:
	return error;
}


int CVICALLBACK ApplyLoadCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	int direzione=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			/************************************************/
			/* Applica carico oleodinamico					*/
			/************************************************/
			
			// acquisizione verso di applicazione della forza
			GetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_CTRL_VAL, &direzione); 
			
			// azzera il carico
			SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 0.0);
			ErrChk( SetHydraulicLoad(direzione,0.0) );

			// alimenta martinetto di carico
			SetSolenoidValve(EV08,OPEN);
			Sleep(200);   
			
			// abilita controllore di carico
			SetEnableLoad(YES);
			
			/************************************************/
			
			// disabilita comando di applicazione
			SetCtrlAttribute (manualpanel, MANPANEL_APPLYLOADBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_DIMMED, NO); 
			SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, NO);
			
			// abilita controllo di connessione del carico sul pistone del servocomando
			SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, YES);
					
			// abilita il comando di stop del carico
			SetCtrlAttribute (manualpanel, MANPANEL_STOPLOADBTN, ATTR_DIMMED, YES);   
			
			break;
	}
	
Error:
	return error;
}



int CVICALLBACK StopLoadCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	int direzione=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			/************************************************/
			/* Azzera carico oleodinamico					*/
			/************************************************/
			
			// acquisizione verso di applicazione della forza
			GetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_CTRL_VAL, &direzione); 
			
			// azzera il carico
			SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 0.0);
			ErrChk( SetHydraulicLoad(direzione,0.0) );
			
			// disabilita controllore di carico
			Sleep(100);
			SetEnableLoad(NO);
			Sleep(100);
		
			// togli alimentazione martinetto di carico
			SetSolenoidValve(EV08,CLOSE);

			/************************************************/
			/*
			if (uut_load_engage==ON)
			{
				// abilita comandi di impostazione carico
				EnableLoadControlManualPanel(YES);
			}
			else
			{
				// abilita possibilita' di commutare su carico inerziale
				SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, YES);     	
			}
			*/
			SetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_DIMMED, YES);
				
			// abilita comando di applicazione
			SetCtrlAttribute (manualpanel, MANPANEL_APPLYLOADBTN, ATTR_DIMMED, YES);   
			
			// disabilita il comando di stop del carico
			SetCtrlAttribute (manualpanel, MANPANEL_STOPLOADBTN, ATTR_DIMMED, YES);   
			
			// abilita disengage
			SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, YES);  
			
			break;
	}
	
Error:
	return error;
}

  
int CVICALLBACK PlotxyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello di Plot X-Y
			if (plotxypanel<0)
			{
				if ((plotxypanel = LoadPanel (0, "PLOTXY.uir", PLOTXYPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello Plot X-Y
			DisplayPanel (plotxypanel);  
			
			break;
	}
	return 0;
}


int CVICALLBACK PlotxCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello di Plot X
			if (plotxpanel<0)
			{
				if ((plotxpanel = LoadPanel (0, "PLOTX.uir", PLOTXPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello Plot X
			DisplayPanel (plotxpanel);
			
			// inizializza controlli e indicatori su pannello
			InitPlotXPanel();
			
			PlotxEnable=YES;
				
			break;
	}
	return 0;
}

int CVICALLBACK PlotyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello di Plot Y
			if (plotypanel<0)
			{
				if ((plotypanel = LoadPanel (0, "PLOTY.uir", PLOTYPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello Plot Y
			DisplayPanel (plotypanel);  
			// inizializza controlli e indicatori su pannello
			InitPlotYPanel();
			
			PlotyEnable=YES;
			
			break;
	}
	return 0;
}

int CVICALLBACK GetStrengthCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello misure di pressione
			if (strainpanel<0)
			{
				if ((strainpanel = LoadPanel (0, "STRAIN.uir", STRAINPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello misure pressioni
			DisplayPanel (strainpanel);
			
			// inizializzazione controlli e indicatori su pannello
			InitStrainPanel();
				
			PlotEffortEnable=YES;
			
			break;
	}
	return 0;
}


int CVICALLBACK GetPressureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello misure di pressione
			if (pressurepanel<0)
			{
				if ((pressurepanel = LoadPanel (0, "PRESSURE.uir", PRESSPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello misure pressioni
			DisplayPanel (pressurepanel);  
			
			break;
	}
	return 0;
}


int CVICALLBACK GetSolenoidCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello di caratterizzazione solenoid valve
			if (solenoidpanel<0)
			{
				if ((solenoidpanel = LoadPanel (0, "SOLENOID.uir", SOLENPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello caratterizzazione solenoidi
			DisplayPanel (solenoidpanel);  
			
			//reimpostazione degli offset di acquisizione			
			Characteristic[SYS1_S_SHUNT].offset = MeasuresAve[SYS1_S_SHUNT]/Characteristic[SYS1_S_SHUNT].conv_factor +Characteristic[SYS1_S_SHUNT].offset;
			Characteristic[SYS2_S_SHUNT].offset = MeasuresAve[SYS2_S_SHUNT]/Characteristic[SYS2_S_SHUNT].conv_factor +Characteristic[SYS2_S_SHUNT].offset;     

			// connetti ramo con resistenza di shunt 
			//SetEnableEvCurrent(YES); da chiarire
			
			break;
	}
	return 0;
}


int CVICALLBACK GetLvdtOutCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello di visualizzazione tensione LVDT
			if (lvdtpanel<0)
			{
				if ((lvdtpanel = LoadPanel (0, "LVDT.uir", LVDTOUTPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello Vout LVDT
			DisplayPanel (lvdtpanel);  
			
			// inizializza i controlli a pannello
			InitLvdtPanel();
			
			PlotCsasEnable=YES;
			
			break;
	}
	return 0;
}


int CVICALLBACK GetNullBiasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello per misure bias servovalvole
			if (biaspanel<0)
			{
				if ((biaspanel = LoadPanel (0, "SERVOBIAS.uir", BIASPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello bias servovalvole
			DisplayPanel (biaspanel);  
			
			//reimpostazione degli offset di acquisizione			
			Characteristic[CV_SYS1_SHUNT].offset = MeasuresAve[CV_SYS1_SHUNT]/Characteristic[CV_SYS1_SHUNT].conv_factor +Characteristic[CV_SYS1_SHUNT].offset;
			Characteristic[CV_SYS2_SHUNT].offset = MeasuresAve[CV_SYS2_SHUNT]/Characteristic[CV_SYS2_SHUNT].conv_factor +Characteristic[CV_SYS2_SHUNT].offset;     
			
			// connetti ramo con resistenza di shunt 
			// SetEnableNullBias(YES);
			
			break;
	}
	return 0;
}

int CVICALLBACK StopCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target ed Host
			EmergencyStop();
			
			break;
	}
	return 0;
}


 
int CVICALLBACK OpenPidTuningCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_RIGHT_DOUBLE_CLICK:		
			
			// carica pannello per misure bias servovalvole
			if (pidpanel<0)
			{
				if ((pidpanel = LoadPanel (0, "PIDCOEFFICIENT.uir", PIDCOEFPNL)) < 0)
					return -1;
			}
			
			// inizializza pannello di regolazione parametri PID
			InitPidPanel();
			
			// visualizza pannello bias servovalvole
			DisplayPanel (pidpanel);  
			
			break;
	}
	return 0;
}


int CVICALLBACK ChangeServoPicCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// inizializza la variabile globale con il tipo di carico
			GetCtrlAttribute (manualpanel, MANPANEL_PARTNUMBERRNG, ATTR_CTRL_VAL, &actuatorPN);
			
			if (actuatorPN==PN_3101)
			{
				SetCtrlAttribute (manualpanel, MANPANEL_SERVOPICRNG, ATTR_CTRL_VAL, 0);
				SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_MAX_VALUE, 8000.0);
			}
			else
			{
				SetCtrlAttribute (manualpanel, MANPANEL_SERVOPICRNG, ATTR_CTRL_VAL, 1);
				SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_MAX_VALUE, 30000.0); 
			}
			
			// se il P/N e' valido allora abilita l'accensione del banco
			if (actuatorPN!=-1)
				SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWERONBTN, ATTR_DIMMED, YES);
			else
				SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWERONBTN, ATTR_DIMMED, NO);
			
			break;
	}
	return 0;
}

int CVICALLBACK GetOilTemperatureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// carica pannello per misure bias servovalvole
			if (temperaturepanel<0)
			{
				if ((temperaturepanel = LoadPanel (0, "TEMPERATURE.uir", TEMPPNL)) < 0)
					return -1;
			}
			
			// visualizza pannello bias servovalvole
			DisplayPanel (temperaturepanel);  
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeLoadTypeCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int LoadType=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			GetCtrlAttribute(manualpanel, MANPANEL_LOADCHOISE, ATTR_CTRL_VAL, &LoadType);
			
			if (LoadType==INERTIAL)
			{
				SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, YES);
				
				// abilita comandi per l'applicazione del carico al servocomando 
				EnableLoadControlManualPanel(NO);
				
				SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, YES);
			}		
			else
			{
				// abilita comandi per l'applicazione del carico al servocomando 
				EnableLoadControlManualPanel(YES);
			
				// abilita controlli scelta carico
				SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, YES);
				SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, NO);
			
				SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
				SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, NO);
			
				// riporta a zero il valore del carico applicato
				SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 1.0);
			}
			
			break;
	}
	return 0;
}
