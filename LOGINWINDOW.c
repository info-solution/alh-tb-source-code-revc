#include <pwctrl.h>
#include <cvirte.h>		
#include <userint.h>
#include "LOGINWINDOW.h"

#include "global_var.h" 


int CVICALLBACK QuitProgramCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			SetStopProgram(HI);
			QuitUserInterface (0);
			break;
	}
	return 0;
}

int CVICALLBACK LoginCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char password[50]="";
	int profile=0;

	switch (event)
	{
		case EVENT_COMMIT:

			// leggo user e password
			GetCtrlAttribute (loginpanel, LOGINPANEL_RING, ATTR_CTRL_VAL, &profile);
			PasswordCtrl_GetAttribute (loginpanel, LOGINPANEL_PASSWORD, ATTR_PASSWORD_VAL, password);
			
			// verifica del login
			CheckLogin(profile,password);
			HidePanel (infopanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK GetInformation (int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_RIGHT_CLICK:
			DisplayPanel (infopanel); 
			break;
	}
	return 0;
}

int CVICALLBACK OkButtonCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			HidePanel (infopanel);
			break;
	}
	return 0;
}




