#include <ansi_c.h>
#include <userint.h>
#include "ATPPANEL.h"
#include "USERFLOW.h"

#include "global_var.h"    


int CVICALLBACK QuitUserPnlCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			HidePanel (userflowpanel);
			DisplayPanel (engpanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK StartUserFlowCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// riempimento dell'array PAT_flow[] di abilitazione dei test nel flusso di ATP/PAT
			GetCtrlAttribute (userflowpanel, USERPNL_TEST2BTN, ATTR_CTRL_VAL, &PAT_flow[1] );	// YES | NO Test di alimentazione elettrica
			GetCtrlAttribute (userflowpanel, USERPNL_TEST3BTN, ATTR_CTRL_VAL, &PAT_flow[2] );	// YES | NO Prova di pressione
			GetCtrlAttribute (userflowpanel, USERPNL_TEST4BTN, ATTR_CTRL_VAL, &PAT_flow[3] );	// YES | NO Corsa
			GetCtrlAttribute (userflowpanel, USERPNL_TEST5BTN, ATTR_CTRL_VAL, &PAT_flow[4] );	// YES | NO Sforzo Pilota
			GetCtrlAttribute (userflowpanel, USERPNL_TEST6BTN, ATTR_CTRL_VAL, &PAT_flow[5] );	// YES | NO Carico di stallo
			GetCtrlAttribute (userflowpanel, USERPNL_TEST7BTN, ATTR_CTRL_VAL, &PAT_flow[6] );	// YES | NO Velocita' di deriva
			GetCtrlAttribute (userflowpanel, USERPNL_TEST8BTN, ATTR_CTRL_VAL, &PAT_flow[7] );	// YES | NO anti-jamming
			GetCtrlAttribute (userflowpanel, USERPNL_TEST9BTN, ATTR_CTRL_VAL, &PAT_flow[8] );	// YES | NO velocita' senza carico
			GetCtrlAttribute (userflowpanel, USERPNL_TEST10BTN, ATTR_CTRL_VAL, &PAT_flow[9] );	// YES | NO threshold
			GetCtrlAttribute (userflowpanel, USERPNL_TEST11BTN, ATTR_CTRL_VAL, &PAT_flow[10] );	// YES | NO transient
			GetCtrlAttribute (userflowpanel, USERPNL_TEST12BTN, ATTR_CTRL_VAL, &PAT_flow[11] );	// YES | NO linearita' leva input
			GetCtrlAttribute (userflowpanel, USERPNL_TEST13BTN, ATTR_CTRL_VAL, &PAT_flow[12] );	// YES | NO isteresi leva input 
			GetCtrlAttribute (userflowpanel, USERPNL_TEST14BTN, ATTR_CTRL_VAL, &PAT_flow[13] );	// YES | NO risposta in frequenza leva input
			GetCtrlAttribute (userflowpanel, USERPNL_TEST15BTN, ATTR_CTRL_VAL, &PAT_flow[14] );	// YES | NO CSAS authority 
			GetCtrlAttribute (userflowpanel, USERPNL_TEST16BTN, ATTR_CTRL_VAL, &PAT_flow[15] );	// YES | NO null bias
			GetCtrlAttribute (userflowpanel, USERPNL_TEST17BTN, ATTR_CTRL_VAL, &PAT_flow[16] );	// YES | NO linearita' CSAS   
			GetCtrlAttribute (userflowpanel, USERPNL_TEST18BTN, ATTR_CTRL_VAL, &PAT_flow[17] );	// YES | NO isteresi CSAS 
			GetCtrlAttribute (userflowpanel, USERPNL_TEST19BTN, ATTR_CTRL_VAL, &PAT_flow[18] );	// YES | NO risposta in frequenza CSAS 
			GetCtrlAttribute (userflowpanel, USERPNL_TEST21BTN, ATTR_CTRL_VAL, &PAT_flow[20] );	// YES | NO leakage interno
			
			// nascondi pannello con menu Test
			HidePanel (userflowpanel);
			
			// carica pannello per funzioni di Testing manuale
			if (atppanel<0)
			{
				if ((atppanel = LoadPanel (0, "ATPPANEL.uir", ATPPNL)) < 0)
					return -1;
			}

			// inizializza i controlli del pannello manuale
			InitAtpPanel();
			
			// visualizza pannello di amministrazione
			DisplayPanel (atppanel);  
			
			
			break;
	}
	return 0;
}

int CVICALLBACK ClearFlowCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			// riazzera la lista dei test da eseguire nel flusso definito dall'utente
			SetCtrlAttribute (userflowpanel, USERPNL_TEST2BTN, ATTR_CTRL_VAL, 0);
			SetCtrlAttribute (userflowpanel, USERPNL_TEST3BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST4BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST5BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST6BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST7BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST8BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST9BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST10BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST11BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST12BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST13BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST14BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST15BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST16BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST17BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST18BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST19BTN, ATTR_CTRL_VAL, 0);	
			SetCtrlAttribute (userflowpanel, USERPNL_TEST21BTN, ATTR_CTRL_VAL, 0);	
			
			break;
	}
	return 0;
}
