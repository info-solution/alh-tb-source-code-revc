/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  BIASPNL                          1
#define  BIASPNL_QUITBTN                  2       /* callback function: QuitBiasCB */
#define  BIASPNL_DECORATION_3             3
#define  BIASPNL_DECORATION               4
#define  BIASPNL_TEXTMSG                  5
#define  BIASPNL_TEXTMSG_3                6
#define  BIASPNL_TEXTMSG_7                7
#define  BIASPNL_TEXTMSG_6                8
#define  BIASPNL_SERVO2AVENMR             9
#define  BIASPNL_SERVO1AVENMR             10


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK QuitBiasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
