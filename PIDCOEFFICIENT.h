/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2008. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PIDCOEFPNL                       1
#define  PIDCOEFPNL_DECORATION_2          2
#define  PIDCOEFPNL_PIDINPUTKCNMR         3       /* callback function: ChangeKcPidInputCB */
#define  PIDCOEFPNL_PIDINPUTTINMR         4       /* callback function: ChangeTiPidInputCB */
#define  PIDCOEFPNL_PIDINPUTTDNMR         5       /* callback function: ChangeTdPidInputCB */
#define  PIDCOEFPNL_PIDCSASKCNMR          6       /* callback function: ChangeKcPidCsasCB */
#define  PIDCOEFPNL_PIDCSASTINMR          7       /* callback function: ChangeTiPidCsasCB */
#define  PIDCOEFPNL_PIDCSASTDNMR          8       /* callback function: ChangeTdPidCsasCB */
#define  PIDCOEFPNL_TXT1                  9
#define  PIDCOEFPNL_DECORATION_3          10
#define  PIDCOEFPNL_PIDLOADKCNMR          11      /* callback function: ChangeKcPidLoadCB */
#define  PIDCOEFPNL_PIDLOADTINMR          12      /* callback function: ChangeTiPidLoadCB */
#define  PIDCOEFPNL_PIDLOADTDNMR          13      /* callback function: ChangeTdPidLoadCB */
#define  PIDCOEFPNL_TXT1_2                14
#define  PIDCOEFPNL_DECORATION_4          15
#define  PIDCOEFPNL_TXT1_3                16
#define  PIDCOEFPNL_QUITBUTTON            17      /* callback function: QuitPidTuningCB */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ChangeKcPidCsasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeKcPidInputCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeKcPidLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeTdPidCsasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeTdPidInputCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeTdPidLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeTiPidCsasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeTiPidInputCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeTiPidLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitPidTuningCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
