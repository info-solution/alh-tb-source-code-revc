#include <cvirte.h>		
#include <userint.h>
#include <windows.h> 
#include <ansi_c.h>

#include "MANUALTEST.h"
#include "STROKEPANEL.h"   

#include "global_var.h" 

/* Variabili con visibilita' file */
static int step_idx=0;
static double out_zeroin=0.0;


int CVICALLBACK NextPositionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char message[100];
	double deltaPosition = 0.0;
	
	switch (event)
	{
		case EVENT_COMMIT:
			
			if (step_idx==0)
			{
				out_zeroin = MeasuresAve[L2_MUX];
				SetCtrlAttribute (strokepanel, STROKEPNL_MESSAGETXT, ATTR_CTRL_VAL, "Set input control to the maximum value, then push <Next> button.");
			
				step_idx++;
			}
			else if (step_idx==1) 
			{
				end_stroke = MeasuresAve[L2_MUX];
				
				SetCtrlAttribute (strokepanel, STROKEPNL_MESSAGETXT, ATTR_CTRL_VAL, "Set input control to the minimum value, then push <Next> button." );	
			
				step_idx++;
			}
			else if (step_idx==2)
			{
				start_stroke = MeasuresAve[L2_MUX]; 
				stroke = fabs(end_stroke-start_stroke);
				
				// centro corsa
				input_center_stroke = (end_stroke+start_stroke)/2;
			
				// differenza di posizione tra sensore sul martinetto di input e centro corsa del servocomando
				deltaPosition = out_zeroin-input_center_stroke;
			
				sprintf(message, "Set input control to the displacement value of %.2f, then push <Next> button.", deltaPosition);
				SetCtrlAttribute (strokepanel, STROKEPNL_MESSAGETXT, ATTR_CTRL_VAL, message);
				
				step_idx++;   
			}
			else if (step_idx==3)
			{
				// Modifica Offset di misura della posizione del main piston
				Characteristic[L2_MUX].offset = ( MeasuresAve[L2_MUX] / Characteristic[L2_MUX].conv_factor ) + Characteristic[L2_MUX].offset;

				// Modifica Offset di misura della posizione del main piston
				Characteristic[L1_MUX].offset = ( MeasuresAve[L1_MUX] / Characteristic[L1_MUX].conv_factor ) + Characteristic[L1_MUX].offset;
				Characteristic[INPUT_MUX].offset = ( MeasuresAve[INPUT_MUX] / Characteristic[INPUT_MUX].conv_factor ) + Characteristic[INPUT_MUX].offset;
			
				sprintf(message, "Procedure completed... Stroke is %.2f", stroke);
				MessagePopup ("Stroke centering", message);
				
				step_idx=0;
							
				// nascondi pannello per caratterizzazione solenoidi
				DiscardPanel (strokepanel);
				strokepanel=-1000;
			}
				
			break;
	}
	return 0;
}


int CVICALLBACK ResetPositionCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			start_stroke = -1000;
			end_stroke = -1000;
			stroke = -1000;
			
			Characteristic[L1_MUX].offset=5.0;
			Characteristic[L2_MUX].offset=5.0;
			Characteristic[INPUT_MUX].offset=5.0; 
	
			MessagePopup ("Stroke centering", "Displacement offsets are resetted to default values.");     
			
			// nascondi pannello per caratterizzazione solenoidi
			DiscardPanel (strokepanel);
			strokepanel=-1000;
			
			break;
	}
	return 0;
}	
	
	

