/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  STROKEPNL                        1
#define  STROKEPNL_RESETBTN               2       /* callback function: ResetPositionCB */
#define  STROKEPNL_NEXTBTN                3       /* callback function: NextPositionCB */
#define  STROKEPNL_MESSAGETXT             4


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK NextPositionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ResetPositionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
