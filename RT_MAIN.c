
#include <windows.h>
#include <ansi_c.h>
#include <userint.h>
#include <rtutil.h>
#include <utility.h>
#include <cvirte.h>
#include "NIDAQmx.h"
#include "HW_Description.h"
#include "error_table.h"
#include "pid_common.h"


/* Routine di verifica errori HW scheda DAQmx */
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else


/* CallBack Thread Functions */
int ControllerCB (void *functionData);
int PresentationCB (void *functionData);

extern int CVIFUNC RTIsShuttingDown (void);


/****************************************************************************/
/*																			*/
/*							REAL TIME  M A I N								*/
/*																			*/
/****************************************************************************/
/*																			*/
/* All'interno della routine RTmain vengono unicamente definiti i thread a	*/
/* differente priorita', le TSQ di comunicazione inter-thread, e vengono	*/
/* allocati i microprocessori del controller dual-core sui differenti 		*/
/* thread paralleli.														*/
/*																			*/
/****************************************************************************/

void CVIFUNC_C RTmain (void)
{
	int error=0;
	
	if (InitCVIRTE (0, 0, 0) == 0)
		return;    /* out of memory */
	
	/*------------------------------------------------------------------*/
	/* INIZIALIZZAZIONE HARDWARE										*/
	/*------------------------------------------------------------------*/
	// Inizializzazione variabili in ambiente Real Time
	InitVar();
		
	// Procedura di auto-calibrazione delle schede di I/O
	ErrChk( ExecCalibration() );

	// Inizializzazione task DAQ analogici e digitali
	ErrChk( InitTask() );
	
	// Inizializzazione canali di I/O digitale
	ErrChk( InitDIO() );
	
	// Inizializzazione canali di conteggio
	ErrChk( InitCNT() );

	// Inizializzazione canali di input analogico
	ErrChk( InitAIN() );
	
	// Inizializzazione canali di condizionamento LVDT
	ErrChk( InitLvdt() );
	
	// Inizializzazione canali di condizionamento Strain gauge
	ErrChk( InitBridge() );
	
	// Inizializzazione canali di output analogico
	ErrChk( InitAOUT() );
	
	// Inizializzazione sincronismi tra I/O analogici
	ErrChk( InitSync() );
	
			// Procedura di auto-zero ponti
	ErrChk( ExecNulling() );
	/*------------------------------------------------------------------*/
	
	
	/*------------------------------------------------------------------*/
	/* Creazione Thread Safe Queue di comunicazione tra thread			*/
	/*------------------------------------------------------------------*/
	
	CmtNewTSQ (500*NUM_MEAS_IN, sizeof(double), OPT_TSQ_AUTO_FLUSH_EXACT, &gTsqDataIn);		// TSQ analog input  	
	CmtNewTSQ (1000, sizeof(double), OPT_TSQ_AUTO_FLUSH_EXACT, &gTsqDataOut);				// TSQ analog output

	/*------------------------------------------------------------------*/
	/* Creazione di un pool diverso da quello su cui gira la RTmain		*/
	/*------------------------------------------------------------------*/
	CmtNewThreadPool (4, &gPoolHandleRT);											// dichiaro nuovo thread pool di massimo 4 elementi

	/*------------------------------------------------------------------*/
	/* Prima esecuzione	delle thread functions per	l'assegnazione		*/
	/* del loro thread ad uno specifico microprocessore.				*/
	/*------------------------------------------------------------------*/
	ControllerCB((void*)1);
	PresentationCB((void*)1);	
	
	/*------------------------------------------------------------------*/
	/* Scheduling dei Thread											*/
	/*------------------------------------------------------------------*/
		// Viene schedulata la routine di gestione della comunicazione Host - RTtarget + sicurezza
	CmtScheduleThreadPoolFunctionAdv (gPoolHandleRT, PresentationCB, NULL, THREAD_PRIORITY_NORMAL, NULL,
									  EVENT_TP_THREAD_FUNCTION_END, NULL, RUN_IN_SCHEDULED_THREAD, &gCommunicationThreadID);
	Sleep(2000);
	
	// Viene schedulata la routine di gestione dei controlli analogici
	CmtScheduleThreadPoolFunctionAdv (gPoolHandleRT, ControllerCB, NULL, THREAD_PRIORITY_HIGHEST, NULL,
									  EVENT_TP_THREAD_FUNCTION_END, NULL, RUN_IN_SCHEDULED_THREAD, &gAnalogThreadID);

	printf ("RTMAIN STARTED\n");
	while ( (!RTIsShuttingDown ())&&(QuitProgram==0) )
	{
		ProcessSystemEvents ();

		/* sleep for some amount of time to give the desired loop rate */
		/* if you do not sleep lower priority threads will never run   */
		Sleep (1000);
	}

	/* The End */
Error:
	StopHwProgram();
	return;
}

/****************************************************************************/
/****************************************************************************/



/* ------------------------------------------------------------------------------------ */
/* Callback Thread Function	su cui girano le acquisizioni analogiche ed i controllori	*/
/* PID. Questo Thread viene assegnato al uP n.1 del controller PXI-8106.			  	*/
/* ------------------------------------------------------------------------------------ */

int CVICALLBACK ControllerCB (void *functionData)
{
	
	// Alla prima chiamata assegna l'esecuzione di questa Thread Function al microprocessore n.1 (0-based)
	if ((int*)functionData)
	{
		SetProcessorAffinityForThread (kProcessorPool_None, 1, NULL, NULL);
		printf ("CONTROLLER STARTED FAKE\n");
		return 1;
	}
	printf ("CONTROLLER STARTED\n");
	/************************************************************/
	/*			Loop infinito di acquisizione e controllo		*/
	/************************************************************/
	RunPid();
	/************************************************************/
	
	return 1;
}



/* ------------------------------------------------------------------------------------ */
/* Callback Thread Function	su cui girano i controlli digitali del banco ed i monitor	*/
/* di sicurezza. Questo Thread viene assegnato al uP n.0 del controller PXI-8106.		*/
/* ------------------------------------------------------------------------------------ */

int CVICALLBACK PresentationCB (void *functionData)
{   
	int error = 0;

	// Alla prima chiamata assegna l'esecuzione di questa Thread Function al microprocessore n.0 (0-based)
	if ((int*)functionData)
	{
		SetProcessorAffinityForThread (kProcessorPool_None, 0, NULL, NULL);
		printf ("PRESENTATION STARTED FAKE\n");
		return 1;
	}
	printf ("PRESENTATION STARTED\n");
	/************************************************************/
	/*			Connessioni e gestione eventi					*/
	/************************************************************/
	error = InitNetworkConnection();
	/************************************************************/
	
	/************************************************************/
	/*			Polling di interrogazione ingressi digitali		*/
	/************************************************************/
	RunDigitalControl();
	/************************************************************/
	return 1;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di uscita standard dal programma												*/
/* ------------------------------------------------------------------------------------ */

int StopHwProgram(void)
{
	/****************************************************************/
	/* Ferma e chiudi task				  							*/
	/****************************************************************/
	printf ("STOPHWPROGRAM\n");
	CloseTask();
	
	/****************************************************************/
	/* Chiudi controllori PID			  							*/
	/****************************************************************/
	
	ClosePid (CSAS1_PID);
	ClosePid (CSAS2_PID);    
	ClosePid (INPUT_PID);    
	ClosePid (LOAD_PID);    
	
	/****************************************************************/
	/* Scarica Thread Function e Pool	  							*/
	/****************************************************************/
	
	CmtWaitForThreadPoolFunctionCompletion (gPoolHandleRT, gCommunicationThreadID, 0);		// si attende l'esecuzione del thread di controllo digitale
																						// poiche' e' quello che soprassiede ai comandi di sicurezza
	CmtReleaseThreadPoolFunctionID (gPoolHandleRT, gAnalogThreadID);
	CmtReleaseThreadPoolFunctionID (gPoolHandleRT, gCommunicationThreadID);
	
	CmtDiscardThreadPool (gPoolHandleRT);
	
	/****************************************************************/
	/* Scarica TSQ di comunicazione inter-thread					*/
	/****************************************************************/
	
	CmtUninstallTSQCallback (gTsqDataIn, SendMeasureQueueCB);
	CmtDiscardTSQ (gTsqDataIn);
	CmtDiscardTSQ (gTsqDataOut);
	
	/****************************************************************/
	/* Scarica variabili nework e connessioni di rete verso l'Host	*/
	/****************************************************************/
	
	ReleaseConnections();
	
	/****************************************************************/
	/* Chiudi Real-Time Engine										*/
	/****************************************************************/
	
	CloseCVIRTE ();
	
	return PASS;
}


