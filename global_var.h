
// ************************************************************
// ************************************************************
// ******************                         *****************
// *****************  ELETTRONICA ASTER S.P.A. ****************
// ******************                         *****************
// ************************************************************
// **************************************************** (c)2008
//			 
//                         R. A. T. B.
//
//                  Rotor Actuator Test Bench
//			
// Program Manager:
//				
//		@gv		Giuseppe Vigo			giuseppe.vigo@elaster.it
//
// SW & HW Development:
//
//		@gm		Gianantonio Moretto		gianni.moretto@elaster.it
//		@jc		John Cervero 			john.cervero@elaster.it
//		@sz		Simone Zaccardi 		simone.zaccardi@elaster.it	
//
//
// ************************************************************
//
// 					Elettronica Aster S.p.A.
// 					Web http://www.elaster.it
// 					Tel. +39.0362.5681
// 					Fax  +39.0362.561109
//
// ************************************************************
// 
// 13/05/2008:
//		- inizio programmazione HMI
// 20/05/2008:
//		- ok pannello manuale + pannelli misure
//		- ok pannello cycling e lvdt
//		- ok pannelli di calibrazione meccanica e elettrica
// 26/05/2008
//		- ok tool per la creazione dei file password .zak
//		- ok caricamento psw da esterno
//		- ok porting #define da .doc a global_var.h
// 09/06/2008
//		- ok per pannelli ATP
// Jun-Jul
//		- studio e sperimentazione controllo macchina RT
// 08/10/2008
//		- inizio scrittura routine "control level" su RT Target
//		- inizio scrittura routine "presentation level" su RT Target
//		- comunicazione networking tra RT Target e PC Host
// 24/10/2008
//		- terminata prima versione programma RT Target
//		- inizio scrittura routine "presentation level" su Host
// 30/10/2008
//		- Debug acquisizone e comunicazione segnali analogici verso Host
//		- Aggiunte funzioni di datalogger
//		- Debug chiusura programma
//		- Visualizzazione misure di pressione sui relativi controlli dei vari pannelli
// 20/11/2008
//		- Debug generazione forme d'onda
//		- Modifica pannello per test manuale
//		- Engage manuale del carico
//		- Inserimento foto per carico inerziale
//		- Debug pannello di calibrazione meccanica
//
// Da fare:
//		 pannello per self test
//		 pannello per calibrazione
//		 pannello report viewer
//		 pannello statistics
//
// ************************************************************















// ************************************************************
//
// FILES ESTERNI DI CONFIGURAZIONE
//
// ************************************************************

#define PASSWORD_FILE		"ratb.zak"			// contiene password
#define CONFIGURATION_FILE	"ratb.ini"			// configurazione di sistema
#define ITALIAN_FILE 		"it_user.ini"		// label in italiano
#define ENGLISH_FILE 		"en_user.ini"		// label in inglese




// ************************************************************
//
// PROTOTIPI FUNZIONI
//
// ************************************************************

void InitVariables(void);
int CheckLogin(int profile,char *psw);
int EnableTestOption(void);
int PrintLog (char *messaggio);
int LoadNetConfigFile(void);
int LoadSensorsConfigFile(void);   

// inizializzazione interfacce grafiche
int InitManualPanel(void);
int InitMechCalibrationPanel(void);
int InitStressPanel(void);
void InitAtpPanel(void);
void InitUserFlowPanel(void);
void InitPlotXPanel(void);
void InitPlotYPanel(void);
void InitLvdtPanel(void);
void InitStrainPanel(void);
void InitPidPanel(void);
void InitStrokePanel(void);

// gestione testing manuale
int EnableMotionControlManualPanel(int option);
int EnableLoadControlManualPanel(int option);

// gestione account
int ValidatePswChar(int evento1, int evento2);
int ChangePassword(int profilo, char *password_old, char *password_new, char *password_confirm);
int LoadPswFile(char *psw_oper,char *psw_eng,char *psw_admin);
int SavePswFile(char *psw_oper,char *psw_eng,char *psw_admin);


// ------------------------------------------------------------
// Routine di livello comunicazione
// ------------------------------------------------------------

int InitNetworkConnection_PC(void); 

int SetStopProgram(int stop_flag);
int SetEmergency(int emergency_flag);

int SetEnableCsas1(int enable_flag); 
int SetEnableCsas2(int enable_flag);
int SetEnableInput(int enable_flag);
int SetEnableLoad(int enable_flag);
int SetEnableUutVoltage(int enable_flag);
int SetEnableUutPressure(int enable_flag);
int SetEnableNullBias(int enable_flag);
int SetEnableEvCurrent(int enable_flag);

int SetEV01(int status_flag);
int SetEV02(int status_flag);
int SetEV03(int status_flag); 
int SetEV04A(int status_flag);
int SetEV04B(int status_flag); 
int SetEV05(int status_flag);
int SetEV06(int status_flag);
int SetEV07(int status_flag);
int SetEV08(int status_flag);
int SetEV09A(int status_flag);
int SetEV09B(int status_flag);

//int SetSolenoid1(int status_flag);
//int SetSolenoid2(int status_flag);        

int SetCsasSetpoint(double setpoint_value);
int SetInputSetpoint(double setpoint_value);
int SetLoadSetpoint(double setpoint_value);  
int SetUutPressureSetpoint(double setpoint_value);
int SetUutVoltageSetpoint(double setpoint_value);

int SetWaveType(int option_value);
int SetInputWaveAmplitude(double amplitude_value);
int SetCsasWaveAmplitude(double amplitude_value);         
int SetWaveFrequency(double frequency_value); 

int SetLoadDirectionType(int option_value);

int SetCsasPid(double* pid_data);
int SetInputPid(double* pid_data);
int SetLoadPid(double* pid_data);

void InitPidParameter(void);
int InitSensorsAcquisition(void);


// ------------------------------------------------------------
// Routine di livello presentazione
// ------------------------------------------------------------

void EmergencyStop(void);
int RatbPowerUp(void);
int RatbShutDown(void);
int UutPowerUp(int option);
int UutShutDown(void);
//int CsasPowerUp(int option);
int CsasShutDown(void);

int GetStatus (int transducer_id, int *value);
												  
int SetSolenoidValve(int solenoid_id, int value);
int SetInputPosition (int type, double offset, double amplitude, double frequency);
int SetCsasPosition (int type, double offset, double amplitude, double frequency);
int SetHydraulicLoad (int direction, double load_value);
int SetUutPressure (int type, double delivery_pressure_value, int return_pressure_value);

int SetLeakageCondition(int option);
int LoadEngageProcedure(void);
int LoadDisengageProcedure(void);


// ************************************************************
//
// HANDLER PANNELLI
//
// ************************************************************

/* i puntatori ai pannelli vengono inizializzati tutti a valori negativi */
int loginpanel;				// pannello di Login
int infopanel; 				// pannello di informazioni richiamabile dal pannello di Login
int adminpanel;				// pannello con funzioni di amministratore di sistema
int engpanel;				// pannello con menu opzioni di testing
int datalogpanel;			// pannello con datalog delle operazioni
int atppanel;				// pannello con procedura di Acceptance Test Procedure

int mechcalibrationpanel;	// pannello per la calibrazione meccanica (bilanciamento pressioni camera e ciclaggio)
int elecalibrationpanel;	// pannello per la calibrazione meccanica-elettrica dei sensori LVDT a bordo dei CSAS in fase di assemblaggio
int stresspanel;			// pannello per le prove di endurance
int userflowpanel;			// pannello per la definizione del flusso custom desiderato dall'utente  

int manualpanel;			// pannello per testing manuale
int plotxpanel;				// pannello per Plot X (spostamento del controllo servocomando)
int plotypanel;				// pannello per Plot Y (spostamento del main piston servocomando)
int plotxypanel;			// pannello per Plot X-Y (spostamento delle caratteristiche x-y servocomando)
int pressurepanel;			// pannello misure di pressione banco e servocomando
int strainpanel;			// pannello misure di sforzo sul main piston e sulla leva di input
int solenoidpanel;			// pannello per caratterizzazione I-V dei solenoidi a bordo servocomando
int lvdtpanel;				// pannello per misure di tensione di uscita LVDT a bordo servocomando
int biaspanel;				// pannello per misura di bias delle servovalvole a bordo del servocomando
int pidpanel;				// pannello per la regolazione dei coefficienti PID dei controllori per input, CSAS e carico
int temperaturepanel;		// pannello per la misura della temperatura dell'olio sulla condotta di ritorno dal servocomando
int strokepanel;			// pannello per l'acquisizione della corsa del servocomando sotto test
int inputeffortpanel;		// pannello per la misura dello sforzo di input

int inputconnectionpanel;		// pannello per indicazioni sulla connessione manuale del martinetto di input alla leva del servocomando
int outputconnectionpanel;		// pannello per indicazioni sulla connessione manuale dell'occhiello del main piston del servocomando allo stelo del banco di collaudo
int inertialconnectionpanel;	// pannello per indicazioni sulla connessione manuale dei pesi di carico inerziali

/* puntatori ai tab presenti nel pannello di ATP "atppanel" */
int init_atppanel;
int inspection_atppanel;
int elepower_atppanel;
int hydpower_atppanel;
int stroke_atppanel;
int effort_atppanel;
int stall_atppanel;
int drift_atppanel;
int jamming_atppanel;
int noloadspeed_atppanel;
int threshold_atppanel;
int transient_atppanel;
int pilotlin_atppanel;
int pilothys_atppanel;
int pilotfreq_atppanel;
int authority_atppanel;
int nullbias_atppanel;
int csaslin_atppanel;
int csashys_atppanel;
int csasfreq_atppanel;
int extleakage_atppanel;
int intleakage_atppanel;
int notes_atppanel;


// ************************************************************
//
// COSTANTI SIMBOLICHE
//
// ************************************************************


// #define DEBUG


// ------------------------------------------------------------
// Descrizione dei Test
// ------------------------------------------------------------

#define LOG_IN					0	// Step di accesso - riconoscimento utente
#define SELF_TEST				1	// Self-Test del banco
#define CALIBRE_TEST			2	// Calibrazione strumenti del banco

#define VISUAL_INSPECTION		3	// Inspezione del servocomando
#define HYD_POWER_UP_TEST		4	// Alimentazione idraulica della UUT (pressione funzionale e over-pressure)
#define ELE_POWER_UP_TEST		5	// Alimentazione elettrica della UUT (check solenoid valve)
#define STROKE_TEST				6	// Misura della corsa dal servocomando
#define NO_LOAD_SPEED_TEST		7	// Test di velocitÓ massima di spostamento del main piston senza carico applicato.
#define PILOT_EFFORT_TEST		8	// Misura dello sforzo necessario al pilota per muovere la leva di input del servocomando in condizioni operative normali e jammed.
#define STALL_LOAD_TEST			9	// Misura del carico di stallo del servocomando
#define DRIFT_SPEED_TEST		10	// Misura della velocitÓ di deriva del servocomando al variare del carico sull'output
#define INT_LEAKAGE_TEST		11	// Misura del leakage interno al servocomando
#define EXT_LEAKAGE_TEST		12	// Check visuale del leakage esterno all'apparato
#define PILOT_LINEARITY_TEST	13	// Test di linearitÓ tra lo spostamento della leva di ingresso e lo spostamento del main piston
#define CSAS_LINEARITY_TEST		14	// Test di linearitÓ tra il segnale elettrico di controllo dei CSAS e lo spostamento del main piston
#define PILOT_HYSTERESIS_TEST	15	// Test di isteresi tra lo spostamento della leva di ingresso e lo spostamento del main piston
#define CSAS_HYSTERESIS_TEST	16	// Test di isteresi tra il segnale elettrico di controllo dei CSAS e lo spostamento del main piston
#define THRESHOLD_TEST			17	// Misura della spostamento della leva di ingresso tale da generare uno spostamento dell'output misurabile
#define PILOT_TRANSIENT_TEST	18	// Rilevazione della risposta del sistema a sollecitazioni a scalino sulla leva di ingresso
#define CSAS_TRANSIENT_TEST		19	// Rilevazione della risposta del sistema a sollecitazioni a scalino in ingresso ai CSAS
#define PILOT_F_RESPONSE_TEST	20	// Misura della risposta in frequenza del servocomando con controllo manuale
#define CSAS_F_RESPONSE_TEST	21	// Misura della risposta in frequenza del servocomando con controllo automatico dei CSAS
#define ANTI_JAMMING_TEST		22	// Test di carico con uno dei due sistemi ridondanti in condizioni di jamming
#define ELE_SETTING_TEST		23	// Check delle condizioni di eccitazione degli LVDT a bordo della UUT
#define CSAS_AUTHORITY_TEST		24	// Misura dello spostamento del main piston con i pistoncini dei CSAS a fondo corsa e relative tensioni dei CSAS
#define NULL_BIAS_TEST			25	// Misura della corrente di 0 delle servovalvole a bordo delle UUT

#define CYCLING_TEST			26	// Test di ciclatura della UUT
#define LVDT_ASSY_TEST			27	// Test di assiemaggio e taratura degli LVDT a bordo della UUT
#define UUT_CALIBRE_TEST		28	// Test di calibrazione meccanica della UUT

#define N_TEST_ATP				21	// Numero di Test compresi nelle procedura di ATP
#define N_TEST_PAT				21	// Numero di Test compresi nelle procedura di PAT


// ------------------------------------------------------------
// Costanti fisiche
// ------------------------------------------------------------

#define UUT_DELIVERY_PRESSURE			206.0		// Pressione nominale di lavoro [bar]
#define UUT_SOLENOID_VOLTAGE			28.0		// Tensione nominale di lavoro delle elettrovalvole [V]
#define PID_RATE						250.0		// Frequenza di campionamento [Hz]
#define DT								1/250.0		// Periodo di campionamento [sec]
#define MAX_LOAD						30000.0		// Valore massimo del carico oleodinamico [N]


// ------------------------------------------------------------
// Descrizione pannelli HMI
// ------------------------------------------------------------

#define NUM_CTRL_MANUALPNL 		56
#define NUM_CTRL_STRESSPNL 		22


// ------------------------------------------------------------
// Variabili simboliche di utilita' generale
// ------------------------------------------------------------


/* general setup */
#define OPERATOR				0		// Utente con privilegi da operatore
#define ENGINEER				1		// Utente con privilegi da ingegnere
#define ADMINISTRATOR			2		// Utente con privilegi da amministratore di sistema

#define IT						0		// Lingua italiana
#define EN						1		// Lingua inglese

#define PN_ALL					0		// Qualsiasi Part Number
#define PN_3101					1		// Part Number 3101 (Tail)
#define PN_3102					2		// Part Number 3102 (Pitch)
#define PN_3103					3		// Part Number 3103 (Roll)
#define PN_3104					4		// Part Number 3104 (Collective)

#define MRA						0		// Main Rotor Actuator
#define TRA						1		// Tail Rotor Actuator

#define NU						-1		// Not Used  

/* binary */
#define YES						0
#define NO						1

#define OFF						0
#define ON						1

#define OK						0		// Operazione consentita
#define STOP					1		// Operazione non ammissibile

#define HI						1		// Stato logico 1
#define LW						0		// Stato logico 0

#define OPEN					1		// Valvola aperta (consente il flusso)
#define CLOSE					0		// Valvola chiusa (impedisce il flusso)

#define CLEAR					0		// Filtro pulito
#define CLOGGED					1		// Filtro intasato

#define DISCONNECT				0		// Azione di sgancio
#define CONNECT					1		// Azione di aggancio

/* control & load */
#define BOTH_SYS				0		// entrambi i sistemi
#define ONLY_SYS1				1		// solo sistema 1
#define ONLY_SYS2				2		// solo sistema 2

#define PILOT					0		// Controllo del servocomando in modalitÓ pilota manuale
#define CSAS					1		// Controllo del servocomando in modalitÓ pilota automatico
#define LOAD					2		// Connessione carico oleodinamico
#define INERTIAL				3		// Connessione carico inerziale

#define INPUT					1		// Connessione azionamento manuale
#define OUTPUT					2		// Connessione main piston

#define NOT_ENGAGE				-1		// non connesso

#define STATIC					1		// controllo di posizione in un punto fisso
#define SINE					2		// controllo di posizione con set point secondo forma d'onda sinusoidale
#define SQUARE					3		// controllo di posizione con set point secondo forma d'onda quadrata
#define TRIANGULAR				4		// controllo di posizione con set point secondo forma d'onda triangolare

#define COMPRESSION				0		// carico oleodinamico in compressione sul main piston
#define EXTENSION				1		// carico oleodinamico in estensione sul main piston

#define FIXED					2		// indica se un valore deve essere prefissato (es. pressioni operative)
#define VARIABLE				1		// indica se lo stesso valore puo' essere variato dall'utente
#define POWER_OFF				0		// indica se la grandezza fisica di regolazione e' ad un valore di "riposo"

/* test variables */
#define SET_INPUT_POSITION		0		// Controllo della posizione/velocitÓ della leva di input
#define SET_INPUT_STRAIN		1		// Controllo della forza sulla leva di ingresso
#define SET_CSAS_POSITION		2		// Controllo della posizione degli CSAS
#define SET_OUTPUT_STRAIN		3		// Controllo del carico sul main piston
#define SET_INERTIAL_STRAIN		4		// Controllo del carico inerziale
#define GET_LEAKAGE				5		// Lettura del leakage interno

#define LOAD_APPLY				1		// Applicazione di un determinato carico imposto dal martinetto idraulico
#define SPEED_UNDER_LOAD		2		// Determinazione del carico che frena il servocomando ad una determinata velocita'
#define DRIFT_SPEED				3		// Impostazione di un carico uguale o superiore a quello di stallo del servocomando

#define CYCLES_DURATION			0		// durata di ciclatura espressa in numero di cicli
#define TIME_DURATION			1		// durata di ciclatura espressa in minuti

#define ACCEPTANCE_FLOW			0		// Flusso di testing predefinito di accettazione
#define PRODUCTION_FLOW			1		// Flusso di testing predefinito di produzione
#define USER_FLOW				2		// Flusso di testing definito dall'utente attuale

/* analisys variables */
#define ALL						1		// Qualsiasi risultato (PASS o FAIL)
#define PASS					0		// Routine eseguita correttamente o test passato
#define FAIL					-1		// Routine in errore o test fallito
#define HARD_BIN				0		// Classificazione per test falliti
#define SOFT_BIN				1		// Classificazione per singola causa di errore

#define HISTOGRAM				0		// Grafico a istogramma
#define PARETO					1		// Grafico di Pareto
#define LIN						0		// Scala lineare
#define LOG						1		// Scala logaritmica

#define CHRONO					0		// Ordinamento cronologico
#define ALH_SN_INC				1		// Ordinamento per SN crescente
#define ALH_SN_DEC				2		// Ordinamento per SN decrescente




// ------------------------------------------------------------
// Elenco elettrovalvole
// ------------------------------------------------------------

#define EV01	5	// elettrovalvola 2 vie
#define EV02	6	// elettrovalvola 2 vie
#define EV03	7	// elettrovalvola 2 vie
#define EV04A	8	// elettrovalvola 4 vie
#define EV04B	9	// elettrovalvola 4 vie
#define EV05	10	// elettrovalvola 2 vie
#define EV06	11	// elettrovalvola 2 vie
#define EV07	12	// elettrovalvola 2 vie
#define EV08	13	// elettrovalvola 2 vie
#define EV09A	14	// elettrovalvola 2 vie
#define EV09B	15	// elettrovalvola 2 vie


// ------------------------------------------------------------
// Ordine canali in Multiplexing verso l'ADC
// ------------------------------------------------------------

#define P02_MUX			0
#define P03_MUX			1
#define P04_MUX			2
#define P05_MUX			3
#define P06_MUX			4
#define P07_MUX			5
#define P08_MUX			6
#define P09_MUX			7
#define P10_MUX			8
#define P11_MUX			9
#define K1_MUX			10
#define PSU3_MUX		11  
#define L1_MUX			12
#define L2_MUX			13
#define CV_SYS1_SHUNT	14	//per corrente di SV SYS1
#define CV_SYS2_SHUNT	15  //per corrente di SV SYS2 
#define SYS1_S_SHUNT	16  //per corrente solenoide SYS1
#define SYS2_S_SHUNT	17  //per corrente solenoide SYS2

#define F1_MUX			18
#define LVDT1_MUX		19
#define LVDT2_MUX		20
#define N1_MUX			21
#define N2_MUX			22
#define INPUT_MUX		23
#define CSAS_MUX		24
#define LOAD_MUX		25

#define F2_MUX			26

#define NUM_CH_MUX		26


// ------------------------------------------------------------
// Stato di attuatori e sensori "digitali" (EV, proximity e FC)
// ------------------------------------------------------------

#define EV01_STATUS		0
#define EV02_STATUS		1
#define EV03_STATUS		2
#define EV04A_STATUS	3		
#define EV04B_STATUS	4		
#define EV05_STATUS		5
#define EV06_STATUS		6
#define EV07_STATUS		7
#define EV08_STATUS		8
#define EV09A_STATUS	9		// valvola sgancio
#define EV09B_STATUS	10		// valvola aggancio

#define FC1_STATUS		11
#define FC2_STATUS		12
#define FC3_STATUS		13

#define G1_STATUS		14

#define FC4_STATUS		15
#define FC5_STATUS		16
#define FC6_STATUS		17


// ************************************************************
//
// COSTANTI GLOBALI
//
// ************************************************************


 
// Flusso di testing di ATP
static const int ATP_flow[N_TEST_ATP] = {
			VISUAL_INSPECTION,	
			ELE_POWER_UP_TEST,	
			HYD_POWER_UP_TEST,	
			STROKE_TEST,		
			PILOT_EFFORT_TEST,	
			STALL_LOAD_TEST,	
			DRIFT_SPEED_TEST,	
			ANTI_JAMMING_TEST,	
			NO_LOAD_SPEED_TEST,	
			THRESHOLD_TEST,			
			PILOT_TRANSIENT_TEST,
			PILOT_LINEARITY_TEST,	
			PILOT_HYSTERESIS_TEST,	
			PILOT_F_RESPONSE_TEST,	
			CSAS_AUTHORITY_TEST,	
			NULL_BIAS_TEST,			
			CSAS_LINEARITY_TEST,	
			CSAS_HYSTERESIS_TEST,	
			CSAS_F_RESPONSE_TEST,	
			EXT_LEAKAGE_TEST,		
			INT_LEAKAGE_TEST,		
};





// ************************************************************
//
// ABSTRACT DATA TYPE
//
// ************************************************************


/* Struttura descrittiva di un Servocomando */
struct servocomando {
	int part_number;
	int serial_number;
	long int batch_number;
	int assy_month;
	int assy_year;
};


/* Struttura descrittiva dei risultati di un flusso di ATP */
struct ATP_report {
	int identificazione;				//	OK|NO
	int dimensioni;						//	OK|NO
	double peso;
	int installazione;					//	OK|NO
	int manipolazioni;					//	OK|NO
	int finitureSuperficie;				//	OK|NO
	int danniMeccanici;					//	OK|NO

	int pressMandata;					//	OK|NO
	int pressRitorno;					//	OK|NO

	double Rsolenoid;					// Resistenza dell'avvolgimento della solenoid valve
	
	double corsa;						// Misura della corsa

	double threshold;					// Misura della soglia di spostamento
	
	int transientCheck;					//	OK|NO
	
	double sforzoIN_2sysCom;			// Sforzo Pilota
	double sforzoIN_2sysEst;
 	double sforzoIN_jamm[2];

	double stallLoad_2sysCom;			// Carico di Stallo
	double stallLoad_2sysEst;
	double stallLoad_com[2];
	double stallLoad_est[2];

	double driftSpeed_com[4][2];		// VelocitÓ di deriva
	double driftSpeed_est[4][2];

	double Vlvdt_com[2];				// Caratteristica CSAS in compressione
	double stroke_com[2];
	double Vlvdt_zeroCom[2];

	double Vlvdt_est[2];				// Caratteristica CSAS in estensione
	double stroke_est[2];
	double Vlvdt_zeroEst[2];

	double Vlvdt_2sys1com;				// Caratteristica CSAS1 & CSAS2 in compressione
	double Vlvdt_2sys2com;
	double stroke_2sysCom;
	double Vlvdt_2sys1zeroCom;
	double Vlvdt_2sys2zeroCom;

	double Vlvdt_2sys1est;				// Caratteristica CSAS1 & CSAS2 in estensione
	double Vlvdt_2sys2est;
	double stroke_2sysEst;
	double Vlvdt_2sys1zeroEst;
	double Vlvdt_2sys2zeroEst;

	double NullBias_sys1;				// Correnti di null bias
	double NullBias_sys2;

	double Load_jammed[2];				// Carico massimo con un sistema in jamming

	double Linearity_input;				// Linearita' e isteresi input lever
	double Hysteresis_input[3];

	double Linearity_csas;				// Linearita' e isteresi CSAS
	double Hysteresis_csas[2];

	double AmplitudeRatio_input[8];		// Risposta in frequenza input lever
	double PhaseLag_input[8];

	double AmplitudeRatio_csas[8];		// Risposta in frequenza CSAS
	double PhaseLag_csas[8];

	int ExtLeakage;						// leakage esterno [numero di gocce]
	int ExtFlangeLeakage;				// leakage esterno tra cilindro e flangia di supporto [numero di gocce]

	double IntLeakage_noCsasCent[2];	// leakage interno senza CSAS
	double IntLeakage_noCsasCom[2];
	double IntLeakage_noCsasEst[2];

	double IntLeakage_CsasCent[2];		// leakage interno con CSAS (solenoid valve aperta)
	double IntLeakage_CsasCom[2];
	double IntLeakage_CsasEst[2];
	
	double noLoadSpeed_com;				// velocita' massima senza carico
	double noLoadSpeed_est;

	char atp_notes[500];				// commenti / note
	
	struct ATP_report *nextReport;
};


/* Struttura descrittiva delle caratteristiche elettriche dei sensori */     
struct sensor {
	double offset;						// zero del sensore [V]
	double conv_factor;					// fattore di conversione grandezza_fisica/tensione es. [bar/V]
};

/* Struttura descrittiva di un controllore automatico PID */
struct PID_ctrl {
	double proportional_gain;		// guadagno proporzionale = 100 / proportional band
	double integral_time;			// tempo di integrazione del controllore [min]
	double derivative_time;			// tempo di derivazione del controllore [min]
};


// ************************************************************
//
// VARIABILI GLOBALI
//
// ************************************************************

char IP_target[200];							// indirizzo IP RT target controller
char IP_host[200];								// indirizzo IP Host Panel PC

int user_profile;								// ID del profilo utente loggato
int user_language;								// lingua HMI
int actuatorType;								// Tipologia di servocomando sotto test: MRA e TRA
int actuatorPN;									// Part Number servocomando sotto test: PN_3101, PN_3102, PN_3103, PN_3104 

double Measures[NUM_CH_MUX][500];				// buffer di acquisizione misure da tutti i canali di analog input
static int number_of_samples; 					// indice di riempimento del buffer di acquisizione misure per ciascun import della relativa network variable
double MeasuresAve[NUM_CH_MUX];					// array contenente le medie dei dati nel buffer
double leakage;									// misura del leakage - valore acquisito da counter non da ADC

double start_stroke;							// punto di inizio corsa
double end_stroke;								// punto di fine corsa
double stroke;									// corsa del servocomando

double csas_center_stroke;						// offset dei sistemi csas per ottenere il centro corsa main piston 
double input_center_stroke;						// offset del martinetto di input per ottenere il centro corsa main piston 

// vettore immagine stato valvole solenoide su banco
int SolenoidValve[11];

// flag di segnalazione
int uut_power_on;								// alimentazione della UUT
int uut_control_engage;							// engage del comando UUT
int uut_load_engage;							// engage del carico UUT

unsigned int bench_status;						// stato elettrovalvole, proximity switch e fine corsa
int control_engage;								// pilota o CSAS
int load_engage;								// carico o inerziale

int Actual_Test ;								// ID del test in esecuzione nella procedura
int Previous_Test ;								// ID dell'ultimo test eseguito nella procedura di testing
int Next_Test ;									// ID del prossimo test previsto dalla procedura

struct sensor Characteristic[NUM_CH_MUX+1];		// Descrive le caratteristiche lineari dei sensori
struct sensor CharacteristicGear;				// Descrive le caratteristiche lineari dei sensori
struct sensor V3;								// Descrive la conversione del setpoint di pressione

struct ATP_report ATP_Results;					// risultati ATP
struct ATP_report ATP_MinLimits;				// Limiti superiori di accettazione
struct ATP_report ATP_MaxLimits;				// Limiti inferiori di accettazione

int PAT_flow[N_TEST_PAT];		// Array di abilitazione dei test nel flusso di ATP/PAT
								// YES | NO Ispezione visiva
								// YES | NO Test di alimentazione elettrica 
								// YES | NO Prova di pressione 
								// YES | NO Corsa
								// YES | NO Sforzo Pilota
								// YES | NO Carico di stallo
								// YES | NO Velocita' di deriva
								// YES | NO anti-jamming								
								// YES | NO velocita' senza carico
								// YES | NO threshold
								// YES | NO transient
								// YES | NO linearita' leva input
								// YES | NO isteresi leva input
								// YES | NO risposta in frequenza leva input
								// YES | NO CSAS authority
								// YES | NO null bias
								// YES | NO linearita' CSAS
								// YES | NO isteresi CSAS
								// YES | NO risposta in frequenza CSAS
								// YES | NO leakage esterno
								// YES | NO leakage interno
								

/* Parametri di regolazione PID */
struct PID_ctrl PID_parameters[3];
								//	ID			GuadagnoProporzionale		TempoDiIntegrazione		TempoDiDerivazione
								//	INPUT				x							x						x
								//	CSAS				x							x						x
								//	LOAD				x							x						x

/* Variabili di controllo per la visualizzazione dei dati */

int PlotCsasEnable;  
int PlotxEnable;
int PlotyEnable;
int PlotEffortEnable;

int PlotxyEnable;
int displacement_char;
int x_signal_char;
int y_signal_char;

/* Variabili di controllo per il salvataggio delle acquisizione mediante file dati e immagini */

int save_plotx; 
char save_plotx_dir[400];

int save_plotcsas;
char save_plotcsas_dir[400];
	
int save_ploty; 
char save_ploty_dir[400];

int save_plotstrain; 
char save_plotstrain_dir[400];

// Vettori dati utilizzati per la caratterizzazione delle forme d'onda
// contengono rispettivamente un periodo del segnale di input e di output
double x_signal_wave[10000];
double y_signal_wave[10000];

// forme d'onda per caratterizzazione isteresi e linearita'
double x_signal_hyst[10000];
double y_signal_hyst[10000];

// numero di cicli nel mechanical panel
unsigned int number_of_cycle; 
double cycling_time;

// valori di massimo e minimo
double previous_max_y;
double previous_max_x;
double previous_min_y;
double previous_min_x;

// X factor
double KM_PRO;
double KT_PRO;
