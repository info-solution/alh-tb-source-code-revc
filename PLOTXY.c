#include "toolbox.h"
#include <formatio.h>
#include <analysis.h>
#include <ansi_c.h>

#include <userint.h>
#include "PLOTXY.h"
#include "MANUALTEST.h"  
#include "global_var.h"


int CVICALLBACK QuitPlotxyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello per Plot X-Y
			HidePanel (plotxypanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK AcquirePlotxyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			PlotxyEnable=YES;
			
			// abilito / disabilito pulsanti
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_ACQUIREBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_CLEARBTN, ATTR_DIMMED, YES);
			
			break;
	}
	return 0;
}

int CVICALLBACK ClearGraphCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// abilito / disabilito pulsanti
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_ACQUIREBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_CLEARBTN, ATTR_DIMMED, NO);
			
			// cancella grafico
			if (displacement_char>0)
				DeleteGraphPlot (plotxypanel, PLOTXYPNL_POSITIONGRP, displacement_char, VAL_IMMEDIATE_DRAW);
			
			// cancella grafico
			if (x_signal_char>0)
				DeleteGraphPlot (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, x_signal_char, VAL_IMMEDIATE_DRAW);
			
			// cancella grafico
			if (y_signal_char>0)
				DeleteGraphPlot (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, y_signal_char, VAL_IMMEDIATE_DRAW);
			
			displacement_char=-1000;
			
			break;
	}
	return 0;
}


int CVICALLBACK ChangeCursorSignalChartCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int ctrl_type;
	double x1=0.0, x2=0.0, y1=0.0, y2=0.0;
	double signal_frequency;
	int samples_per_period;
	double phase_lag=0.0;
	double attenuation=0.0;
	
	double media_x=0.0, media_y=0.0;
	double max_input=0.0, max_output=0.0;
	double min_input=0.0, min_output=0.0;  
	
	int max_input_idx=0, max_output_idx=0;
	int min_input_idx=0, min_output_idx=0;  

	
	double InitXFilterState[50];
	double InitYFilterState[50];
	
	switch (event)
	{
		case EVENT_COMMIT:

			// rilevo la frequenza di oscillazione
			GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &signal_frequency);
		
			if (signal_frequency<=0.0)
				goto Error;
			
			samples_per_period = (int)(PID_RATE*(1.0/signal_frequency));   
			
			// leggo tipo di input utilizzato
			GetCtrlAttribute (plotxypanel, PLOTXYPNL_CTRLCHOISE, ATTR_CTRL_VAL, &ctrl_type);
			
			// coordinate segnale di input
			GetGraphCursor (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, 1, &x1, &y1);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XCURSORIN, ATTR_CTRL_VAL, x1);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YCURSORIN, ATTR_CTRL_VAL, y1);
			
			// coordinate segnale di output
			GetGraphCursor (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, 2, &x2, &y2);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XCURSOROUT, ATTR_CTRL_VAL, x2);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YCURSOROUT, ATTR_CTRL_VAL, y2);
			
			// Estremi dei vettori numericiacquisiti
			MaxMin1D (x_signal_wave, samples_per_period, &max_input, &max_input_idx, &min_input, &min_input_idx);
			MaxMin1D (y_signal_wave, samples_per_period, &max_output, &max_output_idx, &min_output, &min_output_idx);
			
			// Valor medio dei segnali acquisiti
			Mean (x_signal_wave, samples_per_period, &media_x);
			Mean (y_signal_wave, samples_per_period, &media_y);
			
			// sfasamento con indice di correzione dato dall'equazione
			// Dphase = -1.61*f
			
			if (ctrl_type==PILOT)
				phase_lag = -abs(360*(x2-x1)*signal_frequency);
			else
				phase_lag = -abs(360*(x2-x1)*signal_frequency)+(1.61*signal_frequency);
			
			
			
			/************************************/
			/*             X FACTOR             */
			/************************************/
			if (actuatorPN==PN_3101)
				phase_lag=phase_lag*KT_PRO;
			else
				phase_lag=phase_lag*KM_PRO;
			/************************************/
			
			
			
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_PHASELAGNMR, ATTR_CTRL_VAL, phase_lag);
			
			// attenuazione
			
			if ((actuatorPN==PN_3101)&&(ctrl_type==CSAS))
				attenuation = 20*log10( fabs( (max_output-media_y)/(max_input-media_x)/1.0 ) );
			else
				attenuation = 20*log10( fabs( (max_output-media_y)/(max_input-media_x) ) );
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_ATTENUATIONNMR, ATTR_CTRL_VAL, attenuation);
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XMAXNMR, ATTR_CTRL_VAL, max_input);     
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XMINNMR, ATTR_CTRL_VAL, min_input);     
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YMAXNMR, ATTR_CTRL_VAL, max_output);     
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YMINNMR, ATTR_CTRL_VAL, min_output);
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XAVENMR, ATTR_CTRL_VAL, media_x);     
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YAVENMR, ATTR_CTRL_VAL, media_y);     
			
			break;
	}
	
Error:
	return 0;
}


int CVICALLBACK ChangeCursorCharacteristicChartCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double x1=0.0, x2=0.0, y1=0.0, y2=0.0;
	int cursor1_idx=0, cursor2_idx=0;
	double signal_frequency;
	int samples_per_period;

	double distance=0.0;
	double linearity=0.0;

	// fitting lineare
	int lenFitting=0;
	double x_char[10000], y_char[10000];
	double fitted_data[10000];
	double pendenza, intercetta, meanSquareErr;
	double sse, rmse;
	
	char message[200]="";
	
	switch (event)
	{
		case EVENT_COMMIT:

			// rilevo la frequenza di oscillazione
			GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &signal_frequency);
		
			if (signal_frequency<=0.0)
				goto Error;
			
			if (displacement_char<0)
				goto Error;
			
			samples_per_period = (int)(PID_RATE*(1.0/signal_frequency));
			samples_per_period = (samples_per_period + (samples_per_period*10/100)); // #FM/FB : da valutare una percentuale in piu' di samples per period
			
			// coordinate cursore n.1
			GetGraphCursor (plotxypanel, PLOTXYPNL_POSITIONGRP, 1, &x1, &y1);
			GetGraphCursorIndex (plotxypanel, PLOTXYPNL_POSITIONGRP, 1, &displacement_char, &cursor1_idx);
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XCURSOR1, ATTR_CTRL_VAL, x1);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YCURSOR1, ATTR_CTRL_VAL, y1);
			
			// coordinate cursore n.2
			GetGraphCursor (plotxypanel, PLOTXYPNL_POSITIONGRP, 2, &x2, &y2);
			GetGraphCursorIndex (plotxypanel, PLOTXYPNL_POSITIONGRP, 2, &displacement_char, &cursor2_idx);
			
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_XCURSOR2, ATTR_CTRL_VAL, x2);
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_YCURSOR2, ATTR_CTRL_VAL, y2);
			
			// Calcolo della distanza tra i due cursori: utilizzata per la misura dell'isteresi
			distance = sqrt( pow ((x2-x1), 2) + pow ((y2-y1), 2) );
			SetCtrlAttribute (plotxypanel, PLOTXYPNL_HYSTERESISNMR, ATTR_CTRL_VAL, distance);
			
			// linearita'
			lenFitting = fabs(cursor2_idx-cursor1_idx);
			
			sprintf(message,"idx1, idx2, lenFitting: %d %d %d", cursor1_idx, cursor2_idx, lenFitting);
			PrintLog(message);
			
			if (lenFitting<=samples_per_period/2)
			{
				/* estrapolazione del segmento di array da fittare */
				if (cursor2_idx>=cursor1_idx)
				{
					//PrintLog("idx2 > idx1");
					Subset1D (x_signal_hyst, samples_per_period, cursor1_idx, lenFitting, x_char);
					Subset1D (y_signal_hyst, samples_per_period, cursor1_idx, lenFitting, y_char);
				}
				else
				{
					//PrintLog("idx2 < idx1");
					Subset1D (x_signal_hyst, samples_per_period, cursor2_idx, lenFitting, x_char);
					Subset1D (y_signal_hyst, samples_per_period, cursor2_idx, lenFitting, y_char);	
				}
				
				/* fitting lineare */
				LinFit (x_char, y_char, lenFitting, fitted_data, &pendenza, &intercetta, &meanSquareErr);

				/* accuratezza del fitting */
				GoodnessOfFit (y_char, fitted_data, NULL, lenFitting, 0, &sse, &linearity, &rmse);

				/* linearita' percentuale */
				SetCtrlAttribute (plotxypanel, PLOTXYPNL_LINEARITYNMR, ATTR_CTRL_VAL, 100*linearity);
			}
			else
			{
				/* non e' ammesso il fitting per il calcolo della linearita' */	
				SetCtrlAttribute (plotxypanel, PLOTXYPNL_LINEARITYNMR, ATTR_CTRL_VAL, -1.0);
			}
			
			break;
	}
	
Error:
	return 0;
}	
	


int CVICALLBACK SaveCharacteristicsCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double signal_frequency;
	int samples_per_period;
	
	int done=0,i=0;
	char nomefile[400];
	
	double FileData[10000][2];    
	
	switch (event)
	{
		case EVENT_COMMIT:

			// rilevo la frequenza di oscillazione
			GetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, &signal_frequency);

			if (signal_frequency<=0.0)
				goto Error;
			
			samples_per_period = (int)(PID_RATE*(1.0/signal_frequency));   

			/* SALVATAGGIO DATI */
			
			/* directory utente */
			done = DirSelectPopup ("C:", "Select Directory", 1, 1, save_plotcsas_dir);
			
			if (done<=0)
				goto Error;
			
			/* Salvataggio grafico di caratterizzazione X-Y */
			sprintf(nomefile,"%s\\char_graph.bmp",save_plotcsas_dir);
			// al primo click sull'opzione Save converto il controllo in bitmap
			SaveCtrlDisplayToFile (plotxypanel, PLOTXYPNL_POSITIONGRP, 0, -1, -1, nomefile);
			
			/* Salvataggio immagine relativa allo CSAS1 */
			sprintf(nomefile,"%s\\signals_graph.bmp",save_plotcsas_dir);
			// al primo click sull'opzione Save converto il controllo in bitmap
			SaveCtrlDisplayToFile (plotxypanel, PLOTXYPNL_DISPLACEMENTGRPH, 0, -1, -1, nomefile);
			
			/* Salvataggio su file della forma d'onda */
			for (i=0;i<samples_per_period;i++)
			{
				FileData[i][0] = x_signal_wave[i];
				FileData[i][1] = y_signal_wave[i];
			}
			
			sprintf(nomefile,"%s\\characterization_data.dat",save_plotstrain_dir);  
		
			ArrayToFile (nomefile, FileData, VAL_DOUBLE, 2*samples_per_period, 2, VAL_DATA_MULTIPLEXED, VAL_GROUPS_AS_COLUMNS,
						 VAL_CONST_WIDTH, 30, VAL_ASCII, VAL_APPEND);
			
			break;
	}
	
Error:
	return 0;
}

