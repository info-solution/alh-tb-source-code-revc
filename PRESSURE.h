/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PRESSPNL                         1
#define  PRESSPNL_QUITBTN                 2       /* callback function: QuitPressurePanelCB */
#define  PRESSPNL_DECORATION_4            3
#define  PRESSPNL_P11NMR                  4
#define  PRESSPNL_P10NMR                  5
#define  PRESSPNL_P09NMR                  6
#define  PRESSPNL_P06NMR                  7
#define  PRESSPNL_P07NMR                  8
#define  PRESSPNL_P05NMR                  9
#define  PRESSPNL_P04NMR                  10
#define  PRESSPNL_P08NMR                  11
#define  PRESSPNL_P03NMR                  12
#define  PRESSPNL_P02NMR                  13
#define  PRESSPNL_TEXTMSG_5               14
#define  PRESSPNL_DECORATION_3            15
#define  PRESSPNL_TEXTMSG_4               16
#define  PRESSPNL_DECORATION_2            17
#define  PRESSPNL_TEXTMSG_3               18
#define  PRESSPNL_DECORATION              19
#define  PRESSPNL_TEXTMSG_2               20


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK QuitPressurePanelCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
