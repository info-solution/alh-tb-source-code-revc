#include <ansi_c.h>
#include "toolbox.h"
#include <userint.h>
#include "PLOTY.h"

#include "global_var.h"     


int CVICALLBACK QuitPlotyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello per Plot X
			HidePanel (plotypanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK ClearStripChartPlotyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// cancella tracce sulle strip chart
			ClearStripChart (plotypanel, PLOTYPNL_POSITIONSTR);
			DeleteGraphPlot (strainpanel, PLOTYPNL_SPEEDGRPH, -1, VAL_IMMEDIATE_DRAW); 
			
			// azzera storico dei massimi&minimi
			SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONMAXNMR, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONMINNMR, ATTR_CTRL_VAL, 0.0);
			
			SetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDMAXNMR, ATTR_CTRL_VAL, 0.0);
			SetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDMINNMR, ATTR_CTRL_VAL, 0.0);

			// azzera valori di massimo e minimo
			previous_max_y=MeasuresAve[L2_MUX];
			previous_min_y=MeasuresAve[L2_MUX];

			PlotyEnable=YES;
			
			break;
	}
	return 0;
}


int CVICALLBACK StopStripChartPlotyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			PlotyEnable=NO;
			
			break;
	}
	return 0;
}


int CVICALLBACK SaveStripChartPlotyCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int done=0;
	char nomefile[400];
	
	switch (event)
	{
		case EVENT_COMMIT:

			if (save_ploty==0)
			{
				done = DirSelectPopup ("C:", "Select Directory", 1, 1, save_ploty_dir);
				
				if (done<=0)
					goto Error;
				
				sprintf(nomefile,"%s\\displacement_output.bmp",save_ploty_dir);
				
				// al primo click sull'opzione Save converto il controllo in bitmap
				SaveCtrlDisplayToFile (plotypanel, PLOTYPNL_POSITIONSTR, 0, -1, -1, nomefile);
				save_ploty=1;
				
				// modifica la label in STOP
				SetCtrlAttribute(plotypanel,PLOTYPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Stop");  
			}
			else
			{
				// modifica la label in START
				SetCtrlAttribute(plotypanel,PLOTYPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Start");
				save_ploty=0;
			}

			break;
	}

Error:
	return 0;
}

int CVICALLBACK ChangeScaleYCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (plotypanel, PLOTYPNL_POSITIONSTR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>40.0)
			{
				SetAxisScalingMode (plotypanel, PLOTYPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -3.0, 3.0);	
			}
			else if ((maxScale>4.0)&&(maxScale<30.0))
			{
				SetAxisScalingMode (plotypanel, PLOTYPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -50.0, 50.0);	
			}
			else if (maxScale<4.0)
			{
				SetAxisScalingMode (plotypanel, PLOTYPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -15.0, 15.0);	
			}
		break; 
	}
	return 0;	
}

int CVICALLBACK ChangeScaleYdotCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	double x,y;
	
	switch (event)
	{
		case EVENT_RIGHT_CLICK:
			GetAxisScalingMode (plotypanel, PLOTYPNL_SPEEDGRPH , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>180.0)
			{
				SetAxisScalingMode (plotypanel, PLOTYPNL_SPEEDGRPH, VAL_LEFT_YAXIS , VAL_MANUAL, -50.0, 50.0);	
			}
			else if ((maxScale>100.0)&&(maxScale<180.0))
			{
				SetAxisScalingMode (plotypanel, PLOTYPNL_SPEEDGRPH, VAL_LEFT_YAXIS , VAL_MANUAL, -200.0, 200.0);	
			}
			else if (maxScale<100.0)
			{
				SetAxisScalingMode (plotypanel, PLOTYPNL_SPEEDGRPH, VAL_LEFT_YAXIS , VAL_MANUAL, -150.0, 150.0);	
			}
		break;
		case EVENT_COMMIT:
			// coordinate segnale
			GetGraphCursor (plotypanel, PLOTYPNL_SPEEDGRPH, 1, &x, &y);
	
			SetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDAVENMR, ATTR_CTRL_VAL, y); 
		break;
	}
	return 0;	
}
