#include <userint.h>
#include "PIDCOEFFICIENT.h"
#include "global_var.h"      

int CVICALLBACK QuitPidTuningCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			HidePanel (pidpanel);
	
			break;
	}
	return 0;
}


/************************************************************/
/*	Tuning del regolatore PID martinetto di input			*/
/************************************************************/

int CVICALLBACK ChangeKcPidInputCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetInputPid(coefficienti);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeTiPidInputCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetInputPid(coefficienti);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeTdPidInputCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDINPUTTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetInputPid(coefficienti);
			
			break;
	}
	return 0;
}



/************************************************************/
/*	Tuning del regolatore PID degli CSAS					*/
/************************************************************/


int CVICALLBACK ChangeKcPidCsasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetCsasPid(coefficienti);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeTiPidCsasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetCsasPid(coefficienti);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeTdPidCsasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDCSASTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetCsasPid(coefficienti);
			
			break;
	}
	return 0;
}


/************************************************************/
/*	Tuning del regolatore PID di carico						*/
/************************************************************/


int CVICALLBACK ChangeKcPidLoadCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td
	
	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetLoadPid(coefficienti);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeTiPidLoadCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td

	switch (event)
	{
		case EVENT_COMMIT:

			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetLoadPid(coefficienti);
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeTdPidLoadCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double coefficienti[3]={0,0,0};		// Kc,Ti,Td

	switch (event)
	{
		case EVENT_COMMIT:
			
			// acquisizione parametri da pannello
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADKCNMR, ATTR_CTRL_VAL, &coefficienti[0]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADTINMR, ATTR_CTRL_VAL, &coefficienti[1]);
			GetCtrlAttribute(pidpanel,PIDCOEFPNL_PIDLOADTDNMR, ATTR_CTRL_VAL, &coefficienti[2]);
			
			// invio parametri di controllo verso regolatore 
			SetLoadPid(coefficienti);
			
			break;
	}
	return 0;
}
