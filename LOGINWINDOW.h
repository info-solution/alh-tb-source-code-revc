/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  INFOPANEL                        1
#define  INFOPANEL_TEXTBOX                2
#define  INFOPANEL_OKBUTTON               3       /* callback function: OkButtonCB */
#define  INFOPANEL_PICTURE                4
#define  INFOPANEL_PICTURE_2              5

#define  LOGINPANEL                       2       /* callback function: GetInformation */
#define  LOGINPANEL_RING                  2
#define  LOGINPANEL_PASSWORD              3
#define  LOGINPANEL_LOGINBUTTON           4       /* callback function: LoginCB */
#define  LOGINPANEL_QUITLOGINBUTTON       5       /* callback function: QuitProgramCB */
#define  LOGINPANEL_PICTURE               6


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK GetInformation(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LoginCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OkButtonCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitProgramCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
