/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  ATPPNL                           1
#define  ATPPNL_POWERONBTN                2       /* callback function: PowerOnPressureCB */
#define  ATPPNL_BENCHPOWERONBTN           3       /* callback function: BenchPowerOnCB */
#define  ATPPNL_STOPBTN                   4       /* callback function: StopAtpCB */
#define  ATPPNL_DECORATION                5
#define  ATPPNL_QUITBTN                   6       /* callback function: QuitAtpCB */
#define  ATPPNL_TAB                       7
#define  ATPPNL_TEXTMSG_3                 8
#define  ATPPNL_TEXTMSG_2                 9
#define  ATPPNL_RING                      10
#define  ATPPNL_NUMERICTANK_4             11
#define  ATPPNL_NUMERICTANK_6             12
#define  ATPPNL_NUMERICTANK_5             13
#define  ATPPNL_NUMERICTANK_2             14

     /* tab page panel controls */
#define  INITTAB_PARTNUMBERRNG            2
#define  INITTAB_YEARNMR                  3
#define  INITTAB_BATCHNUMBERNMR           4
#define  INITTAB_SERIALNUMBERNMR          5
#define  INITTAB_MONTHRNG                 6
#define  INITTAB_ATPISSUESTR              7       /* callback function: ClearOperatorNameCB */
#define  INITTAB_PNISSUESTR               8       /* callback function: ClearOperatorNameCB */
#define  INITTAB_OPNAMESTR                9       /* callback function: ClearOperatorNameCB */
#define  INITTAB_TEXTMSG                  10

     /* tab page panel controls */
#define  TEST1_MECDAMAGECHK               2
#define  TEST1_SURFACECHK                 3
#define  TEST1_WORKMANSHIPCHK             4
#define  TEST1_INSTALLATIONCHK            5
#define  TEST1_DIMENSIONCHK               6
#define  TEST1_IDENTIFICATIONCHK          7
#define  TEST1_WEIGHTNMR                  8

     /* tab page panel controls */
#define  TEST10_POSITIONGRP               2       /* callback function: ChangeCursorCharacteristicChartCB */
#define  TEST10_ACQUIREBTN                3       /* callback function: AcquirePlotxyCB */
#define  TEST10_SAVEBTN                   4       /* callback function: SaveCharacteristicsCB */
#define  TEST10_CLEARBTN                  5       /* callback function: ClearGraphCB */
#define  TEST10_DECORATION_2              6
#define  TEST10_TEXTMSG_3                 7
#define  TEST10_DECORATION_5              8
#define  TEST10_TEXTMSG_6                 9
#define  TEST10_SYSTEMCHOISERNG           10      /* callback function: ChangeMechCalSystemCB */
#define  TEST10_BINARYSWITCH              11
#define  TEST10_CSASCHOISE                12
#define  TEST10_LINEARITYNMR              13
#define  TEST10_TEXTMSG                   14
#define  TEST10_RINGSLIDE                 15

     /* tab page panel controls */
#define  TEST11_DECORATION_6              2
#define  TEST11_DECORATION_2              3
#define  TEST11_TEXTMSG_7                 4
#define  TEST11_TEXTMSG_3                 5
#define  TEST11_DECORATION_5              6
#define  TEST11_TEXTMSG_6                 7
#define  TEST11_RINGSLIDE                 8
#define  TEST11_BINARYSWITCH              9
#define  TEST11_PHASELAGNMR               10
#define  TEST11_ATTENUATIONNMR            11
#define  TEST11_DISPLACEMENTGRPH          12      /* callback function: ChangeCursorSignalChartCB */
#define  TEST11_SETAMPLITUDENMR           13      /* callback function: ChangeAmplitudeCB */
#define  TEST11_RINGSLIDE_2               14
#define  TEST11_TEXTMSG_8                 15
#define  TEST11_TEXTMSG_9                 16

     /* tab page panel controls */
#define  TEST12_DECORATION_4              2
#define  TEST12_TEXTMSG_5                 3
#define  TEST12_POSITIONSLD               4       /* callback function: ChangePositionCB */
#define  TEST12_DECORATION_6              5
#define  TEST12_DECORATION_5              6
#define  TEST12_TEXTMSG_7                 7
#define  TEST12_TEXTMSG_6                 8
#define  TEST12_ATTENUATIONNMR            9
#define  TEST12_PHASELAGNMR               10
#define  TEST12_BINARYSWITCH              11

     /* tab page panel controls */
#define  TEST13_DECORATION_3              2
#define  TEST13_DECORATION_4              3
#define  TEST13_LEAKAGENMR                4
#define  TEST13_TEXTMSG_7                 5
#define  TEST13_TEXTMSG_4                 6
#define  TEST13_TEXTMSG_8                 7
#define  TEST13_TEXTMSG_3                 8
#define  TEST13_BINARYSWITCH              9
#define  TEST13_DECORATION_5              10
#define  TEST13_DECORATION_2              11
#define  TEST13_SYSTEMCHOISERNG           12      /* callback function: ChangeMechCalSystemCB */
#define  TEST13_SYSTEMCHOISERNG_2         13      /* callback function: ChangeMechCalSystemCB */
#define  TEST13_RINGSLIDE                 14

     /* tab page panel controls */
#define  TEST2_PRESSURESLD                2       /* callback function: ChangeDeliveryPressureAtpCB */
#define  TEST2_RETURNRNG                  3       /* callback function: ChangeReturnPressureAtpCB */
#define  TEST2_DECORATION_2               4
#define  TEST2_DECORATION                 5
#define  TEST2_TEXTMSG_3                  6
#define  TEST2_TEXTMSG_2                  7
#define  TEST2_UUTRETURNNMR               8
#define  TEST2_UUTPRESSURENMR             9
#define  TEST2_BINARYSWITCH               10

     /* tab page panel controls */
#define  TEST22_NEXT22BTN                 2       /* callback function: NextNotesCB */
#define  TEST22_NOTESTXT                  3

     /* tab page panel controls */
#define  TEST3_POSITIONSLD                2       /* callback function: ChangePositionAtpCB */
#define  TEST3_OUTPUTSTR                  3       /* callback function: ChangeScaleOutAtpCB */
#define  TEST3_INPUTSTR                   4       /* callback function: ChangeScaleInAtpCB */
#define  TEST3_GETSTROKEBTN               5       /* callback function: GetStrokeCenterAtpCB */
#define  TEST3_DECORATION_2               6
#define  TEST3_DECORATION                 7
#define  TEST3_STROKECENTERBTN            8       /* callback function: SetStrokeCenterAtpCB */
#define  TEST3_TEXTMSG_3                  9
#define  TEST3_BINARYSWITCH               10
#define  TEST3_TEXTMSG_2                  11
#define  TEST3_OUTPUTAVENMR               12
#define  TEST3_INPUTAVENMR                13

     /* tab page panel controls */
#define  TEST4_OUTPUTSPEEDSTR             2       /* callback function: ChangeScaleSpeedAtpCB */
#define  TEST4_OUTPUTPOSSTR               3       /* callback function: ChangeScaleOutSpeedAtpCB */
#define  TEST4_DECORATION_2               4
#define  TEST4_DECORATION                 5
#define  TEST4_TEXTMSG_3                  6
#define  TEST4_TEXTMSG_2                  7
#define  TEST4_BINARYSWITCH_2             8
#define  TEST4_BINARYSWITCH               9
#define  TEST4_MINSPEEDNMR                10
#define  TEST4_MAXSPEEDNMR                11

     /* tab page panel controls */
#define  TEST5_INPUTEFFORTGRPH            2       /* callback function: ChangeScaleInputStrainCB */
#define  TEST5_OUTPUTPOSSTR               3       /* callback function: ChangeScaleOutSpeedAtpCB */
#define  TEST5_GETSTROKEBTN_3             4       /* callback function: GetStrokeCenterAtpCB */
#define  TEST5_DECORATION                 5
#define  TEST5_GETSTROKEBTN_2             6       /* callback function: GetStrokeCenterAtpCB */
#define  TEST5_TEXTMSG_2                  7
#define  TEST5_BINARYSWITCH_2             8
#define  TEST5_BINARYSWITCH               9
#define  TEST5_DECORATION_2               10
#define  TEST5_TEXTMSG_3                  11
#define  TEST5_MAXSPEEDNMR                12

     /* tab page panel controls */
#define  TEST6_OUTPUTSTRENGTHSTR          2       /* callback function: ChangeScaleOutputStrainCB */
#define  TEST6_DECORATION_4               3
#define  TEST6_TEXTMSG_5                  4
#define  TEST6_DECORATION_2               5
#define  TEST6_TEXTMSG_3                  6
#define  TEST6_DECORATION_3               7
#define  TEST6_DECORATION                 8
#define  TEST6_BINARYSWITCH               9
#define  TEST6_TEXTMSG_4                  10
#define  TEST6_TEXTMSG_2                  11
#define  TEST6_MAXSPEEDNMR_2              12
#define  TEST6_MAXSPEEDNMR                13
#define  TEST6_LOADCHOISE_2               14      /* callback function: ChangeLoadTypeCB */
#define  TEST6_LOADCHOISE                 15      /* callback function: ChangeLoadTypeCB */
#define  TEST6_LOADSLD                    16      /* callback function: ChangeLoadCB */
#define  TEST6_SYSTEMCHOISERNG            17      /* callback function: ChangeMechCalSystemCB */
#define  TEST6_BINARYSWITCH_3             18
#define  TEST6_BINARYSWITCH_2             19

     /* tab page panel controls */
#define  TEST7_DECORATION_3               2
#define  TEST7_OUTPUTPOSSTR_3             3       /* callback function: ChangeScaleOutSpeedAtpCB */
#define  TEST7_OUTPUTPOSSTR_2             4       /* callback function: ChangeScaleOutSpeedAtpCB */
#define  TEST7_OUTPUTPOSSTR               5       /* callback function: ChangeScaleOutSpeedAtpCB */
#define  TEST7_DECORATION_5               6
#define  TEST7_TEXTMSG_6                  7
#define  TEST7_TEXTMSG_4                  8
#define  TEST7_DECORATION_4               9
#define  TEST7_DECORATION_2               10
#define  TEST7_SYSTEMCHOISERNG_2          11      /* callback function: ChangeMechCalSystemCB */
#define  TEST7_TEXTMSG_5                  12
#define  TEST7_TEXTMSG_3                  13
#define  TEST7_BINARYSWITCH               14
#define  TEST7_MAXSPEEDNMR_3              15
#define  TEST7_MAXSPEEDNMR_2              16
#define  TEST7_MAXSPEEDNMR                17
#define  TEST7_SYSTEMCHOISERNG            18      /* callback function: ChangeMechCalSystemCB */
#define  TEST7_POSITIONSLD                19      /* callback function: ChangePositionCB */

     /* tab page panel controls */
#define  TEST8_DECORATION_3               2
#define  TEST8_TEXTMSG_4                  3
#define  TEST8_SYSTEMCHOISERNG_2          4       /* callback function: ChangeMechCalSystemCB */
#define  TEST8_BINARYSWITCH               5
#define  TEST8_DECORATION_5               6
#define  TEST8_TEXTMSG_6                  7
#define  TEST8_DECORATION_2               8
#define  TEST8_TEXTMSG_3                  9
#define  TEST8_SYSTEMCHOISERNG            10      /* callback function: ChangeMechCalSystemCB */
#define  TEST8_MAXSPEEDNMR                11

     /* tab page panel controls */
#define  TEST9_POSITIONGRP                2       /* callback function: ChangeCursorCharacteristicChartCB */
#define  TEST9_ACQUIREBTN                 3       /* callback function: AcquirePlotxyCB */
#define  TEST9_SAVEBTN                    4       /* callback function: SaveCharacteristicsCB */
#define  TEST9_CLEARBTN                   5       /* callback function: ClearGraphCB */
#define  TEST9_DECORATION_2               6
#define  TEST9_TEXTMSG_3                  7
#define  TEST9_CTRLCHOISE                 8       /* callback function: ChangeControlCB */
#define  TEST9_DECORATION_5               9
#define  TEST9_TEXTMSG_6                  10
#define  TEST9_BINARYSWITCH               11
#define  TEST9_LINEARITYNMR               12
#define  TEST9_TEXTMSG                    13


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK AcquirePlotxyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BenchPowerOnCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeAmplitudeCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeControlCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeCursorCharacteristicChartCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeCursorSignalChartCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeDeliveryPressureAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeLoadTypeCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeMechCalSystemCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangePositionAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangePositionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeReturnPressureAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleInAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleInputStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleOutAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleOutputStrainCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleOutSpeedAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleSpeedAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearGraphCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearOperatorNameCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetStrokeCenterAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK NextNotesCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PowerOnPressureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveCharacteristicsCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SetStrokeCenterAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopAtpCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
