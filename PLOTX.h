/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PLOTXPNL                         1
#define  PLOTXPNL_SAVEBTN                 2       /* callback function: SaveStripChartPlotxCB */
#define  PLOTXPNL_CLEARBTN_2              3       /* callback function: StopStripChartPlotxCB */
#define  PLOTXPNL_CLEARBTN                4       /* callback function: ClearStripChartPlotxCB */
#define  PLOTXPNL_QUITBTN                 5       /* callback function: QuitPlotxCB */
#define  PLOTXPNL_POSITIONSTR             6       /* callback function: ChangeScaleXCB */
#define  PLOTXPNL_SPEEDSTR                7
#define  PLOTXPNL_SPEEDAVENMR             8
#define  PLOTXPNL_SPEEDMINNMR             9
#define  PLOTXPNL_SPEEDMAXNMR             10
#define  PLOTXPNL_POSITIONMINNMR          11
#define  PLOTXPNL_POSITIONMAXNMR          12
#define  PLOTXPNL_POSITIONAVENMR          13
#define  PLOTXPNL_DECORATION              14
#define  PLOTXPNL_TEXTMSG_4               15


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ChangeScaleXCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearStripChartPlotxCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitPlotxCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveStripChartPlotxCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopStripChartPlotxCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
