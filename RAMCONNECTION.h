/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  INERCONPNL                       1
#define  INERCONPNL_ABORTBTN              2       /* callback function: ExitInertialConnectionCB */
#define  INERCONPNL_DONEBTN               3       /* callback function: QuitLoadManualConnectionCB */
#define  INERCONPNL_LOADPICRNG            4

#define  INRAMCONPN                       2
#define  INRAMCONPN_ABORTBTN              2       /* callback function: ExitInputConnectionCB */
#define  INRAMCONPN_DONEBTN               3       /* callback function: QuitInputManualConnectionCB */
#define  INRAMCONPN_INPUTPICRNG           4

#define  OUTCONPNL                        3
#define  OUTCONPNL_ABORTBTN               2       /* callback function: ExitPistonConnectionCB */
#define  OUTCONPNL_DONEBTN                3       /* callback function: QuitOutputManualConnectionCB */
#define  OUTCONPNL_OUTPUTPICRNG           4


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ExitInertialConnectionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ExitInputConnectionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ExitPistonConnectionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitInputManualConnectionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitLoadManualConnectionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitOutputManualConnectionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
