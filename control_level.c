#include <ansi_c.h>
#include <windows.h>    
#include <userint.h>
#include <rtutil.h> 
#include <utility.h>
#include <NIDAQmx.h> 
#include "HW_Description.h" 
#include "error_table.h" 
#include "pid_common.h" 



/* Routine di verifica errori HW scheda DAQmx */
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else

// Callback della Thread Safe Queue
void CVICALLBACK SendMeasuresToHostCB (int queueHandle, unsigned int event, int value, void *callbackData);    

/* ------------------------------------------------------------------------------------ */
/* Costanti globali con visibilita' ristretta al livello di controllo					*/
/* ------------------------------------------------------------------------------------ */

#define PI 3.141592

/* ------------------------------------------------------------------------------------ */
/* Variabili globali con visibilita' ristretta al livello di controllo					*/
/* ------------------------------------------------------------------------------------ */

    

/* ------------------------------------------------------------------------------------ */
/* Handler dei Task DAQmx																*/
/* ------------------------------------------------------------------------------------ */

static TaskHandle aiTaskHandle;			// handler del task di analog input
static TaskHandle aoTaskHandle;			// handler del task di analog output
static TaskHandle diTaskHandle;			// handler del task di digital input
static TaskHandle doTaskHandle;			// handler del task di digital output
static TaskHandle ctrTaskHandle;		// handler del task di counter

static TaskHandle do6341TaskHandle;		// handler del task di digital output PXIe 6341
static TaskHandle ao6341TaskHandle;		// handler del task di analog output PXIe 6341

//static TaskHandle aiBridgeCal;			//used only for calibration

/* ------------------------------------------------------------------------------------ */
/* Routine di inizializzazione variabili globali										*/
/* ------------------------------------------------------------------------------------ */

void InitVar(void)
{
	int i=0;

	// Variabile di uscita dal programma
	QuitProgram=0;
	Emergency=0;
		
	// inizializzazione porta di digital output
	for (i=0;i<NUM_EV;i++)
		solenoid_valve[i]=LW;
	
	// inizializzazione porta di digital output PXIe 6341
	for (i=0;i<NUM_EV_6341;i++)
		valve_6341[i]=LW;
	
	// Inizializzazione temporanea dei parametri di regolazione PID

	PID_parameters[CSAS1_PID].proportional_gain = 1e-6;
	PID_parameters[CSAS1_PID].integral_time = 0;
	PID_parameters[CSAS1_PID].derivative_time = 0;
	
	PID_parameters[CSAS2_PID].proportional_gain = 1e-6;
	PID_parameters[CSAS2_PID].integral_time = 0;
	PID_parameters[CSAS2_PID].derivative_time = 0;
	
	PID_parameters[INPUT_PID].proportional_gain = 1e-6;
	PID_parameters[INPUT_PID].integral_time = 0;
	PID_parameters[INPUT_PID].derivative_time = 0;
	
	PID_parameters[LOAD_PID].proportional_gain = 1e-6;
	PID_parameters[LOAD_PID].integral_time = 0;
	PID_parameters[LOAD_PID].derivative_time = 0;
	
	// Abilitazione dei loop di controllo
	enCsas1Pid=0;
	enCsas2Pid=0;
	enInputPid=0;
	enLoadPid=0;
	
	// Abilitazioni regolazioni e misure
	enUutVoltageReg=0;
	enUutPressureReg=0;
	enNullBias=0;
	enEvCurrent=0;
				  
	// Inizializzazione set-point
	CsasSetPoint=0;
	InputSetPoint=0;
	LoadSetPoint=0;

	// Inizializzazione direzione del carico
	LoadDirection = EXTENSION;
	return;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di creazione dei task DAQmx per il controllo hardware						*/
/* ------------------------------------------------------------------------------------ */

int InitTask (void)
{
	int error=0;	
	
	DAQmxErrChk( DAQmxCreateTask ("Analog Input Task", &aiTaskHandle) );
	DAQmxErrChk( DAQmxCreateTask ("Analog Output Task", &aoTaskHandle) );
	DAQmxErrChk( DAQmxCreateTask ("Digital Input Task", &diTaskHandle) );
	DAQmxErrChk( DAQmxCreateTask ("Digital Output Task", &doTaskHandle) ); 
	DAQmxErrChk( DAQmxCreateTask ("Digital Counter Task", &ctrTaskHandle) );
	
	DAQmxErrChk( DAQmxCreateTask ("Digital Output Task 6341", &do6341TaskHandle) );  //DO PXIe 6341
	DAQmxErrChk( DAQmxCreateTask ("Analog Output Task 6341", &ao6341TaskHandle) );   //AO PXIe 6341 
	
	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_INIT_TASK;
}



/* ------------------------------------------------------------------------------------ */
/* Routine di definizione dei canali di digital I/O										*/
/* ------------------------------------------------------------------------------------ */

int InitDIO(void)
{
	int error=0;
	printf ("INIT DIO\n");
	/**************************************************************/
	/* Definizione della port0 come digital output (line0:31)	  */
	/**************************************************************/
	
	DAQmxErrChk( DAQmxCreateDOChan (doTaskHandle, Ch_NI6363_DIO[EV01].address, "EV", DAQmx_Val_ChanForAllLines) );
	
	DAQmxErrChk( DAQmxCreateDOChan (do6341TaskHandle, Ch_NI6341_DIO[EV_SYS1].address, "EV_SYS", DAQmx_Val_ChanForAllLines) );	   //DO PXIe 6341
	
	/**************************************************************/
	/* Definizione della port1 come digital input (line0:7)		  */
	/**************************************************************/
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[G1].address, "G1", DAQmx_Val_ChanPerLine) );
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[FC1].address, "FC1", DAQmx_Val_ChanPerLine) ); 
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[FC2].address, "FC2", DAQmx_Val_ChanPerLine) ); 
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[FC3].address, "FC3", DAQmx_Val_ChanPerLine) ); 
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[FC4].address, "FC4", DAQmx_Val_ChanPerLine) ); 
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[FC5].address, "FC5", DAQmx_Val_ChanPerLine) ); 
	DAQmxErrChk( DAQmxCreateDIChan (diTaskHandle, Ch_NI6363_DIO[FC6].address, "FC6", DAQmx_Val_ChanPerLine) ); 

	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_DIO_CHANNEL_DEF;	
}


/* ------------------------------------------------------------------------------------ */
/* Routine di definizione dei canali di counter											*/
/* ------------------------------------------------------------------------------------ */

int InitCNT(void)
{
	int error=0;
	printf ("INIT CNT\n");
	/**************************************************************/
	/* Definizione dei counter per la lettura di frequenza		  */
	/**************************************************************/
	DAQmxCreateCIFreqChan (ctrTaskHandle, Ch_NI6363_CTR[F2].address, "F2", 4.0, 1500.0, DAQmx_Val_Hz, DAQmx_Val_Rising, DAQmx_Val_LowFreq1Ctr, 0.3, 4, "");
	
	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_CTR_CHANNEL_DEF;	
}



/* ------------------------------------------------------------------------------------ */
/* Routine di definizione dei canali di analog input ed analog output secondo gli       */
/* schemi di cablaggio definiti dal file header "HW_Description.h"						*/
/* ------------------------------------------------------------------------------------ */

int InitAIN(void)
{
	int error=0;
	printf ("INIT AIN\n");
	/**************************************************************/
	/* Definizione dei canali di input analogici				  */
	/**************************************************************/
	
		
	//creo canale di input analogico da scheda PXI-6363 per l'acquisizione di pressioni
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P02].address , "P02", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P03].address , "P03", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P04].address , "P04", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P05].address , "P05", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P06].address , "P06", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P07].address , "P07", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P08].address , "P08", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P09].address , "P09", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P10].address , "P10", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[P11].address , "P11", DAQmx_Val_RSE, PRESSURE_AIMIN, PRESSURE_AIMAX, DAQmx_Val_Volts, "") );

	//lettura della temperatura dell'olio sul ritorno
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[K1].address , "K1", DAQmx_Val_RSE, TEMP_AIMIN, TEMP_AIMAX, DAQmx_Val_Volts, "") );
	
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6341_AIN[PSU3].address, "PSU3", DAQmx_Val_Diff, VOLTAGE_LW_LIMIT, VOLTAGE_UP_LIMIT, DAQmx_Val_Volts, "") );
	
	//lettura di posizione tramite sensori magnetostrittivi
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[L1].address, "L1", DAQmx_Val_Diff, MAGNETIC_AIMIN, MAGNETIC_AIMAX, DAQmx_Val_Volts, "") ); //DIFFERENTIAL
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[L2].address, "L2", DAQmx_Val_Diff, MAGNETIC_AIMIN, MAGNETIC_AIMAX, DAQmx_Val_Volts, "") ); //DIFFERENTIAL

	// canali di lettura analogici PXIe 6341
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6341_AIN[CV_SYS1].address, "CV_SYS1", DAQmx_Val_Diff, SHUNT_LW_LIMIT, SHUNT_UP_LIMIT, DAQmx_Val_Volts, "") ); //DIFFERENTIAL 
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6341_AIN[CV_SYS2].address, "CV_SYS2", DAQmx_Val_Diff, SHUNT_LW_LIMIT, SHUNT_UP_LIMIT, DAQmx_Val_Volts, "") ); //DIFFERENTIAL 
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6341_AIN[SYS1_S].address, "SYS1_S", DAQmx_Val_Diff, SHUNT_LW_LIMIT, SHUNT_UP_LIMIT, DAQmx_Val_Volts, "") ); //DIFFERENTIAL LIMITS????
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6341_AIN[SYS2_S].address, "SYS2_S", DAQmx_Val_Diff, SHUNT_LW_LIMIT, SHUNT_UP_LIMIT, DAQmx_Val_Volts, "") ); //DIFFERENTIAL LIMITS????
	
	// lettura della tensione in uscita dalla turbina F1 per la misura di portata in ingresso al banco idraulico
	DAQmxErrChk( DAQmxCreateAIVoltageChan (aiTaskHandle, Ch_NI6363_AIN[F1].address , "F1", DAQmx_Val_RSE, FREQUENCY_LW_LIMIT, FREQUENCY_UP_LIMIT, DAQmx_Val_Volts, "") );
	
	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_AIN_CHANNEL_DEF;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di creazione canali di input analogico da scheda SCXI di input LVDT			*/
/* ------------------------------------------------------------------------------------ */

int InitLvdt(void)
{
	int error=0;
	printf ("INIT LVDT\n");
	// Sensore LVDT montato a bordo del CSAS sul sistema 1
	DAQmxErrChk( DAQmxCreateAIPosLVDTChan (aiTaskHandle, Ch_NI4340[LVDT_SYS1].address , "LVDT1", LVDT_AIMIN, LVDT_AIMAX, DAQmx_Val_Meters, LVDT_SENSITIVITY,
							  DAQmx_Val_mVoltsPerVoltPerMillimeter, DAQmx_Val_Internal, LVDT_VOLTAGE, LVDT_FREQUENCY, DAQmx_Val_4Wire, "") );
	
	// Sensore LVDT montato a bordo del CSAS sul sistema 2
	DAQmxErrChk( DAQmxCreateAIPosLVDTChan (aiTaskHandle, Ch_NI4340[LVDT_SYS2].address , "LVDT2", LVDT_AIMIN, LVDT_AIMAX, DAQmx_Val_Meters, LVDT_SENSITIVITY,
							  DAQmx_Val_mVoltsPerVoltPerMillimeter, DAQmx_Val_Internal, LVDT_VOLTAGE, LVDT_FREQUENCY, DAQmx_Val_4Wire, "") );
	
	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_DIO_CHANNEL_DEF;	
}


/* ------------------------------------------------------------------------------------ */
/* Routine di creazione canali di input analogico da scheda SCXI di input Strain Gauge	*/
/* ------------------------------------------------------------------------------------ */

int InitBridge(void)
{
	int error=0;
	printf ("INIT BRIDGE\n");
	// Strain gauge montato su martinetto di controllo della leva manuale	
	DAQmxCreateAIVoltageChanWithExcit (aiTaskHandle, Ch_NI4339[N1].address, "N1bridge", DAQmx_Val_Diff, -0.1, 0.1, DAQmx_Val_Volts,
									   DAQmx_Val_FullBridge, DAQmx_Val_Internal, 10.0, 0, "");
	
	// Strain gauge montato sul martinetto di carico del main piston del servocomando	
	DAQmxCreateAIVoltageChanWithExcit (aiTaskHandle, Ch_NI4339[N2].address, "N2bridge", DAQmx_Val_Diff, -0.1, 0.1, DAQmx_Val_Volts,
									   DAQmx_Val_FullBridge, DAQmx_Val_Internal, 10.0, 0, "");
	
	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return error;	
}


/* ------------------------------------------------------------------------------------ */
/* Routine di definizione dei canali di analog output									*/
/* ------------------------------------------------------------------------------------ */

int InitAOUT(void)
{
	int error=0;
	printf ("INIT AOUT\n");
	/**************************************************************/
	/* Definizione dei canali di output analogici				  */
	/**************************************************************/
	
	DAQmxErrChk( DAQmxCreateAOVoltageChan (aoTaskHandle, Ch_NI6363_AOUT[SV1].address, "SV1", SERVOVALVE_AOMIN, SERVOVALVE_AOMAX, DAQmx_Val_Volts, NULL));	// SV1 load
	DAQmxErrChk( DAQmxCreateAOVoltageChan (aoTaskHandle, Ch_NI6363_AOUT[SV2].address, "SV2", SERVOVALVE_AOMIN, SERVOVALVE_AOMAX, DAQmx_Val_Volts, NULL));	// SV2 input
	DAQmxErrChk( DAQmxCreateAOVoltageChan (aoTaskHandle, Ch_NI6363_AOUT[V3].address, "V3", SERVOVALVE_AOMIN, SERVOVALVE_AOMAX, DAQmx_Val_Volts, NULL));	// V3 pressure
	
	DAQmxErrChk( DAQmxCreateAOVoltageChan (ao6341TaskHandle, Ch_NI6341_AOUT[SV_SYS1].address, "SV_SYS1", SERVOVALVE_AOMIN, SERVOVALVE_AOMAX, DAQmx_Val_Volts, NULL));	// CSAS1
	DAQmxErrChk( DAQmxCreateAOVoltageChan (ao6341TaskHandle, Ch_NI6341_AOUT[SV_SYS2].address, "SV_SYS2", SERVOVALVE_AOMIN, SERVOVALVE_AOMAX, DAQmx_Val_Volts, NULL));	// CSAS2
	
	return PASS;
		
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_AOUT_CHANNEL_DEF;	
}
 
/* ------------------------------------------------------------------------------------ */
/* Routine di definizione dei rate di input e output analogici per la sincronizzazione	*/
/* hardware dei sample rate in funzione del PID rate di controllo ritenuto necessario	*/
/* ------------------------------------------------------------------------------------ */

int InitSync (void) //check sync with 6341
{
	int error=0;	
	printf ("SET TIMING ATTRIBUTE\n");
	DAQmxErrChk (DAQmxSetTimingAttribute(aoTaskHandle,DAQmx_RefClk_Rate,100000000.0));
	DAQmxErrChk (DAQmxSetTimingAttribute(aoTaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));

	DAQmxErrChk (DAQmxSetTimingAttribute(aiTaskHandle,DAQmx_RefClk_Rate,100000000.0));
	DAQmxErrChk (DAQmxSetTimingAttribute(aiTaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));
	
	DAQmxErrChk (DAQmxSetTimingAttribute(ao6341TaskHandle,DAQmx_RefClk_Rate,100000000.0));  
	DAQmxErrChk (DAQmxSetTimingAttribute(ao6341TaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));
	
	DAQmxErrChk (DAQmxSetTimingAttribute(do6341TaskHandle,DAQmx_RefClk_Rate,100000000.0));  
	DAQmxErrChk (DAQmxSetTimingAttribute(do6341TaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));
	
	DAQmxErrChk (DAQmxSetTimingAttribute(diTaskHandle,DAQmx_RefClk_Rate,100000000.0));  
	DAQmxErrChk (DAQmxSetTimingAttribute(diTaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));
	
	DAQmxErrChk (DAQmxSetTimingAttribute(doTaskHandle,DAQmx_RefClk_Rate,100000000.0));  
	DAQmxErrChk (DAQmxSetTimingAttribute(doTaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));
	
	DAQmxErrChk (DAQmxSetTimingAttribute(ctrTaskHandle,DAQmx_RefClk_Rate,100000000.0));  
	DAQmxErrChk (DAQmxSetTimingAttribute(ctrTaskHandle,DAQmx_RefClk_Src,"PXIe_Clk100"));
	
	DAQmxErrChk( DAQmxCfgSampClkTiming (aiTaskHandle, NULL, PID_RATE, DAQmx_Val_Rising, DAQmx_Val_HWTimedSinglePoint, 1));
	DAQmxErrChk( DAQmxCfgSampClkTiming (aoTaskHandle, "/PXI1Slot2/ai/SampleClock", PID_RATE, DAQmx_Val_Rising, DAQmx_Val_HWTimedSinglePoint, 1));
	DAQmxErrChk( DAQmxCfgSampClkTiming (ao6341TaskHandle, "/PXI1Slot2/ai/SampleClock", PID_RATE, DAQmx_Val_Rising, DAQmx_Val_HWTimedSinglePoint, 1));

	return PASS;
				
Error:
	StopHwProgram();			// stop del programma su RT target
	return ERROR_SYNC_CHANNEL;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di auto-calibrazione delle schede su cestello PXI							*/
/* ------------------------------------------------------------------------------------ */

int ExecCalibration (void)
{
	int error=0;	
	printf ("CALIBRATION AI...\n"); 
	// calibrazione scheda PXI-6363
	DAQmxErrChk( DAQmxSelfCal ("PXI1Slot2") );
	// calibrazione scheda PXIe 6341
	DAQmxErrChk( DAQmxSelfCal ("PXI1Slot5") );
	printf ("CALIBRATION LVDT...\n"); 
	// calibrazione scheda PXIe 4340
	DAQmxErrChk( DAQmxSelfCal ("PXI1Slot3") );
	
	return PASS;
				
Error:
	StopHwProgram();			// stop del programma su RT target
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di auto-zero ponti estensimetrici											*/
/* ------------------------------------------------------------------------------------ */

int ExecNulling (void)
{
	int error=0;	
	
	printf ("CALIB BRIDGE\n");
	// calibrazione scheda Load Cells
	DAQmxErrChk( DAQmxPerformBridgeOffsetNullingCalEx (aiTaskHandle, "N1bridge", TRUE) );
	DAQmxErrChk( DAQmxPerformBridgeOffsetNullingCalEx (aiTaskHandle, "N2bridge", TRUE) );
	
	return PASS;
				
Error:
	StopHwProgram();			// stop del programma su RT target
	return error;				//-201444
}


/* ------------------------------------------------------------------------------------ */
/* Routine di scrittura del singolo bit sulla porta di digital output port0 con 		*/
/* verifica di integrita' e sicurezza													*/
/* ------------------------------------------------------------------------------------ */

int SetDO(int channel, int bit_value)
{
	int check=0;
	int error=0;
	int i=0;
	uInt32 port0=0x0;
	int old_bit_value=0;
	int channel_id=0;
	
	// verifico parametri di ingresso
	if ((channel<EV01)||(channel>EV09B))
	{
		error=ERROR_CHANNEL_PAR;
		goto Error;
	}
	if ((bit_value!=OPEN)&&(bit_value!=CLOSE))
	{
		error=ERROR_CHANNEL_VAL;
		goto Error;
	}

	// indice nell'array immegine "solenoid_valve"
	channel_id = channel-EV01;
	
	// memorizzazione stato attuale
	old_bit_value = solenoid_valve[channel_id];
	
	// assegnazione stato
	solenoid_valve[channel_id] = bit_value;
		
	// nuovo valore binario della porta 0 di digital output
	for (i=0;i<NUM_EV;i++)
		port0 |= ( (solenoid_valve[i] & 0x1) << (i+EV01));
	
	// scrittura porta0 per controllo elettrovoalvole
	DAQmxErrChk( DAQmxWriteDigitalU32 (doTaskHandle, 1, 1, 10.0, DAQmx_Val_GroupByChannel, &port0, NULL, NULL) );

	return PASS;
	
Error:
	solenoid_valve[channel_id] = old_bit_value;	// ripristino immagine stato EV
	if (error!=0)  
		printf ("%d \n SetDO ",error);
	StopHwProgram();							// stop del programma su RT target
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di scrittura del singolo bit sulla porta di digital output port0 			*/
/* della scheda PXIe 6341 con verifica di integrita' e sicurezza							*/
/* ------------------------------------------------------------------------------------ */
int SetDO6341(int channel, int bit_value)
{
	int check=0;
	int error=0;
	int i=0;
	uInt32 port0=0x0;
	int old_bit_value=0;
	int channel_id=0;
	
	// verifico parametri di ingresso
	if ((channel<EV_SYS1)||(channel>EV_SYS2))
	{
		error=ERROR_CHANNEL_PAR;
		goto Error;
	}
	if ((bit_value!=OPEN)&&(bit_value!=CLOSE))
	{
		error=ERROR_CHANNEL_VAL;
		goto Error;
	}

	// indice nell'array immagine "valve_6341"
	channel_id = channel-EV_SYS1;
	
	// memorizzazione stato attuale
	old_bit_value = valve_6341[channel_id];
	
	// assegnazione stato
	valve_6341[channel_id] = bit_value;
		
	// nuovo valore binario della porta 0 di digital output
	for (i=0;i<NUM_EV_6341;i++)
		port0 |= ( (valve_6341[i] & 0x1) << (i+EV_SYS1));
	
	// scrittura porta0 per controllo elettrovoalvole
	DAQmxErrChk( DAQmxWriteDigitalU32 (do6341TaskHandle, 1, 1, 10.0, DAQmx_Val_GroupByChannel, &port0, NULL, NULL) );

	return PASS;
	
Error:
	valve_6341[channel_id] = old_bit_value;	// ripristino immagine stato EV
	if (error!=0)  
		printf ("%d \n SetDO6341 ",error);
	StopHwProgram();							// stop del programma su RT target
	return error;
}

/* ------------------------------------------------------------------------------------ */
/* Routine di lettura del singolo bit dalla porta di digital input port1 				*/
/* ------------------------------------------------------------------------------------ */

int GetDI(int channel, int *bit_value)
{
	int error=0;
	int i=0;
	uInt8 port1[7];
	
	// verifico parametri di ingresso
	if ((channel<G1)||(channel>FC6))
	{
		error=ERROR_CHANNEL_PAR;
		goto Error;
	}
	
	for (i=0;i<7;i++)
		port1[i]=0;
		
	// lettura porta1 di input
	DAQmxErrChk( DAQmxReadDigitalLines (diTaskHandle, 1, 10.0, DAQmx_Val_GroupByChannel, port1, 7*sizeof(uInt8), NULL, NULL, 0) );
	
	// estrazione della linea di interesse
	*bit_value = port1[channel-G1];
	
	return PASS;
	
Error:
	StopHwProgram();			// stop del programma su RT target
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di lettura di frequenza mediante counter						 				*/
/* ------------------------------------------------------------------------------------ */

int GetFrequency(int channel, double *frequency_value)
{
	int error=0;
	
	// verifico parametri di ingresso
	if (channel!=F2)
	{
		error=ERROR_CHANNEL_PAR;
		goto Error;
	}
	
	// lettura frequenza da counter0
	DAQmxErrChk( DAQmxReadCounterScalarF64 (ctrTaskHandle, 10.0, frequency_value, 0) );
	
	return PASS;
	
Error:
	// l'errore puo' essere dovuto all'assenza di leakage
	*frequency_value=0.0;
	return error;
}
 

/* ------------------------------------------------------------------------------------ */
/* Routine di lettura dello stato di eccitazione di un rele' NI			 				*/
/* ------------------------------------------------------------------------------------ */

//int GetRelay(int channel, int *relay_status)
//{
//	int error=0;
//	
//	// verifico parametri di ingresso
//	if ((channel<SW1)||(channel>SW13))
//	{
//		error=ERROR_CHANNEL_PAR;
//		goto Error;
//	}
//	
//	DAQmxErrChk( DAQmxSwitchGetSingleRelayPos (Ch_NI2566[channel].address , relay_status) );
//	
//	return PASS;
//	
//Error:
//	StopHwProgram();			// stop del programma su RT target
//	return error;	
//}



/* ------------------------------------------------------------------------------------ */
/* Routine per l'impostazione di eccitazione di un rele' NI			 					*/
/* ------------------------------------------------------------------------------------ */

//int SetRelay(int channel, int relay_status)
//{
//	int error=0;
//	
//	// verifico parametri di ingresso
//	if ((channel<SW0)||(channel>SW13))
//	{
//		error=ERROR_CHANNEL_PAR;
//		goto Error;
//	}
//	if ((relay_status<OFF)||(relay_status>ON))
//	{
//		error=ERROR_CHANNEL_VAL;
//		goto Error;
//	}		
//	
//	// Setting del rele'
//	switch (relay_status)
//	{
//		case OFF:
//			DAQmxErrChk( DAQmxSwitchOpenRelays (Ch_NI2566[channel].address, 1) );	
//			break;
//		case ON:
//			DAQmxErrChk( DAQmxSwitchCloseRelays (Ch_NI2566[channel].address, 1) );
//			break;
//	}
//	
//	return PASS;
//	
//Error:
//	StopHwProgram();			// stop del programma su RT target
//	return error;		
//}



/* ------------------------------------------------------------------------------------ */
/* Routine di creazione e inizializzazione istanze dei controllori PID 					*/
/* ------------------------------------------------------------------------------------ */

int InitPid(void)
{
	int error=0;
	double DT = 1.0 / PID_RATE;
	printf ("INIT PID\n"); 
	// crea e configura il PID per il controllo CSAS Sistema1
	ErrChk( CreatePid (CSAS1_PID, DT, SERVOVALVE_AOMIN, SERVOVALVE_AOMAX) );
	ErrChk( SetPidOutputRate (CSAS1_PID, PID_RATE) );
	
	// crea e configura il PID per il controllo CSAS Sistema2
	ErrChk( CreatePid (CSAS2_PID, DT, SERVOVALVE_AOMIN, SERVOVALVE_AOMAX) );	
	ErrChk( SetPidOutputRate (CSAS2_PID, PID_RATE) );
	
	// crea e configura il PID per il controllo del martinetto di input
	ErrChk( CreatePid (INPUT_PID, DT, SERVOVALVE_AOMIN, SERVOVALVE_AOMAX) );	
	ErrChk( SetPidOutputRate (INPUT_PID, PID_RATE) );
	
	// crea e configura il PID per il controllo del martinetto di carico
	ErrChk( CreatePid (LOAD_PID, DT, LOAD_AOMIN, LOAD_AOMAX) );	
	ErrChk( SetPidOutputRate (LOAD_PID, PID_RATE) );

	return PASS;
	
Error:
	StopHwProgram();			// stop del programma su RT target
	return error;	
}



/* ------------------------------------------------------------------------------------ */
/* Routine di gestione dei loop di controllo PID e di acquisizione / generazione dei  	*/
/* segnali analogici da scheda PXIe 6341 e PXI-6363. Il sample rate di tutti i canali e' */
/* pari al PID rate dei controllori.													*/
/* ------------------------------------------------------------------------------------ */

int RunPid(void)
{
	int error=0;
	int i=0;
	
	int tick_count_csas1=0;						// counter per i cicli di regolazione PID eseguiti
	int tick_count_csas2=0;						// per la gestione dei transitori di avvio e spegnimento
	int tick_count_input=0;
	int tick_count_load=0;
	int tick_count_uut_voltage=0;
	int tick_count_uut_pressure=0;

	unsigned long int time=0;
	double set_point=0;				// setpoint del controllore in funzione di set-point statico, frequenza, ampiezza e tipo d'onda
	int periodo=0;					// periodo della forma d'onda dell'onda di setpoint
	int wave_time;					// numero di campione all'interno della singola forma d'onda
	double step;					// pendenza della rampa nel caso di onda sinusoidale [V/sample]
	
	double an_val;
	
	double csas_sp=0.0;					
	double input_sp=0.0;
	double load_sp=0.0;
	
	double safe_input_sp=0.0;		// valore di input_sp che garantisce uno sforzo sulla leva di input appena 
									// superiore al valore di SAFETY_EFFORT_LIMIT
	int safety_flag=0;				// 0-sforzo nei limiti di sicurezza 1-sforzo eccede i limiti di sicurezza
	
	const int transient_tick=2*PID_RATE;		// i transitori di avvio e spegnimento vengono gestiti variando
												// la banda proporzionale dei controllori nei primi/ultimi 2 
												// secondi di regolazione. Si ricorda a tal proposito che la 
												// banda proporzionale del PID e' data dalla relazione
												//			PBu = 100 / Kc
												// dove Kc e' il guadagno proporzionale del controllore. Essendo
												// PBu e Kc inversamente proporzionali si puo' in avvio di
												// variare PBu da 1000*Kc a Kc nominale, eseguendo il "percorso"
												// inverso per il transitorio di spegnimento.
	
	int k;										// fattore di correzione dei coefficienti PID durante i transitori
	double previous_input_buffer[NUM_MEAS_IN];	// array contenente gli ultimi campioni analogici acquisiti
	double input_buffer[NUM_MEAS_IN];			// array contenente il campionamento attuale
	
	double processVariable=0.0;					// campione della variabile di processo
	double output_buffer[NUM_CH_AOUT];			// array contenente i campioni di uscita analogica
	double output_buffer_6341[NUM_CH_AOUT_6341];// array contenente i campioni di uscita analogica PXIe 6341
	
	double SetPoint=0;
		
	for(i=0;i<NUM_CH_AIN;i++)
	{
		previous_input_buffer[i]=0.0;	
		input_buffer[i]=0.0;
	}
	
	for(i=0;i<NUM_CH_AOUT;i++)
		output_buffer[i]=0.0;
	
	for(i=0;i<NUM_CH_AOUT_6341;i++)	// inizializzazione array uscite analogiche PXIe 6341
		output_buffer_6341[i]=0.0;


	/************************************************************/
	/*		INIZIALIZZAZIONE CONTROLLORI SOFTWARE PID			*/
	/************************************************************/	
	InitPid();
	printf ("PIDs INITIALIZED\n");
	/************************************************************/
	/* 				AVVIO TASK DI I/O ANALOGICO					*/
	/************************************************************/	
	DAQmxErrChk( DAQmxStartTask(aoTaskHandle) );
	DAQmxErrChk( DAQmxStartTask(ao6341TaskHandle) );
	DAQmxErrChk( DAQmxStartTask(aiTaskHandle) );
	printf ("ANALOG TASKS STARTED\n");
	/************************************************************/
	/* 					MAIN LOOP DI CONTROLLO					*/
	/************************************************************/	
	/* Termina allo spegnimento o all'uscita dal programma.		*/
	/*															*/
	/* L'acquisizione dei canali analogici avviene durante 		*/
	/* l'intera esecuzione del programma, indipendentemente		*/
	/* dall'attivazione o meno dei controllori PID.				*/
	/************************************************************/
	int cnt = 0;
	
	while ( (!RTIsShuttingDown ())&&(QuitProgram==0) )
	{
		double coefficienti[3]={0,0,0};	// (kC, tI, tD)

		/* ---------------------------------------------------- */
		/* Lettura di tutti i canali analogici dichiarati		*/
		/* 1 S/ch ; timeout=10 ; dim_buffer=NUM_CH_AIN			*/
		/* ---------------------------------------------------- */
		DAQmxErrChk( DAQmxReadAnalogF64 (aiTaskHandle, 1, 10.0, DAQmx_Val_GroupByChannel, input_buffer, NUM_CH_AIN, NULL, 0) );

		/****************************************************************************/		
		/****************************************************************************/
		/* 								LOOP DI REGOLAZIONE							*/
		/****************************************************************************/
		/****************************************************************************/
		
		/*------------------------------*/
		/* Generazione Set Point		*/
		/*------------------------------*/
		
		if ((enCsas1Pid==1)||(enCsas2Pid==1))
		{
			// inizzializzazione coefficienti con i valori di default presi da "PID_parameters", eventualmente tunnati
			if (enCsas1Pid==1)
			{
				coefficienti[0] = PID_parameters[CSAS1_PID].proportional_gain;
				coefficienti[1] = PID_parameters[CSAS1_PID].integral_time;
				coefficienti[2] = PID_parameters[CSAS1_PID].derivative_time;
			}
			else
			{
				coefficienti[0] = PID_parameters[CSAS2_PID].proportional_gain;
				coefficienti[1] = PID_parameters[CSAS2_PID].integral_time;
				coefficienti[2] = PID_parameters[CSAS2_PID].derivative_time;
			}


			// porta la leva di input in posizione statica
			input_sp = InputSetPoint; 	
			
			if (WaveType==STATIC)
			{
				// porta gli autopiloti in posizione statica
				csas_sp = CsasSetPoint;
				time=0;

				if (csas_sp>=-0.9 && csas_sp<=0.9)
				{
					// tunning in caso di WaveType STATIC e csas_sp nel range [-0.9, 0.9] per fix sul rumore in tale posizione
					coefficienti[0] = -1000.0;
					coefficienti[1] = 0.005;
					coefficienti[2] = 0.0;
				}

			}
			else
			{
				// generazione setpoint secondo forma d'onda	
				
				// evita errori runtime
				if (WaveFrequency==0.0)
					WaveFrequency=0.1;
				
				periodo= (int)(PID_RATE/WaveFrequency);		// numero di loop di regolazione in cui viene disegnata la singola forma d'onda
				wave_time=time%periodo;						// indice del numero di campione all'interno della singola forma d'onda

				switch (WaveType)
				{
					case SINE:
					
						csas_sp = CsasSetPoint + WaveAmplitude * sin (2*PI*WaveFrequency*time/PID_RATE);
					
						break;
					case SQUARE:
						
						if (wave_time<(periodo/2))
							csas_sp = CsasSetPoint + WaveAmplitude;
						else
							csas_sp = CsasSetPoint - WaveAmplitude;
							
						break;
					case TRIANGULAR:
						
						step = 4*WaveAmplitude/periodo;
						
						if (wave_time<=(periodo/4))
							csas_sp = CsasSetPoint + step*wave_time;
						else if (wave_time<=(3*periodo/4))
							csas_sp = CsasSetPoint + WaveAmplitude - step* (wave_time - periodo/4);
						else if (wave_time<periodo)
							csas_sp = CsasSetPoint - WaveAmplitude + step* (wave_time - 3*periodo/4);
						
						break;
				}
				
				// il tempo scorre...
				time++;       
			}
		}
		else if (enInputPid==1)
		{
			if (WaveType==STATIC)
			{
				// porta il martinetto in posizione statica
				input_sp = InputSetPoint;
				time=0;
			}
			else
			{
				// generazione setpoint secondo forma d'onda	
				
				// evita errori runtime
				if (WaveFrequency==0.0)
					WaveFrequency=0.1;
				
				periodo= (int)(PID_RATE/WaveFrequency);		// numero di loop di regolazione in cui viene disegnata la singola forma d'onda
				wave_time=time%periodo;						// indice del numero di campione all'interno della singola forma d'onda

				switch (WaveType)
				{
					case SINE:
					
						input_sp = InputSetPoint + WaveAmplitude * sin (2*PI*WaveFrequency*time/PID_RATE);
					
						break;
					case SQUARE:
						
						if (wave_time<(periodo/2))
							input_sp = InputSetPoint + WaveAmplitude;
						else
							input_sp = InputSetPoint - WaveAmplitude;
							
						break;
					case TRIANGULAR:
						
						step = 4*WaveAmplitude/periodo;
						
						if (wave_time<=(periodo/4))
							input_sp = InputSetPoint + step*wave_time;
						else if (wave_time<=(3*periodo/4))
							input_sp = InputSetPoint + WaveAmplitude - step* (wave_time - periodo/4);
						else if (wave_time<periodo)
							input_sp = InputSetPoint - WaveAmplitude + step* (wave_time - 3*periodo/4);  
				
						break;
				}
				
				// il tempo scorre...
				time++;       
			}
		}

		
		
		/* Generazione set-point di carico 	*/
		/*		compressione > 0			*/
		/*		trazione < 0				*/

		if (enLoadPid==1)  //da chiarire
			load_sp = LoadSetPoint;
		else
			load_sp = 0.0;
		
		
		/*--------------------------------------*/
		/* Check di sovraccarico leva di input 	*/
		/*--------------------------------------  
		
		if ( fabs(input_buffer[N1_MUX]) > SAFETY_EFFORT_LIMIT)
		{
			if (safety_flag==0)
			{
				safe_input_sp = input_sp;
				safety_flag=1;
			}
			else
			{
				input_sp = safe_input_sp;
			}
		}
		else
		{
			safety_flag=0;
		}
		*/
		
		/*------------------------------------------*/
		/* Assegnazione setpoint al vettore misure	*/
		/*------------------------------------------*/
		
		input_buffer[INPUT_MUX] = input_sp;
		input_buffer[CSAS_MUX] = csas_sp;
		input_buffer[LOAD_MUX] = load_sp;
		
		
		/*------------------------------*/
		/* Loop di controllo del CSAS1	*/
		/*------------------------------*/
		
		if (enCsas1Pid==1)
		{
			// Parametri di controllo
			SetPidParameters (CSAS1_PID, coefficienti[0], coefficienti[1], coefficienti[2]);
				
			/* Variazione run-time del setpoint della regolazione */
			SetPidSetpoint (CSAS1_PID, -csas_sp);    
			
			// feedback di posizione da LVDT su CSAS1
			processVariable = input_buffer[LVDT1_MUX];
			
			// nuovo segnale di controllo della servovalvola a bordo del CSAS1
			//GetNextPidOutput(CSAS1_PID, processVariable, &output_buffer[SV_SYS1_MUX]);
			GetNextPidOutput(CSAS1_PID, processVariable, &output_buffer_6341[SV_SYS1]); 
			
			// @sz per debug riporto il setpoint come uscita
			//output_buffer[SV_SYS1_MUX] = csas_sp*100.0;
			//output_buffer[SV_SYS1_MUX] = 0.0;
		}
		else
		{
			// se il controllore e' disabilitato si manda a 0V il livello di tensione in uscita
			// sulla servovalvola del CSAS1
			output_buffer_6341[SV_SYS1] = 0.0;	
		}
		
		/*------------------------------*/
		/* Loop di controllo del CSAS2	*/
		/*------------------------------*/
		
		if (enCsas2Pid==1)
		{
			/* Parametri di controllo */		
			SetPidParameters (CSAS2_PID, coefficienti[0], coefficienti[1], coefficienti[2]);

			/* Variazione run-time del setpoint della regolazione */
			SetPidSetpoint (CSAS2_PID, csas_sp);    
			
			// feedback di posizione da LVDT su CSAS2
			processVariable = input_buffer[LVDT2_MUX];
			//processVariable = 0.0;
			
			// nuovo segnale di controllo della servovalvola a bordo del CSAS2
			//GetNextPidOutput(CSAS2_PID, processVariable, &output_buffer[SV_SYS2_MUX]);
			GetNextPidOutput(CSAS2_PID, processVariable, &output_buffer_6341[SV_SYS2]);
			
			// @sz per debug riporto il setpoint come uscita
			//output_buffer[SV_SYS2_MUX] = -csas_sp*100.0;
			//output_buffer[SV_SYS2_MUX] = 0.0; 
		}
		else
		{
			// se il controllore e' disabilitato si manda a 0V il livello di tensione in uscita
			// sulla servovalvola del CSAS2
			output_buffer_6341[SV_SYS2] = 0.0;	
		}

		
		/*------------------------------------------------------*/
		/* Loop di controllo del martinetto sulla leva di input	*/
		/*------------------------------------------------------*/
		
		if (enInputPid==1)
		{
			/* Parametri di controllo */
			SetPidParameters (INPUT_PID, PID_parameters[INPUT_PID].proportional_gain, 
					PID_parameters[INPUT_PID].integral_time, PID_parameters[INPUT_PID].derivative_time);
			
			/* Variazione run-time del setpoint della regolazione */
			SetPidSetpoint (INPUT_PID, input_sp);    
			
			// feedback di posizione da magnetostrittivo su martinetto di input L1
			processVariable = input_buffer[L1_MUX];
			
			// nuovo segnale di controllo della servovalvola del martinetto di input
			GetNextPidOutput(INPUT_PID, processVariable, &output_buffer[SV1]);
			
			// @sz per debug riporto il setpoint come uscita
			//output_buffer[SV1_MUX] = input_sp/10.0;
		}
		else
		{
			// se il controllore e' disabilitato si manda a 0V il livello di tensione in uscita
			// sulla servovalvola del martinetto di input
			output_buffer[SV1] = 0.0;	
		}


		/*------------------------------------------------------------------------------*/
		/* Loop di controllo del martinetto di carico sul main piston del servocomando	*/
		/*------------------------------------------------------------------------------*/
		
		if (enLoadPid==1)
		{
			/* Parametri di controllo */ 
			SetPidParameters (LOAD_PID, PID_parameters[LOAD_PID].proportional_gain, 
					PID_parameters[LOAD_PID].integral_time, PID_parameters[LOAD_PID].derivative_time);
			
			/* Variazione run-time del setpoint della regolazione */
			SetPidSetpoint (LOAD_PID, load_sp);    
			
			// feedback di forza da cella di carico sul main piston
			processVariable = input_buffer[N2_MUX];
			
			// nuovo segnale di controllo della servovalvola del martinetto di carico
			GetNextPidOutput(LOAD_PID, processVariable, &output_buffer[SV2]);
			
			// gestione aggancio/sgancio a portata costante
			if (solenoid_valve[EV09A-EV01]==HI)
			{
				// @sz il pistone di carico e' a singolo stelo per cui la spinta, a parita' di sollecitazione
				// della servovalvola, e' asimmetrica e maggiore in compressione
				if (LoadDirection==COMPRESSION)
				{
					// pistone di carico in avvicinamento al servocomando
					//output_buffer[SV2_MUX] = -0.5;
					output_buffer[SV2] = -0.5; 
				}
				else if (LoadDirection==EXTENSION)
				{
					// pistone di carico in allontanamento dal servocomando
					//output_buffer[SV2_MUX] = 1.0;
					output_buffer[SV2] = 1.0;
				}
			}
		}
		else
		{
			// se il controllore e' disabilitato si manda a 0V il livello di tensione in uscita
			// sulla servovalvola del martinetto di carico
			output_buffer[SV2] = 0.0;	
		}	  			
		

		/*--------------------------------------------------------------------------------------*/
		/* Controllo della pressione di mandata verso il servocomando							*/                          
		/*--------------------------------------------------------------------------------------*/
		if ( UutPressureSetpoint<UUT_PRESSURE_AOMIN )
					output_buffer[2] = UUT_PRESSURE_AOMIN;
		else if ( UutPressureSetpoint>UUT_PRESSURE_AOMAX )
				output_buffer[2] = UUT_PRESSURE_AOMAX;
			else
				output_buffer[2] = UutPressureSetpoint;		  //output_buffer[2] � AO per V3 
	//	
	//	/****************************************************************************/
	//	/****************************************************************************/
	//	
	//	/* ---------------------------------------------------- */ 
	//	/* Setting dei livelli analogici di tensione di uscita  */
	//	/* ---------------------------------------------------- */
	//	DAQmxErrChk( DAQmxWriteAnalogF64 (aoTaskHandle, 1, 0, 10.0, DAQmx_Val_GroupByChannel, output_buffer, NULL, NULL) );			//output_buffer[SV2,SV1,V3]
	//	DAQmxErrChk( DAQmxWriteAnalogF64 (ao6341TaskHandle, 1, 0, 10.0, DAQmx_Val_GroupByChannel, output_buffer_6341, NULL, NULL) );   //output_buffer_6341[SV_SYS1,SV_SYS2]
	
		DAQmxWriteAnalogF64 (aoTaskHandle, 1, 0, 10.0, DAQmx_Val_GroupByChannel, output_buffer, NULL, NULL);			//output_buffer[SV2,SV1,V3]
		DAQmxWriteAnalogF64 (ao6341TaskHandle, 1, 0, 10.0, DAQmx_Val_GroupByChannel, output_buffer_6341, NULL, NULL);   //output_buffer_6341[SV_SYS1,SV_SYS2]
		
	//	/* -------------------------------------------------------------- */ 
	//	/* Scrittura dei valori analogici letti e settati su display RT   */
	//	/* -------------------------------------------------------------- */
		//if (cnt%500 == 0)
		//{
		////	printf("%d \n",clock());
		//	for(i=0;i<NUM_CH_AIN;i++)
		//	{
		//		an_val = input_buffer[i];
		//		printf ("%f ",an_val);
		//	}
		//	printf ("\n");
		//}
		//cnt++;
		//if (cnt%500 == 10)
		//{
		//	for(i=0;i<NUM_CH_AOUT;i++)
		//	{
		//		an_val = output_buffer[i];
		//		printf ("%f ",an_val);
		//	}
		//	printf ("\n");
		//}
		//
		//if (cnt%500 == 20)
		//{
		//	for(i=0;i<NUM_CH_AOUT_6341;i++)
		//	{
		//		an_val = output_buffer_6341[i];
		//		printf ("%f ",an_val);
		//	}
		//	printf ("\n");
		//	printf ("%d \n",enCsas1Pid);
		//	printf ("%d \n",enCsas2Pid);
		//	printf ("%d \n",enInputPid);
		//	printf ("%d \n",enLoadPid);
		//	printf ("%f \n",input_buffer[INPUT_MUX]);
		//	printf ("%f \n",input_buffer[CSAS_MUX]);
		//	printf ("%f \n",input_buffer[LOAD_MUX]);
		//}
	//	
	//	/* ---------------------------------------------------- */ 
	//	/* Scrittura dei valori analogici letti e settati 		*/
	//	/* all'interno di thread safe queue per la   			*/
	//	/* comunicazione verso la HMI e le verifiche di 		*/
	//	/* sicurezza											*/
	//	/* ---------------------------------------------------- */		
	//	
		CmtWriteTSQData (gTsqDataIn, input_buffer, NUM_MEAS_IN, TSQ_INFINITE_TIMEOUT, NULL);
	//	CmtWriteTSQData (gTsqDataOut, output_buffer, NUM_CH_AOUT, TSQ_INFINITE_TIMEOUT, NULL);
	//	
		ProcessSystemEvents ();  
	//	/* ---------------------------------------------------- */ 
	//	/* Sincronizzazione dell'esecuzione del software RT con */
	//	/* il clock hardware della scheda PXI-6363				*/		
	//	/* ---------------------------------------------------- */ 
	//	DAQmxErrChk( DAQmxWaitForNextSampleClock(aiTaskHandle, 10.0, NULL) );
		DAQmxWaitForNextSampleClock(aiTaskHandle, 10.0, NULL);	
		
		for(i=0;i<NUM_CH_AIN;i++)
			previous_input_buffer[i]=input_buffer[i];	
	}
	
	/* ---------------------------------------------------- */ 
	/* Porta a 0V i livelli di tensione in uscita quando il */
	/* programma viene terminato correttamente dall'utente  */
	/* ---------------------------------------------------- */
	for(i=0;i<NUM_CH_AOUT;i++)
		output_buffer[i]=0.0;
	
	for(i=0;i<NUM_CH_AOUT_6341;i++)	// inizializzazione array uscite analogiche PXIe 6341
		output_buffer_6341[i]=0.0;
	
	DAQmxErrChk( DAQmxWriteAnalogF64 (aoTaskHandle, 1, 0, 10.0, DAQmx_Val_GroupByChannel, output_buffer, NULL, NULL) );
	DAQmxErrChk( DAQmxWriteAnalogF64 (ao6341TaskHandle, 1, 0, 10.0, DAQmx_Val_GroupByChannel, output_buffer_6341, NULL, NULL) );
		
	
	return PASS;	
	
Error:
	printf ("%d \n",enCsas1Pid);
	printf ("%d \n",enCsas2Pid);
	printf ("%d \n",enInputPid);
	printf ("%d \n",enLoadPid);
	printf ("%d \n",QuitProgram);
	printf ("%d \n",error);  
	return error;		
}


/* ------------------------------------------------------------------------------------ */
/* Routine di gestione del loop di controllo degli ingressi digitali di allarme  		*/
/* provenienti da proximity switch, fine corsa o indicatori di intasamento.				*/
/* ------------------------------------------------------------------------------------ */

int RunDigitalControl(void)    
{
	int error=0;
	
	int bit;
	unsigned int status=0x0;
	double leakage=0;

	/************************************************************/
	/* Callback di riempimento Thread Safe Queue				*/
	/************************************************************/

	CmtInstallTSQCallback (gTsqDataIn, EVENT_TSQ_ITEMS_IN_QUEUE, 100*NUM_MEAS_IN, SendMeasuresToHostCB, NULL,
		CmtGetCurrentThreadID(), &SendMeasureQueueCB);

	/************************************************************/
	/* 				AVVIO TASK DI I/O DIGITALE					*/
	/************************************************************/	
	DAQmxErrChk( DAQmxStartTask(diTaskHandle) );
	DAQmxErrChk( DAQmxStartTask(doTaskHandle) );
	DAQmxErrChk( DAQmxStartTask(do6341TaskHandle) );
	DAQmxErrChk( DAQmxStartTask(ctrTaskHandle) );
	
	/************************************************************/
	/* 					MAIN LOOP DI CONTROLLO					*/
	/************************************************************/	
	/* Termina allo spegnimento o all'uscita dal programma.		*/
	/*															*/
	/* L'acquisizione dei canali digitali avviene durante 		*/
	/* l'intrera esecuzione del programma, indipendentemente	*/
	/* dall'attivazione o meno dei controllori PID.				*/
	/* Nello stesso loop avviene la lettura delle thread safe	*/
	/* queue e l'invio delle misure analogiche verso il PC Host	*/
	/************************************************************/
	
	while ( (!RTIsShuttingDown ())&&(QuitProgram==0) )
	{
		/************************************************************/
		/* Inizializzazione dello stato con le elettrovalvole		*/
		/************************************************************/
		
		status =	(solenoid_valve[EV01-EV01]<<EV01_STATUS) |
					(solenoid_valve[EV02-EV01]<<EV02_STATUS) |
					(solenoid_valve[EV03-EV01]<<EV03_STATUS) |
					(solenoid_valve[EV04A-EV01]<<EV04A_STATUS) |
					(solenoid_valve[EV04B-EV01]<<EV04B_STATUS) |
					(solenoid_valve[EV05-EV01]<<EV05_STATUS) |
					(solenoid_valve[EV06-EV01]<<EV06_STATUS) |
					(solenoid_valve[EV07-EV01]<<EV07_STATUS) |
					(solenoid_valve[EV08-EV01]<<EV08_STATUS) |
					(solenoid_valve[EV09A-EV01]<<EV09A_STATUS) |
					(solenoid_valve[EV09B-EV01]<<EV09B_STATUS);
		
		/************************************************************/
		/* Check sull'intasamento del filtro						*/
		/************************************************************/
		
		ErrChk( GetDI(G1, &bit) );
		
		if (bit==HI)
			status |= (bit<<G1_STATUS);
		
		/************************************************************/
		/* Acquisizione stato proximity switch						*/
		/************************************************************/
		
		// fissaggio del servocomando alla flangia
		ErrChk( GetDI(FC1, &bit) );
		
		if (bit==1)
			status |= (bit<<FC1_STATUS);
		
		//fissaggio dello stelo servocomando al carico oleodinamico
		ErrChk( GetDI(FC2, &bit) );
		
		if (bit==1)
			status |= (bit<<FC2_STATUS);
		
		/************************************************************/
		/* Acquisizione stato dei segnalatori di fine corsa			*/
		/************************************************************/
		
		// fine corsa chiusura portello sx
		ErrChk( GetDI(FC4, &bit) );
		
		if (bit==1)
			status |= (bit<<FC4_STATUS);
		
		// fine corsa chiusura portello dx
		ErrChk( GetDI(FC5, &bit) );
		
		if (bit==1)
			status |= (bit<<FC5_STATUS);
		
		// fine corsa chiusura carter superiore
		ErrChk( GetDI(FC6, &bit) );
		
		if (bit==1)
			status |= (bit<<FC6_STATUS);
		/*else //carter aperto togliere pressione a tutto #FM si pu� togliere nella versione CE
		{	
			status |= (bit<<FC6_STATUS); 
			error=SetDO(EV01,LW);
			error=SetDO(EV02,LW);
			error=SetDO(EV03,LW);
			error=SetDO(EV04A,LW);
			error=SetDO(EV04B,LW);
			error=SetDO(EV05,LW);
			error=SetDO(EV06,LW);
			error=SetDO(EV07,LW);
			error=SetDO(EV08,LW);
			error=SetDO(EV09A,LW);
			error=SetDO(EV09B,LW);
		}*/
			
		/************************************************************/
		/* Invio informazioni di stato verso l'Host					*/
		/************************************************************/
		SendStatus(status);
				
		/* misura leakage */
		if (solenoid_valve[EV05-EV01]==OPEN)	   
		{
			// senza controllo degli errori
			GetFrequency(F2, &leakage);
			SendLeakage(leakage);
		}
		
		/************************************************************/
		/* Processa eventi scatenati durante il loop di polling		*/
		/* tra cui la lettura dei dati analogici presenti in TSQ	*/
		/************************************************************/
		ProcessSystemEvents (); 
		
		/************************************************************/
		/* Temporizza il loop di polling in RT attendendo multipli	*/
		/* di 500ms													*/
		/************************************************************/
		SleepUntilNextMultipleUS (250000);
	}

Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di chiusura dei Task DAQmx aperti, sia di elaborazione analogica				*/
/* che digitale																		  	*/
/* ------------------------------------------------------------------------------------ */

int CloseTask(void)
{
	int error=0;
	
	// Task di analog input
	if(aiTaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(aiTaskHandle) );
		DAQmxErrChk( DAQmxClearTask(aiTaskHandle) );
	}
	
	// Task di analog output
	if(aoTaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(aoTaskHandle) );
		DAQmxErrChk( DAQmxClearTask(aoTaskHandle) );
	}
	
	// Task di analog output PXIe 6341
	if(ao6341TaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(ao6341TaskHandle) );
		DAQmxErrChk( DAQmxClearTask(ao6341TaskHandle) );
	}
	
	// Task di digital input
	if(diTaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(diTaskHandle) );
		DAQmxErrChk( DAQmxClearTask(diTaskHandle) );
	}
	
	// Task di lettura counter
	if(ctrTaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(ctrTaskHandle) );
		DAQmxErrChk( DAQmxClearTask(ctrTaskHandle) );
	}
	
	// Task di digital output
	if(doTaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(doTaskHandle) );
		DAQmxErrChk( DAQmxClearTask(doTaskHandle) );
	}	

	// Task di digital output PXIe 6341
	if(do6341TaskHandle!=0) 
	{
		DAQmxErrChk( DAQmxStopTask(do6341TaskHandle) );
		DAQmxErrChk( DAQmxClearTask(do6341TaskHandle) );
	}	

Error:
	return error;	
}

				 
/* ------------------------------------------------------------------------------------ */
/* Procedura di arresto di emergenza dell'alimentazione dei circuiti idraulici			*/
/* ------------------------------------------------------------------------------------ */
	
int EmergencyStop(void)
{
	int error=0;
	
	// riposizione i contatti rele' nello stato di default
	//SetRelay(SW0,OFF);
	//SetRelay(SW1,OFF);
	//SetRelay(SW2,OFF);
	//SetRelay(SW3,OFF);
	//SetRelay(SW4,OFF);
	//SetRelay(SW5,OFF);
	//SetRelay(SW6,OFF);
	//SetRelay(SW7,OFF);
	//SetRelay(SW8,OFF);
	//SetRelay(SW9,OFF);
	//SetRelay(SW10,OFF);
	//SetRelay(SW11,OFF);
	//SetRelay(SW12,OFF);
	//SetRelay(SW13,OFF);
	
	//Spegnimento solenoide CSAS1 e CSAS2
	ErrChk( SetDO6341(EV_SYS1,CLOSE) );			 
	ErrChk( SetDO6341(EV_SYS2,CLOSE) );
	
	/* Sequenza di emergenza chiusura elettrovalvole */
	SetDO(EV07,LW);		// martinetto di input
	SetDO(EV08,LW);		// martinetto di output
	Delay(0.5);
	SetDO(EV09B,HI);	// sgancio martinetto di carico
	Delay(0.5);
		
	SetDO(EV04A,LW);	// ritorno
	SetDO(EV04B,LW);
	SetDO(EV05,LW);		// misura leakage
	SetDO(EV02,LW);		// alimentazione sistema1
	SetDO(EV03,LW);		// alimentazione sistema2
	SetDO(EV09B,LW);	// sgancio martinetto di carico
	Delay(0.5);
		
	SetDO(EV01,LW);		// mandata
	
Error:	
	return error;	
}

	

