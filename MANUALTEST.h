/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  MANPANEL                         1
#define  MANPANEL_QUITBTN                 2       /* callback function: QuitManualPanelCB */
#define  MANPANEL_PARTNUMBERRNG           3       /* callback function: ChangeServoPicCB */
#define  MANPANEL_SYSTEMCHOISE            4       /* callback function: ChangePowerUutSystemCB */
#define  MANPANEL_BENCHPOWEROFFBTN        5       /* callback function: RatbPowerOffCB */
#define  MANPANEL_UUTPOWEROFFBTN          6       /* callback function: UutPowerOffCB */
#define  MANPANEL_BENCHPOWERONBTN         7       /* callback function: RatbPowerOnCB */
#define  MANPANEL_UUTPOWERONBTN           8       /* callback function: UutPowerOnCB */
#define  MANPANEL_GETSTROKEBTN            9       /* callback function: GetStrokeCenterCB */
#define  MANPANEL_CTRLCHOISE              10      /* callback function: ChangeControlCB */
#define  MANPANEL_CSASCHOISE              11
#define  MANPANEL_CTRLUNENGAGEBTN         12      /* callback function: CtrlUnEngageCB */
#define  MANPANEL_CTRLENGAGEBTN           13      /* callback function: CtrlEngageCB */
#define  MANPANEL_WAVERSLD                14      /* callback function: ChangeCtrlSignal */
#define  MANPANEL_POSITIONSLD             15      /* callback function: ChangePositionCB */
#define  MANPANEL_SETAMPLITUDENMR         16      /* callback function: ChangeAmplitudeCB */
#define  MANPANEL_SETFREQUENCYNMR         17      /* callback function: ChangeFrequencyCB */
#define  MANPANEL_STOPMOTIONBTN           18      /* callback function: StopMotionCB */
#define  MANPANEL_STARTMOTIONBTN          19      /* callback function: StartMotionCB */
#define  MANPANEL_STROKECENTERBTN         20      /* callback function: SetStrokeCenterCB */
#define  MANPANEL_LOADCHOISE              21      /* callback function: ChangeLoadTypeCB */
#define  MANPANEL_LOADUNENGAGEBTN         22      /* callback function: LoadUnEngageCB */
#define  MANPANEL_LOADENGAGEBTN           23      /* callback function: LoadEngageCB */
#define  MANPANEL_LOADDIRECTIONRSLD       24
#define  MANPANEL_LOADSLD                 25      /* callback function: ChangeLoadCB */
#define  MANPANEL_STOPLOADBTN             26      /* callback function: StopLoadCB */
#define  MANPANEL_APPLYLOADBTN            27      /* callback function: ApplyLoadCB */
#define  MANPANEL_DECORATION_9            28
#define  MANPANEL_GETPLOTXBTN             29      /* callback function: PlotxCB */
#define  MANPANEL_GETPLOTYBTN             30      /* callback function: PlotyCB */
#define  MANPANEL_GETSTRENGTHBTN          31      /* callback function: GetStrengthCB */
#define  MANPANEL_GETPRESSUREBTN          32      /* callback function: GetPressureCB */
#define  MANPANEL_LVDTOUTBTN              33      /* callback function: GetLvdtOutCB */
#define  MANPANEL_SOLENOIDBTN             34      /* callback function: GetSolenoidCB */
#define  MANPANEL_NULLBIASBTN_2           35      /* callback function: GetOilTemperatureCB */
#define  MANPANEL_NULLBIASBTN             36      /* callback function: GetNullBiasCB */
#define  MANPANEL_STOPBTN                 37      /* callback function: StopCB */
#define  MANPANEL_TEXTMSG                 38
#define  MANPANEL_GETPLOTXYBTN            39      /* callback function: PlotxyCB */
#define  MANPANEL_DECORATION_4            40
#define  MANPANEL_DECORATION_7            41
#define  MANPANEL_DECORATION              42
#define  MANPANEL_DECORATION_3            43
#define  MANPANEL_TEXTMSG_4               44
#define  MANPANEL_DECORATION_2            45
#define  MANPANEL_TEXTMSG_2               46
#define  MANPANEL_DECORATION_5            47
#define  MANPANEL_UUTPRESSURESTR          48
#define  MANPANEL_BENCHPRESSURENMR        49
#define  MANPANEL_BENCHPRESSURESTR        50
#define  MANPANEL_UUTPRESSURENMR          51
#define  MANPANEL_TEXTMSG_3               52
#define  MANPANEL_TEXTMSG_5               53
#define  MANPANEL_CANVAS                  54      /* callback function: OpenPidTuningCB */
#define  MANPANEL_SERVOPICRNG             55
#define  MANPANEL_LOGOPIC                 56


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ApplyLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeAmplitudeCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeControlCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeCtrlSignal(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeFrequencyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeLoadTypeCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangePositionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangePowerUutSystemCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeServoPicCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CtrlEngageCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CtrlUnEngageCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetLvdtOutCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetNullBiasCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetOilTemperatureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetPressureCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetSolenoidCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetStrengthCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GetStrokeCenterCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LoadEngageCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LoadUnEngageCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK OpenPidTuningCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PlotxCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PlotxyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PlotyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitManualPanelCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK RatbPowerOffCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK RatbPowerOnCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SetStrokeCenterCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StartMotionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopLoadCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopMotionCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK UutPowerOffCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK UutPowerOnCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
