#include <userint.h>
#include "PRESSURE.h"

#include "global_var.h"     


int CVICALLBACK QuitPressurePanelCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello misure di pressione
			HidePanel (pressurepanel);
			
			break;
	}
	return 0;
}
