
// ************************************************************
//
// 		
// 					CODICI E MESSAGGI DI ERRORE
//				      
//
//
// ************************************************************




// ************************************************************
//
// MESSAGGISTICA
//
// ************************************************************

#define ERROR_MESSAGE_FILE_NOT_FOUND		"File not found!"
#define ERROR_MESSAGE_FILE_DAMAGE			"Damage password file!"

#define ERROR_MESSAGE_INVALID_PASSWORD		"Invalid Password!"
#define ERROR_MESSAGE_DIFFERENT_PASSWORD	"Different Password!"
#define ERROR_MESSAGE_INVALID_CHAR			"You have digited an invalid character: "
#define ERROR_MESSAGE_EMPTY_PSW				"You have digited an empty password!"
#define ERROR_MESSAGE_PSW_DONE				"Password changing done!"
#define ERROR_MESSAGE_CHANNEL_DEF			"Error in channels definition!"


// ************************************************************
//
// TABELLA DEGLI ERRORI
//
// ************************************************************

#define FILE_NOT_FOUND 		20
#define INVALID_PASSWORD	30
#define FILE_DAMAGE			40

#define ERROR_INIT_TASK				-1000
#define ERROR_DIO_CHANNEL_DEF		-1010
#define ERROR_CTR_CHANNEL_DEF		-1020
#define ERROR_AIN_CHANNEL_DEF		-1030
#define ERROR_AOUT_CHANNEL_DEF		-1040
#define ERROR_SYNC_CHANNEL			-1050
#define ERROR_CHANNEL_PAR			-2000
#define ERROR_CHANNEL_VAL			-2010

#define ERROR_DATA_INTEGRITY		-2020
#define ERROR_DANGEROUS_SET			-2030    
#define ERROR_PARAMETER_VAL			-2040    


// ************************************************************
//
// STATI DEL BANCO
//
// ************************************************************

#define STATE_POWER_ON				5100
#define STATE_POWER_OFF				5110

#define STATE_FILTER_JAM			5200
#define STATE_PROX_ACTUATOR			5210
#define STATE_PROX_OUT				5220
#define STATE_CARTER_OPEN			5230


