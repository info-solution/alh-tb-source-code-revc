#include <userint.h>
#include "STRESS.h"

#include "global_var.h"


int CVICALLBACK StopStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target ed Host
			EmergencyStop();
			
			break;
	}
	return 0;
}

int CVICALLBACK QuitStressPanelCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// esci dal pannello di stress screening
			HidePanel (stresspanel);
			DisplayPanel (engpanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK InputEngageStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Procedura di engage del martinetto di comando manuale
			
			
			// abilita comandi			
			SetCtrlAttribute (stresspanel, STRESSPNL_AMPLITUDESLD, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_FREQUENCYSLD, ATTR_DIMMED, YES);   
			SetCtrlAttribute (stresspanel, STRESSPNL_MOVEBTN, ATTR_DIMMED, YES);   
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, YES);   
			
			// disabilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_POWEROFFBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTENGAGEBTN, ATTR_DIMMED, NO);
			
			// riazzera controlli
			SetCtrlAttribute (stresspanel, STRESSPNL_AMPLITUDESLD, ATTR_CTRL_VAL, 0.0);	// ampiezza oscillazione
			SetCtrlAttribute (stresspanel, STRESSPNL_FREQUENCYSLD, ATTR_CTRL_VAL, 0.0);	// frequenza oscillazione
			
			break;
	}
	return 0;
}

int CVICALLBACK InputUnEngageStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Procedura di unengage del martinetto di comando manuale 
			
			
			// disabilita comandi			
			SetCtrlAttribute (stresspanel, STRESSPNL_AMPLITUDESLD, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_FREQUENCYSLD, ATTR_DIMMED, NO);   
			SetCtrlAttribute (stresspanel, STRESSPNL_MOVEBTN, ATTR_DIMMED, NO);   
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, NO);   
			
			// abilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_POWEROFFBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTENGAGEBTN, ATTR_DIMMED, YES);
			
			// riazzera controlli
			SetCtrlAttribute (stresspanel, STRESSPNL_AMPLITUDESLD, ATTR_CTRL_VAL, 0.0);	// ampiezza oscillazione
			SetCtrlAttribute (stresspanel, STRESSPNL_FREQUENCYSLD, ATTR_CTRL_VAL, 0.0);	// frequenza oscillazione
			
			break;
	}
	return 0;
}

int CVICALLBACK PowerOnStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Procedura di Power Up
			
			
			// abilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTENGAGEBTN, ATTR_DIMMED, YES);   
			SetCtrlAttribute (stresspanel, STRESSPNL_POWEROFFBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_DURATIONKNB, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, YES);
			
			// disabilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_POWERONBTN, ATTR_DIMMED, NO);   
			
			break;
	}
	return 0;
}

int CVICALLBACK PowerOffStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Procedura di Power Off
			
			
			// disabilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTENGAGEBTN, ATTR_DIMMED, NO);   
			SetCtrlAttribute (stresspanel, STRESSPNL_POWEROFFBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_DURATIONKNB, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_TIMENMR, ATTR_DIMMED, NO);   
			
			// abilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_POWERONBTN, ATTR_DIMMED, YES);   
			
			break;
	}
	return 0;
}

int CVICALLBACK ChangeStressDurationCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int duration_option=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// leggi impostazione opzione durata
			GetCtrlAttribute (stresspanel, STRESSPNL_DURATIONKNB, ATTR_CTRL_VAL, &duration_option);
			
			if ( duration_option==CYCLES_DURATION)
			{
				SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, YES);  				
				SetCtrlAttribute (stresspanel, STRESSPNL_TIMENMR, ATTR_DIMMED, NO);  
			}
			else
			{
				SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, NO);  				
				SetCtrlAttribute (stresspanel, STRESSPNL_TIMENMR, ATTR_DIMMED, YES);  				
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK MoveStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Avvia ciclatura
			
			
			// abilita comandi			
			SetCtrlAttribute (stresspanel, STRESSPNL_ARRESTBTN, ATTR_DIMMED, YES);
			
			// disabilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_MOVEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_AMPLITUDESLD, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_FREQUENCYSLD, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_DURATIONKNB, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, NO);
			SetCtrlAttribute (stresspanel, STRESSPNL_TIMENMR, ATTR_DIMMED, NO);
			
			break;
	}
	return 0;
}

int CVICALLBACK ArrestStressCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int duration_option=0;
	
	switch (event)
	{
		case EVENT_COMMIT:

			// Arresta ciclatura
			
			
			// disabilita comandi			
			SetCtrlAttribute (stresspanel, STRESSPNL_ARRESTBTN, ATTR_DIMMED, NO);
			
			// abilita comandi
			SetCtrlAttribute (stresspanel, STRESSPNL_MOVEBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_AMPLITUDESLD, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_FREQUENCYSLD, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, YES);
			SetCtrlAttribute (stresspanel, STRESSPNL_DURATIONKNB, ATTR_DIMMED, YES);
			
			// leggi impostazione opzione durata
			GetCtrlAttribute (stresspanel, STRESSPNL_DURATIONKNB, ATTR_CTRL_VAL, &duration_option);
			
			if ( duration_option==CYCLES_DURATION)
			{
				SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, YES);  				
				SetCtrlAttribute (stresspanel, STRESSPNL_TIMENMR, ATTR_DIMMED, NO);  
			}
			else
			{
				SetCtrlAttribute (stresspanel, STRESSPNL_CYCLESNMR, ATTR_DIMMED, NO);  				
				SetCtrlAttribute (stresspanel, STRESSPNL_TIMENMR, ATTR_DIMMED, YES);  				
			}
			
			break;
	}
	return 0;
}
