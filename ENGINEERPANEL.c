#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include "ENGINEERPANEL.h"
#include "MANUALTEST.h"
#include "MECHCALIBRATION.h"
#include "ELECALIBRATION.h"
#include "STRESS.h"
#include "ATPPANEL.h"
#include "USERFLOW.h" 

#include "global_var.h"


int CVICALLBACK LogOffCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			if(datalogpanel>0)
				HidePanel (datalogpanel);
			
			HidePanel (engpanel);
			DisplayPanel (loginpanel);
			
			break;
	}
	return 0;
}



int CVICALLBACK AcceptanceTestProcedureCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello con menu Test
			HidePanel (engpanel);
			
			// carica pannello per funzioni di Testing manuale
			if (atppanel<0)
			{
				if ((atppanel = LoadPanel (0, "ATPPANEL.uir", ATPPNL)) < 0)
					return -1;
			}

			// inizializza i controlli del pannello manuale
			InitAtpPanel();
			
			// visualizza pannello di amministrazione
			DisplayPanel (atppanel);  
			
			break;
	}
	return 0;
}





int CVICALLBACK ProductionAcceptanceTestCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello con menu Test
			HidePanel (engpanel);
			
			// carica pannello per funzioni di di flusso utente
			if (userflowpanel<0)
			{
				if ((userflowpanel = LoadPanel (0, "USERFLOW.uir", USERPNL)) < 0)
					return -1;
			}

			// inizializza i controlli del pannello di flusso utente
			InitUserFlowPanel();
			
			// visualizza pannello di amministrazione
			DisplayPanel (userflowpanel);  
			
			break;
	}
	return 0;
}

int CVICALLBACK ManualTestCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello con menu Test
			HidePanel (engpanel);
			
			// carica pannello per funzioni di Testing manuale
			if (manualpanel<0)
			{
				if ((manualpanel = LoadPanel (0, "MANUALTEST.uir", MANPANEL)) < 0)
					return -1;
			}

			// inizializza i controlli del pannello manuale
			InitManualPanel();
			
			// visualizza pannello di amministrazione
			DisplayPanel (manualpanel);  
			
			break;
	}
	return 0;
}

int CVICALLBACK ReportViewerCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}





int CVICALLBACK EssScreeningCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello con menu Test
			HidePanel (engpanel);
			
			// carica pannello per funzioni di stress screening
			if (stresspanel<0)
			{
				if ((stresspanel = LoadPanel (0, "STRESS.uir", STRESSPNL)) < 0)
					return -1;
			}

			// inizializza i controlli del pannello manuale
			InitStressPanel();
			
			// visualizza pannello di amministrazione
			DisplayPanel (stresspanel);  
			
			break;
	}
	return 0;
}

int CVICALLBACK StatisticsCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK UutMechanicalCalibrationCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello con menu Test
			HidePanel (engpanel);
			
			// carica pannello per funzioni di Testing manuale
			if (mechcalibrationpanel<0)
			{
				if ((mechcalibrationpanel = LoadPanel (0, "MECHCALIBRATION.uir", MECHCALPNL)) < 0)
					return -1;
			}

			// inizializza i controlli del pannello manuale
			InitMechCalibrationPanel();
			
			// visualizza pannello di amministrazione
			DisplayPanel (mechcalibrationpanel);  
			
			break;
	}
	return 0;
}

int CVICALLBACK UutElectricalCalibrationCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello con menu Test
			HidePanel (engpanel);
			
			// carica pannello per funzioni di Testing manuale
			if (elecalibrationpanel<0)
			{
				if ((elecalibrationpanel = LoadPanel (0, "ELECALIBRATION.uir", ELECALPNL)) < 0)
					return -1;
			}

			// visualizza pannello di amministrazione
			DisplayPanel (elecalibrationpanel);  
			
			break;
	}
	return 0;
}


int CVICALLBACK BenchSelfTestCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}


int CVICALLBACK BenchCalibrationCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i=0;
	char message[100];
	
	switch (event)
	{
		case EVENT_COMMIT:

			// Inizializza caratterizzazione sensori mediante file di configurazione
			LoadSensorsConfigFile();
	
			/* Zeri delle celle di carico */ 
			Characteristic[N1_MUX].offset = MeasuresAve[N1_MUX]/Characteristic[N1_MUX].conv_factor +Characteristic[N1_MUX].offset;
			Characteristic[N2_MUX].offset = MeasuresAve[N2_MUX]/Characteristic[N2_MUX].conv_factor +Characteristic[N2_MUX].offset;
			Characteristic[LOAD_MUX].offset = Characteristic[N2_MUX].offset;
			
			/* Zero del flussometro di ingresso al banco */ 
			Characteristic[F1_MUX].offset = MeasuresAve[F1_MUX]/Characteristic[F1_MUX].conv_factor +Characteristic[F1_MUX].offset;  
		
			/* Zero delle resistenze di sensing */  
			Characteristic[SYS1_S_SHUNT].offset = MeasuresAve[SYS1_S_SHUNT]/Characteristic[SYS1_S_SHUNT].conv_factor +Characteristic[SYS1_S_SHUNT].offset;
			Characteristic[SYS2_S_SHUNT].offset = MeasuresAve[SYS2_S_SHUNT]/Characteristic[SYS2_S_SHUNT].conv_factor +Characteristic[SYS2_S_SHUNT].offset;     
			
			Characteristic[CV_SYS1_SHUNT].offset = MeasuresAve[CV_SYS1_SHUNT]/Characteristic[CV_SYS1_SHUNT].conv_factor +Characteristic[CV_SYS1_SHUNT].offset;
			Characteristic[CV_SYS2_SHUNT].offset = MeasuresAve[CV_SYS2_SHUNT]/Characteristic[CV_SYS2_SHUNT].conv_factor +Characteristic[CV_SYS2_SHUNT].offset;     
			
			PrintLog("*********** Test Bench Calibration ***********");

			MessagePopup ("Test Bench Operation", "TRANSDUCERS CALIBRATION DONE...");
			
			#ifdef DEBUG
			for (i=0;i<NUM_CH_MUX+1;i++)
			{
				sprintf(message,"%d -> %f", i, Characteristic[i].offset);
				PrintLog(message);
				sprintf(message,"%d -> %f", i, Characteristic[i].conv_factor);
				PrintLog(message); 
			}
			#endif
			
			break;
	}
	return 0;
}



