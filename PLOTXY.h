/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PLOTXYPNL                        1
#define  PLOTXYPNL_CTRLCHOISE             2
#define  PLOTXYPNL_ACQUIREBTN             3       /* callback function: AcquirePlotxyCB */
#define  PLOTXYPNL_SAVEBTN                4       /* callback function: SaveCharacteristicsCB */
#define  PLOTXYPNL_CLEARBTN               5       /* callback function: ClearGraphCB */
#define  PLOTXYPNL_QUITBTN                6       /* callback function: QuitPlotxyCB */
#define  PLOTXYPNL_POSITIONGRP            7       /* callback function: ChangeCursorCharacteristicChartCB */
#define  PLOTXYPNL_DECORATION_2           8
#define  PLOTXYPNL_TEXTMSG_6              9
#define  PLOTXYPNL_PHASELAGNMR            10
#define  PLOTXYPNL_ATTENUATIONNMR         11
#define  PLOTXYPNL_HYSTERESISNMR          12
#define  PLOTXYPNL_LINEARITYNMR           13
#define  PLOTXYPNL_YMINNMR                14
#define  PLOTXYPNL_YMAXNMR                15
#define  PLOTXYPNL_XMINNMR                16
#define  PLOTXYPNL_YAVENMR                17
#define  PLOTXYPNL_XAVENMR                18
#define  PLOTXYPNL_XMAXNMR                19
#define  PLOTXYPNL_TEXTMSG                20
#define  PLOTXYPNL_TEXTMSG_8              21
#define  PLOTXYPNL_TEXTMSG_7              22
#define  PLOTXYPNL_TEXTMSG_2              23
#define  PLOTXYPNL_DECORATION_5           24
#define  PLOTXYPNL_DECORATION_6           25
#define  PLOTXYPNL_DECORATION_4           26
#define  PLOTXYPNL_DECORATION_3           27
#define  PLOTXYPNL_YCURSOR2               28
#define  PLOTXYPNL_XCURSOR2               29
#define  PLOTXYPNL_DECORATION             30
#define  PLOTXYPNL_YCURSOR1               31
#define  PLOTXYPNL_XCURSOR1               32
#define  PLOTXYPNL_TEXTMSG_10             33
#define  PLOTXYPNL_TEXTMSG_3              34
#define  PLOTXYPNL_YCURSOROUT             35
#define  PLOTXYPNL_XCURSOROUT             36
#define  PLOTXYPNL_DISPLACEMENTGRPH       37      /* callback function: ChangeCursorSignalChartCB */
#define  PLOTXYPNL_YCURSORIN              38
#define  PLOTXYPNL_XCURSORIN              39
#define  PLOTXYPNL_TEXTMSG_4              40
#define  PLOTXYPNL_TEXTMSG_9              41
#define  PLOTXYPNL_TEXTMSG_11             42


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK AcquirePlotxyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeCursorCharacteristicChartCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeCursorSignalChartCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearGraphCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitPlotxyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveCharacteristicsCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
