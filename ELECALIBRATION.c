#include <userint.h>
#include "ELECALIBRATION.h"

#include "global_var.h" 


int CVICALLBACK ArrestEleCalibrationCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// Stop del programma su RT Target e Host computer
			EmergencyStop();
			
			break;
	}
	return 0;
}

int CVICALLBACK QuitEleCalibrationPanelCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			HidePanel (elecalibrationpanel);
			DisplayPanel (engpanel);
			
			break;
	}
	return 0;
}



