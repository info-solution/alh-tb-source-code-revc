/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2008. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  STRESSPNL                        1
#define  STRESSPNL_FREQUENCYSLD           2
#define  STRESSPNL_AMPLITUDESLD           3
#define  STRESSPNL_ARRESTBTN              4       /* callback function: ArrestStressCB */
#define  STRESSPNL_MOVEBTN                5       /* callback function: MoveStressCB */
#define  STRESSPNL_POWEROFFBTN            6       /* callback function: PowerOffStressCB */
#define  STRESSPNL_POWERONBTN             7       /* callback function: PowerOnStressCB */
#define  STRESSPNL_INPUTENGAGEBTN         8       /* callback function: InputEngageStressCB */
#define  STRESSPNL_INPUTUNENGAGEBTN       9       /* callback function: InputUnEngageStressCB */
#define  STRESSPNL_DECORATION_3           10
#define  STRESSPNL_DECORATION_2           11
#define  STRESSPNL_DECORATION             12
#define  STRESSPNL_TEXTMSG_3              13
#define  STRESSPNL_TEXTMSG_2              14
#define  STRESSPNL_TEXTMSG                15
#define  STRESSPNL_QUITBTN                16      /* callback function: QuitStressPanelCB */
#define  STRESSPNL_STOPBTN                17      /* callback function: StopStressCB */
#define  STRESSPNL_DURATIONKNB            18      /* callback function: ChangeStressDurationCB */
#define  STRESSPNL_TIMENMR                19
#define  STRESSPNL_ACTUALTIMENMR          20
#define  STRESSPNL_ACTUALCYCLENMR         21
#define  STRESSPNL_CYCLESNMR              22


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ArrestStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeStressDurationCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK InputEngageStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK InputUnEngageStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK MoveStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PowerOffStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK PowerOnStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitStressPanelCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopStressCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
