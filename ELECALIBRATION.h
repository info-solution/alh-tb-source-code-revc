/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  ELECALPNL                        1
#define  ELECALPNL_QUITBTN                2       /* callback function: QuitEleCalibrationPanelCB */
#define  ELECALPNL_STOPBTN                3       /* callback function: ArrestEleCalibrationCB */
#define  ELECALPNL_LVDT2OUTNMR            4
#define  ELECALPNL_DECORATION_3           5
#define  ELECALPNL_LVDT1OUTNMR            6
#define  ELECALPNL_DECORATION_2           7
#define  ELECALPNL_TEXTMSG                8
#define  ELECALPNL_TEXTMSG_2              9


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ArrestEleCalibrationCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitEleCalibrationPanelCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
