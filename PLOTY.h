/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2009. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PLOTYPNL                         1
#define  PLOTYPNL_SAVEBTN                 2       /* callback function: SaveStripChartPlotyCB */
#define  PLOTYPNL_CLEARBTN_2              3       /* callback function: StopStripChartPlotyCB */
#define  PLOTYPNL_SPEEDGRPH               4       /* callback function: ChangeScaleYdotCB */
#define  PLOTYPNL_CLEARBTN                5       /* callback function: ClearStripChartPlotyCB */
#define  PLOTYPNL_QUITBTN                 6       /* callback function: QuitPlotyCB */
#define  PLOTYPNL_POSITIONSTR             7       /* callback function: ChangeScaleYCB */
#define  PLOTYPNL_DECORATION_2            8
#define  PLOTYPNL_TEXTMSG_2               9
#define  PLOTYPNL_SPEEDMINNMR             10
#define  PLOTYPNL_SPEEDMAXNMR             11
#define  PLOTYPNL_POSITIONMINNMR          12
#define  PLOTYPNL_POSITIONMAXNMR          13
#define  PLOTYPNL_SPEEDAVENMR             14
#define  PLOTYPNL_POSITIONAVENMR          15


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ChangeScaleYCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ChangeScaleYdotCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ClearStripChartPlotyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitPlotyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SaveStripChartPlotyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopStripChartPlotyCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
