#include <ansi_c.h>
#include <windows.h> 
#include <userint.h>
#include <analysis.h>
#include "global_var.h" 
#include "error_table.h"

#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else         



/* ------------------------------------------------------------------------------------ */
/* Routine di arresto d'emergenza														*/
/* ------------------------------------------------------------------------------------ */

void EmergencyStop(void)
{
	// blocco di emergenza di ogni funzione del banco idraulico
	SetEmergency(HI);
	Sleep(50);
	SetEmergency(LW);
	
	// chiusura del programma su RT Target
	SetStopProgram(HI);
		
	// uscita dall'interfaccia grafica
	QuitUserInterface (0);
	
	return;
}



/* ------------------------------------------------------------------------------------ */
/* Routine di controllo elettrovalvole e stampa a datalog								*/
/* ------------------------------------------------------------------------------------ */

int SetSolenoidValve(int solenoid_id, int value)
{
	int error=0;
	char messaggio[200];
	
	if (( value!=OPEN )&&( value!=CLOSE ))
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}
	
	// @sz aggiungere tutti i controlli di sicurezza
	
	switch	(solenoid_id)
	{
		case EV01:
			
			ErrChk( SetEV01(value) );
			SolenoidValve[EV01_STATUS]=value;
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV01: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV01: CLOSE");
				
			break;
			
		case EV02:
			
			ErrChk( SetEV02(value) );
			SolenoidValve[EV02_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV02: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV02: CLOSE");
			
			break;
			
		case EV03:
			
			ErrChk( SetEV03(value) );
			SolenoidValve[EV03_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV03: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV03: CLOSE");
			
			break;
			
		case EV04A:
			
			ErrChk( SetEV04A(value) );
			SolenoidValve[EV04A_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV04A: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV04A: CLOSE");
			
			break;
			
		case EV04B:
			
			ErrChk( SetEV04B(value) );
			SolenoidValve[EV04B_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV04B: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV04B: CLOSE");
			
			break;
			
		case EV05:
			
			ErrChk( SetEV05(value) );
			SolenoidValve[EV05_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV05: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV05: CLOSE");
			
			break;
			
		case EV06:
			
			ErrChk( SetEV06(value) );
			SolenoidValve[EV06_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV06: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV06: CLOSE");
			
			break;
			
		case EV07:
			
			ErrChk( SetEV07(value) );
			SolenoidValve[EV07_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV07: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV07: CLOSE");
			
			break;
			
		case EV08:
			
			ErrChk( SetEV08(value) );
			SolenoidValve[EV08_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV08: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV08: CLOSE");
			
			break;
			
		case EV09A:
			
			ErrChk( SetEV09A(value) );
			SolenoidValve[EV09A_STATUS]=value;    
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV09A: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV09A: CLOSE");
			
			break;
			
		case EV09B:
			
			ErrChk( SetEV09B(value) );
			SolenoidValve[EV09B_STATUS]=value;
			
			if (value==OPEN)
				strcpy(messaggio, "Solenoid valve EV09B: OPEN");
			else
				strcpy(messaggio, "Solenoid valve EV09B: CLOSE");
			
			break;
		
		default:
			
			// il parametro identificativo dell'elettrovalvola e' errato
			error=ERROR_PARAMETER_VAL;
			goto Error;
		
			break;
	}
	
	// Stampa su finestra di data logging il messaggio
	PrintLog (messaggio); 
	
Error:	
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di alimentazione banco di collaudo											*/
/* ------------------------------------------------------------------------------------ */

int RatbPowerUp(void)
{
	int error=0;
	char messaggio[200];
	
	// cover di protezione aperta
	/*if (((bench_status>>FC6_STATUS)&0x1)==LW)
	{
			MessagePopup ("Hydraulic Test Bench", "SAFETY COVER HAS BEEN REMOVED, PLEASE, REPLACE THE COVER !!!");
	} else {*/ 
	// se la pressione di alimentazione del banco supera i 200 bar apri l'elettrovalvola di ingresso
	// @sz if ((MeasuresAve[P02_MUX]>200.0)||(MeasuresAve[P11_MUX]>5.0))
		ErrChk( SetSolenoidValve(EV01,OPEN) );
		ErrChk( SetSolenoidValve(EV09A,OPEN) );
	//}
		
Error:
	if (error==0)
		PrintLog("Test Bench Power Up --> OK");
	else
	{
		sprintf(messaggio,"Test Bench Power Up --> ERROR %d",error);
		PrintLog(messaggio); 
	}
	
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di spegnimento banco di collaudo												*/
/* ------------------------------------------------------------------------------------ */

int RatbShutDown(void)
{
	int error=0;
	char messaggio[200];      

	ErrChk( SetSolenoidValve(EV01,CLOSE) );

Error:
	if (error==0)
		PrintLog("Test Bench Power Down --> OK");
	else
	{
		sprintf(messaggio,"Test Bench Power Down --> ERROR %d",error);
		PrintLog(messaggio); 
	}
		
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di alimentazione UUT															*/
/* ------------------------------------------------------------------------------------ */

int UutPowerUp(int option)
{
	int error=0;

	// se la pressione di mandata del servocomando supera i 200 bar apri le elettrovalvole di ingresso
	// @sz if ((MeasuresAve[P03_MUX]<=310.0)&&(MeasuresAve[P11_MUX]<=5.0))   
	{
		switch (option)
		{
			case BOTH_SYS:
			
				ErrChk( SetSolenoidValve(EV02,OPEN) );
				Sleep(10);
				ErrChk( SetSolenoidValve(EV03,OPEN) );
			
				break;
			
			case ONLY_SYS1:
			
				ErrChk( SetSolenoidValve(EV02,OPEN) );
				Sleep(10);
				ErrChk( SetSolenoidValve(EV03,CLOSE) );
			
				break;
			
			case ONLY_SYS2:
			
				ErrChk( SetSolenoidValve(EV02,CLOSE) );
				Sleep(10);
				ErrChk( SetSolenoidValve(EV03,OPEN) );
			
				break;
			default:
				
				goto Error;
			
				break;
		}
	}
	
	SetSolenoidValve(EV09A,OPEN);	// eccito solenoide di sgancio carico
	
Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di spegnimento UUT															*/
/* ------------------------------------------------------------------------------------ */

int UutShutDown(void)
{
	int error=0;

	SetSolenoidValve(EV09A,CLOSE);
	Sleep(10);
	ErrChk( SetSolenoidValve(EV02,CLOSE) );
	Sleep(10);
	ErrChk( SetSolenoidValve(EV03,CLOSE) );

Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di alimentazione stadi autopiloti CSAS										*/
/* ------------------------------------------------------------------------------------ */
// da chiarire secondo me � possibile eliminarla
//int CsasPowerUp(int option)
//{
//	int error=0;

//	// se la pressione di mandata del servocomando supera i 200 bar apri le elettrovalvole di ingresso
//	// @sz if ((MeasuresAve[P03_MUX]<=310.0)&&(MeasuresAve[P11_MUX]<=5.0))   
//	{
//		switch (option)
//		{
//			case BOTH_SYS:
//			
//				ErrChk( SetSolenoid1(OPEN) );
//				Sleep(10);
//				ErrChk( SetSolenoid2(OPEN) );
//			
//				break;
//			
//			case ONLY_SYS1:
//			
//				ErrChk( SetSolenoid1(OPEN) );
//				Sleep(10);
//				ErrChk( SetSolenoid2(CLOSE) );
//			
//				break;
//			
//			case ONLY_SYS2:
//			
//				ErrChk( SetSolenoid1(CLOSE) );
//				Sleep(10);
//				ErrChk( SetSolenoid2(OPEN) );
//			
//				break;
//				
//			default:
//				
//				goto Error;
//				
//				break;
//		}
//	}
//	
//Error:
//	return error;
//}


/* ------------------------------------------------------------------------------------ */
/* Routine di spegnimento stadi CSAS													*/
/* ------------------------------------------------------------------------------------ */

int CsasShutDown(void)
{
	int error=0;

	//ErrChk( SetSolenoid1(CLOSE) );
	//Sleep(10);
	//ErrChk( SetSolenoid2(CLOSE) );

	//// le funzioni devono essere richiamate due volte per poter permettere
	//// l'apertura di tutti i rele' del percorso che porta la tensione di riferimento all'alimentatore SIEMENS
	//
	//ErrChk( SetSolenoid1(CLOSE) );
	//Sleep(10);
	//ErrChk( SetSolenoid2(CLOSE) );
	
	// disabilita i controlli
	SetEnableCsas1(NO);
	Sleep(500);
	SetEnableCsas2(NO);
	
Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di lettura dello stato di elettrovalvole, proximity switch e fine corsa		*/
/* ------------------------------------------------------------------------------------ */

int GetStatus (int transducer_id, int *value)
{
	int error=0;

	if (( transducer_id<EV01_STATUS )||( transducer_id>FC6_STATUS))
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}
		
	*value = (bench_status>>transducer_id)&0x1;
	
Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di regolazione martinetto di input											*/
/* ------------------------------------------------------------------------------------ */

int SetInputPosition (int type, double offset, double amplitude, double frequency)
{
	int error=0;
	
	// verifica parametro di forma d'onda
	if ( (type!=STATIC)&&(type!=SINE)&&(type!=SQUARE)&&(type!=TRIANGULAR) )
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}

	// verifica offset come posizionamento statico o centro di oscillazione
	if ( (offset<-40.0)||(offset>40.0) )
	{
		MessagePopup ("User's error", "Offset must be between -40mm and +40mm.");     
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	
	
	// verifica ampiezza massima dell'oscillazione
	if ( (amplitude<0.0)||( abs(offset)+amplitude>40.0 ) )
	{
		MessagePopup ("User's error", "Sum of offset and amplitude can't exeed the maximum range.");
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	
	
	// verifica frequenza dell'oscillazione
	if ( (frequency<0.0)||(frequency>10.0) )
	{
		MessagePopup ("User's error", "Frequency must be less than 10Hz.");     
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	

	ErrChk( SetInputSetpoint(offset) );
	
	if (type==STATIC)
	{
		ErrChk( SetWaveType(type) );
		ErrChk( SetInputWaveAmplitude(0.0) );
		ErrChk( SetWaveFrequency(0.1) );
	}
	else
	{
		ErrChk( SetWaveType(type) );
		ErrChk( SetInputWaveAmplitude(amplitude) );
		ErrChk( SetWaveFrequency(frequency) );		
	}
	
Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di regolazione autopiloti CSAS												*/
/* ------------------------------------------------------------------------------------ */

int SetCsasPosition (int type, double offset, double amplitude, double frequency)
{
	int error=0;
	
	// verifica parametro di forma d'onda
	if ( (type!=STATIC)&&(type!=SINE)&&(type!=SQUARE)&&(type!=TRIANGULAR) )
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}

	// verifica offset coem posizionamento statico o centro di oscillazione
	if ( (offset<-40.0)||(offset>40.0) )
	{
		MessagePopup ("User's error", "Offset must be between -40mm and +40mm.");      
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	
	
	// verifica ampiezza massima dell'oscillazione
	if ( (amplitude<0.0)||( abs(offset)+amplitude>40.0 ) )
	{
		MessagePopup ("User's error", "Sum of offset and amplitude can't exeed the maximum range.");
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	
	
	// verifica frequenza dell'oscillazione
	if ( (frequency<0.0)||(frequency>10.0) )
	{
		MessagePopup ("User's error", "Frequency must be less than 10Hz.");     
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	

	ErrChk( SetCsasSetpoint(offset) );
	
	if (type==STATIC)
	{
		ErrChk( SetWaveType(type) );
		ErrChk( SetCsasWaveAmplitude(0.0) );
		ErrChk( SetWaveFrequency(0.1) );
	}
	else
	{
		ErrChk( SetWaveType(type) );
		ErrChk( SetCsasWaveAmplitude(amplitude) );
		ErrChk( SetWaveFrequency(frequency) );		
	}
	
Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di regolazione carico oleodinamico											*/
/* ------------------------------------------------------------------------------------ */

int SetHydraulicLoad (int direction, double load_value)
{
	int error=0;

	// verifica parametro di forma d'onda
	if ( (direction!=COMPRESSION)&&(direction!=EXTENSION) )
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}

	// verifica valore del carico
	if ( (load_value<0.0)||(load_value>MAX_LOAD) )
	{
		MessagePopup ("User's error", "Load exeeds the maximum safety value.");     
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}	

	// varifica che l'entita' del carico attuale sia tale da garantire la sicurezza
	// dell'operazione di inversione di direzione
	/*
	if (uut_load_engage==ON)
	{
		if (direction==COMPRESSION)
		{
			// carico in compressione > 0
		
			// se il carico attuale supera 300N in trazione restituisci errore
			if (MeasuresAve[N2_MUX]<-1000.0)
			{
				MessagePopup ("User's error", "Load inversion could be dangerous because of load value! \nSet load to the minimum value before to change direction of load.");
				error=ERROR_DANGEROUS_SET;
				goto Error;
			}
		}
		else
		{
			// carico in trazione < 0
		
			// se il carico attuale supera 300N in compressione restituisci errore
			if (MeasuresAve[N2_MUX]>1000.0)
			{
				MessagePopup ("User's error", "Load inversion could be dangerous because of load value! \nSet load to the minimum value before to change direction of load.");
				error=ERROR_DANGEROUS_SET;
				goto Error;
			}
		}
	}
	*/
	if (direction==COMPRESSION)
		load_value = abs(load_value);
	else if (direction==EXTENSION)
		load_value = -1*abs(load_value);
	
	ErrChk( SetLoadDirectionType(direction) );
	ErrChk( SetLoadSetpoint(load_value) );
	
Error:
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di impostazione pressioni operative UUT										*/
/* ------------------------------------------------------------------------------------ */

int SetUutPressure (int type, double delivery_pressure_value, int return_pressure_value)
{
	int error=0;
	
	// verifica tipo di regolazione: fissa o proporzionale impostabile da utente
	if ( (type!=FIXED)&&(type!=VARIABLE)&&(type!=POWER_OFF) )
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}
	// verifica valore pressione di mandata
	if ( (delivery_pressure_value<0.0)&&(delivery_pressure_value>310.0) )
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}
	// verifica valore pressione di ritorno: alta o bassa
	if ( (return_pressure_value!=HI)&&(return_pressure_value!=LW) )
	{
		error=ERROR_PARAMETER_VAL;
		goto Error;
	}
	
	switch (type)
	{
		case FIXED:
			
			// disabilito possibilita' di controllo proporzionale pressione
			//ErrChk( SetEnableUutPressure(FIXED) );
		
			Sleep(200);
		
			// azzero setpoint di pressione
			//ErrChk( SetUutPressureSetpoint(0.0) );
			ErrChk( SetUutPressureSetpoint(UUT_DELIVERY_PRESSURE) ); 	//valore di pressione nominale, ossia 206 bar   DEFINE
			
			break;
		case VARIABLE:

			// disabilito possibilita' di controllo proporzionale pressione
			//ErrChk( SetEnableUutPressure(VARIABLE) );
		
			Sleep(200);
		
			// azzero setpoint di pressione
			ErrChk( SetUutPressureSetpoint(delivery_pressure_value) );

			break;
		case POWER_OFF:
			
			// disabilito possibilita' di controllo proporzionale pressione
			//ErrChk( SetEnableUutPressure(POWER_OFF) );
		
			Sleep(200);
		
			// azzero setpoint di pressione
			ErrChk( SetUutPressureSetpoint(0.0) );	//porto il riferimento di pressione a 0
			
			break;
	}
	
	Sleep(200);
	
	// chiudo elettrovalvola di contropressione a 155bar
	ErrChk( SetSolenoidValve(EV06, return_pressure_value) );
		
Error:
	return error;
}

  
/* ------------------------------------------------------------------------------------ */
/* Routine di abilitazione del circuito idraulico di misura del leakage					*/
/* ------------------------------------------------------------------------------------ */

int SetLeakageCondition(int option)
{
	int error=0;
	double input_pressure=0.0;
	double return_pressure=0.0;    

	// se la pressione di mandata del servocomando supera i 200 bar apri le elettrovalvole di ingresso
	if ((MeasuresAve[P03_MUX]<=310.0)&&(MeasuresAve[P11_MUX]<=5.0))   
	{
		switch (option)
		{
			case BOTH_SYS:
			
				ErrChk( SetSolenoidValve(EV04A,LW) );
				Sleep(50);
				ErrChk( SetSolenoidValve(EV04B,LW) );
				Sleep(500);
				ErrChk( SetSolenoidValve(EV05,LW) );
			
				break;
			
			case ONLY_SYS2:
			
				ErrChk( SetSolenoidValve(EV05,HI) );
				Sleep(50);
				ErrChk( SetSolenoidValve(EV04A,LW) );
				Sleep(500);
				ErrChk( SetSolenoidValve(EV04B,HI) );
			
				break;
			
			case ONLY_SYS1:
			
				ErrChk( SetSolenoidValve(EV05,HI) );
				Sleep(50);
				ErrChk( SetSolenoidValve(EV04B,LW) );
				Sleep(500);
				ErrChk( SetSolenoidValve(EV04A,HI) );
			
				break;
				
			default:
				
				goto Error;
				
				break;
		}
	}
	
Error:
	return error;
}



/* ------------------------------------------------------------------------------------ */
/* Routine di esecuzione della procedura di engage del carico oleodinamico				*/
/* ------------------------------------------------------------------------------------ */

int LoadEngageProcedure(void)
{
	int error=0;	
	int Done=0;
	char message[200];
	
	// porta la modalita' di carico in compressione e azzera valore del carico
	ErrChk( SetLoadDirectionType(COMPRESSION) );
	ErrChk( SetLoadSetpoint(0.0) );
	ErrChk( SetEnableLoad(YES) );

	// alimenta il cilindro oleodinamico di carico
	ErrChk( SetSolenoidValve(EV08,HI) );   

	// porta il carico ad un valore minimo per consentire l'avvicinamento del pistone di carico all'innesto
	ErrChk( SetLoadSetpoint(1.0) );
	
	// loop di attesa proximity switch di segnalazione FC2
	while (!Done)
	{
		ProcessDrawEvents();
		ProcessSystemEvents();
		GetStatus(FC2_STATUS,&Done);
		Sleep(3000);
	}
	
	// alimenta l'innesto del carico e ritardo di 200ms
	ErrChk( SetSolenoidValve(EV09A,LW) );	// disattivo solenoide di sgancio
	ErrChk( SetSolenoidValve(EV09B,HI) );   // attivo solenoide di aggancio
	Sleep(500);
	
	// porta il carico a valore minimo in modalita' trazione per cercare di allontanare il pistone di carico
	ErrChk( SetLoadDirectionType(EXTENSION) );
	ErrChk( SetLoadSetpoint(1.0) );
	Sleep(500);

	// verifica stato di proximity switch: se il pistone rimane agganciato al servocomando la procedura e' riuscita
	GetStatus(FC2_STATUS,&Done);
	
	if (Done)
	{
		sprintf(message,"Check load engage ---> OK");
		PrintLog(message);	
	}
	else
	{
		sprintf(message,"Check load engage ---> ERROR");
		error = STATE_PROX_OUT;
		goto Error;
	}
	
	// porta il carico a valore nullo in compressione
	ErrChk( SetLoadDirectionType(COMPRESSION) );
	ErrChk( SetLoadSetpoint(0.0) );
	
Error:
	
	// gestione dell'errore
	if (error!=0)
	{
		SetLoadSetpoint(0.0);
		Sleep(500);
		SetEnableLoad(NO);  
		SetSolenoidValve(EV08,LW); 
		Sleep(500);
		
		GetStatus(EV09B_STATUS,&Done);
		if (Done==HI)
		{
			SetSolenoidValve(EV09B,LW);		// disattivo solenoide di aggancio
			Sleep(500);
		}
		
		SetSolenoidValve(EV09A,HI);			// attivo solenoide di sgancio
		Sleep(1000);
		SetSolenoidValve(EV09A,LW);			// disattivo solenoide di sgancio
		
		PrintLog("ERROR ---> OLEODINAMIC LOAD CAN'T BE APPLIED!!!");
	}
	
	return error;
}


/* ------------------------------------------------------------------------------------ */
/* Routine di esecuzione della procedura di sgancio del carico oleodinamico				*/
/* ------------------------------------------------------------------------------------ */

int LoadDisengageProcedure(void)
{
	int error=0;	
	int Done=0;
	
	SetLoadSetpoint(0.0);
	Sleep(500);
	SetEnableLoad(NO);  
	SetSolenoidValve(EV08,LW); 
	Sleep(500);
		
	GetStatus(EV09B_STATUS,&Done);
	if (Done==HI)
	{
		SetSolenoidValve(EV09B,LW);		// disattivo solenoide di aggancio
		Sleep(500);
	}
		
	SetSolenoidValve(EV09A,HI);			// attivo solenoide di sgancio
	Sleep(1000);
	SetSolenoidValve(EV09A,LW);			// disattivo solenoide di sgancio
	
Error:
	
	if (error!=0)
		PrintLog("ERROR ---> OLEODINAMIC LOAD CAN'T BE APPLIED!!!");
	
	return error;
}
