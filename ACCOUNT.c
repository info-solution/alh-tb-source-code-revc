#include <formatio.h>
#include "pwctrl.h"
#include <cvirte.h>		
#include <userint.h>

#include "ACCOUNT.h"

#include "global_var.h" 
#include "error_table.h" 


int CVICALLBACK ConfirmPswCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int error=0;
	
	int user_id=-1;
	char psw_old[50]="";
	char psw_new[50]="";
	char psw_confirm[50]="";
	
	switch (event)
	{
		case EVENT_COMMIT:
			
			// leggi nuove password impostate
			GetCtrlAttribute (adminpanel, ADMINPANEL_USERRNG, ATTR_CTRL_VAL, &user_id);
			GetCtrlAttribute (adminpanel, ADMINPANEL_OLDPSW, ATTR_CTRL_VAL, psw_old);
			GetCtrlAttribute (adminpanel, ADMINPANEL_NEWPSW, ATTR_CTRL_VAL, psw_new);
			GetCtrlAttribute (adminpanel, ADMINPANEL_CONFIRMNEWPSW, ATTR_CTRL_VAL, psw_confirm);
			
			if ( (CompareStrings (psw_old, 0, "", 0, 1)) && (CompareStrings (psw_new, 0, "", 0, 1)) &&
				(CompareStrings (psw_confirm, 0, "", 0, 1)) )
			{
				// se nuova password e password di conferma non sono uguali allora cancella
				if ( ! CompareStrings (psw_new, 0, psw_confirm, 0, 1) )
				{
					// salva le password nel file .zak
					error = ChangePassword(user_id,psw_old,psw_new,psw_confirm);
					
					// azzera tutte le password
					SetCtrlAttribute (adminpanel, ADMINPANEL_OLDPSW, ATTR_CTRL_VAL, "");
					SetCtrlAttribute (adminpanel, ADMINPANEL_NEWPSW, ATTR_CTRL_VAL, "");
					SetCtrlAttribute (adminpanel, ADMINPANEL_CONFIRMNEWPSW, ATTR_CTRL_VAL, "");
				}
				else
				{
					// la nuova password non e' stata confermata
					MessagePopup ("Invalid password", ERROR_MESSAGE_DIFFERENT_PASSWORD);
					
					// non si e' confermata la nuova password
					SetCtrlAttribute (adminpanel, ADMINPANEL_NEWPSW, ATTR_CTRL_VAL, "");
					SetCtrlAttribute (adminpanel, ADMINPANEL_CONFIRMNEWPSW, ATTR_CTRL_VAL, "");	
				}
			}
			else
			{
				MessagePopup ("Invalid password", ERROR_MESSAGE_EMPTY_PSW);
				
				// azzera tutte le password
				SetCtrlAttribute (adminpanel, ADMINPANEL_OLDPSW, ATTR_CTRL_VAL, "");
				SetCtrlAttribute (adminpanel, ADMINPANEL_NEWPSW, ATTR_CTRL_VAL, "");
				SetCtrlAttribute (adminpanel, ADMINPANEL_CONFIRMNEWPSW, ATTR_CTRL_VAL, "");
			}
			
			break;
	}
	
	// messaggi di errore
	switch (error)
	{
		case INVALID_PASSWORD:
			// old password non trovata
			MessagePopup ("Error", ERROR_MESSAGE_INVALID_PASSWORD);
			break;
		case FILE_NOT_FOUND:
			// file .zak con password non trovato
			MessagePopup ("Error", ERROR_MESSAGE_FILE_NOT_FOUND);
			break;
		case FILE_DAMAGE:
			// formato del contenuto file non compatibile
			MessagePopup ("Error", ERROR_MESSAGE_FILE_DAMAGE);
			break;
	}
	
	if (error!=0)
	{
		// azzera tutte le password
		SetCtrlAttribute (adminpanel, ADMINPANEL_OLDPSW, ATTR_CTRL_VAL, "");
		SetCtrlAttribute (adminpanel, ADMINPANEL_NEWPSW, ATTR_CTRL_VAL, "");
		SetCtrlAttribute (adminpanel, ADMINPANEL_CONFIRMNEWPSW, ATTR_CTRL_VAL, "");			
	}
	
	return 0;
}

int CVICALLBACK LogoffCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			DiscardPanel (adminpanel);
			DisplayPanel (loginpanel);
			
			break;
	}
	return 0;
}

int CVICALLBACK CheckOldPswCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_KEYPRESS:

			ValidatePswChar(eventData1,eventData2); 
			
			break;
	}
	return 0;
}

int CVICALLBACK CheckNewPswCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_KEYPRESS:

			ValidatePswChar(eventData1,eventData2); 
			
			break;
	}
	return 0;
}

int CVICALLBACK CheckConfirmPswCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_KEYPRESS:

			ValidatePswChar(eventData1,eventData2); 
			
			break;
	}
	return 0;
}



