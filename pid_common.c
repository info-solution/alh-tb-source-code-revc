

/****************************************/
/* Source file for PID functionality. 	*/
/****************************************/
/*										*/
/*			Routine modificate @sz		*/
/*										*/
/****************************************/

  
/* Include files */
//#include "global_var.h"
#include "HW_Description.h"
#include "pid_common.h"
#include <PID.h>
#include <toolbox.h>

/* Handler dei controllori PID con visibilita' 	*/
/* ristretta al file pid_common.c				*/
static PidHandle  csas1PidHandle	= 0;
static PidHandle  csas2PidHandle	= 0;
static PidHandle  inputPidHandle	= 0;
static PidHandle  loadPidHandle		= 0;





/// Creates a PID controller
/// @sz pid_type indica quale dei 4 controlli PID si vuole creare
/// kC/Proportional constant of the PID controller
/// tI/Integral time of the PID controller
/// tD/Derivative time of the PID controller
/// dt/The time interval, in seconds, of the PID controller.
/// outputMin/The minimum allowed output value
/// outputMax/The maximum allowed output value
/// PID library error code

int CreatePid (int pid_type, double dt, double outputMin, double outputMax) 
{
	PidHandle  *pidHandle;
	double filter[31] = {
		+1.6089917736446394e-003, 
		+1.3967476066001920e-003, 
		+7.2678890880823030e-004, 
		-1.1034499572868965e-003, 
		-4.5837560009782769e-003, 
		-9.3522646843094658e-003, 
		-1.3774204070959136e-002, 
		-1.5029019217447530e-002, 
		-9.8103433758477531e-003, 
		+4.5200397929181695e-003, 
		+2.8713635057743148e-002, 
		+6.0834200967646537e-002, 
		+9.6257573844515276e-002, 
		+1.2858953033809514e-001, 
		+1.5128161173390686e-001, 
		+1.5944783456590156e-001, 
		+1.5128161173390686e-001, 
		+1.2858953033809514e-001, 
		+9.6257573844515276e-002,
		+6.0834200967646537e-002, 
		+2.8713635057743148e-002, 
		+4.5200397929181695e-003, 
		-9.8103433758477531e-003, 
		-1.5029019217447530e-002, 
		-1.3774204070959136e-002, 
		-9.3522646843094658e-003, 
		-4.5837560009782769e-003, 
		-1.1034499572868965e-003, 
		+7.2678890880823030e-004, 
		+1.3967476066001920e-003, 
		+1.6089917736446394e-003, 
	};
	int	error = 0;
	
	switch (pid_type)
	{
		case CSAS1_PID:
			pidHandle = &csas1PidHandle;
			break;
		case CSAS2_PID:
			pidHandle = &csas2PidHandle;
			break;
		case INPUT_PID:
			pidHandle = &inputPidHandle;
			break;
		case LOAD_PID:
			pidHandle = &loadPidHandle;
			break;
	}
	
	errChk (PidCreate (PID_parameters[pid_type].proportional_gain, PID_parameters[pid_type].integral_time, 
			PID_parameters[pid_type].derivative_time, pidHandle));
	
    errChk (PidSetAttribute (*pidHandle, pidAttrUseInternalTimer, 0));
	errChk (PidSetAttribute (*pidHandle, pidAttrDeltaT, dt));
    errChk (PidSetAttribute (*pidHandle, pidAttrOutputMin, outputMin));
    errChk (PidSetAttribute (*pidHandle, pidAttrOutputMax, outputMax));
	
	if(pid_type==LOAD_PID)
	{
    	errChk (PidSetAttribute (*pidHandle, pidAttrLimitOutputRate, 1));
		errChk (PidSetAttribute (*pidHandle, pidAttrUseFilteredPV, 1));		
	}
	else
	{
    	errChk (PidSetAttribute (*pidHandle, pidAttrLimitOutputRate, 0));
		errChk (PidSetAttribute (*pidHandle, pidAttrUseFilteredPV, 0));
		
		//PidSetProcessVariableFilter (*pidHandle, 31, filter);
		PidSetProcessVariableFilter (*pidHandle, 0, NULL);    
	}	
	
Error:
	if (error < 0)
	{
		if (pidHandle)
		{
			PidDiscard (*pidHandle);
			pidHandle = 0;
		}
	}
	return error;
}






/// Gets the next set of PID data from the PID controller
/// processVariable/On input contains the current process variable
/// output/On output, contains the controller output
/// PID library error code

int GetNextPidOutput (int pid_type, double processVariable, double *output)
{
	int	error = 0;
	PidHandle  *pidHandle;
	
	switch (pid_type)
	{
		case CSAS1_PID:
			pidHandle = &csas1PidHandle;
			break;
		case CSAS2_PID:
			pidHandle = &csas2PidHandle;
			break;
		case INPUT_PID:
			pidHandle = &inputPidHandle;
			break;
		case LOAD_PID:
			pidHandle = &loadPidHandle;
			break;
	}
	
	errChk (PidNextOutput (*pidHandle, processVariable, output));

Error:
	if (error!=0)
		printf ("%d \n Get ",error);
	return error;
}



/// Closes and disposes the PID
void ClosePid (int pid_type) 
{
	PidHandle  *pidHandle;
	int	error = 0;
	
	switch (pid_type)
	{
		case CSAS1_PID:
			pidHandle = &csas1PidHandle;
			break;
		case CSAS2_PID:
			pidHandle = &csas2PidHandle;
			break;
		case INPUT_PID:
			pidHandle = &inputPidHandle;
			break;
		case LOAD_PID:
			pidHandle = &loadPidHandle;
			break;
	}
	
	if (*pidHandle)
	{
		PidDiscard (*pidHandle);
		pidHandle = 0;
	}
}




/// Sets the output rate of the PID
/// outputRate/The maximum rate of change of the controller output in EGU/min
/// PID library error code

int SetPidOutputRate (int pid_type, double outputRate) 
{
	PidHandle  *pidHandle;
	
	switch (pid_type)
	{
		case CSAS1_PID:
			pidHandle = &csas1PidHandle;
			break;
		case CSAS2_PID:
			pidHandle = &csas2PidHandle;
			break;
		case INPUT_PID:
			pidHandle = &inputPidHandle;
			break;
		case LOAD_PID:
			pidHandle = &loadPidHandle;
			break;
	}
	
	return PidSetAttribute (*pidHandle, pidAttrOutputRate, outputRate); 
}




/// Sets the setpoint of the PID
/// setpoint/Pass the setpoint to use in the PID
/// PID library error code

int SetPidSetpoint (int pid_type, double setpoint) 
{
	PidHandle  *pidHandle;
	double percentage_setpoint;
	
	switch (pid_type)
	{
		case CSAS1_PID:
			pidHandle = &csas1PidHandle;
			break;
		case CSAS2_PID:
			pidHandle = &csas2PidHandle;
			break;
		case INPUT_PID:
			pidHandle = &inputPidHandle;
			break;
		case LOAD_PID:
			pidHandle = &loadPidHandle;
			break;
	}
	
	return PidSetAttribute (*pidHandle, pidAttrSetpoint, setpoint); 
}





/// Sets the proportional gain, integral time, and derivative time of the PID
/// kC/Pass the proportional gain to use in the PID
/// tI/Pass the integral time to use in the PID
/// tD/Pass the derivative time to use in the PID
/// PID library error code

int SetPidParameters (int pid_type, double kC, double tI, double tD) 
{
	int	error = 0;
	PidHandle  *pidHandle;
	
	switch (pid_type)
	{
		case CSAS1_PID:
			pidHandle = &csas1PidHandle;
			break;
		case CSAS2_PID:
			pidHandle = &csas2PidHandle;
			break;
		case INPUT_PID:
			pidHandle = &inputPidHandle;
			break;
		case LOAD_PID:
			pidHandle = &loadPidHandle;
			break;
	}
	
	errChk (PidSetAttribute (*pidHandle, pidAttrProportionalFactor, kC));
	errChk (PidSetAttribute (*pidHandle, pidAttrIntegralTime, tI));
	errChk (PidSetAttribute (*pidHandle, pidAttrDerivativeTime, tD));
	
Error:
	if (error!=0)  
		printf ("%d \n Set ",error);
	return error;
}

 
