#include <formatio.h>
#include "ENGINEERPANEL.h"
#include "global_var.h"

void SaveDatalog(void);

int CVICALLBACK ClearLogCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char *emptystr="\0";
	
	switch (event)
	{
		case EVENT_COMMIT:
			
			ResetTextBox (datalogpanel, DATALOGPNL_TEXTBOX, "");
			
			break;
	}
	return 0;
}


int CVICALLBACK SaveLogCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{

	switch (event)
	{
		case EVENT_COMMIT:

			SaveDatalog();
			
			break;
	}
	
	return 0;
}

void SaveDatalog()
{
	char pathstr[300];		// path e nome file
	char buffer[500];		// buffer di singola linea
	int dimBuffer;			// numero di caratteri nella linea di testo
	
	int numLinee=0;			// numero di linee presenti nella message box di datalog da salvare
	int i=0;
	char aCapo[]="\n";
	
	int logfile;			// handler del file di salvataggio
	
	// Seleziona path e nome file in cui salvare il datalog
	if (FileSelectPopup ("", "*.txt", ".txt", "Save Datalogger", VAL_SAVE_BUTTON, 0, 1, 1, 1, pathstr)<=0)
		goto Error;
	
	// apertura/creazione del file di salvataggio
	logfile = OpenFile (pathstr, VAL_READ_WRITE, VAL_APPEND, VAL_ASCII);
	
	// leggo numero di linee presenti nel box di testo
	GetNumTextBoxLines (datalogpanel, DATALOGPNL_TEXTBOX, &numLinee);
	
	for (i=0;i<numLinee;i++)
	{
		// caricamento della linea i della text box nel buffer
		GetTextBoxLine (datalogpanel, DATALOGPNL_TEXTBOX, i, buffer);
		
		// lunghezza della linea della text box
		GetTextBoxLineLength (datalogpanel, DATALOGPNL_TEXTBOX, i, &dimBuffer);
		
		// scrittura della linea nel file di salvataggio
		WriteFile (logfile, buffer, dimBuffer);
		WriteFile (logfile, aCapo, 1);
	}
				
	// chiusura del file di datalog
	CloseFile (logfile);
	
Error:	   
	return;
}

