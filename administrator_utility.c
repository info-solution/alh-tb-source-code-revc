#include <userint.h>
#include <ansi_c.h>
#include <formatio.h>

#include "global_var.h"  
#include "error_table.h" 





//****************************************************************
//
//	Verifica che vengano immessi caratteri validi
//
//****************************************************************


int ValidatePswChar(int evento1, int evento2)
{
	int error=0;
	char character = ' ';
	char message[50] ="You have digited an invalid character: ";
	
	// legge tasto digitato
	character = GetKeyPressEventCharacter (evento2);
	
	// verifica che il tasto sia una cifra (0-9), una lettera maiuscola (A-Z) o minuscola (a-z)
	if (!((character>='0')&&(character<='9')||(character>='A')&&(character<='Z')||
		(character>='a')&&(character<='z')||(evento1==VAL_BACKSPACE_VKEY)||(evento1==VAL_TAB_VKEY )))
	{	
		sprintf (message, "%s %c", message, character);
		MessagePopup ("Invalid character", message);
		SetKeyPressEventKey (evento2, 0, 0, 0, 0, 0);
	}
			
	return error;	
}



//****************************************************************
//
// Legge il file di configurazione password e ne modifica il
// contenuto se l'utente ne e' autorizzato
//
//****************************************************************


int ChangePassword(int profilo, char *password_old, char *password_new, char *password_confirm)
{
	int error=0;

	char password_operator[50]="";
	char password_engineer[50]="";
	char password_administrator[50]="";
	
	// carica password da file di configurazione esterno
	LoadPswFile( password_operator , password_engineer , password_administrator);
	
	switch (profilo)
	{
		case OPERATOR:
			
			if ( !CompareStrings (password_old, 0, password_operator, 0, 1) )
				SavePswFile( password_new , password_engineer , password_administrator);	// salva nuova password
			else
				error = INVALID_PASSWORD;
			
			break;
		
		case ENGINEER:
			
			if ( !CompareStrings (password_old, 0, password_engineer, 0, 1) )
				SavePswFile( password_operator , password_new , password_administrator);	// salva nuova password
			else
				error = INVALID_PASSWORD;
			
			break;
			
		case ADMINISTRATOR:
			
			if ( !CompareStrings (password_old, 0, password_administrator, 0, 1) )
				SavePswFile( password_operator , password_engineer , password_new);			// salva nuova password
			else
				error = INVALID_PASSWORD;
			
			break;
	}
	 
	return error;
}






//****************************************************************
//
//	Leggi le password da un file .zak
//
//****************************************************************


int LoadPswFile(char *psw_oper,char *psw_eng,char *psw_admin)
{
	int error=0;
	
	FILE *id_file;
	char passwords[500]="";
	unsigned int checksum_oper=0;
	unsigned int checksum_eng=0;
	unsigned int checksum_admin=0;
	int i=0;
	
	// apre il file nel path indicato e con il nome impostato
	id_file = fopen (PASSWORD_FILE , "rb");
	
	if (id_file==NULL)
	{
		error = FILE_NOT_FOUND;
		return error;
	}
	
	// scrittura del file
	fread (passwords, sizeof(char), sizeof(passwords), id_file);
	
	// scan della stringa letta da file
	sscanf (passwords,"%s %d %s %d %s %d",psw_oper,&checksum_oper,psw_eng,&checksum_eng,psw_admin,&checksum_admin);
	
	// chiusura file
	fclose(id_file);	
	
	if (  !( CompareStrings (psw_oper, 0, "", 0, 1) && CompareStrings (psw_eng, 0, "", 0, 1) && 
			CompareStrings (psw_admin, 0, "", 0, 1) ) )
		error = FILE_DAMAGE;
	
	// ottieni psw originale operatore
	for (i=0; psw_oper[i]!=NULL ; i++)
		psw_oper[i]-=checksum_oper;
	
	// ottieni psw originale ingegnere
	for (i=0; psw_eng[i]!=NULL ; i++)
		psw_eng[i]-=checksum_eng;
	
	// ottieni psw originale amministratore
	for (i=0; psw_admin[i]!=NULL ; i++)
		psw_admin[i]-=checksum_admin;
	
	return error;
}



//****************************************************************
//
//	Salva le password nel file .zak
//
//****************************************************************


int SavePswFile(char *psw_oper,char *psw_eng,char *psw_admin)
{
	int error=0;
	
	FILE *id_file;
	char passwords[500]="";
	unsigned int checksum_oper=0;
	unsigned int checksum_eng=0;
	unsigned int checksum_admin=0;
	int i=0;
	
	// calcola il checksum operatore
	for (i=0; psw_oper[i]!=NULL ; i++)
		checksum_oper = (checksum_oper + psw_oper[i])%128;
	
	// somma al carattere originale il checksum della word
	for (i=0; psw_oper[i]!=NULL ; i++)
		psw_oper[i]+=checksum_oper;
	
	// calcola il checksum ingegnere
	for (i=0; psw_eng[i]!=NULL ; i++)
		checksum_eng = (checksum_eng + psw_eng[i])%128;
	
	// somma al carattere originale il checksum della word
	for (i=0; psw_eng[i]!=NULL ; i++)
		psw_eng[i]+=checksum_eng;
	
	// calcola il checksum amministratore
	for (i=0; psw_admin[i]!=NULL ; i++)
		checksum_admin = (checksum_admin + psw_admin[i])%128;
	
	// somma al carattere originale il checksum della word
	for (i=0; psw_admin[i]!=NULL ; i++)
		psw_admin[i]+=checksum_admin;
	
	// crea la stringa da scrivere nel file
	sprintf (passwords,"%s %d %s %d %s %d",psw_oper,checksum_oper,psw_eng,checksum_eng,psw_admin,checksum_admin);
	
	// crea il file nel path indicato e con il nome impostato
	id_file = fopen (PASSWORD_FILE, "wb");
	
	if (id_file==NULL)
	{
		error = FILE_NOT_FOUND;
		return error;
	}
		
	// scrittura del file
	fwrite (passwords, sizeof(char), StringLength (passwords), id_file);
	
	// chiusura file
	fclose(id_file);
	
	if (error==0)	
		MessagePopup ("Error", ERROR_MESSAGE_PSW_DONE);
	
	return error;
}


