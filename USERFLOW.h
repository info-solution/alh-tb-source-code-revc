/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2008. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  USERPNL                          1
#define  USERPNL_QUITBTN                  2       /* callback function: QuitUserPnlCB */
#define  USERPNL_STARTBTN                 3       /* callback function: StartUserFlowCB */
#define  USERPNL_TEST2TXT                 4
#define  USERPNL_TEST3TXT                 5
#define  USERPNL_TEST4TXT                 6
#define  USERPNL_DECORATION               7
#define  USERPNL_TEST21BTN                8
#define  USERPNL_TEST19BTN                9
#define  USERPNL_TEST5TXT                 10
#define  USERPNL_TEST6TXT                 11
#define  USERPNL_TEST7TXT                 12
#define  USERPNL_TEST8TXT                 13
#define  USERPNL_TEST9TXT                 14
#define  USERPNL_TEST11TXT                15
#define  USERPNL_TEST10TXT                16
#define  USERPNL_TEST12TXT                17
#define  USERPNL_TEST13TXT                18
#define  USERPNL_TEST14TXT                19
#define  USERPNL_TEST15TXT                20
#define  USERPNL_TEST16TXT                21
#define  USERPNL_TEST17TXT                22
#define  USERPNL_TEST18TXT                23
#define  USERPNL_TEST18BTN                24
#define  USERPNL_TEST19TXT                25
#define  USERPNL_TEST17BTN                26
#define  USERPNL_TEST21TXT                27
#define  USERPNL_TEST16BTN                28
#define  USERPNL_TEST15BTN                29
#define  USERPNL_TEST14BTN                30
#define  USERPNL_TEST13BTN                31
#define  USERPNL_TEST12BTN                32
#define  USERPNL_TEST11BTN                33
#define  USERPNL_TEST10BTN                34
#define  USERPNL_TEST9BTN                 35
#define  USERPNL_TEST8BTN                 36
#define  USERPNL_TEST7BTN                 37
#define  USERPNL_TEST6BTN                 38
#define  USERPNL_TEST5BTN                 39
#define  USERPNL_TEST4BTN                 40
#define  USERPNL_TEST3BTN                 41
#define  USERPNL_TEST2BTN                 42
#define  USERPNL_TEXTMSG                  43
#define  USERPNL_CLEARBTN                 44      /* callback function: ClearFlowCB */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK ClearFlowCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitUserPnlCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StartUserFlowCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
