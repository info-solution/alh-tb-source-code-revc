/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2008. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  ADMINPANEL                       1
#define  ADMINPANEL_USERRNG               2
#define  ADMINPANEL_OLDPSW                3       /* callback function: CheckOldPswCB */
#define  ADMINPANEL_NEWPSW                4       /* callback function: CheckNewPswCB */
#define  ADMINPANEL_CONFIRMNEWPSW         5       /* callback function: CheckConfirmPswCB */
#define  ADMINPANEL_CONFIRMBTN            6       /* callback function: ConfirmPswCB */
#define  ADMINPANEL_LOGOFFBTN             7       /* callback function: LogoffCB */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK CheckConfirmPswCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CheckNewPswCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CheckOldPswCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ConfirmPswCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LogoffCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
