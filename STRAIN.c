#include <ansi_c.h>
#include "toolbox.h"
#include <userint.h>
#include "STRAIN.h"

#include "global_var.h" 

int CVICALLBACK ClearStripChartStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// cancella tracce sulle strip chart
			ClearStripChart (strainpanel, STRAINPNL_POSITIONSTR);
			ClearStripChart (strainpanel, STRAINPNL_SPEEDSTR);  
			DeleteGraphPlot (strainpanel, STRAINPNL_INPUTEFFORTGRPH, -1, VAL_IMMEDIATE_DRAW); 
			ClearStripChart (strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR);
			PlotEffortEnable=YES;
			
			break;
	}
	return 0;
}

int CVICALLBACK QuitStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// nascondi pannello per strain gauge
			HidePanel (strainpanel);
			
			break;
	}
	return 0;
}


int CVICALLBACK StopStripChartStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			PlotEffortEnable=NO;
			
			break;
	}
	return 0;
}


int CVICALLBACK SaveStripChartStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int done=0;
	char nomefile[400];
	
	switch (event)
	{
		case EVENT_COMMIT:

			if (save_plotstrain==0)
			{
				done = DirSelectPopup ("C:", "Select Directory", 1, 1, save_plotstrain_dir);
				
				if (done<=0)
					goto Error;
				
				/* Al primo click sull'opzione Save converto il controllo in bitmap */
				
				// sforzo sulla leva manuale				
				sprintf(nomefile,"%s\\strain_input.bmp",save_plotstrain_dir);
				SaveCtrlDisplayToFile (strainpanel, STRAINPNL_INPUTEFFORTGRPH, 0, -1, -1, nomefile);
				
				// carico sul main piston				
				sprintf(nomefile,"%s\\strain_output.bmp",save_plotstrain_dir);
				SaveCtrlDisplayToFile (strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR, 0, -1, -1, nomefile);
				
				save_plotstrain=1;
				
				// modifica la label in STOP
				SetCtrlAttribute(strainpanel,STRAINPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Stop");  
			}
			else
			{
				// modifica la label in START
				SetCtrlAttribute(strainpanel,STRAINPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Start");
				save_plotstrain=0;
			}

			break;
	}
	
Error:
	return 0;
}



int CVICALLBACK ChangeScaleYstrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (strainpanel, STRAINPNL_POSITIONSTR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>40.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -3.0, 3.0);	
			}
			else if ((maxScale>4.0)&&(maxScale<30.0))
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -50.0, 50.0);	
			}
			else if (maxScale<4.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_POSITIONSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -15.0, 15.0);	
			}
		break; 
	}
	
	return 0;		
}


int CVICALLBACK ChangeScaleYdotStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (strainpanel, STRAINPNL_SPEEDSTR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>140.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_SPEEDSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -5.0, 5.0);	
			}
			else if ((maxScale>40.0)&&(maxScale<140.0))
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_SPEEDSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -150.0, 150.0);	
			}
			else if (maxScale<40.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_SPEEDSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -50.0, 50.0);	
			}
		break; 
	}
	return 0;	
}

int CVICALLBACK ChangeScaleInputStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	double x=0.0, y=0.0;
	
	switch (event)
	{
		case EVENT_RIGHT_CLICK:
			GetAxisScalingMode (strainpanel, STRAINPNL_INPUTEFFORTGRPH , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>140.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_INPUTEFFORTGRPH, VAL_LEFT_YAXIS , VAL_MANUAL, -5.0, 5.0);	
			}
			else if ((maxScale>8.0)&&(maxScale<140.0))
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_INPUTEFFORTGRPH, VAL_LEFT_YAXIS , VAL_MANUAL, -200.0, 200.0);	
			}
			else if (maxScale<8.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_INPUTEFFORTGRPH, VAL_LEFT_YAXIS , VAL_MANUAL, -10.0, 10.0);	
			}
		break;
		
		case EVENT_COMMIT:
			// coordinate segnale
			GetGraphCursor (strainpanel, STRAINPNL_INPUTEFFORTGRPH, 1, &x, &y);
	
			SetCtrlAttribute(strainpanel, STRAINPNL_EFFORTAVENMR, ATTR_CTRL_VAL, y); 
		break;
	}
	return 0;	
}


int CVICALLBACK ChangeScaleOutputStrainCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>20000.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -10000.0, 10000.0);	
			}
			else if (maxScale<20000.0)
			{
				SetAxisScalingMode (strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR, VAL_LEFT_YAXIS , VAL_MANUAL, -30000.0, 30000.0);	
			}
		break; 
	}
	return 0;	
}



int CVICALLBACK AcquireCellsZeroCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			Characteristic[N1_MUX].offset = MeasuresAve[N1_MUX]/Characteristic[N1_MUX].conv_factor + Characteristic[N1_MUX].offset;
			Characteristic[N2_MUX].offset = MeasuresAve[N2_MUX]/Characteristic[N2_MUX].conv_factor + Characteristic[N2_MUX].offset;
			Characteristic[LOAD_MUX].offset = Characteristic[N2_MUX].offset;
			
			break;
	}
	return 0;
}
