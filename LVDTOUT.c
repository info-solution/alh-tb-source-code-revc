#include <ansi_c.h>
#include "toolbox.h"
#include <userint.h>
#include "LVDT.h"

#include "global_var.h"      

int CVICALLBACK ClearStripChartLvdtCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			// cancella tracce sulle strip chart
			ClearStripChart (lvdtpanel, LVDTOUTPNL_POSITION1STR);
			ClearStripChart (lvdtpanel, LVDTOUTPNL_POSITION2STR);
			
			PlotCsasEnable=YES;
				
			break;
	}
	return 0;
}


int CVICALLBACK QuitLvdtCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			
			// raizzera la variabile di controllo del salvataggio dati
			save_plotcsas=0;
			
			// nascondi pannello per plot output LVDT
			HidePanel (lvdtpanel);
			
			break;
	}
	return 0;
}


int CVICALLBACK StopStripChartPlotCsasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			PlotCsasEnable=NO;
			
			break;
	}
	return 0;
}



int CVICALLBACK SaveStripChartPlotCsasCB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int done=0;
	char nomefile[400];
	
	switch (event)
	{
		case EVENT_COMMIT:

			if (save_plotcsas==0)
			{
				done = DirSelectPopup ("C:", "Select Directory", 1, 1, save_plotcsas_dir);
				
				if (done<=0)
					goto Error;
				
				/* Salvataggio immagine relativa allo CSAS1 */
				sprintf(nomefile,"%s\\displacement_csas1.bmp",save_plotcsas_dir);
				// al primo click sull'opzione Save converto il controllo in bitmap
				SaveCtrlDisplayToFile (lvdtpanel, LVDTOUTPNL_POSITION1STR, 0, -1, -1, nomefile);
				
				/* Salvataggio immagine relativa allo CSAS1 */
				sprintf(nomefile,"%s\\displacement_csas2.bmp",save_plotcsas_dir);
				// al primo click sull'opzione Save converto il controllo in bitmap
				SaveCtrlDisplayToFile (lvdtpanel, LVDTOUTPNL_POSITION2STR, 0, -1, -1, nomefile);
				
				save_plotcsas=1;
				
				// modifica la label in STOP
				SetCtrlAttribute(lvdtpanel,LVDTOUTPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Stop");  
			}
			else
			{
				// modifica la label in START
				SetCtrlAttribute(lvdtpanel,LVDTOUTPNL_SAVEBTN,ATTR_LABEL_TEXT, "Save Start");
				save_plotcsas=0;
			}

			break;
	}
	
Error:
	return 0;
}


int CVICALLBACK ChangeScaleLvdt1CB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (lvdtpanel, LVDTOUTPNL_POSITION1STR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>10.0)
			{
				SetAxisScalingMode (lvdtpanel, LVDTOUTPNL_POSITION1STR, VAL_LEFT_YAXIS , VAL_MANUAL, -3.0, 3.0);	
			}
			else if (maxScale<=10.0)
			{
				SetAxisScalingMode (lvdtpanel, LVDTOUTPNL_POSITION1STR, VAL_LEFT_YAXIS , VAL_MANUAL, -20.0, 20.0);	
			}
		
		break; 
	}
	return 0;	
}

int CVICALLBACK ChangeScaleLvdt2CB (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int scaleType;
	double minScale;
	double maxScale;
	
	switch (event)
	{
		case EVENT_LEFT_CLICK:
			GetAxisScalingMode (lvdtpanel, LVDTOUTPNL_POSITION2STR , VAL_LEFT_YAXIS , &scaleType, &minScale, &maxScale);
	
			if (maxScale>10.0)
			{
				SetAxisScalingMode (lvdtpanel, LVDTOUTPNL_POSITION2STR, VAL_LEFT_YAXIS , VAL_MANUAL, -3.0, 3.0);	
			}
			else if (maxScale<=10.0)
			{
				SetAxisScalingMode (lvdtpanel, LVDTOUTPNL_POSITION2STR, VAL_LEFT_YAXIS , VAL_MANUAL, -20.0, 20.0);	
			}
		
		break; 
	}
	return 0;	
}
