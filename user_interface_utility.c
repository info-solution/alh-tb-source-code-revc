#include <analysis.h>
#include <formatio.h>
#include <pwctrl.h>
#include <utility.h>
#include <userint.h>
#include <ansi_c.h>
#include "LOGINWINDOW.h"   
#include "MANUALTEST.h"  
#include "ACCOUNT.h"  
#include "ENGINEERPANEL.h"  
#include "MECHCALIBRATION.h"
#include "ELECALIBRATION.h"
#include "STRESS.h"
#include "ATPPANEL.h" 
#include "USERFLOW.h"      
#include "PLOTX.h"
#include "LVDT.h"    
#include "PLOTY.h"  
#include "STRAIN.h"  
#include "PIDCOEFFICIENT.h"
#include "STROKEPANEL.h"

#include "global_var.h"


/* Routine di verifica errori HW scheda DAQmx */
#define ErrChk(functionCall) if( (error=functionCall)!=0 ) goto Error; else                


// array di 3 stringhe contenente le password per ciascun profilo
static char psw_list[3][50] = {"","",""};



//****************************************************************
//
//	MAIN FUNCTION
//
//****************************************************************


int main (int argc, char *argv[])
{
	// inizializza variabili globali
	InitVariables();
	
	// inizializza connessioni di rete
	InitNetworkConnection_PC();
	
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	
	if ((loginpanel = LoadPanel (0, "LOGINWINDOW.uir", LOGINPANEL)) < 0)
		return -1;
	
	if ((infopanel = LoadPanel (0, "LOGINWINDOW.uir", INFOPANEL)) < 0)
		return -1;
	
	PasswordCtrl_ConvertFromString (loginpanel, LOGINPANEL_PASSWORD);

	// inizializza interfacce utente
	//InitUserInterface();

	// avvio HMI
	DisplayPanel (loginpanel);
	RunUserInterface ();
	DiscardPanel (loginpanel);
	
	return 0;
}



//****************************************************************
//
//	Inizializza le variabili globali
//
//****************************************************************

void InitVariables(void)
{
	int i;
	
	// inizializzazione puntatori a pannelli
	loginpanel = -1000;	
	infopanel = -1000;
	adminpanel = -1000;
	engpanel = -1000;
	atppanel = -1000;
	datalogpanel = -1000;
	
	mechcalibrationpanel = -1000;
	elecalibrationpanel = -1000;
	stresspanel = -1000;
	
	manualpanel = -1000;
	plotxpanel = -1000;
	plotypanel = -1000;
	plotxypanel = -1000;
	pressurepanel = -1000;
	strainpanel = -1000;
	solenoidpanel = -1000;
	lvdtpanel = -1000;
	biaspanel = -1000;
	userflowpanel = -1000;
	pidpanel = -1000;
	temperaturepanel = -1000;
	strokepanel = -1000;
	inputeffortpanel = -1000;
	
	inputconnectionpanel = -1000;
	outputconnectionpanel = -1000;
	inertialconnectionpanel = -1000;

	// inizializzazione puntatori ai tab nel pannello di ATP
	init_atppanel = -1000;
	inspection_atppanel = -1000;
	elepower_atppanel = -1000;
 	hydpower_atppanel = -1000;
 	stroke_atppanel = -1000;
 	effort_atppanel = -1000;
 	stall_atppanel = -1000;
 	drift_atppanel = -1000;
 	jamming_atppanel = -1000;
 	noloadspeed_atppanel = -1000;
 	threshold_atppanel = -1000;
 	transient_atppanel = -1000;
 	pilotlin_atppanel = -1000;
 	pilothys_atppanel = -1000;
 	pilotfreq_atppanel = -1000;
 	authority_atppanel = -1000;
 	nullbias_atppanel = -1000;
 	csaslin_atppanel = -1000;
 	csashys_atppanel = -1000;
 	csasfreq_atppanel = -1000;
 	extleakage_atppanel = -1000;
 	intleakage_atppanel = -1000;
	notes_atppanel = -1000;
	
	// inizializza profilo utente
	user_profile = OPERATOR;

	// stato banco idraulico
	bench_status = 0x0;
	
	// dati relativi alla corsa del servocomando
	start_stroke = 0;
	end_stroke = 0;
	stroke = 0;

	// tipo di controllo e carico sul servocomando
	control_engage = -1;
	load_engage = -1;  
	
	// abilitazioni al plotting
	PlotCsasEnable=YES;
	PlotxEnable=YES;
	PlotyEnable=YES;
	PlotEffortEnable=YES;
	PlotxyEnable=NO;
	displacement_char=-1000; 
	
	// flag per indicazione on/off
	uut_power_on=OFF;			// alimentazione della UUT
	uut_control_engage=OFF;		// engage del comando UUT
	uut_load_engage=OFF;		// engage del carico UUT
	
	// inizializza vettore immagine stato elettrovalvole
	for (i=0;i<11;i++)
		SolenoidValve[i] = CLOSE;
	
	// Acquisisci parametri di inizializzazione nel file di configurazione esterno
	LoadNetConfigFile();
	
	// Inizializza caratterizzazione sensori
	LoadSensorsConfigFile();
	//InitSensorsAcquisition();
	
	// Inizializza variabili di controllo per il salvataggio dei dati acquisiti
	save_plotx=0;
	save_plotcsas=0;
	save_ploty=0;     
	save_plotstrain=0;

	// X factor
	KM_PRO = 1.0;
	KT_PRO = 0.8;
	return;
}



//****************************************************************
//
// Verifica i dati di Login
//
//****************************************************************


int CheckLogin(int profile,char *psw)
{
	int error=0;

	// leggi le password dal file di configurazione .zak
	LoadPswFile( psw_list[OPERATOR] , psw_list[ENGINEER] , psw_list[ADMINISTRATOR]);
	
	// pulisce il campo password
	PasswordCtrl_SetAttribute (loginpanel, LOGINPANEL_PASSWORD, ATTR_PASSWORD_VAL, "");
	
	if ((profile>2)||(profile<0))
	{
		error = 999;
		MessagePopup ("Invalid Value", "Errore non gestito..."); 
	}
	else
	{
		// verifica password
		if ( strcmp( psw_list[profile] , psw)==0 )
		{
			switch (profile)
			{
				case 2:
					MessagePopup ("Valid Login", "Hello Administrator!");
					user_profile = ADMINISTRATOR;
					
					HidePanel (loginpanel);		// nascondi pannello di Login
					
					// carica pannello per funzioni di amministratore di sistema
					if ((adminpanel = LoadPanel (0, "ACCOUNT.uir", ADMINPANEL)) < 0)
						return -1;

					// visualizza pannello di amministrazione
					DisplayPanel (adminpanel);  
					
					break;
				case 1:
					MessagePopup ("Valid Login", "Hello Engineer!");   
					user_profile = ENGINEER;
					
					HidePanel (loginpanel);		// nascondi pannello di Login
										
					// carica pannello con funzione di datalog
					if ((datalogpanel = LoadPanel (0, "ENGINEERPANEL.uir", DATALOGPNL)) < 0)
						return -1;
					
					// visualizza datalog
					DisplayPanel (datalogpanel);
					
					// carica pannello per funzioni di amministratore di sistema
					if ((engpanel = LoadPanel (0, "ENGINEERPANEL.uir", ENGPANEL)) < 0)
						return -1;

					// abilito i comandi relativi al profilo loggato
					EnableTestOption();
					
					// visualizza pannello di amministrazione
					DisplayPanel (engpanel);

					/* Zeri delle celle di carico */ 
					Characteristic[N1_MUX].offset = MeasuresAve[N1_MUX]/Characteristic[N1_MUX].conv_factor +Characteristic[N1_MUX].offset;
					Characteristic[N2_MUX].offset = MeasuresAve[N2_MUX]/Characteristic[N2_MUX].conv_factor +Characteristic[N2_MUX].offset;
					Characteristic[LOAD_MUX].offset = Characteristic[N2_MUX].offset;
			
					break;
				case 0:
					MessagePopup ("Valid Login", "Hello Operator!");   
					user_profile = OPERATOR;
					
					HidePanel (loginpanel);
					
					// carica pannello per funzioni di amministratore di sistema
					if ((engpanel = LoadPanel (0, "ENGINEERPANEL.uir", ENGPANEL)) < 0)
						return -1;

					// abilito i comandi relativi al profilo loggato
					EnableTestOption();
					
					// visualizza pannello di amministrazione
					DisplayPanel (engpanel);  
					break;
				default:
					MessagePopup ("Invalid Value", "Errore non gestito...");
					error=999;
					break;
			}
		}
		else
			MessagePopup ("Invalid Login", "You have digited an invalid password!");
	}
	
	return error;
}




//****************************************************************
//
// Abilita le opzioni del pannello con menu di testing in base
// al profilo dell'utente loggato
//
//****************************************************************

int EnableTestOption(void)
{
	int ctrl_index=0;
	int error=0;
	
	// abilita tutti i controlli
	for (ctrl_index=2 ; ctrl_index<18 ; ctrl_index++)
		SetCtrlAttribute (engpanel, ctrl_index, ATTR_DIMMED, YES);
	
	switch (user_profile)
	{
		case OPERATOR:
			// disabilito le opzioni non disponibili per l'utente OPERATOR
			SetCtrlAttribute (engpanel, ENGPANEL_MANUALBTN, ATTR_DIMMED, NO);
			
			SetCtrlAttribute (engpanel, ENGPANEL_ATPBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_PATBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_STRESSBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_VIEWERBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_STATBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_SELFTESTBTN, ATTR_DIMMED, NO);
				
			break;
		
		case ENGINEER:
			// tutte le opzioni sono abilitate...prima o poi...
			
			SetCtrlAttribute (engpanel, ENGPANEL_ATPBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_PATBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_STRESSBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_VIEWERBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_STATBTN, ATTR_DIMMED, NO);
			SetCtrlAttribute (engpanel, ENGPANEL_SELFTESTBTN, ATTR_DIMMED, NO);
			
			break;
		default:
			MessagePopup ("Invalid Value", "Errore non gestito...");
			error=999;
			break;
	}
	
	// inizializzo parametri di controllo
	InitPidParameter();
			
	return error;
}
			


//****************************************************************
//
// inizializza i controlli del pannello manuale
//
//****************************************************************

int InitManualPanel(void)
{
	int ctrl_index=0;
	int error=0;
	double carico;
	char messaggio[200];

	// inizializza la variabile globale con il tipo di servocomando
	SetCtrlAttribute (manualpanel, MANPANEL_PARTNUMBERRNG, ATTR_CTRL_VAL, -1);
	SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWERONBTN, ATTR_DIMMED, NO);
	
	// Disabilita controlli di power UUT	
	SetCtrlAttribute (manualpanel, MANPANEL_BENCHPOWEROFFBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_SYSTEMCHOISE, ATTR_DIMMED, NO);	
	SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWERONBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_UUTPOWEROFFBTN, ATTR_DIMMED, NO);			
	
	// Disabilita scelta movimentazione
	SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, NO);
	
	// Disabilita scelta carico servocomando
	SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
	
	// Disabilita controllo movimentazione
	SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_GETSTROKEBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_STROKECENTERBTN, ATTR_DIMMED, NO);
	
	// Disabilita controllo forma d'onda di input
	SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_STOPMOTIONBTN, ATTR_DIMMED, NO);
	
	// Disabilita controllo carico
	SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, NO);
	SetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_STOPLOADBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_APPLYLOADBTN, ATTR_DIMMED, NO);			
	SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_MAX_VALUE, 8000.0); 
	
	// Azzera valori dei controlli nel pannello
	SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_CTRL_VAL, 0.0);
	SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_CTRL_VAL, 0.0);
	SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_CTRL_VAL, 0.1);
	SetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, STATIC);
	SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_CTRL_VAL, 0.0);
	
	// Selezione item nella lista del servocomando
	ClearListCtrl (manualpanel, MANPANEL_PARTNUMBERRNG);	// pulisce la lista degli item
	
	if (actuatorType==TRA)
	{
		InsertListItem (manualpanel, MANPANEL_PARTNUMBERRNG, 0, "", NU);
		InsertListItem (manualpanel, MANPANEL_PARTNUMBERRNG, 1, "3101 - Tail", PN_3101);	
	}
	else
	{
		InsertListItem (manualpanel, MANPANEL_PARTNUMBERRNG, 0, "", NU);
		InsertListItem (manualpanel, MANPANEL_PARTNUMBERRNG, 1, "3102 - Pitch", PN_3102);	
		InsertListItem (manualpanel, MANPANEL_PARTNUMBERRNG, 2, "3103 - Roll", PN_3103);
		InsertListItem (manualpanel, MANPANEL_PARTNUMBERRNG, 3, "3104 - Collective", PN_3104);
	}
	
	return error; 
}



//****************************************************************
//
// inizializza i controlli del pannello per la calibrazione
// meccanica della UUT
//
//****************************************************************

int InitMechCalibrationPanel(void) 
{
	int error=0;
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTENGAGEBTN, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_INPUTUNENGAGEBTN, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CTRLSLD, ATTR_DIMMED, NO);
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWERONBTN, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_POWEROFFBTN, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_SYSTEMCHOISERNG, ATTR_DIMMED, NO); 
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWEROFFBTN, ATTR_DIMMED, NO);
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_DIMMED, NO);
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_DIMMED, NO);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGENMR, ATTR_DIMMED, NO);
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_BENCHPOWERONBTN, ATTR_DIMMED, YES);
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_AMPLITUDESLD, ATTR_CTRL_VAL, 0.0);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_FREQUENCYSLD, ATTR_CTRL_VAL, 0.0);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_PRESSURESLD, ATTR_CTRL_VAL, 0.0);
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_RETURNRNG, ATTR_CTRL_VAL, 0);			// 3.5bar
	
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_CSASRNG, ATTR_CTRL_VAL, 4);				// off
	SetCtrlAttribute (mechcalibrationpanel, MECHCALPNL_LEAKAGESYSTEMRNG, ATTR_CTRL_VAL, 4);		// off
	
	return error;
}



//****************************************************************
//
// inizializza i controlli del pannello per la calibrazione
// meccanica della UUT
//
//****************************************************************

int InitStressPanel(void) 
{
	int error=0;
	int ctrl_index=0;
	
	// disabilita tutti i controlli
	for (ctrl_index=2 ; ctrl_index<=NUM_CTRL_STRESSPNL ; ctrl_index++)
		SetCtrlAttribute (stresspanel, ctrl_index, ATTR_DIMMED, NO);
	
	// abilita i comandi spare
	SetCtrlAttribute (stresspanel, STRESSPNL_STOPBTN, ATTR_DIMMED, YES);
	SetCtrlAttribute (stresspanel, STRESSPNL_QUITBTN, ATTR_DIMMED, YES);
	SetCtrlAttribute (stresspanel, STRESSPNL_POWERONBTN, ATTR_DIMMED, YES);
	SetCtrlAttribute (stresspanel, STRESSPNL_ACTUALCYCLENMR, ATTR_DIMMED, YES);
	SetCtrlAttribute (stresspanel, STRESSPNL_ACTUALTIMENMR, ATTR_DIMMED, YES);

	return error;
}



//****************************************************************
//
// inizializza i controlli del pannello per l'esecuzione
// della procedura di testing automatizzata
//
//****************************************************************

void InitAtpPanel(void)
{
	// inizializza i puntatori ai tab nel pannello
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 0, &init_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 1, &inspection_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 2, &elepower_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 3, &hydpower_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 4, &stroke_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 5, &effort_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 6, &stall_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 7, &drift_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 8, &jamming_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 9, &noloadspeed_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 10, &threshold_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 11, &transient_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 12, &pilotlin_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 13, &pilothys_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 14, &pilotfreq_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 15, &authority_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 16, &nullbias_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 17, &csaslin_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 18, &csashys_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 19, &csasfreq_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 20, &extleakage_atppanel); 
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 21, &intleakage_atppanel);
	GetPanelHandleFromTabPage (atppanel, ATPPNL_TAB, 22, &notes_atppanel); 
	
	// rende visibile il tab 0
	SetActiveTabPage (atppanel, ATPPNL_TAB, 0);
	
	// disabilita i tab
	SetCtrlAttribute (atppanel, ATPPNL_TAB, ATTR_DIMMED, NO);
}





//****************************************************************
//
// inizializza i controlli del pannello per la definizione
// della procedura di testing personalizzata dall'utente
//
//****************************************************************


void InitUserFlowPanel(void)
{
	int i=0;
	
	// inizializzo a 0 i controlli su pannello relativi al flusso di testing
	SetCtrlAttribute (userflowpanel, USERPNL_TEST2BTN, ATTR_CTRL_VAL, 0);
	SetCtrlAttribute (userflowpanel, USERPNL_TEST3BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST4BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST5BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST6BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST7BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST8BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST9BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST10BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST11BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST12BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST13BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST14BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST15BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST16BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST17BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST18BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST19BTN, ATTR_CTRL_VAL, 0);	
	SetCtrlAttribute (userflowpanel, USERPNL_TEST21BTN, ATTR_CTRL_VAL, 0);	

	// inizializzo a 0 il vettore che definisce il flusso di testing desiderato dall'utente
	for (i=0 ; i<N_TEST_PAT ; i++)
		PAT_flow[i]=0;
		
	return;
}



//****************************************************************
//
// inizializza i controlli del pannello per le misure di posizione,
// velocita' e accelerazione sul magnetostrittivo di input
//
//****************************************************************

void InitPlotXPanel(void)
{
	SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONSTR, ATTR_XAXIS_GAIN, DT);
	SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDSTR, ATTR_XAXIS_GAIN, DT);	
	
	SetCtrlAttribute(plotxpanel, PLOTXPNL_POSITIONSTR, ATTR_XAXIS_OFFSET, 0.0);
	SetCtrlAttribute(plotxpanel, PLOTXPNL_SPEEDSTR, ATTR_XAXIS_OFFSET, 0.0);	
	
	return;
}
 

//****************************************************************
//
// inizializza i controlli del pannello per le misure di posizione,
// velocita' e accelerazione sul magnetostrittivo di output
//
//****************************************************************

void InitPlotYPanel(void)
{
	SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONSTR, ATTR_XAXIS_GAIN, DT);
	
	SetCtrlAttribute(plotypanel, PLOTYPNL_POSITIONSTR, ATTR_XAXIS_OFFSET, 0.0);
	SetCtrlAttribute(plotypanel, PLOTYPNL_SPEEDGRPH, ATTR_XAXIS_OFFSET, 0.0);	
	
	return;
}


//****************************************************************
//
// inizializza i controlli del pannello per le misure di sforzo
//
//****************************************************************

void InitStrainPanel(void)
{
	SetCtrlAttribute(strainpanel, STRAINPNL_POSITIONSTR, ATTR_XAXIS_GAIN, DT);
	SetCtrlAttribute(strainpanel, STRAINPNL_INPUTEFFORTGRPH, ATTR_XAXIS_GAIN, DT);	
	SetCtrlAttribute(strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR, ATTR_XAXIS_GAIN, DT);	
	SetCtrlAttribute(strainpanel, STRAINPNL_SPEEDSTR, ATTR_XAXIS_GAIN, DT);
	
	SetCtrlAttribute(strainpanel, STRAINPNL_POSITIONSTR, ATTR_XAXIS_OFFSET, 0.0);
	SetCtrlAttribute(strainpanel, STRAINPNL_INPUTEFFORTGRPH, ATTR_XAXIS_OFFSET, 0.0);	
	SetCtrlAttribute(strainpanel, STRAINPNL_OUTPUTSTRENGTHSTR, ATTR_XAXIS_OFFSET, 0.0);	
	SetCtrlAttribute(strainpanel, STRAINPNL_SPEEDSTR, ATTR_XAXIS_OFFSET, 0.0);
	
	return;
}



//****************************************************************
//
// inizializza i controlli del pannello per le misure di posizione 
// dei sensori LVDT a bordo degli autopiloti
//
//****************************************************************

void InitLvdtPanel(void)
{
	SetCtrlAttribute(lvdtpanel, LVDTOUTPNL_POSITION1STR, ATTR_XAXIS_GAIN, DT);
	SetCtrlAttribute(lvdtpanel, LVDTOUTPNL_POSITION2STR, ATTR_XAXIS_GAIN, DT);	
	
	SetCtrlAttribute(lvdtpanel, LVDTOUTPNL_POSITION1STR, ATTR_XAXIS_OFFSET, 0.0);
	SetCtrlAttribute(lvdtpanel, LVDTOUTPNL_POSITION2STR, ATTR_XAXIS_OFFSET, 0.0);	
	
	return;
}


//****************************************************************
//
// inizializza i controlli del pannello di acquisizione corsa 
//
//****************************************************************

void InitStrokePanel(void)
{
	SetCtrlAttribute(strokepanel, STROKEPNL_MESSAGETXT, ATTR_CTRL_VAL, "Set input control to the Zero value, then push <Next> button.");
	
	return;
}


//****************************************************************
//
// inizializza i controlli del pannello per la regolazione dei 
// parametri di controllo dei PID
//
//****************************************************************

void InitPidPanel(void)
{
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDINPUTKCNMR, ATTR_CTRL_VAL, PID_parameters[PILOT].proportional_gain );
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDINPUTTINMR, ATTR_CTRL_VAL, PID_parameters[PILOT].integral_time );	
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDINPUTTDNMR, ATTR_CTRL_VAL, PID_parameters[PILOT].derivative_time );	
	
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDCSASKCNMR, ATTR_CTRL_VAL, PID_parameters[CSAS].proportional_gain );
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDCSASTINMR, ATTR_CTRL_VAL, PID_parameters[CSAS].integral_time );	
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDCSASTDNMR, ATTR_CTRL_VAL, PID_parameters[CSAS].derivative_time );		
					 
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDLOADKCNMR, ATTR_CTRL_VAL, PID_parameters[LOAD].proportional_gain );
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDLOADTINMR, ATTR_CTRL_VAL, PID_parameters[LOAD].integral_time );	
	SetCtrlAttribute(pidpanel, PIDCOEFPNL_PIDLOADTDNMR, ATTR_CTRL_VAL, PID_parameters[LOAD].derivative_time );	
	
	return;	
}


//****************************************************************
//
// abilita comandi per il controllo servocomando
//
//****************************************************************

int EnableMotionControlManualPanel(int option)
{
	int control_type;
	int control_wave;
	int error =0;

	if (option == YES)
	{		
		// disabilita comando di engage controllo
		SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, NO);
		
		// disabilita selezione del controllo
		SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, NO);
		SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, NO);
	
		// disabilita comando di engage controllo
		SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, YES);
	}
	else
	{
		// disabilita comando di engage controllo
		SetCtrlAttribute (manualpanel, MANPANEL_CTRLENGAGEBTN, ATTR_DIMMED, YES);

		// abilita selezione del controllo
		SetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_DIMMED, YES);
		
		// abilita selezione del CSAS se il controllo e' di tipo automatico
		GetCtrlAttribute (manualpanel, MANPANEL_CTRLCHOISE, ATTR_CTRL_VAL, &control_type);
		if (control_type==CSAS)
			SetCtrlAttribute (manualpanel, MANPANEL_CSASCHOISE, ATTR_DIMMED, YES);
		
		// disabilita comando di engage controllo
		SetCtrlAttribute (manualpanel, MANPANEL_CTRLUNENGAGEBTN, ATTR_DIMMED, NO);		
	}
			
	// abilita comandi di controllo
	SetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_DIMMED, option);				// selezione segnale di controllo
	SetCtrlAttribute (manualpanel, MANPANEL_POSITIONSLD, ATTR_DIMMED, option);			// posizionamento statico o di punto medio di oscillazione
			
	SetCtrlAttribute (manualpanel, MANPANEL_STROKECENTERBTN, ATTR_DIMMED, option);		// riporta il main piston in posizione centrale
	SetCtrlAttribute (manualpanel, MANPANEL_GETSTROKEBTN, ATTR_DIMMED, option);			// acquisisce posizione centrale della corsa del pistone
	
	// abilito ampiezza e frequenza se non sono su static
	GetCtrlAttribute (manualpanel, MANPANEL_WAVERSLD, ATTR_CTRL_VAL, &control_wave);
	if (control_wave!=STATIC)
	{
		SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_DIMMED, option);		// ampiezza oscillazione
		SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_DIMMED, option);		// frequenza oscillazione
		SetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, option);		// avvio oscillazione
		SetCtrlAttribute (manualpanel, MANPANEL_STOPMOTIONBTN, ATTR_DIMMED, option);		// stop oscillazione
	}
	else
	{
		SetCtrlAttribute (manualpanel, MANPANEL_SETAMPLITUDENMR, ATTR_DIMMED, NO);		// ampiezza oscillazione
		SetCtrlAttribute (manualpanel, MANPANEL_SETFREQUENCYNMR, ATTR_DIMMED, NO);		// frequenza oscillazione
		SetCtrlAttribute (manualpanel, MANPANEL_STARTMOTIONBTN, ATTR_DIMMED, NO);		// avvio oscillazione
		SetCtrlAttribute (manualpanel, MANPANEL_STOPMOTIONBTN, ATTR_DIMMED, NO);		// stop oscillazione
	}
	
	return error; 
}


//****************************************************************
//
// abilita comandi per il carico del servocomando
//
//****************************************************************

int EnableLoadControlManualPanel(int option)
{
	int error =0;
	int load_function =0;

	if (option == YES)
	{		
		// disabilita selezione del carico
		SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, NO);
		
		// disabilita comando di engage carico
		SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);
	}
	else
	{
		// abilita comando di engage carico
		SetCtrlAttribute (manualpanel, MANPANEL_LOADENGAGEBTN, ATTR_DIMMED, YES);

		// disabilita selezione del carico
		SetCtrlAttribute (manualpanel, MANPANEL_LOADCHOISE, ATTR_DIMMED, NO);
		
		// disabilita comando di engage carico
		SetCtrlAttribute (manualpanel, MANPANEL_LOADUNENGAGEBTN, ATTR_DIMMED, NO);		
	}
			
	// abilita comandi di controllo
	SetCtrlAttribute (manualpanel, MANPANEL_LOADSLD, ATTR_DIMMED, option);				// entita' del carico da impostare in N
	SetCtrlAttribute (manualpanel, MANPANEL_LOADDIRECTIONRSLD, ATTR_DIMMED, option);	// direzione di carico: compressione o estensione
	SetCtrlAttribute (manualpanel, MANPANEL_APPLYLOADBTN, ATTR_DIMMED, option);			// applicazione del carico
	
	return error; 
}


//****************************************************************
//
// Stampa a Datalog una stringa di testo
//
//****************************************************************

int PrintLog (char *messaggio)
{
	int error=0;
	int Visible=0;

	// verifica se il pannello esiste
	if (datalogpanel>0)
	{
		// verifica se il pannello e' visualizzato e stampa a video il messaggio
		GetPanelAttribute (datalogpanel, ATTR_VISIBLE, &Visible);
		
		if (Visible==HI)
			InsertTextBoxLine (datalogpanel, DATALOGPNL_TEXTBOX, -1, messaggio);
	}
	else
		goto Error;
		
Error:
	return error;
}



//****************************************************************
//
// Lettura file di configurazione Rete
//
//****************************************************************

int LoadNetConfigFile (void)
{
	int error=0;
	
	char buffer[200];		// buffer di singola linea
	int dimBuffer;			// numero di caratteri nella linea di testo
	
	int numLinee=0;			// numero di linee presenti nella message box di datalog da salvare
	int i=0;
	char aCapo[]="\n";
	
	int configfile;			// handler del file di salvataggio
	
	// apertura/creazione del file di salvataggio
	configfile = OpenFile ("ratb.ini", VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	// lettura parametri contenuti nel file di configurazione
	if (configfile<0)
		goto Error;
	else
	{
		ReadLine (configfile, buffer, -1);
		sscanf (buffer, "IP_HOST = %s", IP_host);	
										 
		ReadLine (configfile, buffer, -1);
		sscanf (buffer, "IP_TARGET = %s", IP_target);	
	}
				
	// chiusura del file di datalog
	CloseFile (configfile);
		
Error:
	if (error!=0)
		MessagePopup("Import file","Error in ratb.ini reading...");
	return error;
}



//****************************************************************
//
// Lettura file di configurazione caratteristiche sensori
//
//****************************************************************

int LoadSensorsConfigFile (void)
{
	int error=0;
	double temp;
	
	char buffer[200];		// buffer di singola linea
	int dimBuffer;			// numero di caratteri nella linea di testo
	
	int numLinee=0;			// numero di linee presenti nella message box di datalog da salvare
	int i=0;
	char aCapo[]="\n";
	
	int configfile;			// handler del file di salvataggio

	// apertura/creazione del file di salvataggio
	configfile = OpenFile ("transducers.ini", VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	// lettura parametri contenuti nel file di configurazione
	if (configfile<0)
		goto Error;
	else
	{
		// Trasduttori di pressione
		
		// P02
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P02_OFF] =%f", &Characteristic[P02_MUX].offset);
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P02_PRO] =%f", &Characteristic[P02_MUX].conv_factor);
		
		// P03
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P03_OFF] = %f", &Characteristic[P03_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P03_PRO] = %f", &Characteristic[P03_MUX].conv_factor);

		// P04
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P04_OFF] = %f", &Characteristic[P04_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P04_PRO] = %f", &Characteristic[P04_MUX].conv_factor);
		
		// P05
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P05_OFF] = %f", &Characteristic[P05_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P05_PRO] = %f", &Characteristic[P05_MUX].conv_factor);

		// P06
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P06_OFF] = %f", &Characteristic[P06_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P06_PRO] = %f", &Characteristic[P06_MUX].conv_factor);
		
		// P07
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P07_OFF] = %f", &Characteristic[P07_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P07_PRO] = %f", &Characteristic[P07_MUX].conv_factor);

		// P08
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P08_OFF] = %f", &Characteristic[P08_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P08_PRO] = %f", &Characteristic[P08_MUX].conv_factor);

		// P09
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P09_OFF] = %f", &Characteristic[P09_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P09_PRO] = %f", &Characteristic[P09_MUX].conv_factor);

		// P10
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P10_OFF] = %f", &Characteristic[P10_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P10_PRO] = %f", &Characteristic[P10_MUX].conv_factor);

		// P11
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P11_OFF] = %f", &Characteristic[P11_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[P11_PRO] = %f", &Characteristic[P11_MUX].conv_factor);
	
		// Trasduttore di temperatura K1
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[K1_OFF] = %f", &Characteristic[K1_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[K1_PRO] = %f", &Characteristic[K1_MUX].conv_factor);

		// Sensing alimentazione solenoid valve
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[EV_SYS_OFF] = %f", &Characteristic[PSU3_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[EV_SYS_PRO] = %f", &Characteristic[PSU3_MUX].conv_factor);

		// Trasduttori magnetostrittivi
		
		// L1
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[L1_OFF] = %f", &Characteristic[L1_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[L1_PRO] = %f", &Characteristic[L1_MUX].conv_factor);

		// L2
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[L2_OFF] = %f", &Characteristic[L2_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[L2_PRO] = %f", &Characteristic[L2_MUX].conv_factor);

		// Sensing correnti
		
		// servovalvola SYS
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R1_SHUNT_OFF] = %f", &Characteristic[CV_SYS1_SHUNT].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R1_SHUNT_PRO] = %f", &Characteristic[CV_SYS1_SHUNT].conv_factor);

		// servovalvola SYS
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R2_SHUNT_OFF] = %f", &Characteristic[CV_SYS2_SHUNT].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R2_SHUNT_PRO] = %f", &Characteristic[CV_SYS2_SHUNT].conv_factor);

		// solenoid valve SYS
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R9_SHUNT_OFF] = %f", &Characteristic[SYS1_S_SHUNT].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R9_SHUNT_PRO] = %f", &Characteristic[SYS1_S_SHUNT].conv_factor);

		// solenoid valve SYS
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R10_SHUNT_OFF] = %f", &Characteristic[SYS2_S_SHUNT].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[R10_SHUNT_PRO] = %f", &Characteristic[SYS2_S_SHUNT].conv_factor);

		// Flussometro F1
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[F1_OFF] = %f", &Characteristic[F1_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[F1_PRO] = %f", &Characteristic[F1_MUX].conv_factor);

		// Trasduttori LVDT
		
		// LVDT1
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[LVDT1_OFF] = %f", &Characteristic[LVDT1_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[LVDT1_PRO] = %f", &Characteristic[LVDT1_MUX].conv_factor);

		// LVDT2
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[LVDT2_OFF] = %f", &Characteristic[LVDT2_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[LVDT2_PRO] = %f", &Characteristic[LVDT2_MUX].conv_factor);

		// Celle di carico
		
		// N1
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[N1_OFF] = %f", &Characteristic[N1_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[N1_PRO] = %f", &Characteristic[N1_MUX].conv_factor);

		// N2
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[N2_OFF] = %f", &Characteristic[N2_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[N2_PRO] = %f", &Characteristic[N2_MUX].conv_factor);

		// Setpoint
		
		// Input lever
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[INPUT_OFF] = %f", &Characteristic[INPUT_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[INPUT_PRO] = %f", &Characteristic[INPUT_MUX].conv_factor);

		// CSAS
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[CSAS_OFF] = %f", &Characteristic[CSAS_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[CSAS_PRO] = %f", &Characteristic[CSAS_MUX].conv_factor);

		// Load
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[LOAD_OFF] = %f", &Characteristic[LOAD_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[LOAD_PRO] = %f", &Characteristic[LOAD_MUX].conv_factor);

		// Turbina volumetrica F2
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[F2_OFF] = %f", &Characteristic[F2_MUX].offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[F2_PRO] = %f", &Characteristic[F2_MUX].conv_factor);
		
		// Valvola di controllo proporzionale di pressione
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[V3_OFF] = %f", &V3.offset);	
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[V3_PRO] = %f", &V3.conv_factor);
		
		// X factor per MRA
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[KM_PRO] = %f", &KM_PRO);	
		
		// X factor per TRA
		ReadLine (configfile, buffer, -1);
		Scan (buffer, "[KT_PRO] = %f", &KT_PRO);	
	}
			
	// chiusura del file di datalog
	CloseFile (configfile);
		
Error:
	if (error!=0)
		MessagePopup("Import file","Error in transducers.ini reading...");
	return error;
}





//****************************************************************
//
// Lettura file con caratteristiche di linearizzazione sensori
// magnetostrittivi MTS
//
//****************************************************************

int number_of_points[2];					// numero di punti della caratteristica di linearizzazione     
double LinearCharacteristic[2][1000];		// caratteristiche di linearizzazione dei sensori
												// [0]-L1 [1]-L2


int LoadLinearizationFile (void)
{
	int error=0;
	char buffer[200];			// buffer di singola linea
	int dimBuffer;				// numero di caratteri nella linea di testo
	
	int datafile;				// handler del file di salvataggio

	/****************************************/
	/* Caratteristica sensore L1			*/
	/****************************************/  
	
	// apertura/creazione del file di salvataggio
	datafile = OpenFile ("L1.dat", VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	// lettura parametri contenuti nel file di configurazione
	if (datafile<0)
		goto Error;
	else
	{
		number_of_points[0]=0;
		while (ReadLine (datafile, buffer, -1)>0)
		{
			Scan (buffer, "%f", &LinearCharacteristic[0][number_of_points[0]]); 
			number_of_points[0]++;
		}
	}

	// chiusura del file di datalog
	CloseFile (datafile);
	
	/****************************************/
	/* Caratteristica sensore L2			*/
	/****************************************/  

	// apertura/creazione del file di salvataggio
	datafile = OpenFile ("L2.dat", VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	// lettura parametri contenuti nel file di configurazione
	if (datafile<0)
		goto Error;
	else
	{
		number_of_points[1]=0;
		while (ReadLine (datafile, buffer, -1)>0)
		{
			Scan (buffer, "%f", &LinearCharacteristic[1][number_of_points[1]]); 
			number_of_points[1]++;
		}
	}

	// chiusura del file di datalog
	CloseFile (datafile);
	
Error:
	if (error!=0)
		MessagePopup("Import file","Error in L1.dat or L2.dat reading...");
	return error;
}




//****************************************************************
//
// Routine di implementazione della linearizzazione mediante
// applicazione della look-up-table caricata
//
//****************************************************************

int MeasureToLinear (void)
{
	int error=0;




Error:
	return error;
}


int LinearToMeasure (void)
{
	int error=0;




Error:
	return error;
}

